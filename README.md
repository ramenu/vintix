# Vintix: A UNIX-like Operating System for Vintage Computers

## Rationale

Most UNIX-like operating systems that are actively maintained
nowadays do not support very old PCs anymore. Most effort is gone
into supporting new hardware rather than improving support for older
devices. While this is obviously understandable, it does suck for the niche
enthusiasts who want to run a updated UNIX-like OS on these older machines.

Rather than improving support for modern hardware, Vintix takes the opposite approach. Its aspirations 
are to maintain first class support for these older devices, while adding new features, security
fixes, and porting new programs without deprecating support for these computers.

## Getting Started

Consult the [documentation](docs/README.md) for learning about how to configure and build
the kernel.

# Contributing

If you want to get involved developing Vintix, come say hi on `#vintix-dev` on Libera Chat.
There is also a `#vintix` channel for off-topic discussions or general inquires.

