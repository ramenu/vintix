# Vintix Kernel Modules

Vintix kernel modules are loadable drivers that can be added or removed at runtime, providing dynamic
functionality and flexibility to the kernel.

> [!WARNING]  
> Vintix has been meticulously designed to operate on systems with extremely low hardware requirements,
> making it capable of running with as little as **2MB of memory**. While this low memory footprint allows
> Vintix to run on legacy or resource-constrained devices, it also necessitates a careful approach to module
> management to optimize the limited resources available.
>
> Given these constraints, **users must manually select only the modules their hardware requires** to ensure
> optimal performance and stability when running Vintix. This approach prevents unnecessary resource consumption
> while still providing the flexibility to load functionality needed for specific hardware configurations.

Below you'll find a list of all modules in the kernel and their respective descriptions:

- **ata.mod**: AT Attachment driver for hard drives, enabling communication with ATA-compatible storage devices.


- **minix.mod**: MINIX filesystem driver, designed to support the MINIX filesystem structure. The
  driver currently doesn't support MINIXv2 or MINIXv3.


- **keyboard.mod**: Implements support for keyboards.


- **zero.mod**: Adds `/dev/zero` as a character device.


- **ustar.mod**: Adds support for a subset of the standard USTAR archive format. This module serves no
  other purpose other than to parse and extract the contents of the initial ramdisk.