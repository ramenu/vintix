# CMake Configuration Arguments

When building Vintix, it's essential to pass specific arguments to CMake to configure the build process correctly. The
following arguments must be provided to ensure the kernel and drivers are built with the appropriate settings.

## 1. `INITRD_MODULES`

**Description:**

The `INITRD_MODULES` argument specifies the modules to include in the initial ramdisk (initrd). These modules are
essential for the early stages of the boot process, providing necessary drivers and functionalities before the main
system initializes.

**Usage:**

```text
cmake -DINITRD_MODULES=<module1.mod;module2.mod;...>
```

**Parameters:**

- A semicolon-separated list of module names to be included in the initial ramdisk. Each module should correspond to a
  valid kernel module within the project.

## 2. `TARGET_MACHINE`

**Description:**

The `TARGET_MACHINE` argument defines the target architecture for which the kernel and drivers will be built. Specifying
the correct target machine ensures compatibility with the hardware architecture where the OS will run.

**Usage:**

```text
cmake -DTARGET_MACHINE=<architecture>
```

**Parameters:**

- **`<architecture>`**: The identifier for the target architecture. Valid values include:
    - `386|486|586|686`: for 32-bit Intel and AMD x86 processors.