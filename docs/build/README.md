# Read This First!

If you have the time or interest in contributing to Vintix by adding support for other operating systems to
the build system, your efforts would be greatly appreciated. Expanding compatibility to platforms such as Windows,
MacOS,
and various BSDs will help broaden our user base and enhance the project's versatility. Please feel free to submit pull
requests or reach out to the maintainers to get started.

| Operating System | Tested |
|------------------|:------:|
| **Linux**        |   ✅    |
| **Windows**      |   ❌    |
| **MacOS**        |   ❌    |
| **BSDs**         |   ❌    |

# Installing Dependencies on Linux

### Ubuntu/Debian:

```bash
apt install coreutils clang cmake nasm genisoimage
```

### Arch Linux:

```bash
pacman -S coreutils clang cmake nasm cdrtools
```

### Fedora Linux:

```bash
dnf install coreutils clang cmake nasm genisoimage 
```
