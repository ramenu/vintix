# Building a Vintix ISO Image

To build a Vintix ISO image, follow these steps:

## 1. Set Up Directory Structure

First, create a `deps` directory in the project's root directory. Within the `deps` directory, create two
subdirectories: `iso` and `bootloaders`. Your directory structure should look like this:

```text
project root
│   README.md
│   LICENSE    
│   .clang-format    
│   .clang-tidy    
│   .gitignore    
│   CMakeLists.txt    
│   kernel    
│   include    
│   drivers    
│   docs    
└───deps
    │   bootloaders
    │   iso
```

## 2. Download and Prepare GRUB Legacy

Download the [GRUB Legacy tarball through FTP](ftp://alpha.gnu.org/gnu/grub/grub-0.97.tar.gz)
and compile it from source. Once you've finished, extract the `stage2_eltorito` file and place it into the `bootloaders`
directory you created in the previous step.

## 3. Create `menu.lst` Configuration

In the `iso` directory, create a `menu.lst` file. Here's an example stub you can use:

```text
default=0
timeout=0

title vintix
kernel /boot/vintix.elf
module /boot/ustar.mod
module /boot/vintix.initrd
```

**Note:** You need to set the appropriate parameters for the kernel. Refer to the [parameter instructions](../params.md)
for detailed guidance.

## 4. Verify Directory Structure

After completing the previous steps, your `deps` directory should have the following structure:

```text
deps
└───bootloaders
│   │   stage2_eltorito
│
└───iso
│   │   menu.lst
```

## 5. Configure CMake

Before building the kernel and drivers, you must pass the appropriate arguments to CMake. These arguments specify the
target architecture, whether to compress the ramdisk, and more. Refer to the [CMake documentation](CMAKE.md) for a full
list of available arguments and their descriptions.