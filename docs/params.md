# Kernel Command Line Arguments Documentation

## Preface

The kernel command line is a vital interface for passing configuration parameters to the kernel during the system's boot
process. These parameters enable fine-tuning of hardware interactions, filesystem setups, and the loading of essential
drivers, ensuring that the operating system initializes correctly and efficiently. Proper configuration of these
arguments is crucial for system stability, performance, and functionality.

This document provides detailed information on specific kernel command line arguments related to root device
configuration, filesystem types, mounting options, module loading, and swapfile setup. Understanding and correctly
utilizing these parameters will aid developers and system administrators in customizing and optimizing the boot process
to meet their specific requirements.

---

## Kernel Command Line Arguments

### 1. `root`

- **Description**: Specifies the root device that the kernel will mount as the root filesystem during the boot process.
- **Argument Format**: `<device_name>`
    - Typically follows the format `hdX`, where `X` represents the hard drive identifier (e.g., `hda`, `hdb`).
- **Example**:
  ```plaintext
  root=hda
  ```
    - **Explanation**: Instructs the kernel to use the first hard drive (`hda`) as the root device.

- **Usage Notes**:
    - Ensure that the specified device exists and is correctly identified.
    - The device name must correspond to a valid storage device recognized by the kernel.

---

### 2. `root_fs`

- **Description**: Defines the filesystem type of the root device specified by the `root` argument.
- **Argument Format**: `<filesystem_type>`
    - Currently, the only supported value is `old-minix`.
- **Example**:
  ```plaintext
  root_fs=old-minix
  ```
    - **Explanation**: Indicates that the root device uses the `old-minix` filesystem type.

- **Usage Notes**:
    - The filesystem type must be supported by the kernel to ensure proper mounting.
    - Unsupported or incorrect filesystem types can prevent the system from booting correctly.

---

### 3. `root_flags`

- **Description**: Specifies additional flags for mounting the root filesystem, allowing for customized mounting
  behavior.
- **Argument Format**: `<mount_flags>`
    - Common flags include `ro` (read-only), `noexec` (no execution), `noatime` (no access time), among others.
- **Example**:
  ```plaintext
  root_flags=ro
  ```
    - **Explanation**: Mounts the root filesystem in read-only mode during the boot process.

- **Additional Examples**:
  ```plaintext
  root_flags=noatime
  root_flags=ro,noexec
  ```

- **Usage Notes**:
    - Combining multiple flags should be separated by commas.
    - Choose flags that align with the desired behavior and system requirements.

---

### 4. `modules`

- **Description**: Lists additional kernel modules (drivers) that the kernel should load at runtime
  from the root drive. Please note that this is separate from the [initrd modules which are specified
  during compile time](./build/CMAKE.md#1-initrd_modules).
  A list of all the kernel modules is available [here](modules.md).
- **Argument Format**: `<module1,module2,module3,...>`
    - Modules are specified as a comma-separated list without spaces.
- **Example**:
  ```plaintext
  modules=mod1,mod2,mod3
  ```
  - **Explanation**: Directs the kernel to load the `mod1`, `mod2`, and `mod3` modules from the root drive during
    initialization.

- **Use Cases**:
    - Loading drivers for additional hardware or features not included in the default kernel build.

- **Usage Notes**:
    - Ensure that the specified modules are available in the root filesystem's module path: `/boot/modules`
    - Incorrect module names can lead to boot failures or missing hardware functionality.
  - **Do not add the `.mod` suffix! Only list the module names.**

---

### 5. `swapfile`

- **Description**: Defines the device or file that the kernel should use as swap space for virtual memory.
- **Argument Format**: `<swapfile_path>`
    - Can be a device node (e.g., `/dev/hda2`) or a file path (e.g., `/swapfile`).
- **Example**:
  ```plaintext
  swapfile=/dev/hda2
  ```
    - **Explanation**: Configures the kernel to use the second partition on the first hard drive (`/dev/hda2`) as swap
      space.

- **Alternative Example**:
  ```plaintext
  swapfile=/swapfile
  ```
    - **Explanation**: Instructs the kernel to use a file named `swapfile` located at the root directory as swap space.

- **Usage Notes**:
    - The specified swapfile or device must be properly formatted and configured for swap usage.
    - Using a file as swap requires it to exist and have the correct permissions set.
    - Ensure that the swap space is adequately sized to meet the system's memory requirements.