# Vintix Documentation

### Building

- [Installing Dependencies](build/README.md)
- [Configuring CMake](build/CMAKE.md)
- [Building the ISO](build/ISO.md)

### Configuring the Kernel

- [Kernel Parameters](params.md)

### Kernel Modules

- [A List of All Official Modules](modules.md)
