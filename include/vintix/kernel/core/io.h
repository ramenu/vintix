/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_IO_H
#define VINTIX_IO_H

#include <vintix/kernel/lib/errno.h>
#include <vintix/kernel/lib/types.h>
#include <vintix/kernel/lib/vaarg.h>

enum class FBFGColor : u8 {
    Black,
    Blue,
    Green,
    Cyan,
    Red,
    Magenta,
    Brown,
    LightGrey,
    DarkGrey,
    LightBlue,
    LightGreen,
    LightCyan,
    LightRed,
    LightMagenta,
    LightBrown,
    White
};

enum class FBBGColor : u8 {
    Black = 0 << 4,
    Blue = 1 << 4,
    Green = 2 << 4,
    Cyan = 3 << 4,
    Red = 4 << 4,
    Magenta = 5 << 4,
    Brown = 6 << 4,
    LightGrey = 7 << 4,
    DarkGrey = 8 << 4,
    LightBlue = 9 << 4,
    LightGreen = 10 << 4,
    LightCyan = 11 << 4,
    LightRed = 12 << 4,
    LightMagenta = 13 << 4,
    LightBrown = 14 << 4,
    White = 15 << 4
};

static constexpr u8 FRAMEBUFFER_COLUMNS = 80;
static constexpr u8 FRAMEBUFFER_ROWS = 25;
static constexpr u8 FB_CELL_BYTE = 2;

static constexpr auto DEFAULT_FBFG_COLOR = FBFGColor::LightGrey;
static constexpr auto DEFAULT_FBBG_COLOR = FBBGColor::Black;

extern "C" {

    inline void outb(u16 port, u8 data) noexcept
    {
        asm volatile("out     %0, %1" : : "dN"(port), "a"(data) : "memory");
    }
    inline void outw(u16 port, u16 data) noexcept
    {
        asm volatile("out     %0, %1" : : "dN"(port), "a"(data) : "memory");
    }
    inline void outl(u16 port, u32 data) noexcept
    {
        asm volatile("out     %0, %1" : : "dN"(port), "a"(data) : "memory");
    }
    inline auto inb(u16 port) noexcept -> u8
    {
        u8 value;
        asm volatile("in      %0, %1" : "=a"(value) : "dN"(port) : "memory");
        return value;
    }
    inline auto inw(u16 port) noexcept -> u16
    {
        u16 value;
        asm volatile("in      %0, %1" : "=a"(value) : "dN"(port) : "memory");
        return value;
    }
    inline auto inl(u16 port) noexcept -> u32
    {
        u32 value;
        asm volatile("in      %0, %1" : "=a"(value) : "dN"(port) : "memory");
        return value;
    }
    inline void rep_insw(void *dst, u16 port, u32 n) noexcept
    {
        asm volatile("rep insw" : "+D"(dst), "+c"(n) : "dN"(port) : "memory");
    }
    void fb_write(const char *buf, FBFGColor fg = DEFAULT_FBFG_COLOR,
                  FBBGColor bg = DEFAULT_FBBG_COLOR) noexcept;
    void fb_writeln(const char *buf, FBFGColor fg = DEFAULT_FBFG_COLOR,
                    FBBGColor bg = DEFAULT_FBBG_COLOR) noexcept;
    void fb_clear() noexcept;
    void kprintf(const char *format, ...) noexcept;
    void kvfprintf(const char *format, va_list va) noexcept;
    void fb_writec(char c, FBFGColor fg = DEFAULT_FBFG_COLOR,
                   FBBGColor bg = DEFAULT_FBBG_COLOR) noexcept;
    void srl_printf(const char *format, ...) noexcept;
    void srl_vfprintf(const char *format, va_list va) noexcept;
}

auto fb_init() noexcept -> errno;

inline void io_wait() noexcept
{
    // using port 0x80 because it is often used during POST to log information
    // about the motherboard's hex display but is almost always unused after boot
    // See: https://wiki.osdev.org/Inline_Assembly/Examples#IO_WAIT
    outb(0x80, 0);
}

#endif // VINTIX_IO_H
