/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_PMM_H
#define VINTIX_PMM_H

#include <vintix/kernel/core/macros.h>
#include <vintix/kernel/lib/align.h>
#include <vintix/kernel/lib/concepts.h>
#include <vintix/kernel/lib/types.h>

static constexpr usize PAGE_FRAME_SIZE = 4096;
static constexpr usize PTE_ENTRIES = 1024, PDT_ENTRIES = 1024;
static constexpr u32 PF_ADDRESS_OFFSET = 12;
static constexpr u32 PTE_AVL_OFFSET = 9;

struct pf_bitmap_address_range {
    p_addr start {};
    p_addr end {};
};

struct pgs_used {
    usize count;
    usize total;

    // number of pages used for real mode (identity mapped)
    usize real_mode;
};

inline auto vaddr_to_pte(const void *vaddr) noexcept -> u16
{
    return static_cast<u16>(addressof(vaddr) / PAGE_FRAME_SIZE % PTE_ENTRIES);
}

inline auto vaddr_to_pdt(const void *vaddr) noexcept -> u16
{
    return static_cast<u16>(addressof(vaddr) / PAGE_FRAME_SIZE / PDT_ENTRIES);
}

inline auto vaddr_to_pte(v_addr vaddr) noexcept -> u16
{
    return vaddr_to_pte(unsafe_toptr<const void *>(vaddr));
}

inline auto vaddr_to_pdt(v_addr vaddr) noexcept -> u16
{
    return vaddr_to_pdt(unsafe_toptr<const void *>(vaddr));
}

constexpr auto page_align(v_addr vaddr) noexcept -> v_addr
{
    return (vaddr & ~(PAGE_FRAME_SIZE - 1));
}

enum pg_flags {
    PG_NULL = 0,
    PG_P = 1,
    PG_R = 1 << 1,
    PG_W = 1 << 2,
    PG_X = 1 << 3,
    PG_USER = 1 << 4,
    PG_PWT = 1 << 5,
    PG_PCD = 1 << 6,
    PG_A = 1 << 7,
    PG_D = 1 << 8,
    PG_PAT = 1 << 9,
    PG_G = 1 << 10,
};

DEFINE_ENUM_BITWISE_OPS(pg_flags);

extern "C" {
    // flushes the TLB
    inline void flush_tlb() noexcept
    {
        asm volatile("mov     eax, cr3\n\t"
                     "mov     cr3, eax\n\t"
                     :
                     :
                     : "eax");
    }

    void load_pg_directory(pg_directory_t pg_directory[PDT_ENTRIES],
                           pg_table_t *pg_table[PTE_ENTRIES]) noexcept;
    inline void invlpg(const void *RESTRICT addr) noexcept
    {
        asm volatile("invlpg  [%0]" : : "r"(addr) : "memory");
    }
    void alloc_pg_table(u16 pde, void *pg_table) noexcept;
    NODISCARD auto address_is_mapped(v_addr vaddr) noexcept -> bool;
    NODISCARD auto pages_used() noexcept -> pgs_used;

    /**
     * Retrieves the physical address of the page entry corresponding to the given virtual address.
     *
     * @param vaddr The virtual address whose associated page entry is to be retrieved.
     * @return A pointer to the physical address of the page entry, or nullptr if
     *         the page table could not be allocated or the address could not be resolved.
     */
    NODISCARD auto get_page(const void *vaddr) noexcept -> p_addr *;

    /**
     * Allocates a new memory page and returns its physical address.
     *
     * This function is responsible for allocating a single memory page from the
     * physical memory manager, marking it as in use, and returning its physical
     * address. It increments the count of used pages and ensures proper tracking.
     * If no pages are available, 0 is returned.
     *
     * @return The physical address of the allocated page, or 0 if
     *         no free pages are available.
     */
    NODISCARD auto alloc_page() noexcept -> p_addr;

    /**
     * Allocates a specified number of consecutive memory pages and returns the
     * physical address of the starting page.
     *
     * This function attempts to allocate `num_pages` consecutive memory pages
     * from the physical memory manager. If successful, it returns the physical
     * address of the first page in the sequence. If allocation fails (e.g.,
     * due to insufficient available memory), it returns 0.
     *
     * @param num_pages The number of consecutive memory pages to allocate.
     *                  This value must be greater than zero.
     * @return The physical address of the allocated pages if successful,
     *         or returns 0 if allocation fails.
     */
    NODISCARD auto alloc_pages(usize num_pages) noexcept -> p_addr;

    /**
     * Maps a virtual address to a physical address with the given page flags.
     *
     * @param vaddr The virtual address to be mapped.
     * @param paddr The physical address to be mapped to the virtual address.
     * @param flags The page table entry flags determining the properties of the mapping.
     * @return True if the mapping was successfully created, or false if the mapping operation
     * failed.
     */
    NODISCARD auto map_page(v_addr vaddr, p_addr paddr, pg_flags flags) noexcept -> bool;

    /**
     * Frees the specified memory page and releases its resources.
     *
     * This function is responsible for handling the deallocation of a given
     * memory page. It ensures the page is marked as no longer in use, updates
     * system tracking structures, and invalidates the associated TLB entry
     * on supported architectures. Caller must ensure the page passed is valid
     * and currently allocated.
     *
     * @param pg Pointer to the page to be freed. It is expected to point to
     *           a valid memory page that has been previously allocated.
     */
    void free_page(page *) noexcept;

    /**
     * Frees a specified number of memory pages starting from a given virtual address.
     *
     * This function is responsible for deallocating a contiguous range of memory pages.
     * It ensures that the specified pages are marked as no longer in use and updates
     * relevant system tracking structures. It is the caller's responsibility to provide
     * a valid starting address and a non-zero count of pages to be freed.
     *
     * @param start The virtual address of the first page in the range to be freed.
     * @param count The number of contiguous pages to be freed starting from the given address.
     */
    void free_pages(v_addr, usize) noexcept;
}

// no pun intended ;)
template <vintix::IntegralOrPointer T, vintix::IntegralOrPointer V>
constexpr auto on_the_same_page(T v1, V v2) noexcept -> bool
{
    return (align_down<PAGE_FRAME_SIZE>(v1) == align_down<PAGE_FRAME_SIZE>(v2));
}

inline auto get_page(v_addr vaddr) noexcept -> p_addr *
{
    return get_page(unsafe_toptr<const void *>(vaddr));
}

// note this doesn't actually 'initialize' paging. This is already done
// for us. It is only for setting up static variables and such at runtime
auto init_paging(p_addr physical_start, u32 bitmap_count) noexcept -> p_addr;

#endif // VINTIX_PMM_H
