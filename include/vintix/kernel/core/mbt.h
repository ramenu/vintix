/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_MBT_H
#define VINTIX_MBT_H

#include <grub/multiboot.h>
#include <vintix/kernel/lib/types.h>

enum class mbt_module {
    FLAT_BINARY,
    ELF
};

extern const multiboot_info *mbt;
static inline constexpr int MBT_MAGIC_NUMBER = 0x1BADB002;
static inline constexpr int MBT_FLAGS = 0x00000001;

auto multiboot_detect_module_type(const multiboot_mod_list *mb_modules) noexcept -> mbt_module;

#endif // VINTIX_MBT_H
