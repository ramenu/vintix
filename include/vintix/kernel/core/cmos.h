/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_CMOS_H
#define VINTIX_CMOS_H

#include <vintix/kernel/core/io.h>
#include <vintix/kernel/lib/types.h>

enum : u8 {
    CMOS_WRITE_PORT = 0x70,
    CMOS_READ_PORT = 0x71,
};

enum class cmos_register : u8 {
    RTC_SECONDS = 0x00,
    RTC_MINUTES = 0x02,
    RTC_HOURS = 0x04,
    RTC_WEEKDAY = 0x06,
    RTC_DAY_OF_MONTH = 0x07,
    RTC_MONTH = 0x08,
    RTC_YEAR = 0x09,
    RTC_CENTURY = 0x32, // might exist
    STATUS_A = 0x0A,
    STATUS_B = 0x0B,
};

extern "C" inline void cmos_sel_register(cmos_register reg, bool nmi_disable) noexcept
{
    outb(CMOS_WRITE_PORT, static_cast<u8>((nmi_disable << 7) | static_cast<u8>(reg)));
    io_wait();
}

extern "C" inline auto cmos_update_in_progress() noexcept -> bool
{
    cmos_sel_register(cmos_register::STATUS_A, true);
    return (inb(CMOS_READ_PORT) & 0x80);
}

#endif // VINTIX_CMOS_H
