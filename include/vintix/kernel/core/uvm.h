// Copyright (C) 2023-2025 Abdur-Rahman Mansoor
//
// This file is part of Vintix.
//
// Vintix is free software: you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation, either version
// 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
//
// Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Vintix. If
// not, see <https://www.gnu.org/licenses/>.

#ifndef VINTIX_UVM_H
#define VINTIX_UVM_H

#include <vintix/kernel/core/pager.h>
#include <vintix/kernel/lib/vector.h>

enum vm_prot_flags {
    VM_PROT_NONE = 0,
    VM_PROT_READ = 1,
    VM_PROT_WRITE = 1 << 1,
    VM_PROT_EXEC = 1 << 2,
};

DEFINE_ENUM_BITWISE_OPS(vm_prot_flags)

class vm_prot {
    vm_prot_flags v_flags;

  public:
    constexpr vm_prot() noexcept : v_flags { VM_PROT_NONE } {}
    explicit constexpr vm_prot(vm_prot_flags flags) noexcept : v_flags { flags } {}
    constexpr auto is_readable() const noexcept -> bool { return v_flags & VM_PROT_READ; }
    constexpr auto is_writable() const noexcept -> bool { return v_flags & VM_PROT_WRITE; }
    constexpr auto is_executable() const noexcept -> bool { return v_flags & VM_PROT_EXEC; }
    constexpr auto is_readonly() const noexcept -> bool { return v_flags == VM_PROT_READ; }
    constexpr auto operator|=(vm_prot_flags flags) noexcept -> vm_prot &
    {
        v_flags = v_flags | flags;
        return *this;
    }
    constexpr auto operator&=(vm_prot_flags flags) noexcept -> vm_prot &
    {
        v_flags = v_flags & flags;
        return *this;
    }
    constexpr auto operator=(vm_prot_flags flags) noexcept -> vm_prot &
    {
        v_flags = flags;
        return *this;
    }
};

struct vm_object {
    vm_pager pager;
};

struct vm_map_entry {
    vm_off_t start;
    vm_off_t end;
    vm_prot prot;
    vm_object object;
    constexpr auto memory_usage() const noexcept -> vm_off_t { return end - start; }
};

class vm_map {
    vintix::vector<vm_map_entry> v_entries {};
    v_addr v_start {};

  public:
    constexpr vm_map() noexcept = default;
    constexpr auto entrypoint() const noexcept -> const v_addr & { return v_start; }
    explicit constexpr vm_map(v_addr start) noexcept : v_start { start } {}
    constexpr auto size() const noexcept -> const usize & { return v_entries.size(); }
    constexpr auto memory_usage() const noexcept -> vm_off_t
    {
        usize mem_usage = 0;
        for (usize i = 0; i < v_entries.size(); i++)
            mem_usage += v_entries[i].memory_usage();
        return mem_usage;
    }
    VINTIX_CONSTEXPR auto operator[](usize i) const noexcept -> const vm_map_entry &
    {
        KASSERT(i < v_entries.size());
        return v_entries[i];
    }
    VINTIX_CONSTEXPR auto at(usize i) const noexcept -> const vm_map_entry &
    {
        KASSERT(i < v_entries.size());
        return v_entries[i];
    }
    vm_map(vm_map &&) noexcept = default;
    auto operator=(vm_map &&) noexcept -> vm_map & = default;
    auto insert(vm_map_entry &&) noexcept -> errno;
    auto remove(vm_off_t, vm_off_t) noexcept -> void;
    auto lookup(vm_off_t pos) const noexcept -> const vm_map_entry *;
    ~vm_map() noexcept;
};

#endif // VINTIX_UVM_H