/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_MACROS_H
#define VINTIX_MACROS_H

#define DELETE_MOVABLE(T)                                                                          \
    auto operator=(T &&)->T & = delete;                                                            \
    T(T &&) = delete;

#define DELETE_COPYABLE(T)                                                                         \
    auto operator=(const T &)->T & = delete;                                                       \
    T(const T &) = delete;

#define DELETE_COPYABLE_AND_MOVABLE(T)                                                             \
    DELETE_MOVABLE(T)                                                                              \
    DELETE_COPYABLE(T)

#define CONCAT(x, y) x##y
#define EXPAND(x, y) CONCAT(x, y)
#define RESERVED EXPAND(_reserved, __LINE__)

#if defined(__clang__)
#define UNINITIALIZED [[clang::uninitialized]]
#define NODESTROY [[clang::no_destroy]]
#define NOINLINE [[clang::noinline]]
#elif defined(__GNUC__) || defined(__GNUG__)
#define UNINITIALIZED
#define NODESTROY
#define NOINLINE [[gnu::noinline]]
#else
#error Unsupported compiler
#endif

#define RESTRICT __restrict__
#define EXPECT __builtin_expect
#define NODISCARD [[nodiscard]]
#define NORETURN [[noreturn]]
#define DEPRECATED [[deprecated]]
#define COPY(A) (A)
#define LENGTH_OF(A) (sizeof(A) / sizeof(A[0]))
#define BOCHS_BREAKPOINT() asm volatile("xchg 	bx, bx")

#define DEFINE_ENUM_BITWISE_OPS(E)                                                                 \
    constexpr auto operator|(E a, E b) noexcept -> E                                               \
    {                                                                                              \
        return static_cast<E>(static_cast<int>(a) | static_cast<int>(b));                          \
    }                                                                                              \
    constexpr auto operator&(E a, E b) noexcept -> E                                               \
    {                                                                                              \
        return static_cast<E>(static_cast<int>(a) & static_cast<int>(b));                          \
    }                                                                                              \
    constexpr auto operator|=(E &a, E b) noexcept -> E                                             \
    {                                                                                              \
        a = a | b;                                                                                 \
        return a | b;                                                                              \
    }                                                                                              \
    constexpr auto operator&=(E &a, E b) noexcept -> E                                             \
    {                                                                                              \
        a = a & b;                                                                                 \
        return a & b;                                                                              \
    }

#endif // VINTIX_MACROS_H
