/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_VMM_H
#define VINTIX_VMM_H

#include <vintix/kernel/lib/errno.h>
#include <vintix/kernel/lib/types.h>

// these are set by our loader, contains the virtual address offset
// and the physical address offset. We cannot use the symbols from
// the linker directly because our higher half kernel cannot access
// them properly due to paging, so the loader sets these symbols
// manually before the C++ entrypoint (the higher half) is called.
extern "C" inline volatile const v_addr VADDR_OFFSET {};
extern "C" const p_addr PHYSICAL_START;

static constexpr u32 VIRTUAL_ADDRESS_BITS = 32;
static constexpr usize MALLOC_ALIGNMENT = sizeof(void *);

struct kmem {
    p_addr p_start;
    p_addr p_end;
    v_addr v_start;
    v_addr v_end;
};

auto debug_block_state() noexcept -> bool;

extern "C" {
    // returns the number of bytes used by the kernel itself
    [[nodiscard]] auto kernel_memory_usage() noexcept -> usize;

    // returns the number of bytes used by the kernel heap
    [[nodiscard]] auto kernel_heap_memory_usage() noexcept -> usize;

    /**
     * Allocates a block of memory of the specified size and alignment.
     *
     * This function attempts to allocate at least `n` bytes of memory,
     * aligned to `align` bytes. Memory is allocated from a kernel heap.
     * If memory allocation fails, it returns a null pointer.
     *
     * The function ensures the heap remains in a valid state, and performs
     * several preconditions and invariants checks:
     *   - The size `n` must be greater than 0.
     *   - The alignment `align` must be a power of two.
     *   - Additional debug-specific checks are performed in non-release builds,
     *     such as ensuring the heap's internal structure is valid.
     *
     * New blocks are allocated with padding and alignment as required. An attempt
     * to expand the heap by allocating new pages is made where necessary.
     *
     * @param n The size of the memory block, in bytes.
     * @param align The alignment requirements of the memory block, in bytes.
     *
     * @return A pointer to the allocated memory block. Returns `nullptr` if memory
     *         allocation failed.
     *
     * @note The allocated memory region may contain uninitialized data.
     *       It is the caller's responsibility to initialize the memory if
     *       required.
     */
    [[nodiscard]] auto kmalloc(usize n, usize align = MALLOC_ALIGNMENT) noexcept -> void *;

    /**
     * Allocates and clears a contiguous memory block, with the specified count, size, and
     * alignment.
     *
     * This function calculates the total size of memory required as `length * sz`,
     * then allocates it using `kzalloc`, ensuring the memory is both allocated and
     * zero-initialized. Alignment requirements are specified via the `align`
     * parameter. If the multiplication overflows or allocation fails, it returns
     * a null pointer.
     *
     * @param length The number of elements to allocate.
     * @param sz The size of each element in bytes.
     * @param align The alignment requirements for the memory block, in bytes.
     *
     * @return A pointer to the allocated, zero-initialized memory block.
     *         Returns `nullptr` if memory allocation fails or if size calculation
     *         results in an overflow.
     */
    [[nodiscard]] auto kcalloc(usize length, usize sz,
                               usize align = MALLOC_ALIGNMENT) noexcept -> void *;

    /**
     * Allocates a block of memory of the specified size and alignment,
     * and initializes the allocated memory to zero.
     *
     * This function wraps `kmalloc` to allocate at least `n` bytes of memory,
     * aligned to `align`, and sets all allocated memory to zero. If memory
     * allocation fails, it returns a null pointer.
     *
     * The function ensures the following:
     *   - The size `n` must be greater than 0.
     *   - The alignment `align` must be a power of two.
     *   - Memory is zero-initialized before returning a pointer to the caller.
     *
     * @param n The size of the memory block to allocate, in bytes.
     * @param align The alignment requirements of the memory block, in bytes.
     *
     * @return A pointer to the allocated and zero-initialized memory block.
     *         Returns `nullptr` if memory allocation failed.
     *
     * @note The memory is zero-initialized for convenience. It is designed for
     *       scenarios where uninitialized memory could cause issues.
     */
    [[nodiscard]] auto kzalloc(usize n, usize align = MALLOC_ALIGNMENT) noexcept -> void *;

    /**
     * Reallocates a block of memory to a new size and alignment.
     *
     * This function attempts to resize the memory block pointed to by `p` to
     * a new size of `n` bytes with the specified alignment `align`. If the
     * requested size exceeds the available space in the current block, a new
     * memory block is allocated, and the contents of the original block are
     * copied to the new block. If `p` is null, the function behaves like
     * `kzalloc`, allocating a new block of memory with the specified size
     * and alignment. The function ensures several preconditions:
     *   - The pointer `p` must reside within the kernel heap.
     *   - The alignment `align` must be appropriate for the allocator.
     *
     * On successful reallocation, the old memory block is freed, and a pointer
     * to the new memory block is returned. If reallocation fails, the function
     * returns the original pointer, and the error code `err` is set.
     *
     * @param p Pointer to the memory block to be reallocated. Can be null.
     * @param n The new size of the memory block, in bytes.
     * @param err Output parameter to store the error code. Set to `ENOERR`
     *            on success or `ENOMEM` if memory reallocation fails.
     * @param align The alignment requirements of the memory block, in bytes.
     *
     * @return A pointer to the newly allocated memory block. Returns the original
     *         pointer `p` if reallocation fails or a null pointer if `p` was null
     *         and allocation fails.
     *
     * @note If a new memory block is allocated, the unused portion of the block
     *       will be zero-initialized. If allocation fails, the contents of the
     *       original memory block remain unchanged.
     */
    [[nodiscard]] auto krealloc(void *p, usize n, errno &err,
                                usize align = MALLOC_ALIGNMENT) noexcept -> void *;

    /**
     * Frees a previously allocated block of memory.
     *
     * This function releases a memory block that was previously allocated
     * using `kmalloc`, `kzalloc`, or `kcalloc`. It ensures the
     * integrity of the kernel heap, with additional debugging checks in non-release builds.
     *
     * The function is a no-op if the pointer `p` is `nullptr`.
     *
     * @param p A pointer to the memory block to be freed. It must point to the
     *          beginning of a block that was allocated by `kmalloc` or another
     *          compatible allocator. The behavior is undefined if `p` does not
     *          satisfy this condition.
     */
    void kfree(void *p) noexcept;
}

void init_memory(void *heap_start) noexcept;

#endif // VINTIX_VMM_H
