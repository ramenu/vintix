/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_PARTITION_H
#define VINTIX_PARTITION_H

#include <vintix/kernel/arch/x86-32/mbr.h>
#include <vintix/kernel/lib/errno.h>

struct partition_entry {
    u8 drive_attributes;
    mbr_partition_type type;
    u32 start_lba;
    u32 sectors;
    [[nodiscard]] constexpr auto is_empty() const noexcept -> bool
    {
        return (this->type == mbr_partition_type::EMPTY);
    }
};

struct partitions {
    static constexpr usize PARTITIONS = 4;
    dev_t dev;
    partition_entry entries[PARTITIONS];
};

extern "C" {
    auto get_partitions(dev_t dev, partitions &parts) noexcept -> errno;
}

#endif // VINTIX_PARTITION_H
