/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_GLOBAL_H
#define VINTIX_GLOBAL_H

#ifndef NDEBUG
static constexpr bool DEBUG_BUILD = true;
#define VINTIX_CONSTEXPR
#else
#define VINTIX_CONSTEXPR constexpr
static constexpr bool DEBUG_BUILD = false;
#endif

static constexpr bool SHOW_ADDRESS_MAP = true;

#if defined(__SHOW_LOGS__) && __SHOW_LOGS__ == true
static constexpr bool SHOW_LOGS = true;
#else
static constexpr bool SHOW_LOGS = false;
#endif

#endif // VINTIX_GLOBAL_H
