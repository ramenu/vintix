/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_PAGER_H
#define VINTIX_PAGER_H

#include <vintix/kernel/core/pmm.h>
#include <vintix/kernel/lib/errno.h>

class file;

class vm_pager {
  public:
    file *p_fp;
    off_t p_offset;
    off_t p_memsz;

    // this should be redundant, but in the case of ELF,
    // if 'p_filesz' is larger than 'p_memsz' then the
    // extra bytes should simply just contain 0. Otherwise,
    // this value is ignored.
    off_t p_filesz;
    constexpr auto total_number_of_pages() const noexcept -> off_t
    {
        constexpr off_t PAGE_FRAME_SIZE_OFFT = PAGE_FRAME_SIZE;
        if (p_memsz % PAGE_FRAME_SIZE_OFFT == 0)
            return p_memsz / PAGE_FRAME_SIZE_OFFT;
        return p_memsz / PAGE_FRAME_SIZE_OFFT + 1;
    }
    constexpr auto number_of_pages_filesz() const noexcept -> off_t
    {
        constexpr off_t PAGE_FRAME_SIZE_OFFT = PAGE_FRAME_SIZE;
        if (p_filesz % PAGE_FRAME_SIZE_OFFT == 0)
            return p_filesz / PAGE_FRAME_SIZE_OFFT;
        return p_filesz / PAGE_FRAME_SIZE_OFFT + 1;
    }

    // If `p_fp` is NULL, then the buffer will be zero-filled
    auto get_page(void *, off_t = 0) const noexcept -> errno;
    static constexpr auto determine_page_offset(vm_off_t off, vm_off_t start) noexcept -> off_t
    {
        return (off - start) / PAGE_FRAME_SIZE;
    }
};

#endif // VINTIX_PAGER_H