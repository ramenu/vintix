/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_HANDLER_H
#define VINTIX_HANDLER_H

#include <vintix/kernel/core/idt.h>

struct interrupt_state;

/**
 * CPU exception handlers
 */

void terminate_process(const char *msg) noexcept;
void page_fault_handler(const interrupt_state &interrupt) noexcept;
void general_protection_fault_handler() noexcept;

#endif // VINTIX_HANDLER_H
