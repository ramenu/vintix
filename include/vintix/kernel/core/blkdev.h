/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_BLKDEV_H
#define VINTIX_BLKDEV_H

#include <vintix/kernel/arch/x86-32/mbr.h>
#include <vintix/kernel/core/dev.h>
#include <vintix/kernel/lib/errno.h>
#include <vintix/kernel/lib/optional.h>
#include <vintix/kernel/lib/span.h>
#include <vintix/kernel/lib/types.h>

static constexpr u32 DEFAULT_LBA_SECTOR_SIZE = 512;

struct blkdev_query_info {
    u16 cylinders;
    u8 sectors_per_track;
    u8 heads;
};

struct disk_chs_address {
    u16 cylinder;
    u8 head;
    u8 sector;
};

struct blkdev : public device {
    blknum_t lba;
    blknum_t total_sectors;
    u16 log_secsz;
};

extern "C" {
    /**
     * @brief Registers a block device and sets up necessary configurations.
     *
     * This function adds a block device (`blkdev`) to the system, verifies its validity,
     * checks for additional partitions if it's a root device,
     * and parses the master boot record (MBR) if applicable.
     *
     * @param disk Pointer to the block device structure to be added.
     *             It must not be `nullptr`, and its internal properties
     *             such as the device vtable and name must also be valid.
     * @return Returns an error code:
     *         - ENOERR on successful registration.
     *         - ENOMEM if memory allocation for parsing the MBR fails.
     *         - Other error codes may be returned depending on the device addition
     *           or MBR parsing results.
     */
    auto disk_add(blkdev *disk) noexcept -> errno;

    /**
     * @brief Converts a CHS (Cylinder-Head-Sector) address to an LBA (Logical Block Address).
     *
     * This function calculates the logical block address (LBA) from the given CHS address
     * based on the disk geometry information retrieved from the block device.
     * The conversion will succeed only if the block device supports querying its geometry.
     *
     * @param dev Reference to the block device structure containing the device's operation vtable.
     *            The vtable must support the `blk_query` function to retrieve disk geometry.
     * @param chs Reference to a `disk_chs_address` structure containing the cylinder, head, and
     * sector parameters.
     * @param lba Reference to a variable where the converted LBA will be stored if the operation
     * succeeds.
     * @return Returns `true` if the conversion is successful, i.e., the device supports geometry
     * querying and the LBA is computed. Returns `false` if the `blk_query` function is unavailable
     * or the operation fails.
     */
    auto chs_to_lba(const blkdev &dev, const disk_chs_address &chs, u32 &lba) noexcept -> bool;

    /**
     * @brief Reads blocks of data from a block device into the specified destination buffer.
     *
     * This function is used to read a number of sectors (blocks) starting from a
     * specified logical block address (LBA) on a block device. The data is copied
     * into the destination buffer provided.
     *
     * @param dev The device identifier (`dev_t`) representing the block device to read from.
     * @param dst Pointer to the destination buffer where the read data will be stored.
     *            It must be a valid memory location and must not be `nullptr`.
     * @param lba_start The starting logical block address (LBA) from which the read operation
     * begins.
     * @param count The number of sectors to be read.
     * @return Returns an error code:
     *         - ENOERR on successful read.
     *         - EIO if an I/O error occurs during the read operation.
     *         - Other error codes may be returned depending on the underlying device
     *           implementation.
     */
    auto disk_read(dev_t dev, void *dst, blknum_t lba_start, blkcnt_t count) noexcept -> errno;

    /**
     * @brief Writes data to a block device starting at the specified logical block address (LBA).
     *
     * This function writes `count` sectors of data from the buffer `src` to the block device
     * identified by `dev`, starting at the logical block address `lba_start`.
     * It validates the input parameters and utilizes the block devices write callback
     * to perform the operation.
     *
     * @param dev Identifier of the block device where data will be written.
     *            The device must be registered and have a valid block device vtable.
     * @param src Pointer to the buffer containing data to be written. It must not be `nullptr`.
     * @param lba_start Logical block address specifying where the data write will begin.
     * @param count Number of sectors to write from the source buffer to the block device.
     * @return Returns an error code:
     *         - ENOERR on successful write operation.
     *         - Appropriate error codes if the write operation fails, such as errors returned
     *           by the device write callback.
     */
    auto disk_write(dev_t dev, const void *src, blknum_t lba_start,
                    blkcnt_t count) noexcept -> errno;

    auto generic_disk_put_blks(const file *, vintix::span<blkcnt_t>, off_t, bool) noexcept -> errno;
}

// See https://en.wikipedia.org/wiki/CHS_conversion#CHS_conversion
inline constexpr auto lba_to_chs(u32 lba, u8 hpc, u8 spt) noexcept -> disk_chs_address
{
    disk_chs_address chs;
    chs.cylinder = static_cast<u16>(lba / (hpc * spt));
    chs.head = static_cast<u8>((lba / spt) % hpc);
    chs.sector = static_cast<u8>((lba % spt) + 1);
    return chs;
}

inline constexpr auto chs_to_lba(const disk_chs_address &chs, u8 heads_per_cylinder,
                                 u8 sectors_per_track) noexcept -> u32
{
    return ((chs.cylinder * heads_per_cylinder + chs.head) * sectors_per_track + (chs.sector - 1));
}

#endif // VINTIX_BLKDEV_H
