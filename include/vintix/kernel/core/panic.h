/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_PANIC_H
#define VINTIX_PANIC_H

extern "C" {
    /**
     * Triggers a kernel panic, ending execution with error information. This function prints
     * an error message, a stack trace (on non-release builds), disables interrupts, and halts
     * the system.
     *
     * @param format A printf-style format string used to specify the error message.
     *               Must not be null.
     * @param ... Additional arguments for the format string, depending on the placeholders
     *            in the format string.
     */
    [[noreturn]] void kpanic(const char *format, ...) noexcept;
}

#endif // VINTIX_PANIC_H
