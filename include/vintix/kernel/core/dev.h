/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_DEV_H
#define VINTIX_DEV_H

#include <vintix/kernel/core/macros.h>
#include <vintix/kernel/filesystems/fs.h>
#include <vintix/kernel/filesystems/inode.h>
#include <vintix/kernel/lib/cxxstring.h>
#include <vintix/kernel/lib/optional.h>
#include <vintix/kernel/lib/types.h>
#include <vintix/kernel/lib/vector.h>

#define NODEV 0
#define DEVFS_MAGIC 666

enum class device_type : u8 {
    CHRDEV,
    BLKDEV,
};

struct blkdev;
struct chrdev;
struct blkdev_query_info;
struct file_vtable;
struct dentry;
class file;
class inode;
class superblock;
class device;

struct device_vtable {
    errno (*blk_read)(const blkdev *, void *, blknum_t, blkcnt_t);
    errno (*blk_write)(const blkdev *, const void *, blknum_t, blkcnt_t);
    blkdev_query_info (*blk_query)(const blkdev *);
    errno (*close)(device *);
};

class device {
  private:
    dev_t d_id {};
    NODESTROY static inline vintix::vector<device *> d_devices;
    static auto assign_id() noexcept -> vintix::optional<dev_t>;
    static auto get_id(dev_t dev) noexcept -> vintix::optional<usize>
    {
        if (dev == NODEV)
            return vintix::nullopt_t;

        for (usize i = 0; i < d_devices.size(); ++i)
            if (d_devices[i]->id() == dev)
                return i;
        return vintix::nullopt_t;
    }
    static auto get_mutable(dev_t dev) noexcept -> device *
    {
        const vintix::optional<usize> i = get_id(dev);
        return (i.is_some()) ? d_devices[i.value()] : nullptr;
    }
    static auto init_fs_context(const fs_context &context) noexcept -> errptr<superblock>;
    static auto dev_lookup(const dentry *, const char *) noexcept -> errptr<inode>;
    static auto dev_root_inode(superblock *) noexcept -> errptr<inode>;
    static auto dev_open(inode *) noexcept -> errptr<file>;

  public:
    vintix::string d_name {};
    u8 d_major {};
    u8 d_minor {};
    device_type d_type {};
    mode_t d_perms {};
    const device_vtable *dev_vtable {};
    const file_vtable *f_vtable {};
    [[nodiscard]] constexpr auto is_readable() const noexcept -> bool
    {
        return dev_vtable->blk_read != nullptr;
    }
    [[nodiscard]] constexpr auto is_writable() const noexcept -> bool
    {
        return dev_vtable->blk_write != nullptr;
    }
    [[nodiscard]] constexpr auto id() const noexcept -> dev_t { return d_id; }
    [[nodiscard]] constexpr auto major() const noexcept -> u8 { return d_major; }
    [[nodiscard]] constexpr auto minor() const noexcept -> u8 { return d_minor; }
    [[nodiscard]] constexpr auto is_blkdev() const noexcept -> bool
    {
        return (d_type == device_type::BLKDEV);
    }
    [[nodiscard]] constexpr auto is_chrdev() const noexcept -> bool
    {
        return (d_type == device_type::CHRDEV);
    }
    [[nodiscard]] static auto add_device(device *) noexcept -> errno;
    static auto remove_device(dev_t, errno (*close)(device *)) noexcept -> errno;
    static auto find(const char *) noexcept -> vintix::optional<dev_t>;
    static auto init_devfs() noexcept -> errno;
    static auto get(dev_t dev) noexcept -> const device * { return get_mutable(dev); }
    static auto get_blkdev(dev_t dev) noexcept -> const blkdev *
    {
        return reinterpret_cast<const blkdev *>(get(dev));
    }
    static auto get_chrdev(dev_t dev) noexcept -> const chrdev *
    {
        return reinterpret_cast<const chrdev *>(get(dev));
    }
};

extern "C" {
    inline auto generic_device_close(device *dev) noexcept -> errno
    {
        kfree(dev);
        return ENOERR;
    }
}

#endif // VINTIX_DEV_H
