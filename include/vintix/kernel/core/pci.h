/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_PCI_H
#define VINTIX_PCI_H

#include <vintix/kernel/core/kassert.h>
#include <vintix/kernel/lib/types.h>

// Device ID: Identifies the particular device. Where valid IDs are
// allocated by the vendor.
enum class pci_device_id : u16 {
    PLACEHOLDER = 0x0,
    ME_4660I = 4663, // MEILHAUS_ELECTRONIC
};

// Vendor ID: Identifies the manufacturer of the device. Where valid
// IDs are allocated by PCI-SIG to ensure uniqueness and 0xFFFF is an
// invalid value that will be returned on read accesses to Configuration
// Space registers of non-existent devices.
// A list of PCI vendors can be found here:
// https://pcisig.com/membership/member-companies?PageSpeed=noscript
enum class pci_vendor_id : u16 {
    INTEL = 0x8086,
    AMD = 0x1022,
    APPLE = 0x106b,
    LENOVO = 0x17aa,
    ACER = 0x18e4,
    HEWLETT_PACKARD = 0x1590,
    IBM = 0x1014,
    TEXAS_INSTRUMENTS = 0x104c,
    MEILHAUS_ELECTRONIC = 0x57a,
    PCI_NO_DEVICE = 0xFFFF,
};

// A read only register that specifies the specific function the device
// performs.
enum class pci_device_class : u8 {
    UNCLASSIFIED_DEVICE = 0x0,
    MASS_STORAGE_CONTROLLER = 0x1,
    NETWORK_CONTROLLER = 0x2,
    DISPLAY_CONTROLLER = 0x3,
    MULTIMEDIA_CONTROLLER = 0x4,
    MEMORY_CONTROLLER = 0x5,
    BRIDGE = 0x6,
    COMMUNICATION_CONTROLLER = 0x7,
    GENERIC_SYSTEM_PERIPHERAL = 0x8,
    INPUT_DEVICE_CONTROLLER = 0x9,
    DOCKING_STATION = 0xA,
    PROCESSOR = 0xB,
    SERIAL_BUS_CONTROLLER = 0xC,
    WIRELESS_CONTROLLER = 0xD,
    INTELLIGENT_CONTROLLER = 0xE,
    SATELITE_COMMUNICATIONS_CONTROLLER = 0xF,
    ENCRYPTION_CONTROLLER = 0x10,
    SIGNAL_PROCESSING_CONTROLLER = 0x11,
    PROCESSING_ACCELERATORS = 0x12,
    NON_ESSENTIAL_INSTRUMENTATION = 0x13,
    COPROCESSOR = 0x14,
    UNASSIGNED_CLASS = 0xFF
};

enum class pci_header_type : u8 {
    GENERAL_DEVICE = 0x0,
    PCI_TO_PCI_BRIDGE = 0x1,
    PCI_TO_CARDBUS_BRIDGE = 0x2,
};

// List of device subclasses can be found here: https://admin.pci-ids.ucw.cz/read/PD/
enum class pci_device_subclass : u8 {
    // Unclassified device subclasses
    NON_VGA_UNCLASSIFIED_DEVICE = 0x0,
    VGA_COMPATIBLE_UNCLASSIFIED_DEVICE = 0x1,
    IMAGE_COPROCESSOR = 0x5,

    // Mass storage controller subclasses
    SCSI_STORAGE_CONTROLLER = 0x0,
    IDE_INTERFACE = 0x1,
    FLOPPY_DISK_CONTROLLER = 0x2,
    IPI_BUS_CONTROLLER = 0x3,
    RAID_BUS_CONTROLLER = 0x4,
    ATA_CONTROLLER = 0x5,
    SATA_CONTROLLER = 0x6,
    SERIAL_ATTACHED_SCSI_CONTROLLER = 0x7,
    NON_VOLATILE_MEMORY_CONTROLLER = 0x8,
    UNIVERSAL_FLASH_STORAGE_CONTROLLER = 0x9,

    // Network controller subclasses
    ETHERNET_CONTROLLER = 0x0,
    TOKEN_RING_NETWORK_CONTROLLER = 0x1,
    FDDI_NETWORK_CONTROLLER = 0x2,
    ATM_NETWORK_CONTROLLER = 0x3,
    ISDN_CONTROLLER = 0x4,
    WORLDFIP_CONTROLLER = 0x5,
    PICMG_CONTROLLER = 0x6,
    INFINIBAND_CONTROLLER = 0x7,
    FABRIC_CONTROLLER = 0x8,

    // Display controller subclasses
    VGA_COMPATIBLE_CONTROLLER = 0x0,
    XGA_COMPATIBLE_CONTROLLER = 0x1,
    _3D_CONTROLLER = 0x2,

    // Multimedia controller subclasses
    MULTIMEDIA_VIDEO_CONTROLLER = 0x0,
    MULTIMEDIA_AUDIO_CONTROLLER = 0x1,
    COMPUTER_TELEPHONY_DEVICE = 0x2,
    AUDIO_DEVICE = 0X3,

    // Memory controller subclasses
    RAM_MEMORY = 0x0,
    FLASH_MEMORY = 0x1,
    CXL = 0x2,

    // Bridge subclasses
    HOST_BRIDGE = 0x0,
    ISA_BRIDGE = 0x1,
    EISA_BRIDGE = 0x2,
    MICROCHANNEL_BRIDGE = 0x3,
    PCI_BRIDGE = 0x4,
    PCMCIA_BRIDGE = 0x5,
    NUBUS_BRIDGE = 0x6,
    CARDBUS_BRIDGE = 0x7,
    RACEWAY_BRIDGE = 0x8,
    SEMI_TRANSPARENT_PCI_TO_PCI_BRIDGE = 0x9,
    INFINIBAND_TO_PCI_HOST_BRIDGE = 0xA,

    // Communication controller subclasses
    SERIAL_CONTROLLER = 0x0,
    PARALLEL_CONTROLLER = 0x1,
    MULTIPORT_SERIAL_CONTROLLER = 0x2,
    MODEM = 0x3,
    GPIB_CONTROLLER = 0x4,
    SMART_CARD_CONTROLLER = 0x5,

    // Generic system peripheral subclasses
    PIC = 0x0,
    DMA_CONTROLLER = 0x1,
    TIMER = 0x2,
    RTC = 0x3,
    PCI_HOT_PLUG_CONTROLLER = 0x4,
    SD_HOST_CONTROLLER = 0x5,
    IOMMU = 0x6,
    TIMING_CARD = 0x99,

    // Input device controller subclasses
    KEYBOARD_CONTROLLER = 0x0,
    DIGITIZER_PEN = 0x1,
    MOUSE_CONTROLLER = 0x2,
    SCANNER_CONTROLLER = 0x3,
    GAMEPORT_CONTROLLER = 0x4,

    // Docking station subclasses
    GENERIC_DOCKING_STATION = 0x0,

    // Processor subclasses
    I386 = 0x0,
    I486 = 0x1,
    PENTIUM = 0x2,
    ALPHA = 0x10,
    POWERPC = 0x20,
    MIPS = 0x30,
    CO_PROCESSOR = 0x40,

    // Serial bus controller subclasses
    FIREWIRE_IEEE_1394 = 0x0,
    ACCESS_BUS = 0x1,
    SSA = 0x2,
    USB_CONTROLLER = 0x3,
    FIBRE_CHANNEL = 0x4,
    SMBUS = 0x5,
    INFINIBAND = 0x6,
    IPMI_INTERFACE = 0x7,
    SERCOS_INTERFACE = 0x8,
    CANBUS = 0x9,

    // Wireless controller subclasses
    IRDA_CONTROLLER = 0x0,
    CONSUMER_IR_CONTROLLER = 0x1,
    RF_CONTROLLER = 0x10,
    BLUETOOTH = 0x11,
    BROADBAND = 0x12,
    _802_1A_CONTROLLER = 0x20,
    _802_1B_CONTROLLER = 0x21,

    // Intelligent controller subclasses
    I2O = 0x0,

    // Satellite communications provider subclasses
    SATELLITE_TV_CONTROLLER = 0x1,
    SATELLITE_AUDIO_COMMUNICATION_CONTROLLER = 0x2,
    SATELLITE_VOICE_COMMUNICATION_CONTROLLER = 0x3,
    SATELLITE_DATA_COMMUNICATION_CONTROLLER = 0x4,

    // Encryption controller subclasses
    NETWORK_AND_COMPUTING_ENCRYPTION_DEVICE = 0x0,
    ENTERTAINMENT_ENCRYPTION_DEVICE = 0x10,

    // Signal processing controller subclasses
    DPIO_MODULE = 0x0,
    PERFORMANCE_COUNTERS = 0x1,
    COMMUNICATION_SYNCHRONIZER = 0x10,
    SIGNAL_PROCESSING_MANAGEMENT = 0x20,

    // Processing accelerators subclasses
    PROCESSING_ACCELERATORS = 0x0,
    SNIA_SMART_DATA_ACCELERATOR_INTERFACE_CONTROLLER = 0x1,

    OTHER = 0x80,
};

struct pcidev {
    u8 bus;
    u8 device;
    u8 fn;
    pci_device_class clss;
    pci_device_subclass subclss;
    u8 prog_if;
    u8 int_line;
};

struct pcidev_list {
    pcidev *dev;
    usize sz;
};

enum {
    VENDOR_ID_BIT_OFFSET = 0x0,
    DEVICE_ID_BIT_OFFSET = 0x2,
    COMMAND_BIT_OFFSET = 0x4,
    STATUS_BIT_OFFSET = 0x6,
    REVISION_ID_BIT_OFFSET = 0x8,
    PROG_IF_BIT_OFFSET = 0x9,
    SUBCLASS_BIT_OFFSET = 0xA,
    CLASS_CODE_BIT_OFFSET = 0xB,
    HEADER_TYPE_BIT_OFFSET = 0xE,
    BAR0_BIT_OFFSET = 0x10,
    BAR1_BIT_OFFSET = 0x14,
    BAR2_BIT_OFFSET = 0x18,
    BAR3_BIT_OFFSET = 0x1C,
    BAR4_BIT_OFFSET = 0x20,
    BAR5_BIT_OFFSET = 0x24,
    INTERRUPT_LINE_BIT_OFFSET = 0x3C,
    PCI_MULTIFUNCTION_BIT = 1 << 7,
};

extern "C" {
    /**
     * Retrieves a list of all PCI devices available in the system.
     *
     * This function scans the PCI bus to detect connected devices and
     * populates the list of PCI devices with details such as bus number,
     * device number, function number, class, subclass, programming interface,
     * and interrupt line for each device. If no devices are detected or an
     * allocation failure occurs, an empty list is returned.
     *
     * @return A `pcidev_list` structure containing details of all detected PCI devices.
     *         Returns an empty list if no devices are found or in case of errors.
     */
    auto get_pci_devs() noexcept -> pcidev_list;

    auto pci_cfg_read_word(u8 bus, u8 slot, u8 func, u8 offset) noexcept -> u16;
    auto pci_cfg_read_dword(u8 bus, u8 slot, u8 func, u8 offset) noexcept -> u32;
    void pci_cfg_write_word(u8 bus, u8 slot, u8 func, u8 offset, u16 data) noexcept;
    inline auto pci_chk_int_line(u8 bus, u8 slot, u8 function) noexcept -> u8
    {
        return static_cast<u8>(pci_cfg_read_word(bus, slot, function, INTERRUPT_LINE_BIT_OFFSET));
    }
    inline auto pci_chk_int_pin(u8 bus, u8 slot, u8 function) noexcept -> u8
    {
        return static_cast<u8>(pci_cfg_read_word(bus, slot, function, INTERRUPT_LINE_BIT_OFFSET) >>
                               8);
    }
    inline auto pci_chk_header(u8 bus, u8 slot, u8 function) noexcept -> pci_header_type
    {
        return static_cast<pci_header_type>(
            pci_cfg_read_word(bus, slot, function, HEADER_TYPE_BIT_OFFSET) &
            ~PCI_MULTIFUNCTION_BIT);
    }
    inline auto pci_chk_device_class(u8 bus, u8 slot, u8 function) noexcept -> pci_device_class
    {
        return static_cast<pci_device_class>(
            pci_cfg_read_word(bus, slot, function, CLASS_CODE_BIT_OFFSET) >> 8);
    }
    inline auto pci_chk_device_subclass(u8 bus, u8 slot,
                                        u8 function) noexcept -> pci_device_subclass
    {
        return static_cast<pci_device_subclass>(
            pci_cfg_read_word(bus, slot, function, SUBCLASS_BIT_OFFSET));
    }
    inline auto pci_chk_bar0(u8 bus, u8 slot, u8 function) noexcept -> u32
    {
        KASSERT(pci_chk_header(bus, slot, function) == pci_header_type::GENERAL_DEVICE);
        return pci_cfg_read_dword(bus, slot, function, BAR0_BIT_OFFSET);
    }
    inline auto pci_chk_bar1(u8 bus, u8 slot, u8 function) noexcept -> u32
    {
        KASSERT(pci_chk_header(bus, slot, function) == pci_header_type::GENERAL_DEVICE);
        return pci_cfg_read_dword(bus, slot, function, BAR1_BIT_OFFSET);
    }
    inline auto pci_chk_bar2(u8 bus, u8 slot, u8 function) noexcept -> u32
    {
        KASSERT(pci_chk_header(bus, slot, function) == pci_header_type::GENERAL_DEVICE);
        return pci_cfg_read_dword(bus, slot, function, BAR2_BIT_OFFSET);
    }
    inline auto pci_chk_bar3(u8 bus, u8 slot, u8 function) noexcept -> u32
    {
        KASSERT(pci_chk_header(bus, slot, function) == pci_header_type::GENERAL_DEVICE);
        return pci_cfg_read_dword(bus, slot, function, BAR3_BIT_OFFSET);
    }
    inline auto pci_chk_bar4(u8 bus, u8 slot, u8 function) noexcept -> u32
    {
        KASSERT(pci_chk_header(bus, slot, function) == pci_header_type::GENERAL_DEVICE);
        return pci_cfg_read_dword(bus, slot, function, BAR4_BIT_OFFSET);
    }
    inline auto pci_chk_bar5(u8 bus, u8 slot, u8 function) noexcept -> u32
    {
        KASSERT(pci_chk_header(bus, slot, function) == pci_header_type::GENERAL_DEVICE);
        return pci_cfg_read_dword(bus, slot, function, BAR5_BIT_OFFSET);
    }
    inline auto pci_chk_prog_if(u8 bus, u8 slot, u8 function) noexcept -> u8
    {
        return static_cast<u8>(pci_cfg_read_word(bus, slot, function, PROG_IF_BIT_OFFSET) >> 8);
    }
    inline auto pci_chk_vendor(u8 bus, u8 slot, u8 function) noexcept -> pci_vendor_id
    {
        return static_cast<pci_vendor_id>(
            pci_cfg_read_word(bus, slot, function, VENDOR_ID_BIT_OFFSET));
    }
    inline auto pci_is_multifunction_device(u8 bus, u8 slot) noexcept -> bool
    {
        return (pci_cfg_read_word(bus, slot, 0, HEADER_TYPE_BIT_OFFSET) & PCI_MULTIFUNCTION_BIT);
    }
}

#endif // VINTIX_PCI_H
