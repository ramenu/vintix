/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_PIT_H
#define VINTIX_PIT_H

#include <vintix/kernel/lib/types.h>

struct interrupt_state;

void timer_handler(const interrupt_state &interrupt) noexcept;

// pauses regular program execution for the specified number of ticks
// (100 ticks = 1 second)
void ksleep(u32 ticks) noexcept;

// sets the PIT's clock rate to '1193180 / hz'
void set_pit_timer_phase(u32 hz) noexcept;

#endif // VINTIX_PIT_H
