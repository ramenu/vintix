/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_SERIAL_H
#define VINTIX_SERIAL_H

#include <vintix/kernel/core/global.h>
#include <vintix/kernel/core/io.h>
#include <vintix/kernel/lib/types.h>

static constexpr u16 SERIAL_COM1_BASE = 0x3f8;

extern "C" {
    auto serial_init() noexcept -> errno;
    inline auto serial_received() noexcept -> bool { return (inb(SERIAL_COM1_BASE + 5) & 1); }
    inline auto serial_transmit_empty() noexcept -> bool
    {
        return (inb(SERIAL_COM1_BASE + 5) & 0x20);
    }
    inline void serial_putc(char c, FBFGColor = DEFAULT_FBFG_COLOR,
                            FBBGColor = DEFAULT_FBBG_COLOR) noexcept
    {
        if constexpr (DEBUG_BUILD) {
            while (!serial_transmit_empty())
                ;
            outb(SERIAL_COM1_BASE, static_cast<u8>(c));
        }
    }
    inline void serial_puts(const char *s, FBFGColor fg = DEFAULT_FBFG_COLOR,
                            FBBGColor bg = DEFAULT_FBBG_COLOR) noexcept
    {
        for (usize i = 0; s[i] != '\0'; ++i)
            serial_putc(s[i], fg, bg);
    }
}

#endif // VINTIX_SERIAL_H
