/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_KASSERT_H
#define VINTIX_KASSERT_H

#include <vintix/kernel/core/global.h>
#include <vintix/kernel/lib/types.h>

#define KASSERT(expr, ...)                                                                         \
    if constexpr (DEBUG_BUILD) {                                                                   \
        if constexpr (sizeof(#__VA_ARGS__) >= 2)                                                   \
            _kassert(expr, #__VA_ARGS__, __FILE__, __func__, __LINE__);                            \
        else                                                                                       \
            _kassert(expr, "`" #expr "`", __FILE__, __func__, __LINE__);                           \
    }

#define THIS_SHOULDNT_HAPPEN(...)                                                                  \
    if constexpr (DEBUG_BUILD) {                                                                   \
        if constexpr (sizeof(#__VA_ARGS__) >= 1)                                                   \
            _kassert(0 == 1, #__VA_ARGS__, __FILE__, __func__, __LINE__);                          \
        else                                                                                       \
            _kassert(0 == 1, "THIS SHOULDN'T HAPPEN!", __FILE__, __func__, __LINE__);              \
    }

extern "C" {
    void _kassert(bool expr, const char *msg, const char *file, const char *function,
                  u32 line) noexcept;
}

#endif // VINTIX_KASSERT_H
