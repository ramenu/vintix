/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_ELF_H
#define VINTIX_ELF_H

#include <vintix/kernel/core/kassert.h>
#include <vintix/kernel/core/process.h>
#include <vintix/kernel/lib/context.h>
#include <vintix/kernel/lib/errno.h>
#include <vintix/kernel/lib/types.h>
#include <vintix/kernel/lib/utilities.h>

using elf32_half = u16;
using elf32_off = u32;
using elf32_addr = u32;
using elf32_word = u32;
using elf32_sword = i32;

struct elf32_sym;
struct elf32_ehdr;
struct elf32_shdr;
struct elf32_phdr;
struct process_pginfo;

struct elf32_rel {
    elf32_addr r_offset;
    elf32_word r_info;
    [[nodiscard]] auto relocate(const elf32_ehdr &, const elf32_shdr &,
                                kcontext) const noexcept -> errno;
};

struct elf32_rela {
    elf32_addr r_offset;
    elf32_word r_info;
    elf32_sword r_addend;
};

struct elf32_shdr {
    // name of the section. Its value is an index into the section header
    // table string section giving the location of a null-terminated string
    elf32_word sh_name;

    // categorizes the section's contents and semantics
    elf32_word sh_type;

    // sections support 1-bit flags that describe miscellaneous
    // attributes
    elf32_word sh_flags;

    // if the section is to appear in the memory image of a
    // process, this member gives the address at which the
    // section's first byte should reside. Otherwise the member
    // contains 0
    elf32_addr sh_addr;

    // the byte offset from the beginning of the file to the
    // first byte in the section. Section type 'SHT_NOBITS'
    // occupies no space in the file. Its 'sh_offset' member
    // locates the conceptual placement
    elf32_off sh_offset;

    // the section's size in bytes. Unless the section type
    // is 'SHT_NOBITS', the section occupies 'sh_size' in the
    // file. A section of type 'SHT_NOBITS' can have a nonzero
    // size, but it occupies no space
    elf32_word sh_size;

    // a section header table index link, whose interpretation
    // depends on the section type
    elf32_word sh_link;

    // extra information, whose interpretation depends on the
    // section type
    elf32_word sh_info;

    // address alignment constraints
    elf32_word sh_addralign;

    // some sections hold a table of fixed-size entries, such as
    // symbol table. For such a section, this member gives the size
    // in bytes of each entry. The member contains 0 if the section
    // does not hold a table of fixed-size entries
    elf32_word sh_entsize;

    [[nodiscard]] constexpr auto size() const noexcept -> elf32_word
    {
        return sh_size / sh_entsize;
    }

    [[nodiscard]] auto at(const elf32_ehdr &ehdr, elf32_word i) const noexcept -> void *
    {
        return unsafe_toptr<void *>((addressof(&ehdr) + sh_offset) + i * sh_entsize);
    }
    [[nodiscard]] auto get_segment(const elf32_ehdr &ehdr) const noexcept -> const elf32_phdr *;
} __attribute__((packed));

struct elf32_phdr {
    // the kind of segment this array elements describes or how to
    // interpret the array element's information
    elf32_word p_type;

    // the offset from the beginning of the file at which the
    // first byte of the segment resides
    elf32_off p_offset;

    // the virtual address at which the first byte of the
    // segment resides in memory
    elf32_addr p_vaddr;

    // the segment's physical address for systems in which
    // physical addressing is relevant
    elf32_addr p_paddr;

    // the number of bytes in the file image of the segment,
    // which can be zero
    elf32_word p_filesz;

    // the number of bytes in the memory image of the segment,
    // which can be zero
    elf32_word p_memsz;

    // flags relevant to the segment
    elf32_word p_flags;

    // loadable process segments must have congruent values for
    // 'p_vaddr' and 'p_offset' modulo the page size. This member
    // gives the value to which the segments are aligned in memory
    // and in the file. Values 0 and 1 mean no alignment is required.
    // Otherwise, 'p_align' should be a positive, integral power of 2,
    // and 'p_vaddr' should equal 'p_offset % p_align'
    elf32_word p_align;

    [[nodiscard]] constexpr auto is_valid() const noexcept -> bool { return p_memsz >= p_filesz; }
    [[nodiscard]] auto get_section(const elf32_ehdr &ehdr) const noexcept -> const elf32_shdr *;
} __attribute__((packed));

struct elf32_sym;
struct elf32_ehdr {
    static constexpr usize EI_NIDENT = 16;
    // NOLINTBEGIN(cppcoreguidelines-avoid-c-arrays)
    unsigned char e_ident[EI_NIDENT];
    // NOLINTEND(cppcoreguidelines-avoid-c-arrays)

    // identifies object file type
    elf32_half e_type;

    // specifies instruction set architecture (ISA)
    elf32_half e_machine;

    // set to 1 for the original version of ELF
    elf32_word e_version;

    // memory address of the entry point from where the process starts executing.
    // If the file doesn't have an associated entry point then this field should be 0
    elf32_addr e_entry;

    // points to the start of the program header table. It usually follows the
    // file header immediately following this one, making the offset '0x34' or '0x40'
    // for 32-bit and 64-bit ELF executables respectively
    elf32_off e_phoff;

    // points to the start of the section header table
    elf32_off e_shoff;

    // interpretation of this field depends on the target architecture
    elf32_word e_flags;

    // contains the size of the header
    elf32_half e_ehsize;

    // contains the size of a program header table entry
    elf32_half e_phentsize;

    // contains the number of entries in the program header table
    elf32_half e_phnum;

    // contains the size of a section header table entry
    elf32_half e_shentsize;

    // contains the number of entries in the section header table
    elf32_half e_shnum;

    // contains index of the section header table entry that contains the
    // section names
    elf32_half e_shstrndx;

    [[nodiscard]] auto section_header(elf32_word i) const noexcept -> elf32_shdr *
    {
        KASSERT(i < this->e_shnum);
        return unsafe_toptr<elf32_shdr *>(addressof(this) + e_shoff + i * e_shentsize);
    }
    [[nodiscard]] auto section_header(elf32_word sh_type,
                                      elf32_half start) const noexcept -> elf32_shdr *
    {
        KASSERT(start < this->e_shnum);
        for (elf32_half i = start; i < this->e_shnum; ++i) {
            elf32_shdr *shdr = section_header(i);
            if (shdr->sh_type == sh_type)
                return shdr;
        }

        return nullptr;
    }
    [[nodiscard]] auto lookup_section_string(elf32_word sh_name) const noexcept -> const char *
    {
        const auto *shdr = this->section_header(e_shstrndx);
        return unsafe_toptr<const char *>(addressof(this) + shdr->sh_offset + sh_name);
    }
    [[nodiscard]] inline auto program_header(elf32_half i) const noexcept -> const elf32_phdr *
    {
        KASSERT(i < this->e_phnum);
        return unsafe_toptr<const elf32_phdr *>(addressof(this) + this->e_phoff + i * e_phentsize);
    }
} __attribute__((packed));

struct elf32_sym {
    // this member holds an index into the object file's
    // symbol string table
    elf32_word st_name;

    // gives the value of the associated symbol
    elf32_addr st_value;

    // size of the symbol
    elf32_word st_size;

    // specifies symbol's type and binding attributes
    unsigned char st_info;

    // holds 0 and has no defined meaning
    unsigned char st_other;

    // holds the relevant section header table index
    elf32_half st_shndx;

    [[nodiscard]] auto name(const elf32_ehdr &ehdr,
                            const elf32_shdr &symtab) const noexcept -> const char *
    {
        const elf32_shdr *strtab = ehdr.section_header(symtab.sh_link);

        if (strtab == nullptr)
            return nullptr;

        return unsafe_toptr<const char *>(addressof(&ehdr) + strtab->sh_offset + st_name);
    }
} __attribute__((packed));

struct genprocess;
template <typename T> class result;
auto elf_load_kmodule(const char *filename, void *data) noexcept -> result<gen_kernel_process>;
auto elf_load(const char *filename, const void *data) noexcept -> result<vm_map>;

#endif // VINTIX_ELF_H
