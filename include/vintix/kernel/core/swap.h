/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_SWAP_H
#define VINTIX_SWAP_H

#include <vintix/kernel/filesystems/file.h>
#include <vintix/kernel/lib/bitmap.h>

class swapfile {
  private:
    file *fp {};
    usize used_pgs {};
    vintix::heap_bitmap<u32> pg_bitmap {};
};

#endif // VINTIX_SWAP_H
