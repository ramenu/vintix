/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_ARGS_H
#define VINTIX_ARGS_H

#include <grub/multiboot.h>
#include <vintix/kernel/lib/optional.h>
#include <vintix/kernel/lib/vector.h>

using errno = i32;

class kcmdline {
  private:
    static inline char *c_kernel_path, *c_root_fs, *c_root, *c_swapfile;
    static inline int c_root_flags;
    NODESTROY static inline vintix::vector<vintix::string> c_modules;

  public:
    static auto init_cmdline(multiboot_uint32_t) noexcept -> errno;
    static auto path_from(multiboot_uint32_t) noexcept -> char *;
    static auto modules() noexcept -> const vintix::vector<vintix::string> & { return c_modules; }
    static auto kernel_path() noexcept -> const char * { return c_kernel_path; }
    static auto root_dev() noexcept -> const char * { return c_root; }
    static auto root_dev_fs() noexcept -> const char * { return c_root_fs; }
    static auto root_dev_flags() noexcept -> int { return c_root_flags; }
    static auto swapfile() noexcept -> const char * { return c_swapfile; }
};

#endif // VINTIX_ARGS_H
