/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_SYSTEM_H
#define VINTIX_SYSTEM_H

#include <vintix/kernel/lib/types.h>
#include <vintix/kernel/lib/units.h>

static constexpr usize MINIMUM_MEMORY_REQUIRED = 1_mib;
#ifdef KERNEL_STACK_SIZE
extern "C" const u8 kernel_stack[KERNEL_STACK_SIZE];
#else
#error "KERNEL_STACK_SIZE not defined"
#endif

inline constexpr auto has_enough_memory(usize memory_in_bytes) noexcept -> bool
{
    return memory_in_bytes >= MINIMUM_MEMORY_REQUIRED;
}

#endif // VINTIX_SYSTEM_H
