/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_TSS_H
#define VINTIX_TSS_H

#include <vintix/kernel/lib/types.h>

struct tss_entry {
    // the previous TSS - with hardware task switching these form
    // a backward linked list
    u32 prev_tss;

    // the stack pointer to load when changing to kernel mode
    u32 esp0;

    // the stack segment to load when changing to kernel mode
    u32 ss0;

    // EVERYTHING BELOW HERE IS UNUSED
    // -------------------------------

    // esp and ss 1 and 2 would be used when switching to
    // rings 1 or 2
    u32 esp1;
    u32 ss1;
    u32 esp2;
    u32 ss2;
    u32 cr3;
    u32 eip;
    u32 eflags;
    u32 eax;
    u32 ecx;
    u32 edx;
    u32 ebx;
    u32 esp;
    u32 ebp;
    u32 esi;
    u32 edi;
    u32 es;
    u32 cs;
    u32 ss;
    u32 ds;
    u32 fs;
    u32 gs;
    u32 ldt;
    u16 trap;
    u16 iomap_base;
    // -------------------------------
} __attribute__((packed));

static_assert(sizeof(tss_entry) == 104);

#endif // VINTIX_TSS_H
