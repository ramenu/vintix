/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_PROCESS_H
#define VINTIX_PROCESS_H

#include <vintix/kernel/core/macros.h>
#include <vintix/kernel/core/uvm.h>
#include <vintix/kernel/lib/cxxstring.h>
#include <vintix/kernel/lib/errno.h>
#include <vintix/kernel/lib/errptr.h>
#include <vintix/kernel/lib/shared_ptr.h>
#include <vintix/kernel/lib/vector.h>
#include <vintix/kernel/posix/unistd.h>

enum class process_state {
    RUNNING, // currently using the CPU
    BLOCKED, // unable to run unless some external event happens
    READY,   // runnable, temporaily stopped to let another process run
};

struct gen_kernel_process {
    errno (*_drv_entry)() {};
    void (*destructor)() {};
    void *bss {};
    void *data {};
};

extern "C" {
    void _switch_to_usermode(u32 esp, u32 eip) noexcept;
}

struct dentry;
class syscall_regs;
class file;
struct vm_map_entry;

class process {
    pid_t p_pid {};
    uid_t p_uid {};
    gid_t p_gid {};
#ifdef __i386__
    u32 p_eip {}, p_edi {}, p_esi {}, p_ebp {}, p_esp {}, p_ebx {}, p_edx {}, p_ecx {}, p_eax {};
    pg_directory_t *p_pgdir {};
    pg_table_t **p_pgtab {};
#endif
    i8 p_priority {};
    dentry *p_root {};
    dentry *p_workdir {};
    process_state p_state {};
    mode_t p_umask {};
    int p_exit_status {};
    int p_signal_status {};
    vintix::string p_name {};
    vintix::vector<vintix::shared_ptr<file>> p_files {};
    vintix::vector<vintix::string> p_argv {};
    vintix::vector<vintix::string> p_envp {};
    clock_t p_start_time {};
    vm_map_entry p_stack {};
    vm_map p_map {};
    const process *p_parent {};
    NODESTROY static inline vintix::vector<process *> procs {};
    static inline process *p_active_process {};

    // returns -1 if the PID was not found
    static auto get_pid_index(pid_t pid) noexcept -> i32;

    void save_state(const syscall_regs &regs) noexcept;
    NODISCARD auto init_process_stack() noexcept -> errno;
    void run() const noexcept { return _switch_to_usermode(p_esp, p_eip); }

  public:
    static void init() noexcept;
    void set_exit_status(int status) noexcept { p_exit_status = status; }
    NODISCARD auto expand_stack(v_addr page_fault_address) noexcept -> errno;
    NODISCARD auto exit_status() const noexcept -> const int & { return p_exit_status; }
    NODISCARD auto signal_status() const noexcept -> const int & { return p_signal_status; }
    NODISCARD auto priority() const noexcept -> const i8 & { return p_priority; }
    NODISCARD auto state() const noexcept -> const process_state & { return p_state; }
    NODISCARD auto name() const noexcept -> const vintix::string & { return p_name; }
    NODISCARD auto start_time() const noexcept -> const clock_t & { return p_start_time; }
    NODISCARD auto lookup_mapping(vm_off_t off) const noexcept -> const vm_map_entry *
    {
        return p_map.lookup(off);
    }
    NODISCARD static auto processes() noexcept -> const vintix::vector<process *> &
    {
        return procs;
    }
    NODISCARD auto memory_usage() const noexcept -> usize;
    NODISCARD static auto number_of_processes() noexcept -> usize { return procs.size(); }
    NODISCARD static auto exists(pid_t pid) noexcept -> bool { return get_pid_index(pid) != -1; }
    static void terminate(pid_t pid, int sig) noexcept;
    NODISCARD static auto files() noexcept -> vintix::vector<vintix::shared_ptr<file>> &
    {
        return p_active_process->p_files;
    }
    NODISCARD auto pid() const noexcept -> const pid_t & { return p_pid; }
    NODISCARD auto uid() const noexcept -> const uid_t & { return p_uid; }
    NODISCARD auto gid() const noexcept -> const gid_t & { return p_gid; }
    NODISCARD auto working_directory() const noexcept -> const dentry * { return p_workdir; }
    NODISCARD auto root() const noexcept -> const dentry * { return p_root; }
    NODISCARD auto stdin() const noexcept -> const file * { return p_files.at(STDIN_FILENO).get(); }
    NODISCARD auto stdout() const noexcept -> const file *
    {
        return p_files.at(STDOUT_FILENO).get();
    }
    NODISCARD auto stderr() const noexcept -> const file *
    {
        return p_files.at(STDERR_FILENO).get();
    }
    NODISCARD static auto get_file(fd_t fd) noexcept -> errptr<file>
    {
        KASSERT(p_active_process->p_files.size() >= 3);
        if (fd < 0 || static_cast<usize>(fd) >= p_active_process->p_files.size())
            return EBADFD;
        return p_active_process->p_files.at(static_cast<usize>(fd)).unsafe_mutable_data();
    }
    NODISCARD static auto running() noexcept -> process * { return p_active_process; }
    void chroot(dentry *new_root) noexcept { p_root = new_root; }
    void chdir(dentry *new_dir) noexcept { p_workdir = new_dir; }
    auto umask(mode_t mask) noexcept { p_umask = mask; }
    auto load(const char *path) noexcept -> errno;
    static void schedule_new_process(const syscall_regs *) noexcept;
    static auto kill(pid_t pid, int signal) noexcept -> errno;
    static void dtor(void *) noexcept {}
};

class kprocess {
    errno (*_drv_entry)() {};
    void (*k_destructor)() {};
    void *k_data { nullptr };
    void *k_bss { nullptr };
    vintix::string k_name {};
    NODESTROY static inline vintix::vector<kprocess> modules {};

  public:
    constexpr kprocess() noexcept = default;
    static auto load_module(const char *path) noexcept -> errno;
    static auto load_module(const char *name, gen_kernel_process &&gen) noexcept -> errno;
    static void unload_module(const char *name) noexcept;
};

#endif // VINTIX_PROCESS_H
