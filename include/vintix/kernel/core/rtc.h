/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_RTC_H
#define VINTIX_RTC_H

#include <vintix/kernel/core/cmos.h>
#include <vintix/kernel/core/interrupt.h>
#include <vintix/kernel/lib/types.h>

enum class rtc_weekday {
    SUNDAY,
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
};

struct rtc_time {
    u8 second;
    u8 minute;
    u8 hour;
    u8 day;
    u8 month;
    u8 year;

    constexpr auto operator!=(const rtc_time &other) const noexcept -> bool
    {
        return (second != other.second) || (minute != other.minute) || (hour != other.hour) ||
               (day != other.day) || (month != other.month) || (year != other.year);
    }
};

extern "C" auto rtc_get_time() noexcept -> rtc_time;

// Range 0-59
inline auto rtc_get_seconds() noexcept -> u8
{
    // disable_interrupts();
    cmos_sel_register(cmos_register::RTC_SECONDS, true);
    u8 b = inb(CMOS_READ_PORT);
    // enable_interrupts();
    return b;
}

// Range 0-59
inline auto rtc_get_minutes() noexcept -> u8
{
    // disable_interrupts();
    cmos_sel_register(cmos_register::RTC_MINUTES, true);
    u8 b = inb(CMOS_READ_PORT);
    // enable_interrupts();
    return b;
}

// Range 0-23 in 24-hour mode, 1-12 in 12-hour mode, highest bit set if PM
inline auto rtc_get_hours() noexcept -> u8
{
    // disable_interrupts();
    cmos_sel_register(cmos_register::RTC_HOURS, true);
    u8 b = inb(CMOS_READ_PORT);
    // enable_interrupts();
    return b;
}

// Range 1-7, Sunday = 1
inline auto rtc_get_weekday() noexcept -> rtc_weekday
{
    // disable_interrupts();
    cmos_sel_register(cmos_register::RTC_WEEKDAY, true);
    auto weekday = static_cast<rtc_weekday>(inb(CMOS_READ_PORT));
    // enable_interrupts();
    return weekday;
}

// Range 1-31
inline auto rtc_get_day_of_month() noexcept -> u8
{
    // disable_interrupts();
    cmos_sel_register(cmos_register::RTC_DAY_OF_MONTH, true);
    u8 b = inb(CMOS_READ_PORT);
    // enable_interrupts();
    return b;
}

// Range 1-12
inline auto rtc_get_month() noexcept -> u8
{
    // disable_interrupts();
    cmos_sel_register(cmos_register::RTC_MONTH, true);
    u8 b = inb(CMOS_READ_PORT);
    // enable_interrupts();
    return b;
}

// Range 0-99
inline auto rtc_get_year() noexcept -> u8
{
    // disable_interrupts();
    cmos_sel_register(cmos_register::RTC_YEAR, true);
    u8 b = inb(CMOS_READ_PORT);
    // enable_interrupts();
    return b;
}

// Range 19-20?
inline auto rtc_get_century() noexcept -> u8
{
    // disable_interrupts();
    cmos_sel_register(cmos_register::RTC_CENTURY, true);
    u8 b = inb(CMOS_READ_PORT);
    // enable_interrupts();
    return b;
}

inline auto rtc_get_statusB() noexcept -> u8
{
    cmos_sel_register(cmos_register::STATUS_B, true);
    u8 b = inb(CMOS_READ_PORT);
    return b;
}

inline auto rtc_year_to_4digityear(u8 year) noexcept -> u32
{
    if (year > 70)
        return year + 1900;
    return year + 2000;
}

#endif // VINTIX_RTC_H
