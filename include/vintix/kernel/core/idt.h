/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_IDT_H
#define VINTIX_IDT_H

#include <vintix/kernel/core/macros.h>
#include <vintix/kernel/lib/types.h>

struct interrupt_state {
    u32 edi;
    u32 esi;
    u32 ebp;
    u32 RESERVED;
    u32 ebx;
    u32 edx;
    u32 ecx;
    u32 eax;
    u32 no;
    u32 error_code;
    u32 eip;
    u32 cs;
    u32 eflags;
    u32 esp;
    u32 ss;
};

void setup_idt() noexcept;
extern "C" {
    void install_irq_handler(u8 irq, void (*handler)(const interrupt_state &)) noexcept;
    void uninstall_irq_handler(u8 irq) noexcept;
}

#endif // VINTIX_IDT_H
