/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_INTERRUPT_H
#define VINTIX_INTERRUPT_H

#include <vintix/kernel/lib/types.h>

extern "C" {
    // clears the interrupt flag (IF)
    inline void disable_interrupts() noexcept { asm volatile("cli"); }

    // sets the interrupt flag (IF)
    inline void enable_interrupts() noexcept { asm volatile("sti"); }

    inline void trigger_interrupt(u8 interrupt) noexcept
    {
        asm volatile("int %0" ::"N"(interrupt));
    }
}
#endif // VINTIX_INTERRUPT_H
