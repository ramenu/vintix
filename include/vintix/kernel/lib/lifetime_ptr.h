/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _VINTIX_SOURCE_FILE_
#error This file can only be included in CXX source files
#endif // _VINTIX_SOURCE_FILE_

#ifndef VINTIX_LIFETIME_PTR
#define VINTIX_LIFETIME_PTR

#include <vintix/kernel/core/global.h>
#include <vintix/kernel/core/macros.h>
#include <vintix/kernel/core/memory.h>
#include <vintix/kernel/lib/types.h>

namespace vintix {
    template <typename T> class lifetime_ptr {
      private:
        T *_p { nullptr };

      public:
        explicit lifetime_ptr(usize n) noexcept : _p { static_cast<T *>(kzalloc(n)) } {}
        constexpr explicit lifetime_ptr(lifetime_ptr<T> &&other) noexcept : _p { other._p }
        {
            other._p = nullptr;
        }
        constexpr explicit operator bool() const noexcept { return (_p != nullptr); }
        constexpr auto operator==(const void *p) const noexcept -> bool { return (_p == p); }
        constexpr auto operator!=(const void *p) const noexcept -> bool { return (_p != p); }
        constexpr auto operator!() const noexcept -> bool { return (!_p); }
        constexpr auto operator>(const void *p) const noexcept -> bool { return (_p > p); }
        constexpr auto operator<(const void *p) const noexcept -> bool { return (_p < p); }
        constexpr auto operator>=(const void *p) const noexcept -> bool { return (_p >= p); }
        constexpr auto operator<=(const void *p) const noexcept -> bool { return (_p <= p); }
        constexpr auto operator=(lifetime_ptr<T> &&other) noexcept -> lifetime_ptr<T> &
        {
            if (this == other)
                return *this;

            if (_p != nullptr)
                kfree(_p);

            _p = other._p;
            other._p = nullptr;
            return *this;
        }
        VINTIX_CONSTEXPR auto operator+(usize n) const noexcept -> const T *
        {
            KASSERT(_p != nullptr);
            return _p + n;
        }
        VINTIX_CONSTEXPR auto operator->() const noexcept -> const T *
        {
            KASSERT(_p != nullptr);
            return _p;
        }
        VINTIX_CONSTEXPR auto operator*() const noexcept -> const T &
        {
            KASSERT(_p != nullptr);
            return *_p;
        }
        VINTIX_CONSTEXPR auto operator[](usize i) const noexcept -> const T &
        {
            KASSERT(_p != nullptr);
            return _p[i];
        }
        constexpr auto unsafe_mutable_data() noexcept -> T * { return _p; }
        constexpr auto get() const noexcept -> const T * { return _p; }
        ~lifetime_ptr() noexcept
        {
            if (_p != nullptr)
                kfree(_p);
        }
        DELETE_COPYABLE(lifetime_ptr)
    };
} // namespace vintix

#endif // VINTIX_LIFETIME_PTR
