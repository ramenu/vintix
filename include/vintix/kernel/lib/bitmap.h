/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_BITMAP_H
#define VINTIX_BITMAP_H

#include <vintix/kernel/core/global.h>
#include <vintix/kernel/core/kassert.h>
#include <vintix/kernel/core/macros.h>
#include <vintix/kernel/lib/bits.h>
#include <vintix/kernel/lib/concepts.h>
#include <vintix/kernel/lib/storage.h>

#define __DEFINE_BITMAP_METHODS                                                                    \
    [[nodiscard]] VINTIX_CONSTEXPR auto has_free_bit(usize i) const noexcept -> bool               \
    {                                                                                              \
        return bitmap::has_free_bit(*this, i);                                                     \
    }                                                                                              \
    [[nodiscard]] constexpr auto is_full() const noexcept -> bool                                  \
    {                                                                                              \
        return bitmap::is_full(*this);                                                             \
    }                                                                                              \
    void set(usize i, u8 bit) noexcept { bitmap::set(*this, i, bit); }                             \
    void unset(usize i, u8 bit) noexcept { bitmap::unset(*this, i, bit); }                         \
    VINTIX_CONSTEXPR auto is_set(usize i, u8 bit) const noexcept -> bool                           \
    {                                                                                              \
        return bitmap::is_set(*this, i, bit);                                                      \
    }                                                                                              \
    VINTIX_CONSTEXPR auto first_free_bit(usize i) const noexcept -> u8                             \
    {                                                                                              \
        return bitmap::first_free_bit(*this, i);                                                   \
    }                                                                                              \
    VINTIX_CONSTEXPR auto first_free_bit(usize i, u8 offset) const noexcept -> u8                  \
    {                                                                                              \
        return bitmap::first_free_bit(*this, i, offset);                                           \
    }                                                                                              \
    constexpr auto allocate_bit_at(usize i) noexcept -> u8                                         \
    {                                                                                              \
        return bitmap::allocate_bit_at(*this, i);                                                  \
    }                                                                                              \
    constexpr auto allocate_single_bit() noexcept -> usize                                         \
    {                                                                                              \
        return bitmap::allocate_single_bit(*this);                                                 \
    }                                                                                              \
    VINTIX_CONSTEXPR auto allocate_consecutive_bits(usize nbits) noexcept -> usize                 \
    {                                                                                              \
        return bitmap::allocate_consecutive_bits(*this, nbits);                                    \
    }                                                                                              \
    VINTIX_CONSTEXPR void free(usize off) noexcept { return bitmap::free(*this, off); }

namespace vintix {

    template <Integral T, usize N, bool Invert = false> struct fixed_bitmap;
    template <Integral T, bool Invert = false> class heap_bitmap;

    template <typename T>
    concept Bitmap = requires { typename heap_bitmap<typename T::value_type>; } ||
                     requires { typename fixed_bitmap<typename T::value_type, 0>; };

    namespace bitmap {

        template <Bitmap T>
        VINTIX_CONSTEXPR auto has_free_bit(const T &bitmap, usize i) noexcept -> bool
        {
            using U = typename T::value_type;
            KASSERT(i < bitmap.size());
            if constexpr (T::is_inverted)
                return (!bitmap[i]);
            else
                return (bitmap[i] != static_cast<U>(0) - 1);
        }
        template <Bitmap T> constexpr auto is_full(const T &bitmap) noexcept -> bool
        {
            for (usize i = 0; i < bitmap.size(); ++i)
                if (has_free_bit(bitmap, i))
                    return false;
            return true;
        }
        template <Bitmap T> VINTIX_CONSTEXPR void set(T &bitmap, usize i, u8 bit) noexcept
        {
            using U = typename T::value_type;
            KASSERT(i < bitmap.size());
            KASSERT(bit <= bits_in<U>());
            if constexpr (T::is_inverted)
                bitmap[i] &= ~(1 << bit);
            else
                bitmap[i] |= (1 << bit);
        }
        template <Bitmap T> VINTIX_CONSTEXPR void unset(T &bitmap, usize i, u8 bit) noexcept
        {
            using U = typename T::value_type;
            KASSERT(i < bitmap.size());
            KASSERT(bit <= bits_in<U>());
            if constexpr (T::is_inverted)
                bitmap[i] |= (1 << bit);
            else
                bitmap[i] &= ~(1 << bit);
        }
        template <Bitmap T>
        VINTIX_CONSTEXPR auto is_set(const T &bitmap, usize i, u8 bit) noexcept -> bool
        {
            using U = typename T::value_type;
            KASSERT(i < bitmap.size());
            KASSERT(bit <= bits_in<U>());
            if constexpr (T::is_inverted)
                return (!(bitmap[i] & (1 << bit)));
            else
                return (bitmap[i] & (1 << bit));
        }
        template <Bitmap T> VINTIX_CONSTEXPR void free(T &bitmap, usize off) noexcept
        {
            using U = typename T::value_type;
            const usize i = off / bits_in<U>();
            const u8 j = static_cast<u8>(off % bits_in<U>());

            KASSERT(i < bitmap.size());
            KASSERT(bitmap::is_set(bitmap, i, j));
            bitmap::unset(bitmap, i, j);
        }
        template <Integral T, bool Invert = false>
        constexpr auto first_free_bit(T bitmap) noexcept -> u8
        {
            if constexpr (Invert)
                return static_cast<u8>(__builtin_ctz(bitmap));
            else
                return static_cast<u8>(__builtin_ctz(~bitmap));
        }
        template <Bitmap T>
        VINTIX_CONSTEXPR auto first_free_bit(const T &bitmap, usize i) noexcept -> u8
        {
            KASSERT(i < bitmap.size());
            return first_free_bit<typename T::value_type, T::is_inverted>(bitmap[i]);
        }
        template <Bitmap T>
        constexpr auto first_free_bit(const T &bitmap, usize i, u8 offset) noexcept -> u8
        {
            KASSERT(i < bitmap.size());
            typename T::value_type mask;
            if constexpr (T::is_inverted)
                mask = bitmap[i] & ((1 << offset) - 1);
            else
                mask = bitmap[i] & ~((1 << offset) - 1);

            return first_free_bit<decltype(mask), T::is_inverted>(mask);
        }
        template <Bitmap T> VINTIX_CONSTEXPR auto allocate_bit_at(T &bitmap, usize i) noexcept -> u8
        {
            KASSERT(i < bitmap.size());
            const u8 free = bitmap::first_free_bit(bitmap, i);
            bitmap::set(bitmap, i, free);
            return free;
        }
        template <Bitmap T> constexpr auto allocate_single_bit(T &bitmap) noexcept -> usize
        {
            using U = typename T::value_type;
            for (usize i = 0; i < bitmap.size(); ++i) {
                if (!bitmap::has_free_bit(bitmap, i))
                    continue;

                return i * bits_in<U>() + bitmap::allocate_bit_at(bitmap, i);
            }

            return 0;
        }
        template <Bitmap T>
        VINTIX_CONSTEXPR auto allocate_consecutive_bits(T &bitmap, usize nbits) noexcept -> usize
        {
            using U = typename T::value_type;
            KASSERT(nbits != 0);
            usize found = 0, count = 0;
            u8 found_bit = 0, free;

            for (usize i = 0; i < bitmap.size(); ++i) {
                if (count == 0) {
                    if (!bitmap::has_free_bit(bitmap, i))
                        continue;
                    found = i;
                    free = bitmap::first_free_bit(bitmap, i);
                    found_bit = free;
                } else {
                    free = 0;
                }

                u8 bit = free;
                for (; bit < bits_in<U>(); ++bit) {
                    if (bitmap::is_set(bitmap, i, free)) {
                        count = 0;
                        free = bitmap::first_free_bit(bitmap, i, bit);
                        bit = free;
                        found_bit = free;

                        if (free == 0)
                            break;
                    }
                    if (++count == nbits) {
                        usize counted_bits = 0;
                        for (usize j = found; j <= i; ++j) {
                            for (u8 k = (j == found) ? found_bit : 0;
                                 k < bits_in<U>() && counted_bits < nbits; ++k, ++counted_bits)
                                bitmap::set(bitmap, j, k);
                        }
                        return found * bits_in<U>() + found_bit;
                    }
                }
            }
            return 0;
        }

    } // namespace bitmap

    template <Integral T, usize N, bool Invert> struct fixed_bitmap : public array<T, N> {
        static constexpr bool is_inverted = Invert;
        __DEFINE_BITMAP_METHODS
    };

    template <Integral T, bool Invert> class heap_bitmap : public generic_heap_storage<T> {
      public:
        static constexpr bool is_inverted = Invert;
        __DEFINE_BITMAP_METHODS
    };

} // namespace vintix

#endif // VINTIX_BITMAP_H
