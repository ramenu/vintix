/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_STORAGE_H
#define VINTIX_STORAGE_H

#include <vintix/kernel/core/global.h>
#include <vintix/kernel/core/kassert.h>
#include <vintix/kernel/core/vmm.h>
#include <vintix/kernel/lib/unique_ptr.h>

template <typename T> class generic_heap_storage {
  private:
    vintix::unique_ptr<T> _p;
    usize _sz;

  public:
    using value_type = T;
    constexpr generic_heap_storage() noexcept : _p { nullptr }, _sz { 0 } {}
    explicit constexpr generic_heap_storage(nullptr_t) noexcept : _p { nullptr }, _sz { 0 } {}
    explicit constexpr generic_heap_storage(usize nbytes) noexcept : _p { kzalloc(nbytes) }
    {
        if (_p != nullptr)
            _sz = nbytes / sizeof(T);
        else
            _sz = 0;
    }
    constexpr generic_heap_storage(T *p, usize sz) noexcept : _p { p }, _sz { sz } {}
    explicit constexpr operator bool() const noexcept { return _p; }
    constexpr auto operator!() const noexcept -> bool { return !_p; }
    constexpr auto operator==(nullptr_t) const noexcept -> bool { return (_p == nullptr); }
    constexpr auto operator!=(nullptr_t) const noexcept -> bool { return (_p != nullptr); }
    constexpr auto operator[](usize i) const noexcept -> const T & { return _p[i]; }
    constexpr auto operator[](usize i) noexcept -> T & { return _p[i]; }
    constexpr void unsafe_set_pointer(void *p) noexcept { _p.unsafe_set_pointer(p); }
    constexpr void unsafe_set_size(usize sz) noexcept { _sz = sz; }
    constexpr auto data() const noexcept -> const T * { return _p.get(); }
    constexpr auto unsafe_mutable_data() noexcept -> T * { return _p.unsafe_mutable_data(); }
    VINTIX_CONSTEXPR auto at(usize i) const noexcept -> const T &
    {
        KASSERT(i < _sz);
        return _p[i];
    }
    VINTIX_CONSTEXPR auto front() const noexcept -> const T &
    {
        KASSERT(_sz > 0);
        return _p[0];
    }
    VINTIX_CONSTEXPR auto back() const noexcept -> const T &
    {
        KASSERT(_sz > 0);
        return _p[_sz - 1];
    }
    [[nodiscard]] constexpr auto size() const noexcept -> const usize & { return _sz; }
};

#endif // VINTIX_STORAGE_H
