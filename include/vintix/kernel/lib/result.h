/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_RESULT_H
#define VINTIX_RESULT_H

#include <vintix/kernel/core/global.h>
#include <vintix/kernel/core/kassert.h>
#include <vintix/kernel/core/macros.h>
#include <vintix/kernel/lib/concepts.h>
#include <vintix/kernel/lib/errno.h>
#include <vintix/kernel/lib/type_traits.h>

#define UNWRAP_RESULT(F)                                                                           \
    ({                                                                                             \
        auto res = (F);                                                                            \
        if (res.is_err())                                                                          \
            return res.err();                                                                      \
        vintix::move(res.get());                                                                   \
    })
#define ERR(E)                                                                                     \
    result { E }

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Weffc++"

template <typename T> class result {
  private:
    errno r_err;
    T r_result;

  public:
    result() noexcept = delete;
    ~result() noexcept = default;
    constexpr result(T &&r) noexcept
        : r_err { ENOERR }, r_result { vintix::move(r) }
    {} // cppcheck-suppress[noExplicitConstructor,uninitMemberVar]
    constexpr result(errno err) noexcept
        : r_err { err } {} // cppcheck-suppress[noExplicitConstructor,uninitMemberVar]
    [[nodiscard]] constexpr auto is_ok() const noexcept -> bool { return (r_err == ENOERR); }
    [[nodiscard]] constexpr auto is_err() const noexcept -> bool { return (r_err != ENOERR); }
    [[nodiscard]] VINTIX_CONSTEXPR auto get() const noexcept -> const T &
    {
        KASSERT(this->is_ok());
        return r_result;
    }
    [[nodiscard]] VINTIX_CONSTEXPR auto unwrap() noexcept -> T &&
    {
        KASSERT(this->is_ok());
        return vintix::move(r_result);
    }
    [[nodiscard]] VINTIX_CONSTEXPR auto unwrap_or(T &&o) noexcept -> T &&
    {
        return (this->is_ok()) ? vintix::move(r_result) : vintix::move(o);
    }
    [[nodiscard]] VINTIX_CONSTEXPR auto get() noexcept -> T &
    {
        KASSERT(this->is_ok());
        return r_result;
    }
    [[nodiscard]] constexpr auto err() const noexcept -> errno { return r_err; }
    DELETE_COPYABLE_AND_MOVABLE(result)
};

#pragma GCC diagnostic pop

#endif // VINTIX_RESULT_H
