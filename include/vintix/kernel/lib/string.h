/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_STRING_H
#define VINTIX_STRING_H

#include <vintix/kernel/core/macros.h>
#include <vintix/kernel/lib/types.h>

inline auto memsetb(void *RESTRICT dst, u8 val, u32 sz) noexcept -> void *
{
    asm volatile("rep     stosb" : "+D"(dst), "+c"(sz) : "a"(val) : "memory");
    return dst;
}

inline auto memsetd(void *RESTRICT dst, u32 val, u32 sz) noexcept -> void *
{
    asm volatile("rep     stosd" : "+D"(dst), "+c"(sz) : "a"(val) : "memory");
    return dst;
}

inline auto memcpyb(void *__restrict__ dst, const void *__restrict__ src,
                    usize count) noexcept -> void *
{
    asm volatile("rep     movsb" : "+D"(dst), "+S"(src), "+c"(count) : : "memory");
    return dst;
}

inline auto memcpyd(void *__restrict__ dst, const void *__restrict__ src,
                    usize count) noexcept -> void *
{
    asm volatile("rep     movsd" : "+D"(dst), "+S"(src), "+c"(count) : : "memory");
    return dst;
}

constexpr void right_justify(char *s) noexcept
{
    for (usize i = 0; s[i] != '\0'; i += 2) {
        char tmp = s[i];
        s[i] = s[i + 1];
        s[i + 1] = tmp;
    }
}

extern "C" {

    constexpr auto atoi(const char *s) noexcept -> int
    {
        int k = 0;
        while (*s) {
            k = k * 10 + *s - '0';
            ++s;
        }
        return k;
    }

    constexpr auto isspace(char c) noexcept -> bool { return c == ' '; }

    constexpr void rstrip(char *s) noexcept
    {
        usize pos = 0;
        for (usize i = 0; s[i] != '\0'; ++i)
            if (!isspace(s[i]) && isspace(s[i + 1]))
                pos = i + 1;
        if (pos)
            s[pos] = '\0';
    }
    // returns the length of 's'. Assumes 's' is
    // null terminated (note this does not count
    // the null terminator)
    constexpr auto strlen(const char *s) noexcept -> usize
    {
        usize len = 0;
        while (s[len++] != '\0')
            ;
        return len - 1;
    }

    // returns true if the character is a upper case
    // ASCII letter
    constexpr auto is_upper(char c) noexcept -> bool { return (c >= 'A' && c <= 'Z'); }

    // returns true if the character is a lower case
    // ASCII letter
    constexpr auto is_lower(char c) noexcept -> bool { return (c >= 'a' && c <= 'z'); }

    // returns a character's upper case equivalant
    constexpr auto to_upper(char c) noexcept -> char
    {
        if (is_lower(c))
            return static_cast<char>(c - 32);
        return c;
    }

    // returns a character's lower case equivalant
    constexpr auto to_lower(char c) noexcept -> char
    {
        if (is_upper(c))
            return static_cast<char>(c + 32);
        return c;
    }

    inline auto strcpy(char *RESTRICT dst, const char *RESTRICT src) noexcept -> void *
    {
        return memcpyb(dst, src, strlen(src) + 1);
    }

    constexpr auto strcmp(const char *s1, const char *s2) noexcept -> int
    {
        while (*s1 && (*s1 == *s2)) {
            s1++;
            s2++;
        }
        return *s1 - *s2;
    }
    constexpr auto strncmp(const char *s1, const char *s2, usize n) noexcept -> int
    {
        usize i = 0;
        while ((++i != n) && (*s1 && (*s1 == *s2))) {
            s1++;
            s2++;
        }
        return *s1 - *s2;
    }
    auto memcpy(void *__restrict__ dest, const void *__restrict__ src,
                usize len) noexcept -> void *;
    auto memmove(void *dest, const void *src, usize len) noexcept -> void *;
    auto memset(void *dest, int val, usize sz) noexcept -> void *;
    auto itoa(usize val, unsigned base = 10) noexcept -> char *;
}

#endif // VINTIX_STRING_H
