/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_CONCEPTS_H
#define VINTIX_CONCEPTS_H

#include <vintix/kernel/lib/move.h>
#include <vintix/kernel/lib/type_traits.h>

class cstr;
namespace vintix {
    class string;
    class string_view;
    class owned_cstr;

    template <typename T>
    concept Integral = is_integral_v<T>;

    template <typename T>
    concept Array = is_array_v<T>;

    template <typename T>
    concept Pointer = is_pointer_v<T>;

    template <typename T>
    concept IntegralOrPointer = is_integral_v<T> || is_pointer_v<T>;

    template <typename T>
    concept Destructible = Integral<T> || requires(T x) { x.~T(); };

    template <typename T>
    concept NotDestructible = Integral<T> || !Destructible<T>;

    template <typename T>
    concept String = is_same_v<T, string> || is_same_v<T, cstr> || is_same_v<T, string_view> ||
                     is_same_v<T, owned_cstr>;

    template <typename T>
    concept Container = requires(T t) {
        t.unsafe_mutable_data();
        t.data();
        t.size();
    };

    template <typename T>
    concept Signed = is_signed_v<T>;

    template <typename T>
    concept Unsigned = is_unsigned_v<T>;

    template <typename T>
    concept EqualityComparable = requires(T a, T b) {
        a == b;
        a != b;
    };

    template <typename T>
    concept Comparable = requires(T a, T b) { a <=> b; };

    template <typename T, typename V>
    concept AreEqualityComparable = requires(T a, V b) {
        a == b;
        a != b;
    };

    template <typename T>
    concept Copyable = is_integral_v<T> || requires(T x, T y) {
        T(y);
        x = T(y);
    };

    template <typename T>
    concept Movable = is_integral_v<T> || requires(T x) {
        T(move(x));
        x = T(move(x));
    };

    template <typename T>
    concept TriviallyCopyable = Copyable<T> && NotDestructible<T>;

    template <typename T>
    concept RawCStr = is_raw_cstr_v<T>;

} // namespace vintix

#endif // VINTIX_CONCEPTS_H
