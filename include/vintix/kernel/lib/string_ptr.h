/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_STRING_PTR_H
#define VINTIX_STRING_PTR_H

#include <vintix/kernel/core/global.h>
#include <vintix/kernel/core/kassert.h>
#include <vintix/kernel/core/vmm.h>
#include <vintix/kernel/lib/string.h>
#include <vintix/kernel/lib/string_functions.h>

class cstr {
  private:
    const char *s_str;

  public:
    static constexpr bool HasSize = false;
    constexpr cstr() noexcept : s_str { nullptr } {}
    constexpr cstr(const char *s) noexcept
        : s_str { s } {} // cppcheck-suppress noExplicitConstructor
    [[nodiscard]] constexpr auto size() const noexcept -> usize { return strlen(s_str); }
    [[nodiscard]] constexpr auto length() const noexcept -> usize { return this->size(); }
    [[nodiscard]] constexpr auto c_str() const noexcept -> const char * { return s_str; }
    [[nodiscard]] VINTIX_CONSTEXPR auto front() const noexcept -> const char &
    {
        KASSERT(s_str != nullptr);
        return s_str[0];
    }
    [[nodiscard]] constexpr auto operator[](usize i) const noexcept -> const char &
    {
        return s_str[i];
    }
    [[nodiscard]] constexpr auto operator==(nullptr_t) const noexcept -> bool
    {
        return (s_str == nullptr);
    }
    [[nodiscard]] constexpr auto operator!=(nullptr_t) const noexcept -> bool
    {
        return (s_str != nullptr);
    }
    __DEFINE_STRING_READ_METHODS
};

#endif // VINTIX_STRING_PTR_H
