/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_ALIGN_H
#define VINTIX_ALIGN_H

#include <vintix/kernel/lib/types.h>
#include <vintix/kernel/lib/utilities.h>

constexpr auto align_up(usize n, usize align) noexcept -> usize
{
    return ((n + align - 1) & ~(align - 1));
}

template <usize N> constexpr auto align_up(usize n) noexcept -> usize
{
    return ((n + N - 1) & ~(N - 1));
}

template <usize N> constexpr auto align_down(usize n) noexcept -> usize { return (n & ~(N - 1)); }

constexpr auto align_down(usize n, usize align) noexcept -> usize { return (n & ~(align - 1)); }

template <usize N> constexpr auto align_down(const void *p) noexcept -> usize
{
    return (addressof(p) & ~(N - 1));
}

#endif // VINTIX_ALIGN_H
