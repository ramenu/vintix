/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_UTILITIES_H
#define VINTIX_UTILITIES_H

#include <vintix/kernel/lib/concepts.h>
#include <vintix/kernel/lib/types.h>

#define ADDRESSOF(P) reinterpret_cast<usize>(P)

// NOLINTBEGIN(cppcoreguidelines-pro-type-reinterpret-cast)
template <vintix::Pointer P> auto unsafe_toptr(usize n) noexcept -> P
{
    return reinterpret_cast<P>(n);
}

inline auto addressof(const void *p) noexcept -> usize { return reinterpret_cast<usize>(p); }

inline auto addressof(void (*f)()) noexcept -> usize { return reinterpret_cast<usize>(f); }

// NOLINTEND(cppcoreguidelines-pro-type-reinterpret-cast)

#endif // VINTIX_UTILITIES_H
