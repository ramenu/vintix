/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_UNIQUE_PTR_H
#define VINTIX_UNIQUE_PTR_H

#include <vintix/kernel/core/global.h>
#include <vintix/kernel/core/kassert.h>
#include <vintix/kernel/core/macros.h>
#include <vintix/kernel/core/vmm.h>
#include <vintix/kernel/lib/types.h>

namespace vintix {
    template <typename T, bool HasCustomDtor = false> class unique_ptr {
      private:
        T *_p;

      public:
        constexpr unique_ptr() noexcept : _p { nullptr } {}
        explicit constexpr unique_ptr(nullptr_t) noexcept : _p { nullptr } {}
        explicit constexpr unique_ptr(void *p) noexcept : _p { static_cast<T *>(p) } {}
        constexpr unique_ptr(unique_ptr<T, HasCustomDtor> &&other) noexcept : _p { other._p }
        {
            other._p = nullptr;
        }
        constexpr void unsafe_set_pointer(void *p) noexcept { _p = static_cast<T *>(p); }
        constexpr explicit operator bool() const noexcept { return (_p != nullptr); }
        constexpr auto operator==(nullptr_t) const noexcept -> bool { return _p == nullptr; }
        constexpr auto operator<=>(const void *p) const noexcept -> bool { return (_p <=> p); }
        constexpr auto operator!() const noexcept -> bool { return (!_p); }
        VINTIX_CONSTEXPR auto operator+(usize n) const noexcept -> const T *
        {
            KASSERT(_p != nullptr);
            return _p + n;
        }
        constexpr auto
        operator=(unique_ptr<T, HasCustomDtor> &&other) noexcept -> unique_ptr<T, HasCustomDtor> &
        {
            KASSERT(other._p != nullptr);
            if (this == &other)
                return *this;

            if (_p != nullptr)
                this->~unique_ptr();

            _p = other._p;
            other._p = nullptr;
            return *this;
        }
        VINTIX_CONSTEXPR auto operator->() const noexcept -> const T *
        {
            KASSERT(_p != nullptr);
            return _p;
        }
        VINTIX_CONSTEXPR auto operator->() noexcept -> T *
        {
            KASSERT(_p != nullptr);
            return _p;
        }
        VINTIX_CONSTEXPR auto operator*() noexcept -> T &
        {
            KASSERT(_p != nullptr);
            return *_p;
        }
        VINTIX_CONSTEXPR auto operator*() const noexcept -> const T &
        {
            KASSERT(_p != nullptr);
            return *_p;
        }
        VINTIX_CONSTEXPR auto operator[](usize i) const noexcept -> const T &
        {
            KASSERT(_p != nullptr);
            return _p[i];
        }
        VINTIX_CONSTEXPR auto operator[](usize i) noexcept -> T &
        {
            KASSERT(_p != nullptr);
            return _p[i];
        }
        constexpr auto get() const noexcept -> const T *
        {
            KASSERT(_p != nullptr);
            return _p;
        }
        constexpr auto unsafe_mutable_data() const noexcept -> T * { return _p; }
        ~unique_ptr() noexcept
        {
            if (_p != nullptr) {
                if constexpr (HasCustomDtor)
                    _p->~T();
                else
                    kfree(_p);
                _p = nullptr;
            }
        }
        DELETE_COPYABLE(unique_ptr)
    };
} // namespace vintix

#endif // VINTIX_UNIQUE_PTR_H
