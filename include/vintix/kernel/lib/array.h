/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_ARRAY_H
#define VINTIX_ARRAY_H

#include <vintix/kernel/core/global.h>
#include <vintix/kernel/core/kassert.h>
#include <vintix/kernel/lib/types.h>

namespace vintix {
    template <typename T, usize N> struct array {
        // NOLINTBEGIN(cppcoreguidelines-avoid-c-arrays)
        T _data[N];
        // NOLINTEND(cppcoreguidelines-avoid-c-arrays)
        using value_type = T;
        VINTIX_CONSTEXPR auto at(usize i) const noexcept -> const T &
        {
            KASSERT(i < N);
            return _data[i];
        }
        VINTIX_CONSTEXPR auto at(usize i) noexcept -> T &
        {
            KASSERT(i < N);
            return _data[i];
        }
        VINTIX_CONSTEXPR void erase(usize i) noexcept
        {
            for (usize j = i; i < this->size() - 1; ++i)
                _data[j] = move(_data[j + 1]);
        }
        constexpr auto operator[](usize i) const noexcept -> const T & { return _data[i]; }
        constexpr auto operator[](usize i) noexcept -> T & { return _data[i]; }
        [[nodiscard]] static constexpr auto size() noexcept -> usize { return N; }
        [[nodiscard]] static constexpr auto max_size() noexcept -> usize { return N; }
        [[nodiscard]] static constexpr auto empty() noexcept -> bool { return (N == 0); }
        [[nodiscard]] constexpr auto front() const noexcept -> const T &
        {
            static_assert(N > 0);
            return _data[0];
        }
        [[nodiscard]] constexpr auto back() const noexcept -> const T &
        {
            static_assert(N > 0);
            return _data[N - 1];
        }
        constexpr auto data() const noexcept -> const T * { return _data; }
        constexpr auto unsafe_mutable_data() noexcept -> T * { return _data; }
    };

    template <class First, class... Rest>
    array(First, Rest...) -> array<First, 1 + sizeof...(Rest)>;
} // namespace vintix

#endif // VINTIX_ARRAY_H
