/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_SPAN_H
#define VINTIX_SPAN_H

#include <vintix/kernel/core/global.h>
#include <vintix/kernel/core/kassert.h>
#include <vintix/kernel/lib/array.h>
#include <vintix/kernel/lib/concepts.h>
#include <vintix/kernel/lib/string.h>
#include <vintix/kernel/lib/type_traits.h>
#include <vintix/kernel/lib/types.h>

namespace vintix {
    template <typename T> class span {
      private:
        T *_p;
        [[no_unique_address]] usize _sz;

      public:
        explicit constexpr span(const char *s) noexcept : _p { s }, _sz { strlen(s) } {}
        constexpr span(T *p, usize count) noexcept : _p { p }, _sz { count } {}
        template <Container C>
        explicit constexpr span(const C &c) noexcept : _p { c.data() }, _sz { c.size() }
        {
        }
        template <String S>
            requires is_raw_cstr_v<T *>
        explicit constexpr span(const S &c) noexcept : _p { c.data() }, _sz { c.size() }
        {
        }
        template <String S>
            requires is_raw_cstr_v<T *>
        explicit constexpr span(S &c) noexcept : _p { c.unsafe_mutable_data() }, _sz { c.size() }
        {
        }
        template <Container C>
        explicit constexpr span(C &c) noexcept : _p { c.unsafe_mutable_data() }, _sz { c.size() }
        {
        }
        template <typename A, usize ArrayExtent>
        explicit constexpr span(const array<A, ArrayExtent> &arr) noexcept
            : _p { arr.data() }, _sz { arr.size() }
        {
        }
        template <typename A, usize ArrayExtent>
        explicit constexpr span(array<A, ArrayExtent> &arr) noexcept
            : _p { arr.unsafe_mutable_data() }, _sz { arr.size() }
        {
        }
        [[nodiscard]] constexpr auto size() const noexcept -> const usize & { return _sz; }
        [[nodiscard]] constexpr auto size_bytes() const noexcept -> usize
        {
            return (_sz * sizeof(T));
        }
        [[nodiscard]] constexpr auto empty() const noexcept -> bool { return (_sz == 0); }
        [[nodiscard]] constexpr auto data() const noexcept -> const T * { return _p; }
        [[nodiscard]] constexpr auto unsafe_mutable_data() const noexcept -> T * { return _p; }
        [[nodiscard]] VINTIX_CONSTEXPR auto front() const noexcept -> const T &
        {
            KASSERT(_sz > 0);
            return _p[0];
        }
        [[nodiscard]] VINTIX_CONSTEXPR auto back() const noexcept -> const T &
        {
            KASSERT(_sz > 0);
            return _p[_sz - 1];
        }
        [[nodiscard]] constexpr auto operator[](usize i) const noexcept -> T & { return _p[i]; }
        [[nodiscard]] constexpr auto operator==(nullptr_t) const noexcept -> bool
        {
            return (_p == nullptr);
        }
        [[nodiscard]] constexpr auto operator!=(nullptr_t) const noexcept -> bool
        {
            return (_p != nullptr);
        }
        [[nodiscard]] explicit constexpr operator bool() const noexcept { return (_p); }
        [[nodiscard]] VINTIX_CONSTEXPR auto at(usize i) const noexcept -> T &
        {
            KASSERT(i < _sz);
            return _p[i];
        }
    };
} // namespace vintix

#endif // VINTIX_SPAN_H
