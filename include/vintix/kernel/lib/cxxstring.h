/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_CXXSTRING_H
#define VINTIX_CXXSTRING_H

#include <vintix/kernel/core/global.h>
#include <vintix/kernel/core/kassert.h>
#include <vintix/kernel/core/vmm.h>
#include <vintix/kernel/lib/concepts.h>
#include <vintix/kernel/lib/errno.h>
#include <vintix/kernel/lib/ignore.h>
#include <vintix/kernel/lib/string.h>
#include <vintix/kernel/lib/string_functions.h>
#include <vintix/kernel/lib/string_ptr.h>
#include <vintix/kernel/lib/unique_ptr.h>

static constexpr u8 __CSTRING_SELF_HAS_SIZE = 1;
static constexpr u8 __CSTRING_OTHER_HAS_SIZE = 2;

struct _cstring {
    char *s_str;
    usize s_len;
};

extern "C" {
    void _cstring_from_cstr(_cstring *self, const char *s, bool has_size) noexcept;
    void _cstring_dtor(_cstring *self, bool has_size) noexcept;
    auto _cstring_move(_cstring *self, _cstring *other, bool has_size) noexcept -> _cstring *;
    auto _cstring_copy(_cstring *self, const _cstring *other, u8 has_size) noexcept -> errno;
    auto _cstring_prepend(_cstring *self, const _cstring *other, u8 has_size) noexcept -> errno;
    auto _cstring_prepend_char(_cstring *self, char c, bool has_size) noexcept -> errno;
    auto _cstring_append(_cstring *self, const _cstring *other, u8 has_size) noexcept -> errno;
    auto _cstring_append_char(_cstring *self, char c, bool has_size) noexcept -> errno;
}
namespace vintix {

    class owned_cstr {
      private:
        char *s_str { nullptr };

      public:
        static constexpr bool HasSize = false;
        constexpr owned_cstr() noexcept = default;
        constexpr owned_cstr(nullptr_t) noexcept {} // cppcheck-suppress noExplicitConstructor
        explicit owned_cstr(const char *s) noexcept
        {
            _cstring_from_cstr(reinterpret_cast<_cstring *>(this), s, HasSize);
        }
        explicit owned_cstr(const owned_cstr &s) noexcept // cppcheck-suppress noExplicitConstructor
            : s_str { static_cast<char *>(kzalloc(s.size() + 1)) }
        {
            if (s_str != nullptr)
                memcpy(s_str, s.c_str(), s.size() + 1);
        }
        constexpr owned_cstr(owned_cstr &&s) noexcept : s_str { s.s_str } { s.s_str = nullptr; }
        static void dtor(void *s) noexcept { _cstring_dtor(static_cast<_cstring *>(s), HasSize); }
        ~owned_cstr() noexcept { dtor(this); }
        static auto move(void *s, void *o) noexcept -> void *
        {
            return _cstring_move(static_cast<_cstring *>(s), static_cast<_cstring *>(o), HasSize);
        }
        auto operator=(owned_cstr &&other) noexcept -> owned_cstr &
        {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Weffc++"
            return *reinterpret_cast<owned_cstr *>(_cstring_move(
                reinterpret_cast<_cstring *>(this), reinterpret_cast<_cstring *>(&other), HasSize));
#pragma GCC diagnostic pop
        }
        template <RawCStr S> auto operator=(S s) noexcept -> owned_cstr &
        {
            vintix::ignore = this->copy(cstr { s });
            return *this;
        }
        template <String S> auto operator=(const S &s) noexcept -> owned_cstr &
        {
            vintix::ignore = this->copy(s);
            return *this;
        }
        auto operator=(const owned_cstr &s) noexcept -> owned_cstr &
        {
            if (this != &s)
                vintix::ignore = this->copy(s);
            return *this;
        }
        template <String S> explicit owned_cstr(const S &s) noexcept { this->copy(s); }
        [[nodiscard]] auto append(const char *s) noexcept -> errno { return this->append<cstr>(s); }
        [[nodiscard]] auto prepend(const char *s) noexcept -> errno
        {
            return this->prepend<cstr>(s);
        }
        [[nodiscard]] auto prepend(char c) noexcept -> errno
        {
            return _cstring_prepend_char(reinterpret_cast<_cstring *>(this), c, HasSize);
        }
        VINTIX_CONSTEXPR void erase_last() noexcept
        {
            const usize s_len = this->size();
            KASSERT(s_len != 0 && s_str != nullptr);
            s_str[s_len] = '\0';
        }
        [[nodiscard]] auto append(char c) noexcept -> errno
        {
            return _cstring_append_char(reinterpret_cast<_cstring *>(this), c, HasSize);
        }
        template <String S> [[nodiscard]] auto append(const S &s) noexcept -> errno
        {
            return _cstring_append(reinterpret_cast<_cstring *>(this),
                                   reinterpret_cast<const _cstring *>(&s), (1 << S::HasSize));
        }
        template <String S> [[nodiscard]] auto prepend(const S &s) noexcept -> errno
        {
            return _cstring_prepend(reinterpret_cast<_cstring *>(this),
                                    reinterpret_cast<const _cstring *>(&s), (1 << S::HasSize));
        }
        [[nodiscard]] auto copy(const char *s) noexcept -> errno { return this->copy<cstr>(s); }
        template <String S> [[nodiscard]] auto copy(const S &s) noexcept -> errno
        {
            return _cstring_copy(reinterpret_cast<_cstring *>(this),
                                 reinterpret_cast<const _cstring *>(&s), (1 << S::HasSize));
        }
        void reserve(usize nbytes) noexcept { s_str = static_cast<char *>(kzalloc(nbytes)); }
        [[nodiscard]] VINTIX_CONSTEXPR auto c_str() const noexcept -> const char *
        {
            KASSERT(s_str != nullptr);
            return s_str;
        }
        [[nodiscard]] constexpr auto unsafe_mutable_data() noexcept -> char * { return s_str; }
        [[nodiscard]] constexpr auto size() const noexcept -> usize { return strlen(s_str); }

        [[nodiscard]] constexpr auto is_empty() const noexcept -> bool { return s_str[0] == '\0'; }
        [[nodiscard]] constexpr auto length() const noexcept -> usize { return this->size(); }
        [[nodiscard]] constexpr auto front() const noexcept -> const char & { return s_str[0]; }
        [[nodiscard]] constexpr auto front() noexcept -> char & { return s_str[0]; }
        [[nodiscard]] VINTIX_CONSTEXPR auto at(usize i) const noexcept -> const char &
        {
            KASSERT(i < strlen(s_str));
            return s_str[i];
        }
        [[nodiscard]] VINTIX_CONSTEXPR auto at(usize i) noexcept -> char &
        {
            KASSERT(i < strlen(s_str));
            return s_str[i];
        }
        [[nodiscard]] constexpr auto operator[](usize i) const noexcept -> const char &
        {
            return s_str[i];
        }
        [[nodiscard]] constexpr auto operator[](usize i) noexcept -> char & { return s_str[i]; }
        [[nodiscard]] constexpr auto operator==(nullptr_t) const noexcept -> bool
        {
            return (s_str == nullptr);
        }
        [[nodiscard]] constexpr auto operator!=(nullptr_t) const noexcept -> bool
        {
            return (s_str != nullptr);
        }

        __DEFINE_STRING_READ_METHODS
    };

    class string {
      private:
        char *s_str { nullptr };
        usize s_len { 0 };

      public:
        static constexpr bool HasSize = true;
        constexpr string() noexcept = default;
        constexpr string(nullptr_t) noexcept {} // cppcheck-suppress noExplicitConstructor
        explicit string(const char *s) noexcept
        {
            _cstring_from_cstr(reinterpret_cast<_cstring *>(this), s, HasSize);
        }
        explicit string(const string &s) noexcept
            : s_str { static_cast<char *>(kzalloc(s.length() + 1)) }, s_len { s.length() }
        {
            if (s_str != nullptr)
                memcpy(s_str, s.c_str(), s_len + 1);
        }
        constexpr string(string &&s) noexcept : s_str { s.s_str }, s_len { s.s_len }
        {
            s.s_str = nullptr;
            s.s_len = 0;
        }
        static void dtor(void *s) noexcept { _cstring_dtor(static_cast<_cstring *>(s), HasSize); }
        ~string() noexcept { dtor(this); }
        static auto move(void *s, void *o) noexcept -> void *
        {
            return _cstring_move(static_cast<_cstring *>(s), static_cast<_cstring *>(o), HasSize);
        }
        auto operator=(string &&other) noexcept -> string &
        {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Weffc++"
            return *reinterpret_cast<string *>(_cstring_move(
                reinterpret_cast<_cstring *>(this), reinterpret_cast<_cstring *>(&other), HasSize));
#pragma GCC diagnostic pop
        }
        template <RawCStr S> auto operator=(S s) noexcept -> string &
        {
            vintix::ignore = this->copy(cstr { s });
            return *this;
        }
        template <String S> auto operator=(const S &s) noexcept -> string &
        {
            vintix::ignore = this->copy(s);
            return *this;
        }
        auto operator=(const string &s) noexcept -> string &
        {
            if (this != &s)
                vintix::ignore = this->copy(s);
            return *this;
        }
        template <String S> explicit string(const S &s) noexcept { vintix::ignore = this->copy(s); }
        [[nodiscard]] auto append(const char *s) noexcept -> errno { return this->append<cstr>(s); }
        [[nodiscard]] auto prepend(const char *s) noexcept -> errno
        {
            return this->prepend<cstr>(s);
        }
        [[nodiscard]] auto prepend(char c) noexcept -> errno
        {
            return _cstring_prepend_char(reinterpret_cast<_cstring *>(this), c, HasSize);
        }
        VINTIX_CONSTEXPR void erase_last() noexcept
        {
            KASSERT(s_len != 0 && s_str != nullptr);
            s_str[--s_len] = '\0';
        }
        [[nodiscard]] auto append(char c) noexcept -> errno
        {
            return _cstring_append_char(reinterpret_cast<_cstring *>(this), c, HasSize);
        }
        template <String S> [[nodiscard]] auto append(const S &s) noexcept -> errno
        {
            return _cstring_append(reinterpret_cast<_cstring *>(this),
                                   reinterpret_cast<const _cstring *>(&s),
                                   HasSize | (1 << S::HasSize));
        }
        template <String S> [[nodiscard]] auto prepend(const S &s) noexcept -> errno
        {
            return _cstring_prepend(reinterpret_cast<_cstring *>(this),
                                    reinterpret_cast<const _cstring *>(&s),
                                    HasSize | (1 << S::HasSize));
        }
        [[nodiscard]] auto copy(const char *s) noexcept -> errno { return this->copy<cstr>(s); }
        template <String S> [[nodiscard]] auto copy(const S &s) noexcept -> errno
        {
            return _cstring_copy(reinterpret_cast<_cstring *>(this),
                                 reinterpret_cast<const _cstring *>(&s),
                                 HasSize | (1 << S::HasSize));
        }
        void reserve(usize nbytes) noexcept { s_str = static_cast<char *>(kzalloc(nbytes)); }
        [[nodiscard]] VINTIX_CONSTEXPR auto c_str() const noexcept -> const char *
        {
            KASSERT(s_str != nullptr);
            return s_str;
        }
        constexpr void unsafe_set_size(usize sz) noexcept { s_len = sz; }
        [[nodiscard]] constexpr auto unsafe_mutable_data() noexcept -> char * { return s_str; }
        [[nodiscard]] constexpr auto size() const noexcept -> const usize & { return s_len; }
        [[nodiscard]] constexpr auto is_empty() const noexcept -> bool { return s_len == 0; }
        [[nodiscard]] constexpr auto length() const noexcept -> const usize & { return s_len; }
        [[nodiscard]] VINTIX_CONSTEXPR auto front() const noexcept -> const char &
        {
            KASSERT(s_len != 0);
            return s_str[0];
        }
        [[nodiscard]] VINTIX_CONSTEXPR auto front() noexcept -> char &
        {
            KASSERT(s_len != 0);
            return s_str[0];
        }
        [[nodiscard]] VINTIX_CONSTEXPR auto back() const noexcept -> const char &
        {
            KASSERT(s_len != 0);
            return s_str[s_len - 1];
        }
        [[nodiscard]] VINTIX_CONSTEXPR auto back() noexcept -> char &
        {
            KASSERT(s_len != 0);
            return s_str[s_len - 1];
        }
        [[nodiscard]] VINTIX_CONSTEXPR auto at(usize i) const noexcept -> const char &
        {
            KASSERT(i < s_len);
            return s_str[i];
        }
        [[nodiscard]] VINTIX_CONSTEXPR auto at(usize i) noexcept -> char &
        {
            KASSERT(i < s_len);
            return s_str[i];
        }
        [[nodiscard]] constexpr auto operator[](usize i) const noexcept -> const char &
        {
            return s_str[i];
        }
        [[nodiscard]] constexpr auto operator[](usize i) noexcept -> char & { return s_str[i]; }
        [[nodiscard]] constexpr auto operator==(nullptr_t) const noexcept -> bool
        {
            return (s_str == nullptr);
        }
        [[nodiscard]] constexpr auto operator!=(nullptr_t) const noexcept -> bool
        {
            return (s_str != nullptr);
        }

        __DEFINE_STRING_READ_METHODS
    };
} // namespace vintix

#endif // VINTIX_CXXSTRING_H
