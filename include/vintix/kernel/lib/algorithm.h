/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_ALGORITHM_H
#define VINTIX_ALGORITHM_H

#include <vintix/kernel/core/kassert.h>
#include <vintix/kernel/lib/bits.h>
#include <vintix/kernel/lib/concepts.h>
#include <vintix/kernel/lib/errno.h>
#include <vintix/kernel/lib/span.h>
#include <vintix/kernel/lib/types.h>

namespace vintix {
    template <EqualityComparable T>
        requires TriviallyCopyable<T>
    constexpr auto any_of(span<const T> s, T v) noexcept -> bool
    {
        for (usize i = 0; i < s.size(); ++i)
            if (s[i] == v)
                return true;
        return false;
    }

    template <EqualityComparable T>
        requires TriviallyCopyable<T>
    constexpr auto all_of(span<const T> s, T v) noexcept -> bool
    {
        for (usize i = 0; i < s.size(); ++i)
            if (s[i] != v)
                return false;
        return true;
    }

    template <Integral T>
    constexpr auto count_consecutive_numbers(span<const T> s) noexcept -> usize
    {
        if (s.size() == 0)
            return 0;

        usize count = 0;
        for (usize i = 1; i < s.size(); ++i)
            if (s.front() + i == s[i])
                ++count;

        return count;
    }

} // namespace vintix

#endif // VINTIX_ALGORITHM_H
