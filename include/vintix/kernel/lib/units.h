/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_UNITS_H
#define VINTIX_UNITS_H

#include <vintix/kernel/lib/types.h>

enum unit_cnv {
    B,
    KIB,
    MIB,
    GIB,
};

consteval auto operator""_b(unsigned long long n) noexcept -> usize
{
    return static_cast<usize>(n);
}
consteval auto operator""_kib(unsigned long long n) noexcept -> usize
{
    return static_cast<usize>(n * 1024);
}
consteval auto operator""_mib(unsigned long long n) noexcept -> usize
{
    return static_cast<usize>(n * 1024_kib);
}
consteval auto operator""_gib(unsigned long long n) noexcept -> usize
{
    return static_cast<usize>(n * 1024_mib);
}

auto convert_to_unit(usize n, bool automatic_conversion, unit_cnv to = B) noexcept -> char *;

#endif // VINTIX_UNITS_H
