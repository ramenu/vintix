/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_LIST_H
#define VINTIX_LIST_H

#include <vintix/kernel/core/io.h>
#include <vintix/kernel/core/kassert.h>
#include <vintix/kernel/core/macros.h>
#include <vintix/kernel/core/vmm.h>
#include <vintix/kernel/lib/errno.h>
#include <vintix/kernel/lib/klog.h>
#include <vintix/kernel/lib/move.h>
#include <vintix/kernel/lib/types.h>
#include <vintix/kernel/lib/unique_ptr.h>

struct list_node {
    list_node *n_next;
    template <typename T> [[nodiscard]] auto get() const noexcept -> const T &
    {
        return reinterpret_cast<const T &>(
            this + sizeof(list_node)); // cppcheck-suppress returnTempReference
    }
    template <typename T> [[nodiscard]] auto get() noexcept -> T &
    {
        return reinterpret_cast<T &>(this +
                                     sizeof(list_node)); // cppcheck-suppress returnTempReference
    }
};

struct clist {
    list_node *l_head;
};

template <typename T> struct list_node_g {
    list_node_g<T> *n_next;
    T n_data;
};

extern "C" {
    [[nodiscard]] auto list_insert(list_node **, void *, usize) noexcept -> void *;
    [[nodiscard]] auto list_size(list_node *head) noexcept -> usize;
    [[nodiscard]] auto list_move(clist *self, clist *other,
                                 void (*dtor)(void *)) noexcept -> clist *;
    void list_dtor(clist *self, void (*dtor)(void *)) noexcept;
    void list_remove(clist *self, void *elem, void (*dtor)(void *)) noexcept;
}

namespace vintix {

    template <typename T> class list {
      private:
        list_node_g<T> *l_head;
        static void dtor(void *s) noexcept
        {
            auto *self = static_cast<clist *>(s);
            list_dtor(self, T::dtor);
        }

      public:
        constexpr list() noexcept : l_head { nullptr } {}
        constexpr list(nullptr_t) noexcept
            : l_head { nullptr } {} // cppcheck-suppress noExplicitConstructor
        ~list() noexcept { dtor(this); }
        constexpr list(list<T> &&other) noexcept : l_head { other.l_head }
        {
            other.l_head = nullptr;
        }
        auto operator=(list<T> &&other) noexcept -> list<T> &
        {
            return *reinterpret_cast<list<T> *>(list_move(
                reinterpret_cast<clist *>(this), reinterpret_cast<clist *>(&other), list<T>::dtor));
        }
        [[nodiscard]] auto insert(T &&data) noexcept -> T *
        {
            return static_cast<T *>(
                list_insert(reinterpret_cast<list_node **>(&l_head), &data, sizeof(T)));
        }
        void remove(T *data) noexcept
        {
            return list_remove(reinterpret_cast<clist *>(this), data, T::dtor);
        }
        [[nodiscard]] auto size() const noexcept -> usize { return list_size(l_head); }
        [[nodiscard]] constexpr auto unsafe_front() noexcept -> list_node_g<T> *
        {
            KASSERT(l_head != nullptr);
            return l_head;
        }
        [[nodiscard]] constexpr auto operator==(nullptr_t) const noexcept -> bool
        {
            return l_head == nullptr;
        }
        [[nodiscard]] constexpr auto operator!=(nullptr_t) const noexcept -> bool
        {
            return l_head != nullptr;
        }
        [[nodiscard]] constexpr auto front() const noexcept -> const list_node_g<T> *
        {
            KASSERT(l_head != nullptr);
            return l_head;
        }
        DELETE_COPYABLE(list)
    };
} // namespace vintix
#endif // VINTIX_LIST_H
