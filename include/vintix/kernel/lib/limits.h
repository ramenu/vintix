/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_LIMITS_H
#define VINTIX_LIMITS_H

#include <vintix/kernel/lib/concepts.h>
#include <vintix/kernel/lib/types.h>

static constexpr i8 SCHAR_MIN = -128;
static constexpr i8 SCHAR_MAX = 127;
static constexpr u8 UCHAR_MAX = 255;
static constexpr i8 CHAR_MIN = SCHAR_MIN;
static constexpr i8 CHAR_MAX = SCHAR_MAX;
static constexpr i16 SHRT_MIN = -32768;
static constexpr i16 SHRT_MAX = 32767;
static constexpr u16 USHRT_MAX = 65535;
static constexpr i32 INT_MIN = -2147483648;
static constexpr i32 INT_MAX = 2147483647;
static constexpr u32 UINT_MAX = 4294967295;
static constexpr ssize SSIZE_MAX = INT_MAX;
static constexpr usize SIZE_MAX = UINT_MAX;

template <vintix::Unsigned N> consteval auto limit_of() noexcept { return static_cast<N>(0 - 1); }

#endif // VINTIX_LIMITS_H
