/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_OPTIONAL_H
#define VINTIX_OPTIONAL_H

#include <vintix/kernel/core/global.h>
#include <vintix/kernel/core/kassert.h>
#include <vintix/kernel/lib/concepts.h>

namespace vintix {

    struct nullopt_t {
        constexpr explicit nullopt_t() noexcept = default;
    };

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Weffc++"
    template <typename T> class optional {
      private:
        bool _has_value;
        T _value;

      public:
        using value_type = T;
        constexpr optional() noexcept : _has_value { false } {} // cppcheck-suppress uninitMemberVar
        constexpr optional(nullopt_t) noexcept
            : _has_value { false } {} // cppcheck-suppress[noExplicitConstructor,uninitMemberVar]
        constexpr optional(T &&value) noexcept
            : _has_value { true }, _value { value }
        {} // cppcheck-suppress[noExplicitConstructor,uninitMemberVar]
        [[nodiscard]] explicit constexpr operator bool() const noexcept { return _has_value; }
        [[nodiscard]] constexpr auto has_value() const noexcept -> bool { return _has_value; }
        [[nodiscard]] constexpr auto is_some() const noexcept -> bool { return _has_value; }
        [[nodiscard]] constexpr auto is_none() const noexcept -> bool { return !_has_value; }
        [[nodiscard]] VINTIX_CONSTEXPR auto value() const noexcept -> T
        {
            KASSERT(_has_value);
            return _value;
        }
        [[nodiscard]] constexpr auto value_or(T alt) const noexcept -> T
        {
            return (_has_value) ? _value : alt;
        }
    };

    static constexpr nullopt_t nullopt_t {};
} // namespace vintix
#pragma GCC diagnostic pop

#endif // VINTIX_OPTIONAL_H
