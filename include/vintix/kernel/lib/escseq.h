/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_ESCSEQ_H
#define VINTIX_ESCSEQ_H

/* Defined constants of ANSI escape sequences */

#define ANSI_ESC_OCTAL '\33'
#define ANSI_ESC_HEX '\x1b'

#define ANSI_COLORCODE_BLACK '0'
#define ANSI_COLORCODE_RED '1'
#define ANSI_COLORCODE_GREEN '2'
#define ANSI_COLORCODE_YELLOW '3'
#define ANSI_COLORCODE_BLUE '4'
#define ANSI_COLORCODE_MAGENTA '5'
#define ANSI_COLORCODE_CYAN '6'
#define ANSI_COLORCODE_WHITE '7'

#define ANSI_COLOR_RESET "\033[0m"
#define ANSI_COLOR_BLACK "\033[30m"   /* Black */
#define ANSI_COLOR_RED "\033[31m"     /* Red */
#define ANSI_COLOR_GREEN "\033[32m"   /* Green */
#define ANSI_COLOR_YELLOW "\033[33m"  /* Yellow */
#define ANSI_COLOR_BLUE "\033[34m"    /* Blue */
#define ANSI_COLOR_MAGENTA "\033[35m" /* Magenta */
#define ANSI_COLOR_CYAN "\033[36m"    /* Cyan */
#define ANSI_COLOR_WHITE "\033[37m"   /* White */

constexpr auto is_ansi_escape_character(char c) noexcept -> bool { return c == ANSI_ESC_OCTAL; }

constexpr auto parameter_is_foreground(const char *s) noexcept -> bool
{
    return (s[0] == '3' && (s[1] >= '0' && s[1] <= '7'));
}

#endif // VINTIX_ESCSEQ_H
