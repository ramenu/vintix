/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_SHARED_PTR_H
#define VINTIX_SHARED_PTR_H

#include <vintix/kernel/core/global.h>
#include <vintix/kernel/core/kassert.h>
#include <vintix/kernel/core/vmm.h>
#include <vintix/kernel/lib/types.h>

namespace vintix {

    template <typename T> class shared_ptr {
        T *_p { nullptr };
        usize *_ref { nullptr };

      public:
        shared_ptr() noexcept : _ref { static_cast<usize *>(kmalloc(sizeof(usize))) }
        {
            if (_ref != nullptr) [[likely]]
                *_ref = 0;
        }
        explicit shared_ptr(nullptr_t) noexcept
            : _ref { static_cast<usize *>(kmalloc(sizeof(usize))) }
        {
            if (_ref != nullptr) [[likely]]
                *_ref = 0;
        }
        explicit shared_ptr(T *p) noexcept
            : _p { p }, _ref { static_cast<usize *>(kmalloc(sizeof(usize))) }
        {
            if (_ref != nullptr) [[likely]]
                *_ref = 1;
        }
        static void dtor(void *s) noexcept
        {
            vintix::shared_ptr<T> *self = static_cast<vintix::shared_ptr<T> *>(s);
            if (self->_p == nullptr)
                return;

            if (self->_ref != nullptr && *self->_ref != 0) {
                --*self->_ref;

                if (*self->_ref == 0) {
                    self->_p->~T();
                    kfree(self->_ref);
                }
            }
        }
        ~shared_ptr() noexcept { dtor(this); }
        constexpr explicit operator bool() const noexcept { return (_p != nullptr); }
        NODISCARD constexpr auto is_ok() const noexcept { return _ref != nullptr; }
        constexpr auto operator==(nullptr_t) const noexcept { return (_p == nullptr); }
        constexpr auto operator!=(nullptr_t) const noexcept { return (_p != nullptr); }
        constexpr auto operator<=>(const void *p) const noexcept -> bool { return (_p <=> p); }
        constexpr shared_ptr(shared_ptr &&other) noexcept : _p { other._p }, _ref { other._ref }
        {
            other._p = nullptr;
            other._ref = nullptr;
        }
        shared_ptr(const shared_ptr &other) noexcept : _p { other._p }, _ref { other._ref }
        {
            if (other != nullptr)
                ++*_ref;
        }
        auto operator=(const shared_ptr &other) noexcept -> shared_ptr &
        {
            if (this == &other)
                return *this;

            this->~shared_ptr();

            if (other != nullptr) {
                _p = other._p;
                _ref = other._ref;
                ++*_ref;
            }

            return *this;
        }
        auto operator=(shared_ptr &&other) noexcept -> shared_ptr &
        {
            if (this == &other)
                return *this;

            this->~shared_ptr();
            _p = other._p;
            _ref = other._ref;
            other._p = nullptr;
            other._ref = nullptr;

            return *this;
        }
        [[nodiscard]] VINTIX_CONSTEXPR auto use_count() const noexcept -> const usize &
        {
            KASSERT(_ref != nullptr);
            return *_ref;
        }
        [[nodiscard]] VINTIX_CONSTEXPR auto unique() const noexcept -> bool
        {
            KASSERT(_ref != nullptr);
            return *_ref == 1;
        }
        VINTIX_CONSTEXPR auto operator+(usize n) const noexcept -> const T *
        {
            KASSERT(_p != nullptr);
            return _p + n;
        }
        VINTIX_CONSTEXPR auto operator->() const noexcept -> const T *
        {
            KASSERT(_p != nullptr);
            return _p;
        }
        VINTIX_CONSTEXPR auto operator->() noexcept -> T *
        {
            KASSERT(_p != nullptr);
            return _p;
        }
        VINTIX_CONSTEXPR auto operator*() const noexcept -> const T &
        {
            KASSERT(_p != nullptr);
            return *_p;
        }
        VINTIX_CONSTEXPR auto operator*() noexcept -> T &
        {
            KASSERT(_p != nullptr);
            return *_p;
        }
        VINTIX_CONSTEXPR auto operator[](usize i) const noexcept -> const T &
        {
            KASSERT(_p != nullptr);
            return _p[i];
        }
        constexpr auto get() const noexcept -> const T * { return _p; }
        constexpr auto unsafe_mutable_data() noexcept -> T * { return _p; }
    };
} // namespace vintix

#endif // VINTIX_SHARED_PTR_H
