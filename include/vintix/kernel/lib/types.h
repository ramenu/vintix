/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_TYPES_H
#define VINTIX_TYPES_H

#ifdef __i386__
using i8 = char;
using u8 = unsigned char;
using i16 = short;
using u16 = unsigned short;
using i32 = int;
using u32 = unsigned int;
using i64 = long long;
using u64 = unsigned long long;
using usize = unsigned int;
using ssize = i32;
using v_addr = u32;
using p_addr = u32;
using pg_directory_t = u32;
using pg_table_t = u32;
#else
#error "target architecture not defined"
#endif // __i386__

// Vintix-speciifc types
using fd_t = i32;
using blknum_t = usize;
using ublksize_t = usize;

// Generic Architecture constants
using int8_t = i8;
using uint8_t = u8;
using int16_t = i16;
using uint16_t = u16;
using int32_t = i32;
using uint32_t = u32;
using size_t = usize;
using ssize_t = ssize;
using uintptr_t = usize;
using page = p_addr;
using nullptr_t = decltype(nullptr);
using BYTE = u8;
using WORD = u16;
using DWORD = u32;
using QWORD = u64;

// UNIX types
// Taken from: https://pubs.opengroup.org/onlinepubs/009695399/basedefs/sys/types.h.html#tag_13_67
using blkcnt_t = usize;   // Used for file block counts
using blksize_t = ssize;  // Used for block size;
using clock_t = usize;    // Used for system times in clock ticks or CLOCKS_PER_SEC
using clockid_t = usize;  // Used for clock ID type in the clock and timer functions
using dev_t = u32;        // Used for device IDs
using fsblkcnt_t = usize; // Used for file system block counts
using fsfilcnt_t = usize; // Used for file system counts
using id_t =
    u16; // Used as a general identifier (can be used to contain at least a pid_t, uid_t, or gid_t)
using gid_t = id_t;  // Used for group IDs
using ino_t = u32;   // Used for file serial numbers
using key_t = usize; // Used for XSI interprocess communication
using mode_t = u16;  // Used for some file attributes
using nlink_t = u16; // Used for link counts

/* Used for file sizes and offsets */
using off_t = i32;
using loff_t = i64;

using vm_off_t = u32;  // Used for virtual memory offsets
using vm_size_t = u32; // Used for vm_object sizes
using pid_t = i16;     // Used for process IDs and process group IDs
using uid_t = id_t;    // Used for user IDs
using time_t = u32;    // Used for UNIX timestamps

struct destroying_delete_t {
    explicit destroying_delete_t() = default;
};
inline constexpr destroying_delete_t destroying_delete {};

#endif // VINTIX_TYPES_H
