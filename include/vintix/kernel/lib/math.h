/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_MATH_H
#define VINTIX_MATH_H

#include <vintix/kernel/lib/errno.h>
#include <vintix/kernel/lib/limits.h>
#include <vintix/kernel/lib/types.h>

constexpr auto min(usize x, usize y) noexcept -> usize { return (y > x) ? x : y; }

constexpr auto max(usize x, usize y) noexcept -> usize { return (y > x) ? y : x; }

constexpr auto abs(i32 x) noexcept -> usize { return static_cast<usize>((x < 0) ? -x : x); }
constexpr auto abserr(errno x) noexcept -> errno { return (x < 0) ? -x : x; }

constexpr auto neg(i32 x) noexcept -> i32 { return (x <= 0) ? x : -x; }

constexpr auto diff(usize x, usize y) noexcept -> usize { return (y > x) ? y - x : x - y; }

constexpr auto pow2(usize n) noexcept -> usize { return (1 << n); }

constexpr auto log2(u32 n) noexcept -> u32
{
    return 8 * sizeof(u32) - static_cast<u32>(__builtin_clz(n)) - 1;
}

constexpr auto is_power_of_two(usize n) noexcept -> bool { return ((n & (n - 1)) == 0); }

constexpr auto percentage(usize nume, usize deno) noexcept -> usize { return nume * 100 / deno; }

constexpr auto pow(usize n, usize exp) noexcept -> usize
{
    while (exp--)
        n *= n;
    return n;
}

constexpr auto round_up(usize x, usize y) noexcept -> usize
{
    const usize round_mask = y - 1;
    return (x - 1) | (round_mask + 1);
}

#endif // VINTIX_MATH_H
