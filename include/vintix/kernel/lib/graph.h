/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_GRAPH_H
#define VINTIX_GRAPH_H

#include <vintix/kernel/lib/concepts.h>
#include <vintix/kernel/lib/list.h>

namespace vintix {
    template <EqualityComparable T> class graph {
      private:
        T g_data;
        graph<T> *g_parent;
        list<graph<T>> g_vertices;

      public:
        constexpr graph() noexcept = default;
        [[nodiscard]] constexpr auto get() const noexcept -> const T & { return g_data; }
        [[nodiscard]] constexpr auto parent() const noexcept -> const graph<T> *
        {
            return g_parent;
        }
        template <Comparable V>
        [[nodiscard]] constexpr auto search(const V &v) noexcept -> graph<T> *
        {
            list_node_g<T> *vertex = g_vertices.front();

            while (vertex != nullptr) {
                if (vertex->n_data == v)
                    return &vertex->n_data;

                vertex = vertex->n_next;
            }

            return nullptr;
        }
        [[nodiscard]] auto insert(T &&value) noexcept -> errno
        {
            graph<T> g_graph {};
            g_graph.g_parent = this;
            g_graph.g_data = value;

            return g_vertices.insert(value);
        }
    };
} // namespace vintix

#endif // VINTIX_GRAPH_H
