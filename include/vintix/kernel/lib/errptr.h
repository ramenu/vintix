/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_ERRPTR_H
#define VINTIX_ERRPTR_H

#include <vintix/kernel/lib/errno.h>

#define UNWRAP_PTR(P)                                                                              \
    ({                                                                                             \
        auto res = (P);                                                                            \
        if (res.is_err())                                                                          \
            return res.err();                                                                      \
        res.get();                                                                                 \
    })

#define UNWRAP_NPTR(P)                                                                             \
    ({                                                                                             \
        auto res = (P);                                                                            \
        if (res.is_err())                                                                          \
            return -res.err();                                                                     \
        res.get();                                                                                 \
    })

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Weffc++"
template <typename T> class errptr {
  private:
    T *ptr;

  public:
    constexpr errptr() noexcept : ptr { nullptr } {}
    constexpr errptr(errno err) noexcept
        : ptr { reinterpret_cast<T *>(err) }
    {} // cppcheck-suppress[noExplicitConstructor,uninitMemberVar]
    constexpr errptr(T *p) noexcept
        : ptr { static_cast<T *>(p) } {} // cppcheck-suppress[noExplicitConstructor,uninitMemberVar]

    [[nodiscard]] constexpr auto get() const noexcept -> const T * { return ptr; }
    [[nodiscard]] constexpr auto get() noexcept -> T * { return ptr; }
    [[nodiscard]] constexpr auto operator*() const noexcept -> const T & { return *ptr; }
    [[nodiscard]] constexpr auto operator*() noexcept -> T & { return *ptr; }
    [[nodiscard]] constexpr auto operator+(usize n) const noexcept -> const T * { return ptr + n; }
    [[nodiscard]] constexpr auto operator+(usize n) noexcept -> T * { return ptr + n; }
    [[nodiscard]] constexpr auto operator->() const noexcept -> const T * { return ptr; }
    [[nodiscard]] constexpr auto operator->() noexcept -> T * { return ptr; }
    [[nodiscard]] constexpr auto operator==(const void *other) const noexcept
    {
        return ptr == other;
    }
    [[nodiscard]] constexpr auto operator!=(const void *other) const noexcept
    {
        return ptr != other;
    }
    [[nodiscard]] constexpr auto operator==(errno err) const noexcept { return this->err() == err; }
    [[nodiscard]] constexpr auto operator!=(errno err) const noexcept { return this->err() != err; }
    [[nodiscard]] constexpr auto is_err() const noexcept -> bool
    {
        return reinterpret_cast<usize>(ptr) <= static_cast<usize>(MAX_ERRNO);
    }
    [[nodiscard]] constexpr auto is_ok() const noexcept -> bool { return !this->is_err(); }
    [[nodiscard]] constexpr auto err() const noexcept -> errno
    {
        return reinterpret_cast<errno>(ptr);
    }
    [[nodiscard]] constexpr auto err_is(errno err) const noexcept -> bool
    {
        return this->err() == err;
    }
    [[nodiscard]] constexpr auto is_err_but_not(errno err) const noexcept -> bool
    {
        return this->is_err() && this->err() != err;
    }
};

#endif // VINTIX_ERRPTR_H
