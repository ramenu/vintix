/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_STRING_VIEW_H
#define VINTIX_STRING_VIEW_H

#include <vintix/kernel/core/global.h>
#include <vintix/kernel/core/kassert.h>
#include <vintix/kernel/lib/optional.h>
#include <vintix/kernel/lib/string.h>
#include <vintix/kernel/lib/string_functions.h>
#include <vintix/kernel/lib/string_ptr.h>
#include <vintix/kernel/lib/types.h>

namespace vintix {

    class string;

    class string_view {
      private:
        const char *_s;
        usize _lngth;

      public:
        static constexpr bool HasSize = true;
        explicit constexpr string_view() noexcept : _s { nullptr }, _lngth { 0 } {}
        explicit constexpr string_view(const char *s) noexcept : _s { s }, _lngth { strlen(_s) } {}
        explicit constexpr string_view(nullptr_t) noexcept : _s { nullptr }, _lngth { 0 } {}
        template <String S>
        explicit constexpr string_view(const S &s) noexcept
            : _s { s.c_str() }, _lngth { s.length() }
        {
        }
        constexpr string_view(const char *s, usize length) noexcept : _s { s }, _lngth { length } {}
        [[nodiscard]] constexpr auto c_str() const noexcept -> const char * { return _s; }
        [[nodiscard]] constexpr auto data() const noexcept -> const char * { return _s; }
        [[nodiscard]] constexpr auto size() const noexcept -> usize { return _lngth; }
        [[nodiscard]] constexpr auto length() const noexcept -> usize { return _lngth; }
        [[nodiscard]] constexpr auto empty() const noexcept -> bool { return (_lngth == 0); }
        constexpr auto operator=(const char *s) noexcept -> string_view &
        {
            _s = s;
            _lngth = strlen(s);
            return *this;
        }
        VINTIX_CONSTEXPR void remove_prefix(usize n) noexcept
        {
            KASSERT(n < _lngth);
            _s += n;
            _lngth += n;
        }
        VINTIX_CONSTEXPR void remove_suffix(usize n) noexcept
        {
            KASSERT(n < _lngth);
            _lngth -= n;
        }
        [[nodiscard]] VINTIX_CONSTEXPR auto front() const noexcept -> char
        {
            KASSERT(_lngth > 0);
            return _s[0];
        }
        [[nodiscard]] VINTIX_CONSTEXPR auto back() const noexcept -> char
        {
            KASSERT(_lngth > 0);
            return _s[_lngth - 1];
        }
        [[nodiscard]] constexpr auto operator[](usize i) const noexcept -> char { return _s[i]; }
        [[nodiscard]] constexpr auto operator==(nullptr_t) const noexcept -> bool
        {
            return _s == nullptr;
        }
        [[nodiscard]] constexpr auto operator!=(nullptr_t) const noexcept -> bool
        {
            return _s != nullptr;
        }
        [[nodiscard]] constexpr auto operator*() const noexcept -> char { return *_s; }
        [[nodiscard]] VINTIX_CONSTEXPR auto at(usize i) const noexcept -> char
        {
            KASSERT(i < _lngth);
            return _s[i];
        }
        __DEFINE_STRING_READ_METHODS
    };
} // namespace vintix

#endif // VINTIX_STRING_VIEW_H
