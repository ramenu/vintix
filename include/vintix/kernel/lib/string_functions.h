/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_STRING_FUNCTIONS_H
#define VINTIX_STRING_FUNCTIONS_H

#include <vintix/kernel/core/io.h>
#include <vintix/kernel/core/kassert.h>
#include <vintix/kernel/core/vmm.h>
#include <vintix/kernel/lib/concepts.h>
#include <vintix/kernel/lib/errno.h>
#include <vintix/kernel/lib/optional.h>
#include <vintix/kernel/lib/result.h>
#include <vintix/kernel/lib/string.h>
#include <vintix/kernel/lib/vector.h>

#define __DEFINE_STRING_READ_METHODS                                                               \
    constexpr auto find(char c) const noexcept { return vintix::cxxstr::find(*this, c); }          \
    constexpr auto find(char c, usize off) const noexcept                                          \
    {                                                                                              \
        return vintix::cxxstr::find(*this, c, off);                                                \
    }                                                                                              \
    constexpr auto rfind(char c) const noexcept { return vintix::cxxstr::rfind(*this, c); }        \
    constexpr auto starts_with(char c) const noexcept                                              \
    {                                                                                              \
        return vintix::cxxstr::starts_with(*this, c);                                              \
    }                                                                                              \
    template <vintix::String S> auto starts_with(const S &other) const noexcept -> bool            \
    {                                                                                              \
        return _cstring_starts_with(reinterpret_cast<const _cstring *>(this),                      \
                                    reinterpret_cast<const _cstring *>(&other),                    \
                                    this->HasSize | (1 << S::HasSize));                            \
    }                                                                                              \
    template <vintix::RawCStr T> auto starts_with(T o) const noexcept -> bool                      \
    {                                                                                              \
        const cstr other { o };                                                                    \
        return _cstring_starts_with(reinterpret_cast<const _cstring *>(this),                      \
                                    reinterpret_cast<const _cstring *>(&other), this->HasSize);    \
    }                                                                                              \
    template <vintix::String S> auto ends_with(const S &other) const noexcept -> bool              \
    {                                                                                              \
        return _cstring_ends_with(reinterpret_cast<const _cstring *>(this),                        \
                                  reinterpret_cast<const _cstring *>(&other),                      \
                                  this->HasSize | (1 << S::HasSize));                              \
    }                                                                                              \
    template <vintix::RawCStr T> auto ends_with(T o) const noexcept -> bool                        \
    {                                                                                              \
        const cstr other { o };                                                                    \
        return _cstring_ends_with(reinterpret_cast<const _cstring *>(this),                        \
                                  reinterpret_cast<const _cstring *>(&other), this->HasSize);      \
    }                                                                                              \
    template <vintix::String S> auto contains(const S &other) const noexcept -> bool               \
    {                                                                                              \
        return _cstring_contains(reinterpret_cast<const _cstring *>(this),                         \
                                 reinterpret_cast<const _cstring *>(&other),                       \
                                 this->HasSize | (1 << S::HasSize));                               \
    }                                                                                              \
    template <vintix::RawCStr T> auto contains(T o) const noexcept -> bool                         \
    {                                                                                              \
        const cstr other { o };                                                                    \
        return _cstring_contains(reinterpret_cast<const _cstring *>(this),                         \
                                 reinterpret_cast<const _cstring *>(&other), this->HasSize);       \
    }                                                                                              \
    [[nodiscard]] auto ends_with(char c) const noexcept -> bool                                    \
    {                                                                                              \
        return vintix::cxxstr::ends_with(*this, c);                                                \
    }                                                                                              \
    auto contains(char c) const noexcept -> bool                                                   \
    {                                                                                              \
        return _cstring_contains_char(reinterpret_cast<const _cstring *>(this), c);                \
    }                                                                                              \
    template <vintix::String S> auto operator==(const S &other) const noexcept -> bool             \
    {                                                                                              \
        return _cstring_equals(reinterpret_cast<const _cstring *>(this),                           \
                               reinterpret_cast<const _cstring *>(&other),                         \
                               this->HasSize | (1 << S::HasSize));                                 \
    }                                                                                              \
    template <vintix::RawCStr T> constexpr auto operator==(T other) const noexcept                 \
    {                                                                                              \
        return strcmp(this->c_str(), other) == 0;                                                  \
    }                                                                                              \
    template <vintix::RawCStr T> constexpr auto operator!=(T other) const noexcept                 \
    {                                                                                              \
        return strcmp(this->c_str(), other) != 0;                                                  \
    }                                                                                              \
    template <vintix::String S> auto operator!=(const S &other) const noexcept -> bool             \
    {                                                                                              \
        return !_cstring_equals(reinterpret_cast<const _cstring *>(this),                          \
                                reinterpret_cast<const _cstring *>(&other),                        \
                                this->HasSize | (1 << S::HasSize));                                \
    }                                                                                              \
    auto count(char c) const noexcept -> usize                                                     \
    {                                                                                              \
        return _cstring_count_char(reinterpret_cast<const _cstring *>(this), c);                   \
    }                                                                                              \
    auto split(char c) const noexcept -> result<vintix::vector<vintix::string>>                    \
    {                                                                                              \
        vintix::vector<vintix::string> vec;                                                        \
        if (auto err = _cstring_split(reinterpret_cast<const _cstring *>(this), c, vec))           \
            return err;                                                                            \
        return vec;                                                                                \
    }

struct _cstring;
extern "C" {
    auto _cstring_starts_with(const _cstring *self, const _cstring *other,
                              u8 has_size) noexcept -> bool;
    auto _cstring_ends_with(const _cstring *self, const _cstring *other,
                            u8 has_size) noexcept -> bool;
    auto _cstring_contains(const _cstring *self, const _cstring *other,
                           u8 has_size) noexcept -> bool;
    auto _cstring_equals(const _cstring *self, const _cstring *other, u8 has_size) noexcept -> bool;
    auto _cstring_count_char(const _cstring *self, char c) noexcept -> usize;
    auto _cstring_contains_char(const _cstring *self, char c) noexcept -> bool;
    auto _cstring_split(const _cstring *self, char c,
                        vintix::vector<vintix::string> &buf) noexcept -> errno;
}

namespace vintix::cxxstr {

    template <String S> constexpr auto get_size(const S &self) noexcept -> usize
    {
        if constexpr (S::HasSize)
            return self.size();
        else
            return strlen(self.c_str());
    }
    template <String S> constexpr auto find(const S &self, char c) noexcept -> optional<usize>
    {
        for (usize i = 0; self[i] != '\0'; ++i)
            if (self[i] == c)
                return i;
        return nullopt_t;
    }
    template <String S>
    constexpr auto find(const S &self, char c, usize off) noexcept -> optional<usize>
    {
        usize count = 0;
        for (usize i = 0; self[i] != '\0'; ++i)
            if (self[i] == c && count++ == off)
                return i;
        return nullopt_t;
    }
    template <String S> constexpr auto rfind(const S &self, char c) noexcept -> optional<usize>
    {
        const usize sz = get_size(self);
        KASSERT(sz > 0);
        for (usize i = sz; i != 0; --i)
            if (self[i - 1] == c)
                return i - 1;
        return nullopt_t;
    }
    template <String S> constexpr auto starts_with(const S &self, char c) noexcept -> bool
    {
        KASSERT(self.size() > 0);
        return self[0] == c;
    }
    template <String S> constexpr auto ends_with(const S &self, char c) noexcept -> bool
    {
        const usize sz = get_size(self);
        KASSERT(sz > 0);
        return self[sz - 1] == c;
    }
} // namespace vintix::cxxstr

#endif // VINTIX_STRING_FUNCTIONS_H
