/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_KVECTOR_H
#define VINTIX_KVECTOR_H

#include <vintix/kernel/core/global.h>
#include <vintix/kernel/core/kassert.h>
#include <vintix/kernel/core/macros.h>
#include <vintix/kernel/core/vmm.h>
#include <vintix/kernel/lib/concepts.h>
#include <vintix/kernel/lib/errno.h>
#include <vintix/kernel/lib/optional.h>
#include <vintix/kernel/lib/types.h>

struct cvector {
    void *v_data;
    usize v_sz;
};

extern "C" {
    auto cvector_push_back(cvector *self, void *data, usize obj_size) noexcept -> errno;
    void cvector_erase(cvector *self, usize i, void (*dtor)(void *), void *(*move)(void *, void *),
                       usize obj_size) noexcept;
    void cvector_erase_primitive(cvector *self, usize i, usize obj_size) noexcept;
    void cvector_dtor(cvector *self, void (*dtor)(void *), usize obj_size) noexcept;
    auto cvector_move(cvector *self, cvector *other, void (*dtor)(void *)) noexcept -> cvector *;
    void cvector_dtor_primitive(cvector *self) noexcept;
}

namespace vintix {
    template <typename T> class vector {
      private:
        T *v_data { nullptr };
        usize v_sz { 0 };
        static void dtor(void *self) noexcept
        {
            if constexpr (is_destructible_v<T>)
                cvector_dtor(static_cast<cvector *>(self), T::dtor, sizeof(T));
            else
                cvector_dtor_primitive(static_cast<cvector *>(self));
        }

      public:
        constexpr vector() noexcept = default;
        constexpr vector(nullptr_t) noexcept {} // cppcheck-suppress noExplicitConstructor
        constexpr vector(T *p, usize n) noexcept : v_data { p }, v_sz { n } {}
        constexpr vector(vector<T> &&v) noexcept : v_data { v.v_data }, v_sz { v.v_sz }
        {
            v.v_data = nullptr;
            v.v_sz = 0;
        }
        ~vector() noexcept { dtor(this); }
        void reserve(usize nbytes) noexcept { v_data = static_cast<T *>(kzalloc(nbytes)); }
        template <typename V>
            requires AreEqualityComparable<T, V>
        [[nodiscard]] auto contains(const V &v) const noexcept -> bool
        {
            for (usize i = 0; i < v_sz; ++i)
                if (v_data[i] == v)
                    return true;
            return false;
        }
        [[nodiscard]] auto push_back(T &&v) noexcept -> errno
        {
            return cvector_push_back(reinterpret_cast<cvector *>(this), &v, sizeof(T));
        }
        [[nodiscard]] auto push_back_copy(T v) noexcept -> errno
        {
            return cvector_push_back(reinterpret_cast<cvector *>(this), &v, sizeof(T));
        }
        void erase(usize i) noexcept
        {
            if constexpr (is_destructible_v<T>)
                return cvector_erase(reinterpret_cast<cvector *>(this), i, T::dtor, T::move,
                                     sizeof(T));
            else
                return cvector_erase_primitive(reinterpret_cast<cvector *>(this), i, sizeof(T));
        }
        [[nodiscard]] auto at(usize i) const noexcept -> const T &
        {
            KASSERT(i < v_sz);
            return v_data[i];
        }
        [[nodiscard]] auto at(usize i) noexcept -> T &
        {
            KASSERT(i < v_sz);
            return v_data[i];
        }
        template <typename V>
            requires AreEqualityComparable<T, V>
        auto find(const V &v) const noexcept -> optional<usize>
        {
            for (usize i = 0; i < v_sz; ++i)
                if (v_data[i] == v)
                    return i;
            return nullopt_t;
        }
        auto operator=(vector<T> &&other) noexcept -> vector<T> &
        {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Weffc++"
            return *reinterpret_cast<vector<T> *>(cvector_move(
                reinterpret_cast<cvector *>(this), reinterpret_cast<cvector *>(&other), dtor));
#pragma GCC diagnostic pop
        }
        [[nodiscard]] auto operator[](usize i) const noexcept -> const T & { return v_data[i]; }
        [[nodiscard]] auto operator[](usize i) noexcept -> T & { return v_data[i]; }
        [[nodiscard]] constexpr auto operator==(nullptr_t) const noexcept
        {
            return (v_data == nullptr);
        }
        [[nodiscard]] constexpr auto operator!=(nullptr_t) const noexcept
        {
            return (v_data != nullptr);
        }
        [[nodiscard]] constexpr auto empty() const noexcept -> bool { return (v_sz == 0); }
        [[nodiscard]] constexpr auto size() const noexcept -> const usize & { return v_sz; }
        [[nodiscard]] VINTIX_CONSTEXPR auto front() const noexcept -> const T &
        {
            KASSERT(v_sz != 0);
            return v_data[0];
        }
        [[nodiscard]] VINTIX_CONSTEXPR auto front() noexcept -> T &
        {
            KASSERT(v_sz != 0);
            return v_data[0];
        }
        [[nodiscard]] VINTIX_CONSTEXPR auto back() const noexcept -> const T &
        {
            KASSERT(v_sz > 0);
            return v_data[v_sz - 1];
        }
        [[nodiscard]] VINTIX_CONSTEXPR auto back() noexcept -> T &
        {
            KASSERT(v_sz > 0);
            return v_data[v_sz - 1];
        }
        [[nodiscard]] auto data() const noexcept -> const T * { return v_data; }
        [[nodiscard]] auto unsafe_mutable_data() const noexcept -> T * { return v_data; }
        DELETE_COPYABLE(vector)
    };
} // namespace vintix

#endif // VINTIX_KVECTOR_H
