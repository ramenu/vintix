/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_KLOG_H
#define VINTIX_KLOG_H

#include <vintix/kernel/core/global.h>
#include <vintix/kernel/core/interrupt.h>

#define PCI(...)                                                                                   \
    if constexpr (SHOW_LOGS)                                                                       \
    pci_log(__VA_ARGS__)
#define DEBUG(...)                                                                                 \
    if constexpr (DEBUG_BUILD && SHOW_LOGS)                                                        \
    debug_log(__VA_ARGS__)

extern "C" {
    void debug_log(const char *format, ...) noexcept;
    void ok(const char *format, ...) noexcept;
    void pci_log(u32 pci, const char *format, ...) noexcept;
}

#endif // VINTIX_KLOG_H
