/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_TYPE_TRAITS_H
#define VINTIX_TYPE_TRAITS_H

#include <vintix/kernel/lib/types.h>

namespace vintix {

    template <typename T, usize V> struct array;

    template <typename T> struct is_integral {
        static constexpr bool value = false;
    };
    template <> struct is_integral<i8> {
        static constexpr bool value = true;
    };
    template <> struct is_integral<u8> {
        static constexpr bool value = true;
    };
    template <> struct is_integral<i16> {
        static constexpr bool value = true;
    };
    template <> struct is_integral<u16> {
        static constexpr bool value = true;
    };
    template <> struct is_integral<i32> {
        static constexpr bool value = true;
    };
    template <> struct is_integral<u32> {
        static constexpr bool value = true;
    };
    template <typename T> static constexpr bool is_integral_v = is_integral<T>::value;

    template <typename T> struct remove_pointer {
        using type = T;
    };
    template <typename T> struct remove_pointer<T *> {
        using type = T;
    };
    template <typename T> struct remove_pointer<T *const> {
        using type = T;
    };
    template <typename T> struct remove_pointer<T *volatile> {
        using type = T;
    };
    template <typename T> struct remove_pointer<T *const volatile> {
        using type = T;
    };
    template <typename T> using remove_pointer_t = typename remove_pointer<T>::type;

    template <typename T> struct is_pointer {
        static constexpr bool value = false;
    };
    template <typename T> struct is_pointer<T *> {
        static constexpr bool value = true;
    };
    template <typename T> struct is_pointer<T *const> {
        static constexpr bool value = true;
    };
    template <typename T> struct is_pointer<T *volatile> {
        static constexpr bool value = true;
    };
    template <typename T> struct is_pointer<T *const volatile> {
        static constexpr bool value = true;
    };
    template <typename T> static constexpr bool is_pointer_v = is_pointer<T>::value;

    template <typename T, typename U> struct is_same {
        static constexpr bool value = false;
    };
    template <typename T> struct is_same<T, T> {
        static constexpr bool value = true;
    };
    template <typename T, typename U> static constexpr bool is_same_v = is_same<T, U>::value;

    template <typename T> struct is_array {
        static constexpr bool value = false;
    };
    template <typename T, usize N> struct is_array<array<T, N>> {
        static constexpr bool value = true;
    };
    template <typename T> static constexpr bool is_array_v = is_array<T>::value;

    template <typename T> struct is_signed {
        static constexpr bool value = false;
    };
    template <> struct is_signed<i8> {
        static constexpr bool value = true;
    };
    template <> struct is_signed<i16> {
        static constexpr bool value = true;
    };
    template <> struct is_signed<i32> {
        static constexpr bool value = true;
    };
    template <> struct is_signed<i64> {
        static constexpr bool value = true;
    };
    template <typename T> static constexpr bool is_signed_v = is_signed<T>::value;
    template <typename T> struct is_unsigned {
        static constexpr bool value = true;
    };
    template <> struct is_unsigned<u8> {
        static constexpr bool value = true;
    };
    template <> struct is_unsigned<u16> {
        static constexpr bool value = true;
    };
    template <> struct is_unsigned<u32> {
        static constexpr bool value = true;
    };
    template <> struct is_unsigned<u64> {
        static constexpr bool value = true;
    };
    template <typename T> static constexpr bool is_unsigned_v = is_unsigned<T>::value;

    template <typename T> struct is_string {
        static constexpr bool value = false;
    };

    template <typename T> struct is_raw_cstr {
        static constexpr bool value = false;
    };

    template <> struct is_raw_cstr<const char *> {
        static constexpr bool value = true;
    };

    template <> struct is_raw_cstr<char *> {
        static constexpr bool value = true;
    };

    template <typename T> static constexpr bool is_raw_cstr_v = is_raw_cstr<T>::value;

    template <class T, T v> struct integral_constant {
        static constexpr T value = v;
        using value_type = T;
        using type = integral_constant;
        constexpr operator value_type() const noexcept { return value; }
        constexpr auto operator()() const noexcept -> value_type { return value; }
    };

    template <typename T> struct is_destructible : integral_constant < bool, requires(T o) {
        {
            o.~T()
        } noexcept;
        T::dtor;
    } > {};

    template <typename T> static constexpr bool is_destructible_v = is_destructible<T>::value;

} // namespace vintix

#endif // VINTIX_TYPE_TRAITS_H
