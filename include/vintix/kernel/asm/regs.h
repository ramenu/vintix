/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_REGS_H
#define VINTIX_REGS_H

#include <vintix/kernel/lib/types.h>

inline auto cr2() noexcept -> u32
{
    u32 cr2;
    asm volatile("mov    %0, cr2" : "=a"(cr2));
    return cr2;
}

inline auto esp() noexcept -> u32
{
    u32 esp;
    asm volatile("mov    %0, esp" : "=a"(esp));
    return esp;
}

inline auto ebp() noexcept -> u32
{
    u32 ebp;
    asm volatile("mov    %0, ebp" : "=a"(ebp));
    return ebp;
}

#endif // VINTIX_REGS_H
