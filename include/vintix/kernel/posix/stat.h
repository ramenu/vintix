/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

// https://pubs.opengroup.org/onlinepubs/009695399/basedefs/sys/stat.h.html

#ifndef VINTIX_POSIX_STAT_H
#define VINTIX_POSIX_STAT_H

#include <vintix/kernel/lib/types.h>

struct stat {
    dev_t st_dev;     // Device ID of device containing file
    ino_t st_ino;     // File serial number
    mode_t st_mode;   // Mode of file
    nlink_t st_nlink; // Number of hard links to the file
    uid_t st_uid;     // User ID of the file
    gid_t st_gid;     // Group ID of the file
    // For regular files, the file size in bytes. For symbolic links, the length in bytes of the
    // pathname contained in the symbolic link.
    off_t st_size;
    time_t st_mtime; // Time of last data modification
    // A file system-specific preferred I/O block size for this object. In some file system types,
    // this may vary from file to file.
    blksize_t st_blksize;
    blkcnt_t st_blocks; // Number of blocks allocated for this object
};

enum {
    // Type of file
    S_IFBLK = 1,  // Block special
    S_IFCHR = 2,  // Character special
    S_IFIFO = 3,  // FIFO special
    S_IFREG = 4,  // Regular
    S_IFDIR = 5,  // Directory
    S_IFLNK = 6,  // Symbolic link
    S_IFSOCK = 7, // Socket

    // File mode bits
    S_IXOTH = 00001,                       // Execute/search permission (others)
    S_IWOTH = 00002,                       // Write permission (others)
    S_IROTH = 00004,                       // Read permission (others)
    S_IRWXO = S_IXOTH | S_IWOTH | S_IROTH, // Read, write, execute (others)
    S_IXGRP = 00010,                       // Execute/search permission (group)
    S_IWGRP = 00020,                       // Write permission (group)
    S_IRGRP = 00040,                       // Read permission (group)
    S_IRWXG = S_IXGRP | S_IWGRP | S_IRGRP, // Read, write, execute (group)
    S_IXUSR = 00100,                       // Execute/search permission (owner)
    S_IWUSR = 00200,                       // Write permission (owner)
    S_IRUSR = 00400,                       // Read permission (owner)
    S_IRWXU = S_IXUSR | S_IWUSR | S_IRUSR, // Read, write, execute (owner)
    S_ISUID = 0004000,                     // Set user UID on execution
    S_ISGID = 0002000,                     // Set group ID on execution
    S_ISVTX = 0001000                      // Sticky bit
};

#define S_ISBLK(m) ((m) == S_IFBLK)
#define S_ISCHR(m) ((m) == S_ISCHR)
#define S_ISDIR(m) ((m) == S_ISDIR)
#define S_ISFIFO(m) ((m) == S_ISFIFO)
#define S_ISREG(m) ((m) == S_ISREG)
#define S_ISLNK(m) ((m) == S_ISLNK)
#define S_ISSOCK(m) ((m) == S_ISSOCK)

#endif // VINTIX_POSIX_STAT_H
