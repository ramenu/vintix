/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_POSIX_MMAN_H
#define VINTIX_POSIX_MMAN_H

/* Constant to indicate error from mmap() */
#define MAP_FAILED nullptr

/* Page cannot be accessed. */
#define PROT_NONE 0

/* Page can be executed. */
#define PROT_EXEC 1

/* Page can be written. */
#define PROT_WRITE 2

/* Page can be read. */
#define PROT_READ 4

/* Share changes. */
#define MAP_SHARED 1

/* Changes are private. */
#define MAP_PRIVATE 2

/* Interpret `addr` exactly. */
#define MAP_FIXED 4

/* This mapping is not backed by any file; its contents
 * are initialized to zero. */
#define MAP_ANONYMOUS 8

#endif // VINTIX_POSIX_MMAN_H
