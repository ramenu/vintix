/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_POSIX_SIGNAL_H
#define VINTIX_POSIX_SIGNAL_H

#define SIGABRT 1    /* Process abort signal */
#define SIGALRM 2    /* Alarm clock */
#define SIGFPE 3     /* Erroneous arithmetic operation */
#define SIGHUP 4     /* Hangup */
#define SIGILL 5     /* Illegal instruction */
#define SIGINT 6     /* Terminal interrupt signal */
#define SIGKILL 7    /* Kill (cannot be caught or ignored) */
#define SIGPIPE 8    /* Write on a pipe with no one to read it */
#define SIGQUIT 9    /* Terminal quit signal */
#define SIGSEGV 10   /* Invalid memory reference */
#define SIGTERM 11   /* Termination signal */
#define SIGUSR1 12   /* User-defined signal 1 */
#define SIGUSR2 13   /* User-defined signal 2 */
#define SIGCHLD 14   /* Child process terminated or stopped */
#define SIGCONT 15   /* Continue executing, if stopped */
#define SIGSTOP 16   /* Stop executing (cannot be caught or ignored) */
#define SIGSTP 17    /* Terminal stop signal */
#define SIGTTIN 18   /* Background process attempting read */
#define SIGTTOU 19   /* Background process attempting write */
#define SIGBUS 20    /* Access to an undefined portion of a memory object */
#define SIGPOLL 21   /* Pollable event */
#define SIGPROF 22   /* Profiling timer expired */
#define SIGSYS 23    /* Bad system call */
#define SIGTRAP 24   /* Trace/breakpoint trap */
#define SIGURG 25    /* High bandwidth data is available at a socket */
#define SIGVTALRM 26 /* Virtual timer expired */
#define SIGXCPU 27   /* CPU time limit exceeded */
#define SIGXFSZ 28   /* File size limit exceeded */
#define _MAX_SIGNAL_NO 28

auto signumtostr(int) noexcept -> const char *;
#endif // VINTIX_POSIX_SIGNAL_H
