/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

// https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/fcntl.h.html

#ifndef VINTIX_FCNTL_H
#define VINTIX_FCNTL_H

#define F_DUPFD 1 /* Duplicate file descriptor */

/* Duplicate file descriptor with the close-on-exec flag FD_CLOEXEC set */
#define F_DUPFD_CLOEXEC 2

#define F_GETFD 3 /* Get file descriptor flags */

#define F_SETFD 4 /* Set file descriptor flags */

#define F_GETFL 5 /* Get file status flags and file access modes */

#define F_SETFL 6 /* Set file status flags */

#define F_GETLK 7 /* Get record locking information */

#define F_SETLK 8 /* Set record locking information */

#define F_SETLKW 9 /* Set record locking information; wait if blocked */

#define F_GETOWN 10 /* Get process or process group ID to receive SIGURG signals */

#define F_SETOWN 11 /* Set process or process group ID to receive SIGURG signals */

#define FD_CLOEXEC 12 /* Close the file descriptor upon execution of an exec family function */

#define F_RDLCK 13 /* Shared or read lock */

#define F_UNLCK 14 /* Unlock */

#define F_WRLCK 15 /* Exclusive or write lock */

/* Set the termios structure terminal parameters to a state that provides conforming behavior */
#define O_TTY_INIT 0

/* The FD_CLOEXEC flag associated with the new descriptor shall be set to close the file descriptor
 * upon execution of an exec family function */
#define O_CLOEXEC 1

#define O_CREAT (1 << 1) /* Create file if it does not exist */

#define O_DIRECTORY (1 << 2) /* Fail if file is a non-directory file */

#define O_EXCL (1 << 3) /* Exclusive use flag */

#define O_NOCTTY (1 << 4) /* Do not assign controlling terminal */

#define O_NOFOLLOW (1 << 5) /* Do not follow symbolic links */

#define O_TRUNC (1 << 6) /* Truncate flag */

#define O_APPEND (1 << 7) /* Set file append mode */

#define O_DSYNC (1 << 8) /* Write according to synchronized I/O data integrity completion */

#define O_NONBLOCK (1 << 9) /* Non-blocking mode */

#define O_RSYNC (1 << 10) /* Synchronized read I/O operations */

#define O_SYNC (1 << 11) /* Write according to synchronized I/O file integrity completion */

#define O_KERNEL (1 << 17) /* Open file with kernel privileges */

/* Open for execute only (non-directory files). The result is unspecified if this flag is
 * applied to a directory */
#define O_EXEC (1 << 12)

#define O_RDONLY (1 << 13) /* Open for reading only */

#define O_RDWR (1 << 14) /* Open for reading and writing */

/* Open directory for search only. The result is unspecified if this flag is applied to a
 * non-directory file */
#define O_SEARCH (1 << 15)

#define O_WRONLY (1 << 16) /* Open for writing only */

/* Mask for file access modes */
#define O_ACCMODE (O_EXEC | O_RDONLY | O_RDWR | O_SEARCH | O_WRONLY)
#endif // VINTIX_FCNTL_H
