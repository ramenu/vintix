/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_X86_32_MBR_H
#define VINTIX_X86_32_MBR_H

#include <vintix/kernel/lib/errno.h>
#include <vintix/kernel/lib/types.h>

enum {
    MBR_VALID_BOOTSECTOR_SIGNATURE = 0xaa55,
};

// See https://thestarman.pcministry.com/asm/mbr/PartTypes.htm
// Didn't add all of them cause there are too many, but I added the
// relevant ones
enum class mbr_partition_type : u8 {
    EMPTY = 0x00,
    FAT12 = 0x01,
    XENIX_ROOT = 0x02,
    XENIX_USR = 0x03,
    FAT16_LESS_32MB = 0x04,
    EXTENDED_PARTITION = 0x05,
    FAT16_GE_32MB = 0x06,
    INSTALLABLE_FS = 0x07,
    AIX_BOOTABLE_PARTITION = 0x08,
    AIX_DATA_PARTITION = 0x09,
    COHERENT_SWAP_PARTITION = 0x0a,
    FAT32 = 0x0b,
    FAT32_INT13_EXT = 0x0c,
    FAT16_GE_32MB_INT13_EXT = 0x0e,
    EXTENDED_PARTITION_INT13_EXT = 0x0f,
    OPUS = 0x10,
    HIDDEN_FAT12 = 0x11,
    COMPAQ_DIAGNOSTICS = 0x12,
    HIDDEN_FAT16_LESS_32MB = 0x14,
    HIDDEN_FAT16_GE_32MB = 0x16,
    HIDDEN_IFS = 0x17,
    AST_WINDOWS_SWAPFILE = 0x18,
    WILLOWTECH_PHOTON_COS = 0x19,
    HIDDEN_FAT32 = 0x1b,
    HIDDEN_FAT32_INT13_EXT = 0x1c,
    HIDDEN_FAT16_GE_32MB_INT13_EXT_POWERQUEST = 0x1e,
    WILLOWSOFT_OVERTUNE_FS = 0x20,
    OXYGEN_EXTENDED = 0x22,
    NEC_MSDOS_3X = 0x24,
    THEOS = 0x38,
    POWERQUEST_FILES_PARTITION_FORMAT = 0x3c,
    HIDDEN_NETWARE = 0x3d,
    VENIX_80286 = 0x40,
    POWERPC_BOOT_PARTITION = 0x41,
    SECURE_FS = 0x42,
    ALTERNATIVE_LINUX_NATIVE_FS = 0x43,
    ALFS = 0x4a,
    GNU_HURD = 0x63,
    DISKSECURE_MULTIBOOT = 0x70,
    IBM_PC = 0x75,
    OLD_MINIX = 0x80,
    MINIX = 0x81,
    LINUX_SWAP_PARTITION = 0x82,
    LINUX_NATIVE_FS = 0x83,
    FREEBSD = 0xa5,
    OPENBSD = 0xa6,
    NETBSD = 0xa9,
    SOLARIS_BOOT_PARTITION = 0xbe,
    CPM_86 = 0xd8,
    BEOS_FS = 0xeb,
};

// Master Boot Record Partition Table Entry
struct mbr_pte {
    u8 drive_attributes;
    u8 partition_start_chs[3];
    mbr_partition_type partition_type;
    u8 partition_end_chs[3];
    u32 partition_start_lba;
    u32 partition_sectors;
    [[nodiscard]] inline constexpr auto is_empty() const noexcept -> bool
    {
        return this->partition_type == mbr_partition_type::EMPTY;
    }
} __attribute__((packed));

static_assert(sizeof(mbr_pte) == 16);

// Master Boot Record Format
struct mbr {
    static constexpr usize TOTAL_PTES = 4;
    u8 mbr_bootstrap[440];
    u32 unique_disk_id; // optional
    u16 readonly;       // Optional, reserved 0x0000
    mbr_pte pte[TOTAL_PTES];
    u16 valid_bootsector_signature;

    // See https://neosmart.net/wiki/mbr-boot-process/
    [[nodiscard]] inline constexpr auto is_readonly() const noexcept -> bool
    {
        return (this->readonly == 0x5a5a);
    }
    [[nodiscard]] inline constexpr auto is_valid() const noexcept -> bool
    {
        return (this->valid_bootsector_signature == MBR_VALID_BOOTSECTOR_SIGNATURE);
    }
} __attribute__((packed));

static_assert(sizeof(mbr) == 0x200);

struct blkdev;

[[nodiscard]] auto parse_master_boot_record(dev_t, mbr *) noexcept -> errno;

#endif // VINTIX_X86_32_MBR_H
