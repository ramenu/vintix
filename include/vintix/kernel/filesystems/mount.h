/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_FILESYSTEMS_MOUNT_H
#define VINTIX_FILESYSTEMS_MOUNT_H

#include <vintix/kernel/lib/errno.h>

extern "C" {
    auto root_is_mounted() noexcept -> bool;
    auto kmount(const char *src, const char *target, const char *type, int mount_flags,
                v_addr start = 0x0) noexcept -> errno;
    auto kumount(const char *target, int umount_flags = 0) noexcept -> errno;
}
#endif // VINTIX_FILESYSTEMS_MOUNT_H
