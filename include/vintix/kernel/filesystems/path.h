/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_FILESYSTEMS_PATH_H
#define VINTIX_FILESYSTEMS_PATH_H

#include <vintix/kernel/lib/errno.h>
#include <vintix/kernel/lib/span.h>
#include <vintix/kernel/lib/string_view.h>
#include <vintix/kernel/lib/types.h>

extern "C" {

    /**
     * Extracts and returns the parent path of the given path string, handling edge cases
     * such as root directories and paths without parents.
     *
     * The function identifies the last directory delimiter ('/') in the given path and extracts
     * the substring up to (but not including) the delimiter. Handles special cases for root paths
     * or paths with only one parent.
     *
     * @param path A string view representing the input path for which the parent path is to be
     * determined.
     * @return A string view representing the parent path:
     *         - An empty string view if the path represents the root directory with no parent.
     *         - A string view containing "/" if the input path is directly under the root.
     *         - A string view representing the portion of the input path up to the last directory
     * delimiter.
     *
     * @note The function panics if an unexpected condition is encountered (logic error).
     */
    [[nodiscard]] auto path_parent_of(vintix::string_view) noexcept -> vintix::string_view;

    /**
     * Consumes a portion of the input path and stores it in the provided buffer, updating the
     * offset accordingly.
     *
     * The method extracts a subpath (delimited by '/'), appending it into the supplied buffer.
     * It advances the offset after processing the extracted subpath and ensures proper error
     * handling.
     *
     * @param path A null-terminated C-style string representing the full path to be consumed.
     * @param buf A buffer to store the extracted portion of the path.
     * @param off A reference to the current offset position in the input path; modified to reflect
     * the new position after consuming.
     * @return An errno value indicating the status of the operation:
     *         - `ENOERR` if successful.
     *         - `EPARSE` if no more delimiters ('/') are found in the path.
     *         - `ENAMETOOLONG` if the extracted portion of the path exceeds the buffer size.
     */
    [[nodiscard]] auto path_consume(const char *path, vintix::span<char> buf,
                                    usize &off) noexcept -> errno;

    /**
     * Consumes segments of the provided path string into a buffer until a specified condition is
     * met or an error occurs. Supports limiting the number of steps to consume from the path.
     *
     * The function repeatedly extracts portions of the input path and appends them to the buffer,
     * replacing the buffer's previous content while ensuring proper directory delimiters ('/') are
     * applied. If a "steps" limit is provided, it restricts the number of segments consumed
     * accordingly. Handles errors such as invalid path segments or buffer overflows.
     *
     * @param path A pointer to a null-terminated string representing the input path to be parsed.
     * @param buf A writable span of characters that acts as the buffer where the consumed portions
     * of the path will be stored.
     * @param steps An optional value specifying the maximum number of path segments to consume.
     * If no steps are provided, the function continues consuming until the entire path
     * is processed or an error occurs.
     * @return An `errno` value:
     *         - `ENOERR` on successful consumption of the path or when the entire path is processed
     * without error.
     *         - `EPARSE` if a parsing failure occurs on a path segment.
     *         - Any other error indicating failure based on invalid input or runtime constraints.
     *
     * @note The function assumes the caller provides a pre-allocated and sufficiently sized buffer.
     * Undefined behavior may occur if the buffer is too small to accommodate the path segments.
     * The input path string is expected to be null-terminated.
     * The function guarantees not to throw exceptions and adheres to the `noexcept` specifier.
     */
    [[nodiscard]] auto path_consume_until(const char *path, vintix::span<char> buf,
                                          vintix::optional<usize> steps) noexcept -> errno;

    [[nodiscard]] constexpr auto path_is_root(vintix::string_view path) noexcept -> bool
    {
        return path == "/";
    }

    // Converts a path like '////usr///bin/////ls//' to '/usr/bin/ls'
    void path_normalize(vintix::string &path) noexcept;

    /**
     * Retrieves a pointer to the beginning of the last segment (final entry) in a given path
     * string.
     *
     * The function scans the input path from the end to the beginning, searching for the last
     * directory delimiter ('/'). Once located, it returns a pointer to the character immediately
     * following the delimiter, representing the start of the last path segment. If no delimiter is
     * found, the path is treated as a single entry and a pointer to the start of the input string
     * is returned.
     *
     * @param path A string view representing the input path for which the last entry is to be
     * determined. The path must not be empty and should not end with a directory delimiter ('/').
     * @return A constant character pointer to the start of the last segment:
     *         - A pointer to the first character in the string if no delimiter is found.
     *         - A pointer to the character immediately after the last delimiter ('/') if found.
     *         - Returns nullptr if an unexpected logic error occurs.
     *
     * @note The function assumes the input path is correctly formatted and does not end with a
     * slash. It performs assertions to validate these expectations and may terminate the program if
     * they are not met.
     */
    auto path_last_entry(vintix::string_view path) noexcept -> const char *;

    /**
     * Checks the validity of a given path string and returns an error code if the path
     * is invalid, otherwise returns a successful result.
     *
     * This function ensures that the input path satisfies specific criteria for validity,
     * such as proper formatting or adherence to system path requirements. It operates in
     * a noexcept context and does not throw exceptions.
     *
     * @param path A string view representing the input path to be validated.
     * @return An errno value:
     *         - `ENOERR` if the path is valid.
     *         - A non-zero errno code indicating the first detected invalid condition in the path.
     *
     * @note The function performs validation within the constraints of the noexcept guarantee.
     */
    [[nodiscard]] auto path_chk_valid(vintix::string_view path) noexcept -> errno;

    [[nodiscard]] constexpr auto path_last_entry_sz(vintix::string_view path,
                                                    const char *last_entry) noexcept -> usize
    {
        return static_cast<usize>(path.data() + path.size() - last_entry);
    }
}

#endif // VINTIX_FILESYSTEMS_PATH_H
