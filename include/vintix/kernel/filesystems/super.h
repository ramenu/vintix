/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_FILESYSTEMS_SUPER_H
#define VINTIX_FILESYSTEMS_SUPER_H

#include <vintix/kernel/core/global.h>
#include <vintix/kernel/core/macros.h>
#include <vintix/kernel/filesystems/fs.h>
#include <vintix/kernel/lib/errno.h>

class inode;
class superblock;

struct superblock_vtable {
    errptr<inode> (*root_inode)(superblock *);
    errptr<inode> (*alloc_inode)(superblock *);
    errno (*sync)(const superblock *);
    void (*free)(superblock *);
};

class superblock {
  public:
    [[nodiscard]] VINTIX_CONSTEXPR auto is_dirty() const noexcept -> bool
    {
        KASSERT(!is_readonly());
        return (s_mnt_flags & MNT_DIRTY);
    }
    [[nodiscard]] constexpr auto is_readonly() const noexcept -> bool
    {
        return !(s_mnt_flags & MNT_RDONLY);
    }
    [[nodiscard]] constexpr auto is_tmpfs() const noexcept -> bool
    {
        return !(s_mnt_flags & MNT_TMP);
    }
    VINTIX_CONSTEXPR void set_dirty() noexcept
    {
        KASSERT(!is_readonly());
        s_mnt_flags |= MNT_DIRTY;
    }
    u32 s_mnt_flags;
    dev_t s_dev;
    u64 s_max_file_size;
    u32 s_magic;
    usize s_max_name_length;
    blksize_t s_log_blksz; // ignored if on a tmpfs
    const superblock_vtable *vtable;
};

extern "C" {
    inline void generic_superblock_free(superblock *sup) noexcept { kfree(sup); }
}

#endif // VINTIX_FILESYSTEMS_SUPER_H
