/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_FILESYSTEMS_STAT_H
#define VINTIX_FILESYSTEMS_STAT_H

#include <vintix/kernel/lib/types.h>

enum class mbr_partition_type;

struct kstatfs {
    mbr_partition_type type;
    blksize_t blksz;
    usize total_blks;
    usize free_blks;
    usize num_files;
    usize free_files;
    usize namelen;
};

#endif // VINTIX_FILESYSTEMS_STAT_H
