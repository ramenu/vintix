/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_FILESYSTEMS_FILE_H
#define VINTIX_FILESYSTEMS_FILE_H

#include <vintix/kernel/core/blkdev.h>
#include <vintix/kernel/filesystems/dentry.h>
#include <vintix/kernel/filesystems/inode.h>
#include <vintix/kernel/filesystems/super.h>
#include <vintix/kernel/lib/errno.h>
#include <vintix/kernel/lib/types.h>

namespace vintix {
    template <typename T> class span;
}

class file;

struct file_vtable {
    off_t (*lseek)(const file *, off_t, int);
    ssize (*read)(file *, void *, usize);
    ssize (*write)(file *, const void *, usize);
    errno (*put_blks)(const file *, vintix::span<blkcnt_t>, off_t, bool);
    errno (*close)(file *);
};

class file {
  public:
    const file_vtable *vtable {};
    dentry *f_dentry {};
    off_t f_pos {};
    int f_oflags {};
    constexpr file() noexcept = default;
    static void dtor(void *self) noexcept { kclose(static_cast<file *>(self)); }
    ~file() noexcept { kclose(this); }
    [[nodiscard]] auto uoff() const noexcept -> usize
    {
        KASSERT(f_pos >= 0);
        return static_cast<usize>(f_pos);
    }
    [[nodiscard]] constexpr auto size() const noexcept -> usize
    {
        return static_cast<usize>(f_dentry->d_ino->i_size);
    }
    [[nodiscard]] auto log_blksz() const noexcept -> blksize_t
    {
        if (this->is_blkdev()) {
            const blkdev *dev = device::get_blkdev(this->inode()->i_num - 1);
            return dev->log_secsz;
        }
        return this->inode()->i_sup->s_log_blksz;
    }
    [[nodiscard]] auto maximum_file_size() const noexcept -> u64
    {
        return f_dentry->d_ino->i_sup->s_max_file_size;
    }
    [[nodiscard]] constexpr auto inode() const noexcept -> const inode * { return f_dentry->d_ino; }
    [[nodiscard]] constexpr auto super() const noexcept -> const superblock *
    {
        return f_dentry->d_ino->i_sup;
    }
    [[nodiscard]] auto absolute_path() const noexcept -> vintix::string
    {
        return f_dentry->absolute_path();
    }
    [[nodiscard]] constexpr auto ssize() const noexcept -> off_t { return f_dentry->d_ino->i_size; }
    [[nodiscard]] constexpr auto name() const noexcept -> const vintix::string &
    {
        return f_dentry->d_name;
    }
    [[nodiscard]] constexpr auto uid() const noexcept -> uid_t { return f_dentry->d_ino->i_uid; }
    [[nodiscard]] constexpr auto gid() const noexcept -> gid_t { return f_dentry->d_ino->i_gid; }
    [[nodiscard]] constexpr auto modified_time() const noexcept -> time_t
    {
        return f_dentry->d_ino->i_mtime;
    }
    [[nodiscard]] constexpr auto mode() const noexcept -> mode_t { return f_dentry->d_ino->mode(); }
    [[nodiscard]] constexpr auto is_dir() const noexcept -> bool
    {
        return f_dentry->d_ino->is_dir();
    }
    [[nodiscard]] constexpr auto is_device() const noexcept -> bool
    {
        return f_dentry->d_ino->is_device();
    }
    [[nodiscard]] constexpr auto is_regular_file() const noexcept -> bool
    {
        return f_dentry->d_ino->is_regular_file();
    }
    [[nodiscard]] constexpr auto is_socket() const noexcept -> bool
    {
        return f_dentry->d_ino->is_socket();
    }
    [[nodiscard]] constexpr auto is_symlink() const noexcept -> bool
    {
        return f_dentry->d_ino->is_symlink();
    }
    [[nodiscard]] constexpr auto is_blkdev() const noexcept -> bool
    {
        return f_dentry->d_ino->is_blkdev();
    }
    [[nodiscard]] constexpr auto is_chrdev() const noexcept -> bool
    {
        return f_dentry->d_ino->is_chrdev();
    }
    [[nodiscard]] constexpr auto suid_is_set() const noexcept -> bool
    {
        return f_dentry->d_ino->suid_is_set();
    }
    [[nodiscard]] constexpr auto sgid_is_set() const noexcept -> bool
    {
        return f_dentry->d_ino->sgid_is_set();
    }
    [[nodiscard]] constexpr auto is_readonly() const noexcept -> bool
    {
        return (f_oflags & O_RDONLY);
    }
    [[nodiscard]] auto is_readable() const noexcept -> bool
    {
        return f_dentry->d_ino->is_readable();
    }
    [[nodiscard]] auto is_writable() const noexcept -> bool
    {
        return f_dentry->d_ino->is_writable();
    }
    [[nodiscard]] auto is_executable() const noexcept -> bool
    {
        return f_dentry->d_ino->is_executable();
    }
    DELETE_COPYABLE_AND_MOVABLE(file)
};

using raw_file = file;
namespace vintix::noleak {
    using file = vintix::unique_ptr<raw_file, true>;
}

extern "C" {
    inline auto generic_file_close(file *fp) noexcept -> errno
    {
        kfree(fp);
        return ENOERR;
    }
}

#endif // VINTIX_FILESYSTEMS_FILE_H
