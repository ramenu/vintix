/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_FS_H
#define VINTIX_FS_H

#include <vintix/kernel/lib/errno.h>
#include <vintix/kernel/lib/errptr.h>
#include <vintix/kernel/lib/types.h>
#include <vintix/kernel/posix/fcntl.h>
#include <vintix/kernel/posix/stat.h>

#define INF_FILE_SIZE 0

// Used to signal EOF
#define EOF 0

// The file offset is set to 'offset' bytes.
#define SEEK_SET 1

// The file offset is moved to its current location plus
// 'offset' bytes.
#define SEEK_CUR 2

// The file offset is set to the size of the file plus
// 'offset' bytes.
#define SEEK_END 4

// use this constant when supplying the 'name' value of kmount
// if you're mounting on a tmpfs
#define S_NOTMOUNTED ""

#define ROOT_UID 0
#define ROOT_GID 0

#define PATH_MAX 64
#define NAME_MAX 32

#define MNT_RDONLY                                                                                 \
    1 /* The file system should be treated as read-only; even the super-user may not write on it.  \
       */
#define MNT_NOEXEC (1 << 1)  /* Do not allow files to be executed from the filesystem. */
#define MNT_NODEV (1 << 2)   /* Do not interpret special files on the system. */
#define MNT_NOSUID (1 << 3)  /* Do not honor setuid or setgid bits on files when executing them. */
#define MNT_NOATIME (1 << 4) /* Never update access time in the file system. */
#define MNT_RELATIME                                                                               \
    (1 << 5) /* Update access time on write and change. This helps programs verify that the file   \
                has been read after written to work. */
#define MNT_SYMPERM                                                                                \
    (1 << 6) /* Recognize the permission of symbolic links when reading or traversing. */

/* Should be set if the mount is dirty. This is a private flag which should only be modified by the
 * kernel internally. */
#define MNT_DIRTY (1 << 7)

/* Should be set if mounted on a tmpfs. This is a private flag which should only be modified by the
 * kernel internally. */
#define MNT_TMP (1 << 8)

/* Should be set if the filesystem does not have support for directories. */
#define MNT_NODIR (1 << 9)

/* Should be set if the kernel is mounting the filesystem */
#define MNT_KERNEL (1 << 10)

class file;
class superblock;
template <typename T> class result;

namespace vintix {
    template <typename T> class vector;
    class string;
} // namespace vintix

struct fs_context {
    u32 flags;
    // these fields are only relevant when reading from a disk (i.e., READ_FROM_DISK is set)
    dev_t fs_dev;
    // these fields are only relevant when reading from a virtual address (i.e., READ_FROM_MEMORY is
    // set)
    v_addr vaddr_offset;
};

enum class fs_type {
    GENERIC_FS = 0,
    DEVFS = 1,
    TMPFS = 2,
};

struct genfs {
    const char *name;
    errptr<superblock> (*init_fs_context)(const fs_context &context) noexcept;
    u16 flags;
    fs_type type {};
};

auto ls(const file *) noexcept -> result<vintix::vector<vintix::string>>;

extern "C" {
    /**
     * Registers a new filesystem with the virtual file system (VFS).
     *
     * This method is responsible for adding a new filesystem to the internal
     * list of available filesystems. It ensures there is an available slot and
     * populates it with the provided `genfs` structure details such as name,
     * flags, initialization function, and type.
     *
     * @param fs A `genfs` structure representing the filesystem to register,
     *           including its name, flags, initialization context, and type.
     * @return Returns `ENOERR` if the filesystem is successfully registered.
     *         Returns `ENOMEM` if there is no available slot to register the filesystem.
     */
    auto register_filesystem(const genfs &) noexcept -> errno;

    /**
     * Unregisters an existing filesystem from the virtual file system (VFS).
     *
     * This method removes the specified filesystem identified by its name
     * from the internal list of registered filesystems. If the given filesystem
     * is found, it is unregistered and its associated resources are cleaned up.
     *
     * @param fs_name The name of the filesystem to unregister.
     * @return Returns `ENOERR` if the filesystem is successfully unregistered.
     *         Returns `ENOENT` if the specified filesystem is not found.
     */
    auto unregister_filesystem(const char *) noexcept -> errno;

    auto generic_file_lseek(const file *, off_t, int) noexcept -> off_t;

    /**
     * Opens a file or directory using the provided flags and mode.
     *
     * This function attempts to access or create a file or directory based on the
     * specified path, flags, and mode. The flags determine the access mode and
     * usage, while the mode specifies the permissions if the file is created. The
     * function performs validation on the flags, mode, and path, and handles special
     * cases for directories and executable files.
     *
     * @param p The path to the file or directory to open. Must be a valid string.
     * @param flags A combination of flags specifying the access mode and options
     *              for opening the file, such as `O_RDONLY`, `O_WRONLY`, `O_CREAT`, etc.
     * @param mode The file permission mode applied when creating a new file. Ignored
     *             if the file already exists. Should not exceed `0777`.
     * @return An `errptr<file>` structure representing the opened file or directory.
     *         Returns an error pointer with appropriate error codes (e.g., `-EINVAL`,
     *         `-ENOMEM`, `-EISDIR`, etc.) if the operation fails.
     */
    auto kopen(const char *path, int flags, mode_t mode = 0) noexcept -> errptr<file>;

    /**
     * Performs I/O operations on the given file object.
     *
     * This method handles reading from or writing to the provided file object
     * based on the specified parameters. It accounts for the buffer, size of
     * operation, and whether the operation is read or write.
     *
     * @param file A pointer to the file object on which the I/O operation is to be performed.
     * @param buffer A pointer to the memory buffer used for reading data from or writing data to
     * the file.
     * @param size The size, in bytes, of the data to be read or written during the operation.
     * @param is_write A boolean value indicating the operation type; `true` for write operation,
     * `false` for read operation.
     * @return The number of bytes successfully read or written, or a negative value if an error
     * occurred.
     */
    auto kio(file *, void *, usize, bool) noexcept -> ssize;

    /**
     * Reads data from an open file and writes it into a user-provided buffer.
     *
     * This method reads up to `count` bytes from the file pointed to by `fp`
     * starting at the current file offset. The data is written into the buffer
     * specified by `buf`. The file offset is incremented by the number of bytes
     * read. Special handling is applied for directory files, write-only files,
     * and cases where the end-of-file (EOF) or other edge conditions are encountered.
     *
     * @param fp A pointer to a `file` structure representing the open file from
     *           which data is to be read.
     * @param buf A pointer to the buffer where the read data will be stored.
     * @param count The maximum number of bytes to read from the file.
     * @return Returns the number of bytes read on success. If an error occurs,
     *         a negative error code is returned:
     *         - `-EISDIR` if attempting to read from a directory.
     *         - `-EPERM` if permissions are insufficient.
     *         - `-EINVAL` if `count` exceeds the allowed maximum value.
     *         - `-EOVERFLOW` if file offset or size constraints are violated.
     */
    auto kread(file *, void *buf, usize count) noexcept -> ssize;

    /**
     * Writes data to a file.
     *
     * This method writes `count` bytes of data from the buffer `buf` into the file
     * represented by the pointer `fp`. The operation considers various constraints
     * including file permissions, file size limits, and special device handling.
     *
     * @param fp A pointer to the `file` structure representing the file descriptor
     *           where data will be written.
     * @param buf A pointer to the buffer containing the data to be written to the file.
     *            The caller must ensure this buffer remains valid for the duration of
     *            the write operation.
     * @param count The number of bytes to write from the buffer to the file. It must
     *              not exceed the maximum allowable size or file size limits.
     * @return Returns the number of bytes successfully written. In case of an error,
     *         it returns a negative error code:
     *         - `EPERM`: If the file is read-only or not writable.
     *         - `EROFS`: If the filesystem is mounted as read-only.
     *         - `EISDIR`: If attempting to write to a directory.
     *         - `EINVAL`: If `count` exceeds implementation-defined limits.
     *         - `EFBIG`: If the write would exceed the maximum file size allowed.
     *         - `ENOMEM`: If memory allocation fails during the operation.
     *         - Other negative codes depending on specific implementation errors.
     */
    auto kwrite(file *, const void *buf, usize count) noexcept -> ssize;

    /**
     * Adjusts the file offset of the specified file according to the input parameters.
     *
     * This function calls the file's lseek implementation to modify its current
     * position (offset) based on the specified values. The offset is adjusted
     * relative to the file's beginning, current position, or end, depending on the
     * `whence` parameter.
     *
     * @param fp A pointer to the file structure whose offset is to be adjusted.
     *           Must not be null and should reference a valid open file.
     * @param offset The offset value used to adjust the file's current position.
     *               The interpretation of this value depends on the `whence` parameter.
     * @param whence An integer indicating how the `offset` is applied:
     *               - `SEEK_SET`: The offset is set relative to the start of the file.
     *               - `SEEK_CUR`: The offset is set relative to the current position.
     *               - `SEEK_END`: The offset is set relative to the end of the file.
     * @return Returns the new offset location in the file if the operation is successful.
     *         If an error occurs, the method may return an implementation-defined
     *         negative value.
     */
    auto klseek(file *, off_t offset, int whence) noexcept -> off_t;

    /**
     * Synchronizes the state of a file with its associated storage and metadata.
     *
     * This function ensures that changes made to a file or its parent directory's
     * metadata are synchronized with the underlying storage device. It updates
     * the last modified time and ensures that dirty inode flags are cleared after
     * syncing. Additionally, it calls the specific synchronization function in the
     * file or parent directory's vtable if available.
     *
     * @param fp A pointer to the `file` structure representing the file to be synchronized.
     * @return Returns `ENOERR` if synchronization is successful.
     *         Returns `EPERM` if the file or its parent is read-only.
     *         Returns `EROFS` if the file resides on a read-only filesystem.
     */
    auto ksync(file *) noexcept -> errno;

    /**
     * Closes a file and releases associated resources.
     *
     * This method is responsible for performing cleanup operations associated
     * with a file structure (e.g., reducing reference counts, deallocations)
     * and invoking the filesystem-specific `close` handler if implemented.
     * It ensures the file descriptor and related objects are properly released
     * to avoid resource leaks.
     *
     * @param fp A pointer to the `file` structure representing the file to be closed.
     * @return Returns `ENOERR` if the file is successfully closed and all resources are released.
     *         May return other error codes if the filesystem-specific `close` operation fails.
     */
    auto kclose(file *) noexcept -> errno;

    auto get_fs(const char *) noexcept -> const genfs *;
    auto file_from_dev(dev_t) noexcept -> file *;

    /**
     * Populates the provided `stat` structure with metadata about a file.
     *
     * This method extracts critical metadata from the given file pointer, such as
     * device ID, inode number, file size, mode, and other attributes, and populates
     * the corresponding fields of the `stat` structure. It ensures the data is
     * correctly filled for further processing or system calls.
     *
     * @param fp Pointer to the file object whose metadata is being queried. It must
     *           be a valid pointer to an open file.
     * @param st Pointer to the `stat` structure where the extracted metadata
     *           will be stored. This pointer must not be null.
     * @return Returns `ENOERR` on success, indicating that the `stat` structure
     *         was successfully populated. If there is an error, suitable error
     *         codes are returned.
     */
    auto kstat(file *, stat *) noexcept -> errno;

    /**
     * Creates a new directory with the specified path and mode.
     *
     * This method attempts to create a directory at the given path with the specified
     * permissions. It uses the `kopen` function with flags to ensure that the operation
     * is exclusive and intended for directory creation. If the directory creation
     * is successful, it returns a valid file pointer wrapped in an `errptr`.
     *
     * @param path A constant character pointer representing the path where the directory
     *             should be created.
     * @param mode The mode (permissions) to set for the newly created directory.
     * @return An `errptr<file>` containing the created directory's file pointer if successful.
     *         If the operation fails, it returns an appropriate error pointer.
     */
    inline auto kmkdir(const char *path, mode_t mode) noexcept -> errptr<file>
    {
        return kopen(path, O_CREAT | O_WRONLY | O_EXCL | O_DIRECTORY, mode);
    }

    /**
     * Creates and opens a new file at the specified path with the given mode.
     *
     * This method is a convenience wrapper around the `kopen` method for creating
     * and opening a file. It opens the file in write-only mode, truncating it if it
     * already exists, or creating it if it does not. The file is created with the
     * specified access permissions.
     *
     * @param path A C-style string to the path where the file is to be created.
     * @param mode The file permissions to use for the newly created file.
     * @return Returns an `errptr<file>` object, which may indicate either the
     *         successfully created file or an error status.
     */
    inline auto kcreat(const char *path, mode_t mode) noexcept -> errptr<file>
    {
        return kopen(path, O_CREAT | O_WRONLY | O_TRUNC, mode);
    }
}

#endif // VINTIX_FS_H
