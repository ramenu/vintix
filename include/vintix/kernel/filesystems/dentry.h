/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_FILESYSTEMS_DENTRY_H
#define VINTIX_FILESYSTEMS_DENTRY_H

#include <vintix/kernel/filesystems/fs.h>
#include <vintix/kernel/filesystems/inode.h>
#include <vintix/kernel/lib/cxxstring.h>
#include <vintix/kernel/lib/list.h>

// flags for get_dentry
#define DENTRY_SEARCH_CACHE 1
#define DENTRY_DONT_CACHE (1 << 1)
#define DENTRY_CHECK_PERMS (1 << 2)

struct dentry {
    inode *d_ino { nullptr };
    dentry *d_parent { nullptr };
    vintix::string d_name { nullptr };
    vintix::list<dentry> d_entries { nullptr };

    constexpr dentry() noexcept = default;
    constexpr dentry(dentry &&) noexcept = default;
    static void dtor(void *s) noexcept
    {
        auto *self = static_cast<dentry *>(s);
        if (self->d_ino != nullptr) {
            KASSERT(self->d_ino->vtable != nullptr);
            self->d_ino->vtable->free(self->d_ino);
            self->d_ino = nullptr;
        }
        self->d_name.~string();
        self->d_entries.~list();
        self->d_parent = nullptr;
    }
    ~dentry() noexcept { dtor(this); }
    auto operator=(dentry &&other) noexcept -> dentry &
    {
        if (this == &other)
            return *this;

        d_ino = other.d_ino;
        d_parent = other.d_parent;
        d_name = vintix::move(other.d_name);
        d_entries = vintix::move(other.d_entries);

        other.d_ino = nullptr;
        other.d_parent = nullptr;

        return *this;
    }
    [[nodiscard]] constexpr auto parent() const noexcept -> const dentry * { return d_parent; }
    template <vintix::String S>
    [[nodiscard]] constexpr auto operator==(const S &name) const noexcept -> bool
    {
        return d_name == name;
    }
    [[nodiscard]] constexpr auto root() const noexcept -> const dentry *
    {
        const dentry *cur = this;

        while (cur->d_parent != nullptr)
            cur = cur->d_parent;

        return cur;
    }
    [[nodiscard]] constexpr auto is_root_dir() const noexcept -> bool
    {
        return this->d_name.size() == 1;
    }

    [[nodiscard]] auto absolute_path() const noexcept -> vintix::string
    {
        vintix::string path {};
        path.reserve(PATH_MAX);
        const dentry *cur = this;

        while (cur != nullptr) {
            if (!path.is_empty() || cur->is_root_dir())
                if (path.prepend('/') != ENOERR)
                    return nullptr;

            if (!cur->is_root_dir())
                if (path.prepend(cur->d_name) != ENOERR)
                    return nullptr;

            cur = cur->d_parent;
        }

        return path;
    }

    template <vintix::String S> [[nodiscard]] constexpr auto search(const S &s) noexcept -> dentry *
    {
        if (this->d_name == s)
            return this;

        if (d_entries == nullptr)
            return nullptr;

        list_node_g<dentry> *vertex = d_entries.unsafe_front();

        while (vertex != nullptr) {
            KASSERT(vertex->n_data.d_name != nullptr);
            if (vertex->n_data.d_name == s) {
                return &vertex->n_data;
            }

            vertex = vertex->n_next;
        }

        return nullptr;
    }
    void remove(dentry *entry) noexcept { d_entries.remove(entry); }
    auto insert(dentry &&entry) noexcept -> errptr<dentry>
    {
        if constexpr (!DEBUG_BUILD) {
            return d_entries.insert(vintix::move(entry));
        } else {
            errptr<dentry> ent = d_entries.insert(vintix::move(entry));

            if (ent.is_ok()) {
                KASSERT(ent.get()->d_ino != nullptr);
                KASSERT(ent.get()->d_ino->vtable != nullptr);
                KASSERT(ent.get()->d_name != nullptr);
            }

            return ent;
        }
    }
    DELETE_COPYABLE(dentry)
};

static_assert(sizeof(dentry) == 20);

void debug_mount_graph(const dentry *root) noexcept;
auto change_dentry_root(inode *ino, const char *name) noexcept -> errno;
auto get_dentry_root() noexcept -> dentry *;
void decrement_dentry_count(dentry *entry) noexcept;
void increment_dentry_parent_count(dentry *) noexcept;
auto add_dentry(dentry *, vintix::string_view name, inode *) noexcept -> errptr<dentry>;
auto get_dentry(vintix::string_view path, int flags = 0) noexcept -> errptr<dentry>;
auto get_dentry(dentry *, vintix::string_view name) noexcept -> errptr<dentry>;
auto remove_dentry(dentry *) noexcept -> errno;
auto remove_dentry(vintix::string_view path) noexcept -> errno;

#endif // VINTIX_FILESYSTEMS_DENTRY_H
