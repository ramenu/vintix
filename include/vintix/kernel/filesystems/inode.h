/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_FILESYSTEMS_INODE_H
#define VINTIX_FILESYSTEMS_INODE_H

#include <vintix/kernel/core/process.h>
#include <vintix/kernel/filesystems/fs.h>
#include <vintix/kernel/lib/errno.h>
#include <vintix/kernel/lib/result.h>
#include <vintix/kernel/lib/types.h>
#include <vintix/kernel/lib/vector.h>

#define INODE_IS_DIRTY 1       /* needs to be written back to disk */
#define INODE_PIPE (1 << 1)    /* ??? */
#define INODE_MOUNTED (1 << 2) /* set if the file is mounted */
#define INODE_SEEK (1 << 3)    /* set on LSEEK, cleared on read/write */

class inode;
struct dentry;
class file;
class superblock;

enum inode_mode : mode_t {
    // socket
    SOCKET = 0140000,

    // symlink
    SYMLINK = 0120000,

    // regular file
    REGULAR_FILE = 0100000,

    // block special file
    BLOCK_SPECIAL = 0060000,

    // directory
    DIRECTORY = 0040000,

    // character special file
    CHAR_SPECIAL = 0020000,

    // setuid
    SETUID = 0004000,

    // setgid
    SETGID = 0002000,

    // read bit (user)
    USRREAD = 0000400,

    // write bit (user)
    USRWRIT = 0000200,

    // execute bit (user)
    USREXEC = 0000100,

    // read bit (group)
    GRPREAD = 0000010,

    // write bit (group)
    GRPWRIT = 0000020,

    // execute bit (group)
    GRPEXEC = 0000040,

    // read bit (other)
    OTHREAD = 0000004,

    // write bit (other)
    OTHWRIT = 0000002,

    // execute bit (other)
    OTHEXEC = 0000001,

    USRRWX = USRREAD | USRWRIT | USREXEC,
    GRPRWX = GRPREAD | GRPWRIT | GRPEXEC,
    OTHRWX = OTHREAD | OTHWRIT | OTHEXEC,
};

template <typename T> class errptr;

struct inode_vtable {
    errptr<inode> (*create)(const dentry *, const char *name);
    errno (*rm)(const dentry *);
    errptr<inode> (*mkdir)(const dentry *, const char *name);
    errno (*rmdir)(const dentry *);
    errptr<inode> (*lookup)(const dentry *, const char *);
    errno (*sync)(const dentry *);
    errptr<file> (*open)(inode *);
    result<vintix::vector<vintix::string>> (*ls)(const dentry *);
    void (*free)(inode *);
};

class inode {
  public:
    constexpr inode() noexcept = default;
    explicit inode(const superblock &super) noexcept;
    ~inode() noexcept = default;
    void set_dirty() noexcept { i_flags |= INODE_IS_DIRTY; }
    [[nodiscard]] constexpr auto mode() const noexcept -> const mode_t & { return i_mode; }
    off_t i_size {};
    ino_t i_num {};
    uid_t i_uid {};
    gid_t i_gid {};
    time_t i_mtime {};
    mode_t i_mode {};
    nlink_t i_nlinks {};
    superblock *i_sup {};
    u8 i_flags {};
    u8 i_count {};
    [[nodiscard]] auto is_readable() const noexcept -> bool
    {
        if (i_mode & OTHREAD)
            return true;

        const uid_t uid = process::running()->uid();
        if ((i_mode & USRREAD) && (uid == ROOT_UID || uid == i_uid))
            return true;

        const gid_t gid = process::running()->gid();
        if ((i_mode & GRPREAD) && (gid == ROOT_GID || gid == i_gid))
            return true;

        return false;
    }
    [[nodiscard]] auto is_writable() const noexcept -> bool
    {
        if (i_mode & OTHWRIT)
            return true;

        const uid_t uid = process::running()->uid();
        if ((i_mode & USRWRIT) && (uid == ROOT_UID || uid == i_uid))
            return true;

        const gid_t gid = process::running()->gid();
        if ((i_mode & GRPWRIT) && (gid == ROOT_GID || gid == i_gid))
            return true;

        return false;
    }
    [[nodiscard]] auto is_executable() const noexcept -> bool
    {
        if (i_mode & OTHEXEC)
            return true;

        const uid_t uid = process::running()->uid();
        if ((i_mode & USREXEC) && (uid == ROOT_UID || uid == i_uid))
            return true;

        const gid_t gid = process::running()->gid();
        if ((i_mode & GRPEXEC) && (gid == ROOT_GID || gid == i_gid))
            return true;

        return false;
    }
    [[nodiscard]] constexpr auto is_dirty() const noexcept -> bool
    {
        return (i_flags & INODE_IS_DIRTY);
    }
    [[nodiscard]] constexpr auto is_mounted() const noexcept -> bool
    {
        return (i_flags & INODE_MOUNTED);
    }
    [[nodiscard]] constexpr auto is_seeking() const noexcept -> bool
    {
        return (i_flags & INODE_SEEK);
    }
    [[nodiscard]] constexpr auto is_dir() const noexcept -> bool
    {
        // Since DIRECTORY conflicts with BLOCK_SPECIAL, device files are
        // marked as regular files. Without this check, block devices would
        // be incorrectly marked as regular files.
        if (i_mode & REGULAR_FILE)
            return false;
        return (i_mode & DIRECTORY);
    }
    [[nodiscard]] constexpr auto is_device() const noexcept -> bool
    {
        return is_blkdev() || is_chrdev();
    }
    [[nodiscard]] constexpr auto is_regular_file() const noexcept -> bool
    {
        // Remember, BLOCK_SPECIAL and CHAR_SPECIAL files aren't actually
        // regular files. We just mark them as so to prevent bit conflicts.
        if (is_device())
            return false;

        return (mode() & REGULAR_FILE);
    }
    [[nodiscard]] constexpr auto is_socket() const noexcept -> bool { return (mode() & SOCKET); }
    [[nodiscard]] constexpr auto is_symlink() const noexcept -> bool { return (mode() & SYMLINK); }
    [[nodiscard]] constexpr auto is_blkdev() const noexcept -> bool
    {
        return (mode() & BLOCK_SPECIAL);
    }
    [[nodiscard]] constexpr auto is_chrdev() const noexcept -> bool
    {
        return (mode() & CHAR_SPECIAL);
    }
    [[nodiscard]] constexpr auto suid_is_set() const noexcept -> bool { return (mode() & SETUID); }
    [[nodiscard]] constexpr auto sgid_is_set() const noexcept -> bool { return (mode() & SETGID); }
    const inode_vtable *vtable {};
    DELETE_COPYABLE_AND_MOVABLE(inode)
};

static_assert(sizeof(inode) == 32);

extern "C" {
    inline void generic_inode_free(inode *ino) noexcept { kfree(ino); }
}

#endif // VINTIX_FILESYSTEMS_INODE_H
