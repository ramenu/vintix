/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_BIOS_H
#define VINTIX_BIOS_H

#include <vintix/kernel/lib/types.h>

static constexpr p_addr REAL_MODE_ADDRESS_SPACE_START = 0x0;
// Remember, with segmentation, physical addresses are formed through the following
// formula: (A * 0x10) + B
static constexpr p_addr REAL_MODE_ADDRESS_SPACE_END = 0xffff * 0x10 + 0xffff;

extern "C" {
    auto _pci_mechanism_supported() noexcept -> u32;
}

#endif // VINTIX_BIOS_H
