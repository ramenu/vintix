/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_COLOR_H
#define VINTIX_COLOR_H

#include <vintix/kernel/core/io.h>

#define COLOR_RESET "\033[0m"
#define COLOR_BLACK "\033[30m"         /* Black */
#define COLOR_RED "\033[31m"           /* Red */
#define COLOR_GREEN "\033[32m"         /* Green */
#define COLOR_YELLOW "\033[33m"        /* Yellow */
#define COLOR_BLUE "\033[34m"          /* Blue */
#define COLOR_MAGENTA "\033[35m"       /* Magenta */
#define COLOR_CYAN "\033[36m"          /* Cyan */
#define COLOR_WHITE "\033[37m"         /* White */
#define COLOR_LIGHT_BLACK "\033[90m"   /* Bright Black */
#define COLOR_LIGHT_RED "\033[91m"     /* Bright Red */
#define COLOR_LIGHT_GREEN "\033[92m"   /* Bright Green */
#define COLOR_LIGHT_YELLOW "\033[93m"  /* Bright Yellow */
#define COLOR_LIGHT_BLUE "\033[94m"    /* Bright Blue */
#define COLOR_LIGHT_MAGENTA "\033[95m" /* Bright Magenta */
#define COLOR_LIGHT_CYAN "\033[96m"    /* Bright Cyan */
#define COLOR_LIGHT_WHITE "\033[97m"   /* Bright White */

constexpr auto get_fbfgcolor_asstr(FBFGColor fg) noexcept -> const char *
{
    switch (fg) {
        case FBFGColor::Red:
            return COLOR_RED;
        case FBFGColor::LightRed:
            return COLOR_LIGHT_RED;
        case FBFGColor::Blue:
            return COLOR_BLUE;
        case FBFGColor::LightBlue:
            return COLOR_LIGHT_BLUE;
        case FBFGColor::Cyan:
            return COLOR_CYAN;
        case FBFGColor::LightCyan:
            return COLOR_LIGHT_CYAN;
        case FBFGColor::Black:
            return COLOR_BLACK;
        case FBFGColor::DarkGrey:
            return COLOR_LIGHT_BLACK;
        case FBFGColor::LightGrey:
            return COLOR_LIGHT_WHITE;
        case FBFGColor::Brown:
            return COLOR_YELLOW;
        case FBFGColor::LightBrown:
            return COLOR_LIGHT_YELLOW;
        case FBFGColor::Green:
            return COLOR_GREEN;
        case FBFGColor::LightGreen:
            return COLOR_LIGHT_GREEN;
        case FBFGColor::Magenta:
            return COLOR_MAGENTA;
        case FBFGColor::LightMagenta:
            return COLOR_LIGHT_MAGENTA;
        case FBFGColor::White:
            return COLOR_WHITE;
    }
}

#endif /* VINTIX_COLOR_H */
