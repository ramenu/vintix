/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_KERNEL_SYSCALL_H
#define VINTIX_KERNEL_SYSCALL_H

#include <vintix/kernel/lib/concepts.h>
#include <vintix/kernel/lib/types.h>

struct stat;

class x86_register {
  private:
    u32 reg;

  public:
    [[nodiscard]] constexpr auto get() const noexcept -> u32 { return reg; }

    template <vintix::Integral T> [[nodiscard]] constexpr auto get() const noexcept -> T
    {
        return static_cast<T>(reg);
    }

    template <vintix::Pointer P> [[nodiscard]] constexpr auto as_ptr() const noexcept -> P
    {
        return unsafe_toptr<P>(reg);
    }
};

class syscall_regs {
  private:
    x86_register s_eax {};
    x86_register s_ebx {};
    x86_register s_ecx {};
    x86_register s_edx {};
    x86_register s_esi {};
    x86_register s_edi {};
    x86_register s_ebp {};
    x86_register s_eip {};
    u32 s_cs {};
    u32 s_eflags {};
    x86_register s_esp {};
    u32 s_ss {};

  public:
    [[nodiscard]] constexpr auto eax() const noexcept -> x86_register { return s_eax; }
    [[nodiscard]] constexpr auto ebx() const noexcept -> x86_register { return s_ebx; }
    [[nodiscard]] constexpr auto ecx() const noexcept -> x86_register { return s_ecx; }
    [[nodiscard]] constexpr auto edx() const noexcept -> x86_register { return s_edx; }
    [[nodiscard]] constexpr auto esi() const noexcept -> x86_register { return s_esi; }
    [[nodiscard]] constexpr auto edi() const noexcept -> x86_register { return s_edi; }
    [[nodiscard]] constexpr auto ebp() const noexcept -> x86_register { return s_ebp; }
    [[nodiscard]] constexpr auto eip() const noexcept -> x86_register { return s_eip; }
    [[nodiscard]] constexpr auto esp() const noexcept -> x86_register { return s_esp; }
};

extern "C" {
    auto sys_open(const char *, int, mode_t) noexcept -> int;
    auto sys_creat(const char *, mode_t) noexcept -> int;
    auto sys_mkdir(const char *, mode_t) noexcept -> int;
    auto sys_read(int, void *, size_t) noexcept -> ssize_t;
    auto sys_write(int, const void *, size_t) noexcept -> ssize_t;
    void sys_exit(int) noexcept;
    auto sys_mount(const char *, const char *, const char *, int) noexcept -> int;
    auto sys_umount(const char *) noexcept -> int;
    auto sys_close(int) noexcept -> int;
    auto sys_mmap(void *, usize, int, int, fd_t, off_t) noexcept -> void *;
    auto sys_getuid() noexcept -> uid_t;
    auto sys_getgid() noexcept -> gid_t;
    auto sys_getpid() noexcept -> pid_t;
    auto sys_lseek(fd_t, off_t, int) noexcept -> off_t;
    auto sys_chdir(const char *) noexcept -> int;
    auto sys_chroot(const char *) noexcept -> int;
    auto sys_sched_yield(const syscall_regs &) noexcept -> int;
    auto sys_umask(mode_t) noexcept -> mode_t;
    auto sys_munmap(void *, usize) noexcept -> int;
    auto sys_kill(pid_t, int) noexcept -> int;
    auto sys_wait(int *) noexcept -> pid_t;
    auto sys_waitpid(pid_t, int *, int) noexcept -> pid_t;
    auto sys_stat(const char *__restrict__, stat *__restrict__) noexcept -> int;
    auto sys_lstat(const char *, struct stat *__restrict__) noexcept -> int;
    auto sys_fstat(int, struct stat *) noexcept -> int;
    auto sys_chmod(const char *, mode_t) noexcept -> int;
    auto sys_fchmod(int, mode_t) noexcept -> int;
    auto sys_chown(const char *, uid_t, gid_t) noexcept -> int;
    auto sys_lchown(const char *, uid_t, gid_t) noexcept -> int;
    auto sys_fchown(int, uid_t, gid_t) noexcept -> int;
    auto sys_mprotect(void *, usize, int) noexcept -> int;
}

#endif // VINTIX_KERNEL_SYSCALL_H
