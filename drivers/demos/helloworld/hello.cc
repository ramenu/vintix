#include <vintix/kernel/core/io.h>
#include <vintix/kernel/lib/errno.h>

extern "C" volatile const int _DRV_PRIORITY = 1;

extern "C" auto _drv_entry() noexcept -> errno
{
    kprintf("Hello world!\n");
    return ENOERR;
}
