/**
 * Copyright (C) 2023-2024 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 *
 *
 * References:
 * 		OSDev Article (https://wiki.osdev.org/ATA_PIO_Mode)
 * 		ATA/ATAPI Standard 1994 (https://hddguru.com/documentation/2006.01.27-ATA-ATAPI-1/)
 *
 */

#include "vintix/kernel/filesystems/file.h"
#include "vintix/kernel/filesystems/inode.h"
#include "vintix/kernel/lib/types.h"
#include <vintix/kernel/core/blkdev.h>
#include <vintix/kernel/core/dev.h>
#include <vintix/kernel/core/idt.h>
#include <vintix/kernel/core/io.h>
#include <vintix/kernel/core/kassert.h>
#include <vintix/kernel/core/pci.h>
#include <vintix/kernel/core/pit.h>
#include <vintix/kernel/core/vmm.h>
#include <vintix/kernel/lib/errno.h>
#include <vintix/kernel/lib/klog.h>
#include <vintix/kernel/lib/limits.h>
#include <vintix/kernel/lib/math.h>
#include <vintix/kernel/lib/string.h>
#include <vintix/kernel/lib/unique_ptr.h>
#include <vintix/kernel/lib/units.h>
#include <vintix/kernel/misc/color.h>

extern "C" const u32 _DRV_PRIORITY = 0;

static constexpr bool ATA_ENABLE_LOGS = SHOW_LOGS;
static constexpr u8 DRIVER_VERSION_MAJOR = 0x01;

enum : u16 {
    TOTAL_IDE_DRIVES = 4,
    ATAPI_PACKET_SIZE = 12,
    TOTAL_IDE_CHANNEL_REGISTERS = 2,
    SECTOR_DEFAULT_DATA_SIZE = 512,

    // I/O Ports
    ATA_PRIMARY_IO_BASE_PORT = 0x1F0,
    ATA_SECONDARY_IO_BASE_PORT = 0x170,
    ATA_PRIMARY_DC_BASE_PORT = 0x3F6,
    ATA_SECONDARY_DC_BASE_PORT = 0x376,
};

enum ata_ctrl : u8 {
    // Always set to zero
    ATA_CTRL_NA = 0,
    // Set this to stop the current device from sending interrupts
    ATA_CTRL_NIEN = 1 << 1,
    // Set, then clear (after 5us), this to do a 'Software Reset' on
    // all ATA drives on a bus, if one is misbehaving
    ATA_CTRL_SRST = 1 << 2,
    // BIT 3 (RESERVED)
    // BIT 4 (RESERVED)
    // BIT 5 (RESERVED)
    // BIT 6 (RESERVED)

    // Set this to read back the High Order Byte of the last LBA48
    // value sent to an IO port
    ATA_CTRL_HOB = 1 << 7,
};

// The Command/Status Port returns a bit mask referring to
// the status of a channel when read.
enum ata_status : u8 {
    ATA_SR_NUL = 0x00,  // Null
    ATA_SR_ERR = 0x01,  // Error
    ATA_SR_IDX = 0x02,  // Index
    ATA_SR_CORR = 0x04, // Corrected Data
    ATA_SR_DRQ = 0x08,  // Data Request Ready
    ATA_SR_DSC = 0x10,  // Drive Seek Complete
    ATA_SR_DWF = 0x20,  // Drive Write Fault
    ATA_SR_DRDY = 0x40, // Drive Ready

    // Set whenever the drive has access to the command block
    // registers. The host should not access the command block
    // register when 'BSY=1'. When BSY=1, a read of any command
    // block register shall return the contents of the status
    // register.
    ATA_SR_BSY = 0x80, // Busy
};

// The Features/Error port, which returns the most recent error
// upon read, has these possible bit masks.
enum ata_err : u8 {
    ATA_ER_NUL = 0x00,   // Null
    ATA_ER_AMNF = 0x01,  // No Address Mark
    ATA_ER_TK0NF = 0x02, // Track 0 Not Found
    ATA_ER_ABRT = 0x04,  // Command Aborted
    ATA_ER_MCR = 0x08,   // Media Change Request
    ATA_ER_IDNF = 0x10,  // ID Mark Not Found
    ATA_ER_MC = 0x20,    // Media Changed
    ATA_ER_UNC = 0x40,   // Uncorrectable Data
    ATA_ER_BBK = 0x80,   // Bad Block
};

// When you write to the Command/Status port, you are executing one of
// the commands below.
enum ata_cmd : u8 {
    ATA_CMD_READ_PIO = 0x20,
    ATA_CMD_READ_PIO_EXT = 0x24,
    ATA_CMD_READ_DMA = 0xC8,
    ATA_CMD_READ_DMA_EXT = 0x25,
    ATA_CMD_WRITE_PIO = 0x30,
    ATA_CMD_WRITE_PIO_EXT = 0x34,
    ATA_CMD_WRITE_DMA = 0xCA,
    ATA_CMD_WRITE_DMA_EXT = 0x35,
    ATA_CMD_CACHE_FLUSH = 0xE7,
    ATA_CMD_CACHE_FLUSH_EXT = 0xEA,
    ATA_CMD_PACKET = 0xA0,
    ATA_CMD_IDENTIFY_PACKET = 0xA1,
    ATA_CMD_IDENTIFY = 0xEC,

    // The commands below are for ATAPI devices
    ATAPI_CMD_READ = 0xA8,
    ATAPI_CMD_EJECT = 0x1B,
};

// The following definitions are used to read information from the
// identification space.
enum ata_ident : u8 {
    ATA_IDENT_DEVICETYPE = 0,
    ATA_IDENT_CYLINDERS = 2,
    ATA_IDENT_HEADS = 6,
    ATA_IDENT_SECTORS = 12,
    ATA_IDENT_SERIAL = 20,
    ATA_IDENT_MODEL = 54,
    ATA_IDENT_CAPABILITIES = 98,
    ATA_IDENT_FIELDVALID = 106,
    ATA_IDENT_MAX_LBA = 120,
    ATA_IDENT_COMMANDSETS = 164,
    ATA_IDENT_MAX_LBA_EXT = 200,
};

enum ata_reg : u8 {
    ATA_REG_DATA = 0x00,
    ATA_REG_ERROR = 0x01,
    ATA_REG_FEATURES = 0x01,
    ATA_REG_SECCOUNT0 = 0x02,
    ATA_REG_LBA0 = 0x03,
    ATA_REG_LBA1 = 0x04,
    ATA_REG_LBA2 = 0x05,
    ATA_REG_HDDEVSEL = 0x06,
    ATA_REG_COMMAND = 0x07,
    ATA_REG_STATUS = 0x07,
    ATA_REG_SECCOUNT1 = 0x08,
    ATA_REG_LBA3 = 0x09,
    ATA_REG_LBA4 = 0x0A,
    ATA_REG_LBA5 = 0x0B,
    ATA_REG_CONTROL = 0x0C,
    ATA_REG_ALTSTATUS = 0x0C,
    ATA_REG_DEVADDRESS = 0x0D,
};

enum ata_channel : u8 {
    ATA_PRIMARY = 0x00,
    ATA_SECONDARY = 0x01,
};

enum ata_drive : u8 {
    ATA_MASTER = 0x00,
    ATA_SLAVE = 0x01,
};

enum ata_capabilities : u16 {
    DMA_SUPPORTED = 1 << 8,
    LBA_SUPPORTED = 1 << 9,
};

enum class ata_drive_sel : u8 {
    ATA_MASTER = 0xA0,
    ATA_SLAVE = 0xB0,
};

struct ide_channel_register {
    u16 base {};  // I/O Base
    u16 ctrl {};  // Control Base
    u16 bmide {}; // Bus Master IDE
    u32 nIEN {};  // nIEN (No Interrupt)
};

struct ata_identify {
    // NOLINTBEGIN(cppcoreguidelines-avoid-c-arrays)
    u16 config;
    u16 num_cylinders;
    u16 RESERVED;
    u16 num_heads;
    u16 num_unformatted_bytes_per_track;
    u16 num_unformatted_bytes_per_sector;
    u16 num_sectors_per_track;
    u16 vendor_uniq1[3];
    char serial[20];
    u16 buffer_type;
    u16 buffer_size_512b;
    u16 ecc_bytes_avail;
    char firm_revision[8];
    char model[40];
    u16 word47;
    u16 perform_dword_io;
    ata_capabilities capabilities;
    u16 RESERVED;
    u16 pio_transfer_cycle_timing_mode;
    u16 dma_transfer_cycle_timing_mode;
    u16 words54_to_58_valid;
    u16 num_current_cylinders;
    u16 num_current_heads;
    u16 num_current_sectors_per_track;
    u32 capacity_in_sectors;
    u16 sector_settings;
    u32 num_user_addressable_sectors;
    u16 sword_dma_transfer_mode;
    u16 mword_dma_transfer_mode;
    u16 RESERVED[64];
    vintix::array<u16, 32> vendor_uniq2;
    u16 RESERVED[96];
    // NOLINTEND(cppcoreguidelines-avoid-c-arrays)
} __attribute__((packed));

static_assert(sizeof(ata_identify) == 512);

class ide_device final : public blkdev {
  public:
    u32 pci;                       // PCI bus, device, function
    ata_channel channel;           // 0 (Primary Channel) or 1 (Secondary Channel)
    ata_drive_sel drive;           // 0xA0 (Master Drive) or 0xB0 (Slave Drive)
    ata_capabilities capabilities; // Features
    u32 command_sets;              // Command Sets Supported
    u16 cylinders;                 // # of cylinders
    u16 sectors_per_track;         // # of sectors per track
    u16 heads;                     // # of heads

    [[nodiscard]] constexpr auto is_master_drive() const noexcept -> bool
    {
        return (this->drive == ata_drive_sel::ATA_MASTER);
    }
    [[nodiscard]] constexpr auto is_slave_drive() const noexcept -> bool
    {
        return !this->is_master_drive();
    }
    [[nodiscard]] constexpr auto is_primary_channel() const noexcept -> bool
    {
        return !this->channel;
    }
    [[nodiscard]] constexpr auto is_secondary_channel() const noexcept -> bool
    {
        return !this->is_primary_channel();
    }
};

// static constinit u8 atapi_packet[ATAPI_PACKET_SIZE] {0xA8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
// static constinit bool ide_irq_invoked = false;
static ide_device *ide_devices { nullptr };
static ide_channel_register *channels { nullptr };
static constexpr vintix::array drive_sel { ata_drive_sel::ATA_MASTER, ata_drive_sel::ATA_SLAVE };
static constexpr bool CHECK_DRQ = true;

// Returns the base I/O port associated with the channel
static auto ata_channel_ioprt(ata_channel channel) noexcept -> u16
{
    KASSERT(channels != nullptr);
    KASSERT(channel == ata_channel::ATA_PRIMARY || channel == ata_channel::ATA_SECONDARY);

    return channels[channel].base;
}

// Returns the base device control port associated with the channel
static auto ata_channel_ctrlprt(ata_channel channel) noexcept -> u16
{
    KASSERT(channels != nullptr);
    KASSERT(channel == ata_channel::ATA_PRIMARY || channel == ata_channel::ATA_SECONDARY);

    return channels[channel].ctrl;
}

static auto ata_status_is_ok(ata_status status, bool check_drq) noexcept -> bool;
static auto ata_poll(ata_channel channel, bool check_drq) noexcept -> bool;
static auto ata_identify_cmd(ata_channel channel, ata_drive_sel sel,
                             ata_identify *dst) noexcept -> ata_status;
static auto ata_drive_is_busy(ata_channel channel) noexcept -> bool;
static auto ata_chk_alt_status(ata_channel channel) noexcept -> ata_status;
static auto ata_read(const blkdev *self, void *buf, u32 sector_start,
                     u32 num_sectors) noexcept -> errno;
static auto ata_write(const blkdev *self, const void *buf, u32 sector_start,
                      u32 num_sectors) noexcept -> errno;
static auto ata_lba28_write(const ide_device &ata, const void *src, u32 lba,
                            u32 sector_count) noexcept -> errno;
static auto ata_lba28_read(const ide_device &ata, void *dst, u32 lba,
                           u32 sector_count) noexcept -> errno;
static auto ata_chs_write(const ide_device &ata, const void *src, u32 sector_count, u8 head,
                          u16 cylinder, u8 sector) noexcept -> errno;
static auto ata_chs_read(const ide_device &ata, void *dst, u32 sector_count, u8 head, u16 cylinder,
                         u8 sector) noexcept -> errno;
static void ata_lba28_select(const ide_device &ata, u32 lba) noexcept;
static void ata_chs_select(const ide_device &ata, u8 lba_count, u8 head, u16 cylinder,
                           u8 sector) noexcept;
static void ata_log(const ide_device &ata, const char *format, ...) noexcept;
extern "C" void ata_irq_handler(const interrupt_state &) noexcept;

static const device_vtable dev_vtable {
    .blk_read = &ata_read,
    .blk_write = &ata_write,
    .blk_query = nullptr,
    .close = nullptr,
};

static const file_vtable f_vtable {
    .lseek = generic_file_lseek,
    .read = nullptr,
    .write = nullptr,
    .put_blks = generic_disk_put_blks,
    .close = generic_file_close,
};

// Read/Write PIO data bytes
static auto ata_chk_data(ata_channel channel) noexcept -> u16
{
    return inw(ata_channel_ioprt(channel));
}

static auto ata_cpy_data(ata_channel channel, void *dst, u32 sector_count) noexcept -> bool
{
    auto d = static_cast<u16 *>(dst);
    for (u32 i = 0; i < sector_count; ++i) {
        rep_insw(d, ata_channel_ioprt(channel), 256);
        if (i != sector_count - 1 && !ata_poll(channel, CHECK_DRQ))
            return false;
        d += 256;
    }
    return ata_poll(channel, !CHECK_DRQ);
}

// Used to retrieve any error generated by the last ATA command executed
static auto ata_chk_error(ata_channel channel) noexcept -> ata_err
{
    return static_cast<ata_err>(inb(ata_channel_ioprt(channel) + 1));
}

// Used to control command specific interface features
static void ata_write_feats(ata_channel channel, u8 data) noexcept
{
    outb(ata_channel_ioprt(channel) + 1, data);
}

// Number of sectors to read/write (0 is a special value)
static auto ata_chk_sector_count(ata_channel channel) noexcept -> u8
{
    return inb(ata_channel_ioprt(channel) + 2);
}
static void ata_write_sector_count(ata_channel channel, u8 data) noexcept
{
    outb(ata_channel_ioprt(channel) + 2, data);
}

// Read/write sector number (LBAlo)
static auto ata_chk_sector_number(ata_channel channel) noexcept -> u8
{
    return inb(ata_channel_ioprt(channel) + 3);
}
static void ata_write_sector_number(ata_channel channel, u8 data) noexcept
{
    outb(ata_channel_ioprt(channel) + 3, data);
}

// Read/write cylinder low (LBAmid)
static auto ata_chk_cylinder_lo(ata_channel channel) noexcept -> u8
{
    return inb(ata_channel_ioprt(channel) + 4);
}
static void ata_write_cylinder_lo(ata_channel channel, u8 data) noexcept
{
    outb(ata_channel_ioprt(channel) + 4, data);
}

// Read/write cylinder high (LBAhi)
static auto ata_chk_cylinder_hi(ata_channel channel) noexcept -> u8
{
    return inb(ata_channel_ioprt(channel) + 5);
}
static void ata_write_cylinder_hi(ata_channel channel, u8 data) noexcept
{
    outb(ata_channel_ioprt(channel) + 5, data);
}

// Used to select a drive and/or head. Supports extra address/flag bits
static auto ata_chk_drive_head(ata_channel channel) noexcept -> u8
{
    return inb(ata_channel_ioprt(channel) + 6);
}
static void ata_write_drive_head(ata_channel channel, ata_drive_sel sel) noexcept
{
    outb(ata_channel_ioprt(channel) + 6, static_cast<u8>(sel));
}

// Used to read the current status
static auto ata_chk_status(ata_channel channel) noexcept -> ata_status
{
    return static_cast<ata_status>(inb(ata_channel_ioprt(channel) + 7));
}

static void ata_write_data(ata_channel channel, u16 data) noexcept
{
    outw(ata_channel_ioprt(channel), data);
}

static auto ata_write_data(ata_channel channel, const void *data, u32 sector_count) noexcept -> bool
{
    auto src = static_cast<const u16 *>(data);
    for (u32 i = 0; i < sector_count; ++i) {
        for (usize j = 0; j < 256; ++j)
            ata_write_data(channel, src[j]);

        if (i != sector_count - 1 && !ata_poll(channel, CHECK_DRQ))
            return false;

        src += 256;
    }
    return ata_poll(channel, !CHECK_DRQ);
}

// Used to send ATA commands to the device
static auto ata_write_cmd(ata_channel channel, ata_cmd cmd) noexcept -> bool
{
    outb(ata_channel_ioprt(channel) + 7, cmd);

    if (cmd == ATA_CMD_READ_PIO || cmd == ATA_CMD_WRITE_PIO)
        return ata_poll(channel, CHECK_DRQ);
    return ata_poll(channel, !CHECK_DRQ);
}

// A duplicate of `ata_chk_status` which does not affect interrupts
static auto ata_chk_alt_status(ata_channel channel) noexcept -> ata_status
{
    return static_cast<ata_status>(inb(ata_channel_ctrlprt(channel)));
}

// Used to reset the bus or enable/disable interrupts
static void ata_write_dev_ctrl(ata_channel channel, u8 data) noexcept
{
    outb(ata_channel_ctrlprt(channel), data);
}

// Provides drive select and head select information
static auto ata_chk_drv_address(ata_channel channel) noexcept -> u8
{
    return inb(ata_channel_ctrlprt(channel) + 1);
}

extern "C" auto _drv_entry() noexcept -> errno
{
    KASSERT(channels == nullptr && ide_devices == nullptr);
    usize i = 0;

    const pcidev_list pcidevs { get_pci_devs() };

    // Most likely no PCI bus, ancient trash
    if (pcidevs.sz == 0)
        return ENODEV;

    if (pcidevs.dev == nullptr)
        return ENOMEM;

    for (i = 0; i < pcidevs.sz; ++i) {
        if (pcidevs.dev[i].clss == pci_device_class::MASS_STORAGE_CONTROLLER &&
            pcidevs.dev[i].subclss == pci_device_subclass::IDE_INTERFACE) {
            DEBUG("(ATA) Detected IDE Interface Controller\n");
            break;
        }
    }

    if (i == pcidevs.sz) {
        kfree(pcidevs.dev);
        return ENODEV;
    }

    channels = static_cast<ide_channel_register *>(
        kzalloc(TOTAL_IDE_CHANNEL_REGISTERS * sizeof(ide_channel_register)));
    ide_devices = static_cast<ide_device *>(kzalloc(TOTAL_IDE_DRIVES * sizeof(ide_device)));

    if (ide_devices == nullptr || channels == nullptr) {
        kfree(channels);
        kfree(ide_devices);
        kfree(pcidevs.dev);
        return ENOMEM;
    }

    const u32 pci = static_cast<u32>(pcidevs.dev[i].bus | (pcidevs.dev[i].device << 8) |
                                     (pcidevs.dev[i].fn << 16));
    const u32 bar0 = pci_chk_bar0(pcidevs.dev[i].bus, pcidevs.dev[i].device, pcidevs.dev[i].fn);
    const u32 bar1 = pci_chk_bar1(pcidevs.dev[i].bus, pcidevs.dev[i].device, pcidevs.dev[i].fn);
    const u32 bar2 = pci_chk_bar2(pcidevs.dev[i].bus, pcidevs.dev[i].device, pcidevs.dev[i].fn);
    const u32 bar3 = pci_chk_bar3(pcidevs.dev[i].bus, pcidevs.dev[i].device, pcidevs.dev[i].fn);
    const u32 bar4 = pci_chk_bar4(pcidevs.dev[i].bus, pcidevs.dev[i].device, pcidevs.dev[i].fn);

    // The I/O space BAR layout has bit 1 set to reserved and bit 0 is always 0, so we need
    // to ignore these bits (See: https://wiki.osdev.org/PCI).
    static constexpr u32 IO_BAR_MASK = 0xFFFFFFFC;
    channels[ATA_PRIMARY].base = static_cast<u16>(bar0 & IO_BAR_MASK) + ATA_PRIMARY_IO_BASE_PORT;
    channels[ATA_PRIMARY].ctrl = static_cast<u16>(bar1 & IO_BAR_MASK) + ATA_PRIMARY_DC_BASE_PORT;
    channels[ATA_SECONDARY].base =
        static_cast<u16>(bar2 & IO_BAR_MASK) + ATA_SECONDARY_IO_BASE_PORT;
    channels[ATA_SECONDARY].ctrl =
        static_cast<u16>(bar3 & IO_BAR_MASK) + ATA_SECONDARY_DC_BASE_PORT;
    channels[ATA_PRIMARY].bmide = static_cast<u8>(bar4 & IO_BAR_MASK) + 0;   // Bus Master IDE
    channels[ATA_SECONDARY].bmide = static_cast<u8>(bar4 & IO_BAR_MASK) + 8; // Bus Master IDE

    // Disable IRQs in both channels
    ata_write_dev_ctrl(ATA_PRIMARY, ATA_CTRL_NIEN);
    ata_write_dev_ctrl(ATA_SECONDARY, ATA_CTRL_NIEN);

    errno status = ENOERR;

    // Detect if the device is ATA
    for (usize j = 0; j < 2; ++j) {
        ide_devices[j].channel = ATA_PRIMARY;
        ide_devices[j].drive = drive_sel[j];
        ide_devices[j].pci = pci;
        /*u8 irq;

        if (ide_devices[j].channel == ATA_PRIMARY) {
            // Check if channel is in PCI native mode
            if (pcidevs.dev[i].prog_if & 0x1)
                irq = pcidevs.dev[i].int_line;
            // device is in compatibility mode
            else
                irq = 14;
        } else {
            if (pcidevs.dev[i].prog_if & 0x4)
                irq = pcidevs.dev[i].int_line;
            else
                irq = 15;
        }
        install_irq_handler(irq, ata_irq_handler);*/
        ata_write_drive_head(ATA_PRIMARY, drive_sel[j]);

        // for a explaination why we're writing here, see:
        // https://wiki.osdev.org/ATA_PIO_Mode#IDENTIFY_command
        ata_write_sector_count(ATA_PRIMARY, 0);
        ata_write_sector_number(ATA_PRIMARY, 0);
        ata_write_cylinder_lo(ATA_PRIMARY, 0);
        ata_write_cylinder_hi(ATA_PRIMARY, 0);

        vintix::unique_ptr<ata_identify> cident { kmalloc(sizeof(ata_identify)) };

        if (cident == nullptr) {
            status = ENOMEM;
            DEBUG("(ATA) failed to allocate memory for IDENTIFY structure\n");
            continue;
        }

        const ata_status identify =
            ata_identify_cmd(ATA_PRIMARY, drive_sel[j], cident.unsafe_mutable_data());

        // Device doesnt exist. Ignore
        if (!identify)
            break;

        // Device is ATAPI if true. Ignore
        if (identify & ATA_SR_ERR)
            break;

        const u8 sn = ata_chk_sector_number(ATA_PRIMARY);
        const u8 lo = ata_chk_cylinder_lo(ATA_PRIMARY);

        // most definitely not a ATA device if this condition is true. Ignore
        if (sn || lo)
            break;

        if constexpr (ATA_ENABLE_LOGS) {
            if (cident->serial[0] != '\0') {
                right_justify(static_cast<char *>(cident->serial));
                rstrip(static_cast<char *>(cident->serial));
                ata_log(ide_devices[j], "Serial Number: \"%s\"\n",
                        static_cast<char *>(cident->serial));
            }
            if (cident->firm_revision[0] != '\0') {
                right_justify(static_cast<char *>(cident->firm_revision));
                rstrip(static_cast<char *>(cident->firm_revision));
                ata_log(ide_devices[j], "Model Name: \"%s\"\n",
                        static_cast<char *>(cident->firm_revision));
            } else if (cident->model[0] != '\0') {
                right_justify(static_cast<char *>(cident->model));
                rstrip(static_cast<char *>(cident->model));
                ata_log(ide_devices[j], "Model Name: \"%s\"\n", static_cast<char *>(cident->model));
            }
        }

        ide_devices[j].capabilities = cident->capabilities;
        ide_devices[j].total_sectors = cident->capacity_in_sectors;
        ide_devices[j].log_secsz = log2(SECTOR_DEFAULT_DATA_SIZE);

        if constexpr (ATA_ENABLE_LOGS) {
            const ublksize_t blksz = 1 << ide_devices[j].log_secsz;
            ata_log(ide_devices[j], "Capabilities: 0x%x\n", ide_devices[j].capabilities);
            if (ide_devices[j].total_sectors * blksz >= 1_gib)
                ata_log(ide_devices[j], "Disk Size: %uGiB\n",
                        ide_devices[j].total_sectors * blksz / 1_gib);
            else if (ide_devices[j].total_sectors * blksz >= 1_mib)
                ata_log(ide_devices[j], "Disk Size: %uMiB\n",
                        ide_devices[j].total_sectors * blksz / 1_mib);
            else if (ide_devices[j].total_sectors * blksz >= 1_kib)
                ata_log(ide_devices[j], "Disk Size: %uKiB\n",
                        ide_devices[j].total_sectors * blksz / 1_kib);
        }
        ide_devices[j].heads = cident->num_heads;
        ide_devices[j].cylinders = cident->num_cylinders;
        ide_devices[j].sectors_per_track = cident->num_sectors_per_track;
        ata_log(ide_devices[j], "Number of heads: %u\n", ide_devices[j].heads);
        ata_log(ide_devices[j], "Number of cylinders: %u\n", ide_devices[j].cylinders);
        ata_log(ide_devices[j], "Number of sectors per track: %u\n",
                ide_devices[j].sectors_per_track);

        ide_devices[j].lba = 0x0;
        UNWRAP_ERR(ide_devices[j].d_name.copy("hd"));
        ide_devices[j].d_name.reserve(4);
        if (ide_devices[j].d_name == nullptr)
            return ENOMEM;
        ide_devices[j].d_name[0] = 'h';
        ide_devices[j].d_name[1] = 'd';
        // hda: first drive on the primary controller
        // hdb: second drive on the primary controller
        // hdc: first drive on the secondary controller
        // hdd: second drive on the secondary controller
        // hde: first drive on the third controller
        // hdf: second drive on the third controller
        // hdg: first drive on the fourth controller
        // hdh: second drive on the fourth controller
        // ...
        ide_devices[j].d_name[2] = ide_devices[j].is_primary_channel() ? 'a' : 'b';
        ata_log(ide_devices[j], "Registering device as %s\n", ide_devices[j].d_name.c_str());
        ide_devices[j].d_major = DRIVER_VERSION_MAJOR;
        ide_devices[j].d_minor = static_cast<u8>(j);
        ide_devices[j].dev_vtable = &dev_vtable;
        ide_devices[j].f_vtable = &f_vtable;
        ide_devices[j].d_perms = USRREAD | USRWRIT;
        ide_devices[j].d_type = device_type::BLKDEV;
        status = disk_add(&ide_devices[j]);
    }

    kfree(pcidevs.dev);
    return status;
}

static auto ata_read(const blkdev *self, void *buf, u32 sector_start,
                     u32 num_sectors) noexcept -> errno
{
    KASSERT(buf != nullptr);
    KASSERT(self != nullptr);
    KASSERT(num_sectors < UCHAR_MAX); // TODO: add in functionality to read more than 255 sectors
    // NOLINTBEGIN(clang-analyzer-core.NullDereference)
    const auto *dev = static_cast<const ide_device *>(self);
    while (ata_drive_is_busy(dev->channel))
        ;

    if (dev->capabilities & LBA_SUPPORTED) {
        return ata_lba28_read(*dev, buf, sector_start, num_sectors);
    }
    const auto [cylinder, head, sector] { lba_to_chs(sector_start, static_cast<u8>(dev->heads),
                                                     static_cast<u8>(dev->sectors_per_track)) };
    return ata_chs_read(*dev, buf, static_cast<u8>(num_sectors), head, cylinder, sector);
    // NOLINTEND(clang-analyzer-core.NullDereference)
}

static auto ata_write(const blkdev *self, const void *buf, u32 sector_start,
                      u32 num_sectors) noexcept -> errno
{
    KASSERT(buf != nullptr);
    KASSERT(self != nullptr);
    KASSERT(num_sectors < UCHAR_MAX); // TODO: add in functionality to write more than 255 sectors
    // NOLINTBEGIN(clang-analyzer-core.NullDereference)
    const auto *dev = static_cast<const ide_device *>(self);
    while (ata_drive_is_busy(dev->channel))
        ;

    if (dev->capabilities & LBA_SUPPORTED) {
        return ata_lba28_write(*dev, buf, sector_start, num_sectors);
    }
    const auto [cylinder, head, sector] { lba_to_chs(sector_start, static_cast<u8>(dev->heads),
                                                     static_cast<u8>(dev->sectors_per_track)) };
    return ata_chs_write(*dev, buf, static_cast<u8>(num_sectors), head, cylinder, sector);
    // NOLINTEND(clang-analyzer-core.NullDereference)
}

static void ata_log(const ide_device &ata, const char *format, ...) noexcept
{
    if constexpr (ATA_ENABLE_LOGS) {
        const bool drive = ata.is_slave_drive();
        const bool channel = ata.is_secondary_channel();

        PCI(ata.pci, COLOR_LIGHT_YELLOW "ATA(D%u.C%u): " COLOR_RESET, drive, channel);
        va_list va;
        va_start(va, format);
        srl_vfprintf(format, va);
        va_end(va);
    }
}

static void ata_chs_select(const ide_device &ata, u32 sector_count, u8 head, u16 cylinder,
                           u8 sector) noexcept
{
    KASSERT(head <= ata.heads);
    KASSERT(cylinder <= ata.cylinders);
    KASSERT(sector != 0);
    KASSERT(sector <= ata.sectors_per_track);
    KASSERT(sector_count <= 256);
    ata_write_drive_head(ata.channel,
                         static_cast<ata_drive_sel>(static_cast<u8>(ata.drive) | head));
    ata_write_sector_count(ata.channel, (sector_count == 256) ? 0 : static_cast<u8>(sector_count));
    ata_write_sector_number(ata.channel, sector);
    ata_write_cylinder_lo(ata.channel, static_cast<u8>(cylinder & 0x00ff));
    ata_write_cylinder_hi(ata.channel, static_cast<u8>((cylinder & 0xff00) >> 8));
}

static void ata_lba28_select(const ide_device &ata, u32 lba, u32 sector_count) noexcept
{
    KASSERT(sector_count <= UCHAR_MAX);
    static constexpr u8 LBA_MODE_SEL = 1 << 6;
    ata_write_drive_head(ata.channel,
                         static_cast<ata_drive_sel>(static_cast<u8>(ata.drive) | LBA_MODE_SEL |
                                                    ((lba >> 24) & 0x0f)));
    ata_write_sector_count(ata.channel, (sector_count == 256) ? 0 : static_cast<u8>(sector_count));
    ata_write_sector_number(ata.channel, static_cast<u8>(lba));     // LBAlo
    ata_write_cylinder_lo(ata.channel, static_cast<u8>(lba >> 8));  // LBAmid
    ata_write_cylinder_hi(ata.channel, static_cast<u8>(lba >> 16)); // LBAhi
}

static auto ata_status_is_ok(ata_channel channel, bool check_drq) noexcept -> bool
{
    const ata_status status = ata_chk_status(channel);

    if (status & ATA_SR_BSY) {
        DEBUG("ATA: Channel %u: Drive is busy...\n", channel);
        return false;
    }

    if (status & ATA_SR_ERR) {
        DEBUG("ATA: Channel %u: Drive encountered an error!\n", channel);
        return false;
    }

    if (status & ATA_SR_DWF) {
        DEBUG("ATA: Channel %u: Drive fault!\n", channel);
        return false;
    }

    if (!check_drq && (status & ATA_SR_DRQ)) {
        DEBUG("ATA: Channel %u: Drive is still ready to send or accept PIO data!\n", channel);
        return false;
    }
    if (check_drq && !(status & ATA_SR_DRQ)) {
        DEBUG("ATA: Channel %u: Drive is not ready to send or accept PIO data!\n", channel);
        return false;
    }

    return true;
}

static auto ata_lba28_write(const ide_device &ata, const void *src, u32 lba,
                            u32 sector_count) noexcept -> errno
{
    ata_lba28_select(ata, lba, sector_count);

    if (!ata_write_cmd(ata.channel, ATA_CMD_WRITE_PIO))
        return EIO;

    if (!ata_write_data(ata.channel, src, sector_count))
        return EIO;

    if (!ata_write_cmd(ata.channel, ATA_CMD_CACHE_FLUSH))
        return EIO;

    return ENOERR;
}

static auto ata_lba28_read(const ide_device &ata, void *dst, u32 lba,
                           u32 sector_count) noexcept -> errno
{
    ata_lba28_select(ata, lba, sector_count);

    if (!ata_write_cmd(ata.channel, ATA_CMD_READ_PIO))
        return EIO;

    if (!ata_cpy_data(ata.channel, dst, sector_count))
        return EIO;

    return ENOERR;
}

static auto ata_chs_write(const ide_device &ata, const void *src, u32 sector_count, u8 head,
                          u16 cylinder, u8 sector) noexcept -> errno
{
    ata_chs_select(ata, sector_count, head, cylinder, sector);

    if (!ata_write_cmd(ata.channel, ATA_CMD_WRITE_PIO))
        return EIO;

    if (!ata_write_data(ata.channel, src, sector_count))
        return EIO;

    if (!ata_write_cmd(ata.channel, ATA_CMD_CACHE_FLUSH))
        return EIO;

    return ENOERR;
}

static auto ata_chs_read(const ide_device &ata, void *dst, u32 sector_count, u8 head, u16 cylinder,
                         u8 sector) noexcept -> errno
{
    ata_chs_select(ata, sector_count, head, cylinder, sector);
    if (!ata_write_cmd(ata.channel, ATA_CMD_READ_PIO))
        return EIO;

    if (!ata_cpy_data(ata.channel, dst, sector_count))
        return EIO;

    return ENOERR;
}

static auto ata_identify_cmd(ata_channel channel, ata_drive_sel sel,
                             ata_identify *dst) noexcept -> ata_status
{
    ata_write_drive_head(channel, sel);
    ata_write_cmd(channel, ATA_CMD_IDENTIFY);

    ata_status status = ata_chk_status(channel);

    // device doesnt exist or is an ATAPI device
    if (!status || (status & ATA_SR_ERR))
        return status;

    ata_cpy_data(channel, dst, 1);
    return status;
}

static auto ata_drive_is_busy(ata_channel channel) noexcept -> bool
{
    const ata_status status = ata_chk_status(channel);

    if (status & ATA_SR_BSY)
        return true;

    if (status & ATA_SR_DRQ)
        return true;

    return false;
}

static auto ata_poll(ata_channel channel, bool check_drq) noexcept -> bool
{
    // Delay 400 nanosecond for BSY to be set:
    for (int i = 0; i < 4; ++i)
        ata_chk_alt_status(channel); // Reading the alternate status port wastes 100ns

    while (ata_chk_status(channel) & ATA_SR_BSY)
        ;

    return ata_status_is_ok(channel, check_drq);
}
