cmake_minimum_required(VERSION 3.22)
project(ustar CXX)
file(GLOB_RECURSE SOURCES *.cc *.h ../../../include/*.h)
add_library(${PROJECT_NAME}.mod ${SOURCES})
target_compile_options(${PROJECT_NAME}.mod PRIVATE ${DRIVER_CXX_FLAGS})
add_dependencies(drivers ${PROJECT_NAME}.mod)
