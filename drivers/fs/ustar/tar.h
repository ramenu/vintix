/**
 * Copyright (C) Copyright (C) 2023-2024 Abdur-Rahman Mansoor
 *
 * This file is part of vintix.
 *
 * vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_FILESYSTEMS_TAR_H
#define VINTIX_FILESYSTEMS_TAR_H

#include <vintix/kernel/filesystems/inode.h>
#include <vintix/kernel/filesystems/super.h>
#include <vintix/kernel/lib/errptr.h>
#include <vintix/kernel/lib/vector.h>

struct tar_header;

class tar_superblock : public superblock {
  public:
    tar_header *header_start;
    usize maximum_files;
    bool dynamically_allocated;
};

class tar_inode : public inode {
  public:
    const tar_header *hdr;
};

auto tar_init() noexcept -> errno;

#endif // VINTIX_FILESYSTEMS_TAR_H
