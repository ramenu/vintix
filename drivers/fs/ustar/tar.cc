/**
 * Copyright (C) Copyright (C) 2023-2024 Abdur-Rahman Mansoor
 *
 * This file is part of vintix.
 *
 * vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include "tar.h"
#include <vintix/kernel/core/dev.h>
#include <vintix/kernel/core/panic.h>
#include <vintix/kernel/filesystems/file.h>
#include <vintix/kernel/filesystems/fs.h>
#include <vintix/kernel/filesystems/mount.h>
#include <vintix/kernel/filesystems/path.h>
#include <vintix/kernel/lib/math.h>
#include <vintix/kernel/lib/move.h>
#include <vintix/kernel/lib/pair.h>
#include <vintix/kernel/lib/string_view.h>

enum {
    TAR_HEADER_SIZE = 512,
};

enum tar_type {
    TAR_REGULAR_FILE = '0',
    TAR_HARD_LINK = '1',
    TAR_SYMBOLIC = '2',
    TAR_CHRDEV = '3',
    TAR_BLKDEV = '4',
    TAR_DIRECTORY = '5',
    TAR_FIFO = '6',
};

static constexpr usize TAR_FILENAME_SZ = 100;
static constexpr usize TAR_MODE_SZ = 8;
static constexpr usize TAR_UID_SZ = 8;
static constexpr usize TAR_GID_SZ = 8;
static constexpr usize TAR_SIZE_SZ = 12;
static constexpr usize TAR_MTIME_SZ = 12;
static constexpr usize TAR_CHKSUM_SZ = 8;
static constexpr usize TAR_TYPEFLAG_SZ = 1;

// NOLINTBEGIN(cppcoreguidelines-avoid-c-arrays)
struct tar_header {
    char filename[TAR_FILENAME_SZ];
    char mode[TAR_MODE_SZ];
    char uid[TAR_UID_SZ];
    char gid[TAR_GID_SZ];
    char size[TAR_SIZE_SZ];
    char mtime[TAR_MTIME_SZ];
    char chksum[TAR_CHKSUM_SZ];
    char typeflag[TAR_TYPEFLAG_SZ];

    [[nodiscard]] constexpr auto is_null_entry() const noexcept -> bool
    {
        return filename[0] == '\0';
    }
    [[nodiscard]] constexpr auto is_directory() const noexcept -> bool
    {
        return typeflag[0] == TAR_DIRECTORY;
    }
    [[nodiscard]] constexpr auto is_blkdev() const noexcept -> bool
    {
        return typeflag[0] == TAR_BLKDEV;
    }
    [[nodiscard]] constexpr auto is_chrdev() const noexcept -> bool
    {
        return typeflag[0] == TAR_CHRDEV;
    }
    [[nodiscard]] constexpr auto is_symbolic() const noexcept -> bool
    {
        return typeflag[0] == TAR_SYMBOLIC;
    }
    [[nodiscard]] constexpr auto is_hard_link() const noexcept -> bool
    {
        return typeflag[0] == TAR_HARD_LINK;
    }
    [[nodiscard]] constexpr auto is_regular_file() const noexcept -> bool
    {
        return typeflag[0] == TAR_REGULAR_FILE;
    }
} __attribute__((packed));
// NOLINTEND(cppcoreguidelines-avoid-c-arrays)

// NOLINTBEGIN(cppcoreguidelines-pro-bounds-array-to-pointer-decay)
static constexpr auto oct2bin(const char *, usize) noexcept -> usize;
static auto tar_next_header(const tar_header *hdr) noexcept -> tar_header *;
static auto tar_init_fs_context(const fs_context &context) noexcept -> errptr<superblock>;
static auto tar_header_to_inode(tar_superblock *, const tar_header *hdr,
                                usize off) noexcept -> errptr<inode>;
static auto tar_lookup(const dentry *, const char *) noexcept -> errptr<inode>;
static auto tar_ls(const dentry *entry) noexcept -> result<vintix::vector<vintix::string>>;

extern "C" auto _drv_entry() noexcept -> errno
{
    genfs fs {};
    fs.name = "tarfs";
    fs.flags = MNT_RDONLY | MNT_NOATIME;
    fs.init_fs_context = &tar_init_fs_context;
    fs.type = fs_type::TMPFS;

    return register_filesystem(fs);
}

static void tar_free(superblock *sup) noexcept
{
    auto *t_sup = static_cast<tar_superblock *>(sup);

    if (t_sup->dynamically_allocated)
        kfree(t_sup->header_start);
}

static auto tar_get_size(const tar_header *hdr_start) noexcept -> result<vintix::pair<usize, usize>>
{
    const tar_header *hdr = hdr_start;
    usize byte_count = 0;

    for (usize i = 0;; ++i) {
        // might as well do some filesystem sanity checks as well while
        // we're at it. This saves us from doing these checks later on.
        if (strlen(hdr->filename) > TAR_FILENAME_SZ)
            return ENAMETOOLONG;

        if (hdr->filename[0] == '\0')
            return vintix::pair { .one = i, .two = byte_count };

        byte_count += TAR_HEADER_SIZE + oct2bin(hdr->size, TAR_SIZE_SZ - 1);
        hdr = tar_next_header(hdr);
    }
}

static auto tar_root_inode(superblock *sup) noexcept -> errptr<inode>
{
    auto *t_sup = static_cast<tar_superblock *>(sup);
    return tar_header_to_inode(t_sup, t_sup->header_start, 1);
}

static constexpr superblock_vtable tar_superblock_vtable {
    .root_inode = tar_root_inode,
    .alloc_inode = nullptr,
    .sync = nullptr,
    .free = tar_free,
};

static auto tar_init_fs_context(const fs_context &context) noexcept -> errptr<superblock>
{
    // only support reading from memory for now
    if (context.fs_dev != NODEV)
        return EINVAL;

    auto *t_sup = static_cast<tar_superblock *>(kmalloc(sizeof(tar_superblock)));

    if (t_sup == nullptr)
        return ENOMEM;

    t_sup->s_magic = 'T' + 'A' + 'R';
    t_sup->header_start = unsafe_toptr<tar_header *>(context.vaddr_offset);
    result<vintix::pair<usize, usize>> res = tar_get_size(t_sup->header_start);

    if (res.is_err()) {
        kfree(t_sup);
        return res.err();
    }

    t_sup->maximum_files = res.get().one;
    // i don't think there technically is a maximum file size?
    t_sup->s_max_file_size = INF_FILE_SIZE;
    t_sup->s_mnt_flags = MNT_RDONLY | MNT_NOATIME;
    t_sup->dynamically_allocated = false;
    t_sup->s_dev = context.fs_dev;
    t_sup->s_log_blksz = 0;
    t_sup->vtable = &tar_superblock_vtable;
    t_sup->s_max_name_length = TAR_FILENAME_SZ;

    return t_sup;
}

static constexpr auto tar_type_to_vintix(const char *type) noexcept -> mode_t
{
    switch (type[0]) {
        case TAR_REGULAR_FILE:
            return REGULAR_FILE;
        case TAR_SYMBOLIC:
            return TAR_SYMBOLIC;
        case TAR_CHRDEV:
            return CHAR_SPECIAL;
        case TAR_BLKDEV:
            return BLOCK_SPECIAL;
        case TAR_DIRECTORY:
            return DIRECTORY;
    };

    kpanic("unsupported tar type field found: %c\n", type[0]);
}

static constexpr auto oct2bin(const char *s, usize sz) noexcept -> usize
{
    usize n = 0;
    while (sz-- > 0) {
        n *= 8;
        n += static_cast<usize>(*s - '0');
        ++s;
    }
    return n;
}

static auto tar_read(file *fp, void *dst, usize count) noexcept -> ssize
{
    const tar_header *hdr = static_cast<const tar_inode *>(fp->inode())->hdr;

    auto *file_data_offset =
        unsafe_toptr<const u8 *>(addressof(hdr) + TAR_HEADER_SIZE + fp->uoff());
    memcpy(dst, file_data_offset, count);
    return static_cast<ssize>(count);
}

static constexpr file_vtable tar_file_vtable {
    .lseek = generic_file_lseek,
    .read = tar_read,
    .write = nullptr,
    .put_blks = nullptr,
    .close = nullptr,
};

static auto tar_open(inode *) noexcept -> errptr<file>
{
    auto *fp = static_cast<file *>(kmalloc(sizeof(file)));

    if (fp == nullptr)
        return ENOMEM;

    fp->f_pos = 0;
    fp->vtable = &tar_file_vtable;

    return fp;
}

static void tar_free(inode *ino) noexcept { kfree(ino); }

static const inode_vtable tar_inode_vtable {
    .create = nullptr,
    .rm = nullptr,
    .mkdir = nullptr,
    .rmdir = nullptr,
    .lookup = tar_lookup,
    .sync = nullptr,
    .open = tar_open,
    .ls = tar_ls,
    .free = tar_free,
};

static auto tar_header_to_inode(tar_superblock *sup, const tar_header *hdr,
                                usize off) noexcept -> errptr<inode>
{
    auto *ino = static_cast<tar_inode *>(kzalloc(sizeof(tar_inode)));

    if (ino == nullptr)
        return ENOMEM;

    ino->i_uid = static_cast<uid_t>(oct2bin(hdr->uid, TAR_UID_SZ - 1));
    ino->i_gid = static_cast<gid_t>(oct2bin(hdr->gid, TAR_GID_SZ - 1));
    ino->i_mode = static_cast<mode_t>(oct2bin(hdr->mode, TAR_MODE_SZ - 1)) |
                  tar_type_to_vintix(hdr->typeflag);
    ino->i_mtime = oct2bin(hdr->mtime, TAR_MTIME_SZ - 1);
    ino->i_size = static_cast<off_t>(oct2bin(hdr->size, TAR_SIZE_SZ - 1));
    ino->i_num = off;
    ino->hdr = hdr;
    ino->vtable = &tar_inode_vtable;
    // this is a lot more complicated to calculate cause the header doesn't embed this in.
    // later on we need to calculate this properly.
    ino->i_nlinks = 1;
    ino->i_sup = sup;

    return ino;
}

static auto tar_next_header(const tar_header *hdr) noexcept -> tar_header *
{
    const usize sz = oct2bin(hdr->size, TAR_SIZE_SZ - 1);
    v_addr v = addressof(hdr) + ((sz / TAR_HEADER_SIZE + 1) * TAR_HEADER_SIZE);
    if (sz % TAR_HEADER_SIZE)
        v += TAR_HEADER_SIZE;

    return unsafe_toptr<tar_header *>(v);
}

static auto tar_ls(const dentry *entry) noexcept -> result<vintix::vector<vintix::string>>
{
    auto *t_sup = static_cast<const tar_superblock *>(entry->d_ino->i_sup);
    const tar_header *hdr = t_sup->header_start;
    vintix::vector<vintix::string> entries;
    entries.reserve(128);

    if (entries == nullptr)
        return ENOMEM;

    vintix::string dir_path { entry->absolute_path() };

    if (dir_path == nullptr)
        return ENOMEM;

    const usize path_slashes = dir_path.count('/') + 1;
    vintix::string filename;
    filename.reserve(TAR_FILENAME_SZ);

    if (filename == nullptr)
        return ENOMEM;

    for (; !hdr->is_null_entry(); hdr = tar_next_header(hdr)) {
        filename = COPY(hdr->filename);

        if (filename.size() == 1 || !filename.starts_with(dir_path))
            continue;

        if (filename.ends_with('/'))
            filename.erase_last();

        usize filename_slashes = filename.count('/');

        if (entry->is_root_dir())
            ++filename_slashes;

        if (path_slashes != filename_slashes)
            continue;

        filename = COPY(path_last_entry(vintix::string_view { filename }));
        if (auto err = entries.push_back(move(filename)))
            return err;
    }

    return entries;
}

static auto tar_lookup(const dentry *entry, const char *n) noexcept -> errptr<inode>
{
    auto *t_sup = static_cast<tar_superblock *>(entry->d_ino->i_sup);
    const tar_header *hdr = t_sup->header_start;
    vintix::string path { entry->absolute_path() };

    if (path == nullptr)
        return ENOMEM;

    if (!path.ends_with('/')) {
        if (path.size() + 1 > TAR_FILENAME_SZ || path.size() + 1 > PATH_MAX)
            return ENAMETOOLONG;

        if (auto err = path.append('/'))
            return err;
    }

    vintix::string_view name { n };
    if (path.size() + name.size() > TAR_FILENAME_SZ || path.size() + name.size() > PATH_MAX)
        return ENAMETOOLONG;

    if (auto err = path.append(name))
        return err;

    for (usize i = 0; i < t_sup->maximum_files; ++i) {
        if (hdr->is_null_entry())
            return ENOENT;

        // directories have a trailing slash so we have to make a special comparsion
        // for them
        if (hdr->is_directory()) {
            const usize filename_length = strlen(hdr->filename);
            const usize len = max(path.length(), filename_length);
            if (strncmp(path.c_str(), hdr->filename, len - 1) == 0) {
                return tar_header_to_inode(t_sup, hdr, i + 1);
            }
        }

        if (path == hdr->filename)
            return tar_header_to_inode(t_sup, hdr, i + 1);

        hdr = tar_next_header(hdr);
    }

    return ENOENT;
}
// NOLINTEND(cppcoreguidelines-pro-bounds-array-to-pointer-decay)
