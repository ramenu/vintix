/**
 * Copyright (C) 2023-2024 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_FS_EXT2_H
#define VINTIX_FS_EXT2_H

#include <vintix/kernel/lib/errno.h>
#include <vintix/kernel/lib/types.h>

enum {
    EXT2_TOTAL_BLOCKS = 15,
    EXT2_SIGNATURE = 0xef53,
};

enum class ext2_fs_state {
    FILESYSTEM_IS_CLEAN = 0x1,
    FILESYSTEM_HAS_ERRORS = 0x2,
};

enum class ext2_err_handling_method {
    IGNORE_THE_ERROR = 0x1,
    REMOUNT_FILESYSTEM_AS_READONLY = 0x2,
    KERNEL_PANIC = 0x3,
};

enum class ext2_osid {
    LINUX = 0x0,
    GNU_HURD = 0x1,
    MASIX = 0x2,
    FREEBSD = 0x3,
    BSDLITE = 0x4,
};

enum class ext2_inode_type : mode_t {
    FIFO = 0x1000,
    CHARACTER_DEVICE = 0x2000,
    DIRECTORY = 0x4000,
    BLOCK_DEVICE = 0x6000,
    REGULAR_FILE = 0x8000,
    SYMBOLIC_LINK = 0xA000,
    UNIX_SOCKET = 0xC000,
};

enum class ext2_inode_perm : mode_t {
    OTHER_EXEC = 0x001,
    OTHER_WRITE = 0x002,
    OTHER_READ = 0x004,
    GROUP_EXEC = 0x008,
    GROUP_WRITE = 0x010,
    GROUP_READ = 0x020,
    USER_EXEC = 0x040,
    USER_WRITE = 0x080,
    USER_READ = 0x100,
    STICKY_BIT = 0x200,
    SGID = 0x400,
    SUID = 0x800,
};

enum class ext2_inode_flags : u32 {
    SECURE_DELETION = 0x1,
    KEEP_COPY_WHEN_DELETED = 0x2,
    FILE_COMPRESSION = 0x4,
    SYNCHRONOUS_UPDATES = 0x8,
    IMMUTABLE_FILE = 0x10,
    APPEND_ONLY = 0x20,
    FILE_NOT_INCLUDED_IN_DUMP_CMD = 0x40,
    DONT_UPDATE_LAST_ACCESS_TIME = 0x80,
    HASH_INDEXED_DIRECTORY = 0x10000,
    AFS_DIRECTORY = 0x20000,
    JOURNAL_FILE_DATA = 0x40000,
};

enum class ext2_dir_indicator {
    UNKNOWN_TYPE = 0,
    REGULAR_FILE = 1,
    DIRECTORY = 2,
    CHARACTER_DEVICE = 3,
    BLOCK_DEVICE = 4,
    FIFO = 5,
    SOCKET = 6,
    SYMBOLIC_LINK = 7,
};

struct ext2_superblock {
    // total number of inodes in the file system
    u32 total_inodes;

    // total number of blocks in the file system
    u32 total_blocks;

    // number of blocks reserved for superuser (see offset 80)
    u32 blocks_reserved_for_superuser;

    // total number of unallocated blocks
    u32 total_unallocated_blocks;

    // total number of unallocated inodes
    u32 total_unallocated_inodes;

    // block number of the block containing the superblock (also
    // the starting block number, NOT always zero)
    u32 starting_block;

    // log2(block size) - 10. In other words, the number to shift 1,024
    // to the left by to obtain the block size
    u32 block_size;

    // log2(fragment size) - 10. In other words, the number to shift 1,024
    // to the left by to obtain the fragment size
    u32 frag_size;

    // number of blocks in each block group
    u32 blocks_per_group;

    // number of fragments inn each block group
    u32 frags_per_group;

    // number of inodes in each block group
    u32 inodes_per_group;

    // last mount time (in POSIX time)
    time_t last_mount;

    // last written time (in POSIX time)
    time_t last_written;

    // number of times the volume has been mounted since its last
    // consistency check (fsck)
    u16 mnt_count;

    // number of mounts allowed before a consistency check (fsck)
    // must be done
    u16 num_mounts_allowed;

    // ext2 signature (0xef53) used to help confirm the presence of
    // ext2 on a volume
    u16 signature;

    // file system state
    u16 fs_state;

    // what to do when an error is detected
    u16 on_err;

    // minor portion of version (combine with major portion below
    // to construct full version field)
    u16 version_minor;

    // POSIX time of last consistency check (fsck)
    time_t last_consistency_check;

    // interval (in POSIX time) between forced consistency check (fsck)
    time_t consistency_check_interval;

    // operating system ID from which the filesystem on this volume
    // was created
    u32 os_id;

    // major portion of version (combine with minor portion above to
    // construct full version field)
    u32 version_major;

    // user ID that can use reserved blocks
    uid_t uid;

    // group ID that can use reserved blocks
    gid_t gid;

    // extended superblock fields
    // these fields are only present if major version specified in the
    // base superblock fields is greater than or equal to 1
    // ---------------------------------------------------------------

    // first non-reserved inode in file system (in versions < 1.0, this is
    // fixed as 11)
    u32 first_nonreserved_inode;

    // size of each inode structure in bytes (in versions < 1.0, this is
    // fixed as 128
    u16 inode_sz;

    // block group that this superblock is part of (if backup copy)
    u16 superblock_group;

    // optional features present (features that are not required to
    // read or write, but usually result in a performance increase)
    u32 optional_feats_present;

    // required features present (features that are required to be
    // supported to read or write)
    u32 required_feats_present;

    // features that if not supported, the volume must be mounted
    // read-only
    u32 feats_needed_to_write;

    // file system ID (what is output by blkid)
    u32 fsid[4];

    // volume name (C-style string: characters terminated by a 0 byte)
    char volume_name[16];

    // path volume was last mounted to (C-style string: characters terminated
    // by a 0 byte)
    char volume_last_mounted[64];

    // compression algorithms used (see required features above)
    u32 compression_algorithms_used;

    // number of blocks to preallocate for files
    u8 blocks_to_prealloc_for_files;

    // number of blocks to preallocate for directories
    u8 blocks_to_prealloc_for_dirs;

    // (Unused)
    u16 __unused;

    // journal ID (same style as file system ID above)
    u32 journal_id[4];

    // journal inode
    u32 journal_inode;

    // journal device
    u32 journal_device;

    // head of orphan inode list
    u32 head_orphan_inode_list;

    // (Unused)
    u8 __unused2[788];
} __attribute__((packed));

static_assert(sizeof(ext2_superblock) == 1024);

struct ext2_block_group_descriptor {
    // block address of block usage bitmap
    u32 block_usage_bitmap_block_address;

    // block address of inode usage bitmap
    u32 inode_usage_bitmap_block_address;

    // starting block address of inode table
    u32 inode_table_block_address_start;

    // number of unallocated blocks in group
    u16 num_unallocated_blocks_in_group;

    // number of unallocated inodes in group
    u16 num_unallocated_inodes_in_group;

    // (Unused)
    u8 __unused[16];
} __attribute__((packed));

static_assert(sizeof(ext2_block_group_descriptor) == 32);

struct ext2_inode {
    // file type, owner, group, others
    mode_t mode;

    // user id
    uid_t uid;

    // lower 32 bits of size in bytes
    u32 sz_low;

    // last access time (in POSIX time)
    time_t last_access_time;

    // creation time (in POSIX time)
    time_t creation_time;

    // last modification time (in POSIX time)
    time_t last_modification_time;

    // deletion time (in POSIX time)
    time_t deletion_time;

    // group id
    gid_t gid;

    // count of hard links (directory entries) to this inode.
    // When this reaches 0, the data blocks are marked as unallocated
    nlink_t nlinks;

    // count of disk sectors (not ext2 blocks) in use by this inode,
    // not counting the actual inode structure nor directory entries
    // linking to the inode
    u32 ndisk_sectors;

    // flags
    ext2_inode_flags flags;

    // operating system specific value #1
    u32 os_specific1;

    // direct block pointers
    u32 *blocks[EXT2_TOTAL_BLOCKS];

    // generation number (primarily used for NFS)
    u32 generation_number;

    // in ext2 version 0, this field is reserved. In version >= 1, extended
    // attribute block (file ACL)
    u32 acl;

    // in ext2 version 0, this field is reserved. In version >= 1, upper 32
    // bits of file size (if feature bit set) if it's a file, Directory ACL
    // if it's a directory
    u32 upp32;

    // block address of fragment
    u32 frag_blk_address;

    // operating system specific value #2
    u32 os_specific[3];
} __attribute__((packed));

static_assert(sizeof(ext2_inode) == 128);

struct ext2_directory_entry {
    // inode
    u32 inode;

    // total size of this entry (including all subfields)
    u16 size;

    // name length least-significant 8 bits
    u8 name_len_lo;

    // type indicator (only if the feature bit for "directory
    // entries have file type byte" is set, else this is the most
    // significant 8 bits of the name length)
    u8 type;

    // name characters
    char *name;
};

struct blkdev;

auto ext2_init(const blkdev &dev) noexcept -> errno;
auto ext2_get_superblock(const blkdev &dev) noexcept -> ext2_superblock *;

#endif // VINTIX_FS_EXT2_H
