/**
 * Copyright (C) 2023-2024 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_FS_MINIX_H
#define VINTIX_FS_MINIX_H

#include <vintix/kernel/core/blkdev.h>
#include <vintix/kernel/filesystems/fs.h>
#include <vintix/kernel/filesystems/inode.h>
#include <vintix/kernel/filesystems/super.h>
#include <vintix/kernel/lib/array.h>
#include <vintix/kernel/lib/bitmap.h>
#include <vintix/kernel/lib/errno.h>
#include <vintix/kernel/lib/klog.h>
#include <vintix/kernel/lib/optional.h>
#include <vintix/kernel/lib/string_view.h>

#define MINIX_DEBUG(...) DEBUG("minixfsv1 " __VA_ARGS__)

// the values of the MINIX superblock present on disk only
struct minix_disk_superblock {
    // number of inodes
    u16 ninodes;

    // number of data zones
    u16 nzones;

    // space used by inode map (blocks)
    u16 imap_blocks;

    // space used by zone map (blocks)
    u16 zmap_blocks;

    // first zone with ''file'' data
    u16 firstdatazone;

    // size of a data zone
    u16 log_zone_size;

    // maximum file size (bytes)
    u32 max_file_size;

    // Minix 14/30 ID number
    u16 magic;
} __attribute__((packed));

static_assert(sizeof(minix_disk_superblock) == 18);

static constexpr usize NR_ZONES = 7;
// NOLINTBEGIN(cppcoreguidelines-avoid-c-arrays)
struct minix_raw_inode_v1 {
    mode_t mode;
    uid_t uid;
    u32 size;
    time_t time;
    u8 gid;
    u8 nlinks;
    u16 zone[NR_ZONES];
    u16 indirect;
    u16 double_indirect;
} __attribute__((packed));
// NOLINTEND(cppcoreguidelines-avoid-c-arrays)

static_assert(sizeof(minix_raw_inode_v1) == 32);

static constexpr usize MINIX_NAME_LENGTH = 30;
// NOLINTBEGIN(cppcoreguidelines-avoid-c-arrays)
struct minix_dir_entry {
    u16 inode;
    char name[MINIX_NAME_LENGTH];
} __attribute__((packed));
// NOLINTEND(cppcoreguidelines-avoid-c-arrays)

static_assert(sizeof(minix_dir_entry) == 32);

enum minix_inode_mode : mode_t {
    // inode type
    MINIX_TYPE = 0170000,

    // socket
    MINIX_SOCKET = 0140000,

    // symlink
    MINIX_SYMLINK = 0120000,

    // regular file
    MINIX_REGULAR_FILE = 0100000,

    // block special file
    MINIX_BLOCK_SPECIAL = 0060000,

    // directory
    MINIX_DIRECTORY = 0040000,

    // character special file
    MINIX_CHAR_SPECIAL = 0020000,

    // setuid
    MINIX_SETUID = 0004000,

    // setgid
    MINIX_SETGID = 0002000,

    // read bit (user)
    MINIX_USRREAD = 0000400,

    // write bit (user)
    MINIX_USRWRIT = 0000200,

    // execute bit (user)
    MINIX_USREXEC = 0000100,

    // read bit (group)
    MINIX_GRPREAD = 0000010,

    // write bit (group)
    MINIX_GRPWRIT = 0000020,

    // execute bit (group)
    MINIX_GRPEXEC = 0000010,

    // read bit (other)
    MINIX_OTHREAD = 0000004,

    // write bit (other)
    MINIX_OTHWRIT = 0000002,

    // execute bit (other)
    MINIX_OTHEXEC = 0000001,
};

enum minix_inode_flags {
    MINODE_UNSAVED_DIR = 1 << 8,
};

enum : u32 {
    MINIX_MAGIC = 0x138f,
    NIL_INODE = 0,
    NIL_ZONE = 0,
    ROOT_DIR_INODE = 1,
    MODE_CREATE_FILE = 01000,
    MINIX_BLOCK_SIZE = 1024,
    MINIX_LINK_MAX = 250,
    ROOT_DEV_NR = 256,
    BOOT_DEV_NR = 512, // taken from https://github.com/gdevic/minix1/blob/master/h/const.h
    DIRECTORY_CREATION_SIZE = 64,
};

class minix_superblock final : public superblock {
  public:
    auto alloc_zone() noexcept -> u16;
    u16 ninodes;
    u16 nzones;
    u16 imap_blocks;
    u16 zmap_blocks;
    u16 firstdatazone;
    u16 log_zone_size;
    vintix::heap_bitmap<u32> inode_bitmap;
    vintix::heap_bitmap<u32> zone_bitmap;
};

constexpr auto blkstosec(u32 blks) noexcept -> u32
{
    return blks * (MINIX_BLOCK_SIZE / DEFAULT_LBA_SECTOR_SIZE);
}

constexpr auto data_zone_lba_offset(const minix_superblock &super) noexcept -> u32
{
    return blkstosec(super.firstdatazone);
}

constexpr auto get_zone_size(u16 log_zone_size) noexcept -> u16
{
    return MINIX_BLOCK_SIZE >> log_zone_size;
}

constexpr auto get_zone_lba(const minix_superblock &super, u16 zone) noexcept -> u32
{
    return static_cast<u32>(zone << super.s_log_blksz) / DEFAULT_LBA_SECTOR_SIZE;
}

constexpr auto imap_lba_offset() noexcept -> u32 { return 4; }

constexpr auto zmap_lba_offset(const minix_superblock &super) noexcept -> u32
{
    return imap_lba_offset() + blkstosec(super.imap_blocks);
}

constexpr auto inode_table_lba_offset(const minix_superblock &super) noexcept -> u32
{
    return zmap_lba_offset(super) + blkstosec(super.zmap_blocks);
}

inline auto unload_inode_bitmap(const minix_superblock &super) noexcept -> errno
{
    return disk_write(super.s_dev, super.inode_bitmap.data(), imap_lba_offset(),
                      blkstosec(super.imap_blocks));
}

inline auto unload_zone_bitmap(const minix_superblock &super) noexcept -> errno
{
    return disk_write(super.s_dev, super.zone_bitmap.data(), zmap_lba_offset(super),
                      blkstosec(super.zmap_blocks));
}

inline auto fetch_inode_bitmap(minix_superblock &super) noexcept -> errno
{
    auto err = disk_read(super.s_dev, super.inode_bitmap.unsafe_mutable_data(), imap_lba_offset(),
                         blkstosec(super.imap_blocks));
    return err;
}

inline auto fetch_zone_bitmap(minix_superblock &super) noexcept -> errno
{
    auto err = disk_read(super.s_dev, super.zone_bitmap.unsafe_mutable_data(),
                         zmap_lba_offset(super), blkstosec(super.zmap_blocks));
    return err;
}

[[nodiscard]] inline auto get_zone(const minix_superblock &sup, void *dst,
                                   u16 zone) noexcept -> errno
{
    KASSERT(dst != nullptr);
    KASSERT(zone < sup.nzones);

    const u32 lba = get_zone_lba(sup, zone);

    return disk_read(sup.s_dev, dst, lba,
                     get_zone_size(sup.log_zone_size) / DEFAULT_LBA_SECTOR_SIZE);
}
[[nodiscard]] inline auto write_zone(const minix_superblock &sup, u16 zone,
                                     const void *src) noexcept -> errno
{
    KASSERT(src != nullptr);
    KASSERT(zone != 0);
    const u32 lba = get_zone_lba(sup, zone);

    return disk_write(sup.s_dev, src, lba,
                      get_zone_size(sup.log_zone_size) / DEFAULT_LBA_SECTOR_SIZE);
}

class minix_inode final : public inode {
  public:
    [[nodiscard]] static auto get(minix_superblock &sup, ino_t num) noexcept -> errptr<inode>;
    vintix::array<u16, NR_ZONES> zone;
    u16 indirect;
    u16 double_indirect;
};

#endif // VINTIX_FS_MINIX_H
