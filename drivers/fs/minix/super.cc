/**
 * Copyright (C) 2023-2024 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Vintix. If not, see <https://www.gnu.org/licenses/>.
 */

#include "minix.h"
#include <vintix/kernel/core/blkdev.h>
#include <vintix/kernel/filesystems/fs.h>
#include <vintix/kernel/filesystems/mount.h>
#include <vintix/kernel/lib/klog.h>
#include <vintix/kernel/lib/math.h>

extern "C" const u32 _DRV_PRIORITY = 1;

auto minix_superblock::alloc_zone() noexcept -> u16
{
    auto zone = static_cast<u16>(this->zone_bitmap.allocate_single_bit());

    if (zone == NIL_ZONE)
        return NIL_ZONE;

    this->set_dirty();
    return zone;
}

static auto alloc_inode(superblock *s) noexcept -> errptr<inode>
{
    auto *self = static_cast<minix_superblock *>(s);
    const auto num = static_cast<u16>(self->inode_bitmap.allocate_single_bit());

    if (num == NIL_INODE)
        return ENOSPC;

    auto *ino = static_cast<minix_inode *>(kmalloc(sizeof(minix_inode)));

    if (ino == nullptr) {
        self->inode_bitmap.free(num);
        return ENOMEM;
    }

    ino->i_num = num;
    s->set_dirty();
    return ino;
}

static auto root_inode(superblock *s) noexcept -> errptr<inode>
{
    return minix_inode::get(*static_cast<minix_superblock *>(s), ROOT_DIR_INODE);
}

static void minix_free(superblock *s) noexcept
{
    auto *self = static_cast<minix_superblock *>(s);
    kfree(self->inode_bitmap.unsafe_mutable_data());
    kfree(self->zone_bitmap.unsafe_mutable_data());
}

static auto minix_sync(const superblock *s) noexcept -> errno
{
    auto *self = static_cast<const minix_superblock *>(s);
    if (auto err = unload_inode_bitmap(*self))
        return err;
    if (auto err = unload_zone_bitmap(*self))
        return err;

    return ENOERR;
}

static constexpr superblock_vtable minix_superblock_vtable {
    .root_inode = root_inode,
    .alloc_inode = alloc_inode,
    .sync = minix_sync,
    .free = minix_free,
};

static auto minix_init_superblock(const fs_context &context) noexcept -> errptr<superblock>
{
    vintix::unique_ptr<minix_disk_superblock> super { kmalloc(DEFAULT_LBA_SECTOR_SIZE) };

    if (super == nullptr)
        return ENOMEM;

    if (auto err = disk_read(context.fs_dev, super.unsafe_mutable_data(), 2, 1))
        return err;

    if (super->magic != MINIX_MAGIC)
        return EINVAL;

    MINIX_DEBUG("superblock: magic number is 0x%x\n", super->magic);
    MINIX_DEBUG("superblock: max file size is %u bytes\n", super->max_file_size);
    MINIX_DEBUG("superblock: %u zones available\n", super->nzones);
    MINIX_DEBUG("superblock: %u inodes available\n", super->ninodes);
    MINIX_DEBUG("superblock: %u blocks reserved for zone bitmap\n", super->zmap_blocks);
    MINIX_DEBUG("superblock: %u blocks reserved for inode bitmap\n", super->imap_blocks);
    MINIX_DEBUG("superblock: zone size is %u bytes\n", get_zone_size(super->log_zone_size));

    auto *m_sup = static_cast<minix_superblock *>(kzalloc(sizeof(minix_superblock)));

    if (m_sup == nullptr)
        return ENOMEM;

    m_sup->s_max_file_size = super->max_file_size;
    m_sup->zmap_blocks = super->zmap_blocks;
    m_sup->imap_blocks = super->imap_blocks;
    m_sup->s_magic = super->magic;
    m_sup->nzones = super->nzones;
    m_sup->firstdatazone = super->firstdatazone;
    m_sup->ninodes = super->ninodes;
    m_sup->log_zone_size = super->log_zone_size;
    m_sup->s_log_blksz = log2(MINIX_BLOCK_SIZE);
    m_sup->s_dev = context.fs_dev;
    m_sup->inode_bitmap.unsafe_set_pointer(kmalloc(MINIX_BLOCK_SIZE * super->imap_blocks));
    m_sup->inode_bitmap.unsafe_set_size(MINIX_BLOCK_SIZE * super->imap_blocks / sizeof(u32));

    if (auto err = fetch_inode_bitmap(*m_sup)) {
        kfree(m_sup);
        return err;
    }

    m_sup->zone_bitmap.unsafe_set_pointer(kmalloc(MINIX_BLOCK_SIZE * super->zmap_blocks));
    m_sup->zone_bitmap.unsafe_set_size(MINIX_BLOCK_SIZE * super->zmap_blocks / sizeof(u32));

    if (auto err = fetch_zone_bitmap(*m_sup)) {
        kfree(m_sup);
        return err;
    }

    m_sup->vtable = &minix_superblock_vtable;
    m_sup->s_mnt_flags = context.flags;
    m_sup->s_max_name_length = MINIX_NAME_LENGTH;

    return m_sup;
}

extern "C" auto _drv_entry() noexcept -> errno
{
    genfs fs {};
    fs.name = "old-minix";
    fs.flags = MNT_NOATIME;
    fs.init_fs_context = &minix_init_superblock;
    fs.type = fs_type::GENERIC_FS;

    return register_filesystem(fs);
}
