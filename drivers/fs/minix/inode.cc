/**
 * Copyright (C) 2023-2024 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include "minix.h"
#include <vintix/kernel/core/blkdev.h>
#include <vintix/kernel/filesystems/file.h>
#include <vintix/kernel/filesystems/fs.h>
#include <vintix/kernel/lib/limits.h>
#include <vintix/kernel/lib/math.h>
#include <vintix/kernel/lib/result.h>

using zone_t = u16;

static auto minix_sync(const dentry *) noexcept -> errno;
static auto minix_lookup(const dentry *, const char *) noexcept -> errptr<inode>;
static auto minix_create(const dentry *, const char *) noexcept -> errptr<inode>;
static auto minix_mkdir(const dentry *, const char *) noexcept -> errptr<inode>;
static auto minix_open(inode *) noexcept -> errptr<file>;
static void minix_free(inode *) noexcept;

static constexpr inode_vtable minix_inode_vtable {
    .create = minix_create,
    .rm = nullptr,
    .mkdir = minix_mkdir,
    .rmdir = nullptr,
    .lookup = minix_lookup,
    .sync = minix_sync,
    .open = minix_open,
    .ls = nullptr,
    .free = minix_free,
};

static auto search_single_zone(const minix_superblock &sup, minix_dir_entry *dir, const char *name,
                               u16 zone) noexcept -> i32
{
    if (auto err = get_zone(sup, dir, zone))
        return neg(err);

    const usize nentries = get_zone_size(sup.log_zone_size) / sizeof(minix_dir_entry);

    if (name != nullptr) {
        for (usize k = 0; k < nentries; ++k) {
            if (dir[k].inode == NIL_INODE)
                continue;

            if (strcmp(static_cast<const char *>(dir[k].name), name) == 0)
                return static_cast<i32>(k);
        }

        return -ENOENT;
    }

    for (u16 k = 0; k < nentries; ++k)
        if (dir[k].inode == NIL_INODE)
            return k;

    return -ENOSPC;
}

static auto search_indirect_zone(const minix_superblock &sup, u16 *indirect, minix_dir_entry *dir,
                                 const char *name, u16 indirect_zone) noexcept -> i32
{
    if (auto err = get_zone(sup, indirect, indirect_zone))
        return -err;

    const errno ssz_err = name == nullptr ? -ENOSPC : -ENOENT;
    const usize zone_size = get_zone_size(sup.log_zone_size);

    for (usize z = 0; z < zone_size / sizeof(u16); ++z) {
        if (indirect[z] == NIL_ZONE)
            return -ENOENT;

        i32 off = search_single_zone(sup, dir, name, indirect[z]);
        if (off >= 0 || off != ssz_err)
            return off;
    }

    return ssz_err;
}

static auto minix_sync(const dentry *entry) noexcept -> errno
{
    auto *self = static_cast<const minix_inode *>(entry->d_ino);
    KASSERT(self->i_num != NIL_INODE);
    auto *m_sup = static_cast<const minix_superblock *>(entry->d_ino->i_sup);
    vintix::unique_ptr<minix_raw_inode_v1> ino_buf { kmalloc(MINIX_BLOCK_SIZE
                                                             << m_sup->log_zone_size) };

    const u32 lba = inode_table_lba_offset(*m_sup) +
                    (self->i_num - 1) * sizeof(minix_raw_inode_v1) / DEFAULT_LBA_SECTOR_SIZE;

    if (auto err = disk_read(m_sup->s_dev, ino_buf.unsafe_mutable_data(), lba, 1))
        return err;

    const u32 start = (self->i_num - 1) * sizeof(minix_raw_inode_v1) % DEFAULT_LBA_SECTOR_SIZE /
                      sizeof(minix_raw_inode_v1);

    ino_buf[start].size = static_cast<usize>(self->i_size);
    ino_buf[start].uid = self->i_uid;
    ino_buf[start].gid = static_cast<u8>(self->i_gid);
    ino_buf[start].mode = self->mode();
    ino_buf[start].time = self->i_mtime;
    ino_buf[start].nlinks = static_cast<u8>(self->i_nlinks);
    for (usize i = 0; i < self->zone.size(); ++i)
        ino_buf[start].zone[i] = self->zone[i];
    ino_buf[start].indirect = self->indirect;
    ino_buf[start].double_indirect = self->double_indirect;

    if (auto err = disk_write(m_sup->s_dev, ino_buf.get(), lba, 1))
        return err;

    return ENOERR;
}

static void minix_free(inode *ino) noexcept { kfree(ino); }

static auto minix_lookup(const dentry *dir, const char *name) noexcept -> errptr<inode>
{
    auto *self = static_cast<const minix_inode *>(dir->d_ino);
    auto m_sup = static_cast<minix_superblock *>(dir->d_ino->i_sup);
    const usize zone_size = get_zone_size(m_sup->log_zone_size);
    vintix::unique_ptr<u8> buf { kmalloc(zone_size * 3) };

    if (buf == nullptr)
        return ENOMEM;

    auto *dir_buf = reinterpret_cast<minix_dir_entry *>(buf.unsafe_mutable_data());

    for (usize i = 0; i < self->zone.size(); ++i) {
        if (self->zone[i] == NIL_ZONE)
            return ENOENT;

        i32 off = search_single_zone(*m_sup, dir_buf, name, self->zone[i]);
        if (off >= 0)
            return minix_inode::get(*m_sup, dir_buf[off].inode);
        if (off != -ENOENT)
            return -off;
    }

    if (self->indirect == NIL_ZONE)
        return ENOENT;

    auto *indir = reinterpret_cast<u16 *>(buf.unsafe_mutable_data() + zone_size);
    i32 off = search_indirect_zone(*m_sup, indir, dir_buf, name, self->indirect);

    if (off >= 0)
        return minix_inode::get(*m_sup, dir_buf[off].inode);
    if (off != -ENOENT)
        return -off;

    if (self->double_indirect == NIL_ZONE)
        return ENOENT;

    auto *double_indir = reinterpret_cast<u16 *>(buf.unsafe_mutable_data() + zone_size * 2);

    if (auto err = get_zone(*m_sup, indir, self->double_indirect))
        return err;

    for (usize i = 0; i < zone_size / sizeof(u16); ++i) {

        if (double_indir[i] == NIL_ZONE)
            return ENOENT;

        off = search_indirect_zone(*m_sup, indir, dir_buf, name, double_indir[i]);

        if (off >= 0)
            return minix_inode::get(*m_sup, dir_buf[off].inode);
        if (off != -ENOENT)
            return -off;
    }

    return ENOENT;
}

static auto minix_create(const dentry *entry, const char *) noexcept -> errptr<inode>
{
    auto *m_sup = static_cast<minix_superblock *>(entry->d_ino->i_sup);
    auto *m_ino = static_cast<minix_inode *>(kzalloc(sizeof(minix_inode)));

    if (m_ino == nullptr)
        return ENOMEM;

    m_ino->i_num = m_sup->inode_bitmap.allocate_single_bit();

    if (m_ino->i_num == NIL_INODE) {
        kfree(m_ino);
        return ENOSPC;
    }

    m_ino->vtable = &minix_inode_vtable;
    m_ino->i_mode = REGULAR_FILE;
    return m_ino;
}

static auto minix_mkdir(const dentry *entry, const char *) noexcept -> errptr<inode>
{
    auto *m_sup = static_cast<minix_superblock *>(entry->d_ino->i_sup);
    auto *m_ino = static_cast<minix_inode *>(kzalloc(sizeof(minix_inode)));

    if (m_ino == nullptr)
        return ENOMEM;

    m_ino->i_num = m_sup->inode_bitmap.allocate_single_bit();

    if (m_ino->i_num == NIL_INODE) {
        kfree(m_ino);
        return ENOSPC;
    }

    m_ino->zone[0] = static_cast<u16>(m_sup->zone_bitmap.allocate_single_bit());

    if (m_ino->zone[0] == NIL_ZONE) {
        kfree(m_ino);
        return ENOSPC;
    }

    m_ino->i_size = DIRECTORY_CREATION_SIZE;
    m_ino->vtable = &minix_inode_vtable;
    m_ino->i_mode = DIRECTORY;
    return m_ino;
}

static auto minix_zone_from_off(const file *fp, off_t off, u16 *buf,
                                bool read) noexcept -> result<zone_t>
{
    auto *m_sup = static_cast<minix_superblock *>(fp->inode()->i_sup);
    auto *m_ino = static_cast<minix_inode *>(fp->f_dentry->d_ino);
    usize zone_off = static_cast<usize>(off) >> m_sup->s_log_blksz;
    const usize zone_size = 1 << m_sup->s_log_blksz;
    zone_t zone_num = NIL_ZONE;
    u16 *indirect = buf;

    if (zone_off < m_ino->zone.size()) {
        if (!m_ino->zone[zone_off] && !read) {
            m_ino->zone[zone_off] = m_sup->alloc_zone();
            // failed to allocate the zone
            if (!m_ino->zone[zone_off])
                return ENOSPC;
        }
        zone_num = m_ino->zone[zone_off];
    } else if (m_ino->indirect != NIL_ZONE) {
        zone_off -= m_ino->zone.size();
        if (zone_off < zone_size / sizeof(u16)) {
            if (!indirect[zone_off] && !read) {
                indirect[zone_off] = m_sup->alloc_zone();

                if (indirect[zone_off] == NIL_ZONE)
                    return ENOSPC;

                if (auto err = write_zone(*m_sup, m_ino->indirect, indirect))
                    return err;
            }
            zone_num = indirect[zone_off];
        } else if (m_ino->double_indirect != NIL_ZONE) {
            u16 *double_indirect = buf + zone_size / sizeof(u16);
            const usize double_indirect_offset = (zone_off >> m_sup->s_log_blksz) - zone_size;
            KASSERT(double_indirect_offset <= USHRT_MAX);

            if (auto err = get_zone(*m_sup, indirect, double_indirect[double_indirect_offset]))
                return err;

            const usize indirect_offset = zone_off % (zone_size / sizeof(u16));

            if (indirect[indirect_offset] == NIL_ZONE && !read) {
                indirect[indirect_offset] = m_sup->alloc_zone();

                if (indirect[indirect_offset] == NIL_ZONE)
                    return ENOSPC;

                if (auto err =
                        write_zone(*m_sup, double_indirect[double_indirect_offset], indirect))
                    return err;
            }

            zone_num = indirect[indirect_offset];
        }
    }

    return zone_num;
}

static auto minix_put_blks(const file *fp, vintix::span<blkcnt_t> blks, off_t off,
                           bool read) noexcept -> errno
{
    auto *m_sup = static_cast<const minix_superblock *>(fp->inode()->i_sup);
    auto *m_ino = static_cast<const minix_inode *>(fp->inode());
    const usize zone_size = 1 << m_sup->s_log_blksz;
    const auto uoff = static_cast<usize>(off);
    vintix::unique_ptr<u16> buf { kmalloc(zone_size * 2) };

    if (buf == nullptr)
        return ENOMEM;

    const usize max_zone = (uoff >> m_sup->s_log_blksz) + blks.size();

    if (max_zone > m_ino->zone.size()) {
        KASSERT(m_ino->indirect != NIL_ZONE);
        if (auto err = get_zone(*m_sup, buf.unsafe_mutable_data(), m_ino->indirect))
            return err;
        if (max_zone > zone_size / sizeof(u16) + m_ino->zone.size()) {
            KASSERT(m_ino->double_indirect != NIL_ZONE);
            if (auto err =
                    get_zone(*m_sup, buf.unsafe_mutable_data() + zone_size, m_ino->double_indirect))
                return err;
        }
    }

    for (usize i = 0; i < blks.size(); ++i) {
        blks[i] = UNWRAP_RESULT(minix_zone_from_off(fp, off, buf.unsafe_mutable_data(), read));
        off += static_cast<off_t>(zone_size);
    }

    return ENOERR;
}

static constexpr file_vtable minix_file_vtable {
    .lseek = generic_file_lseek,
    .read = nullptr,
    .write = nullptr,
    .put_blks = minix_put_blks,
    .close = nullptr,
};

static auto minix_open(inode *) noexcept -> errptr<file>
{
    auto fp = static_cast<file *>(kmalloc(sizeof(file)));

    if (fp == nullptr)
        return ENOMEM;

    fp->f_pos = 0;
    fp->vtable = &minix_file_vtable;

    return fp;
}

auto minix_inode::get(minix_superblock &sup, ino_t num) noexcept -> errptr<inode>
{
    KASSERT(num != NIL_INODE);

    if (num > sup.ninodes)
        return ENOENT;

    const u32 lba = inode_table_lba_offset(sup) +
                    (num - 1) * sizeof(minix_raw_inode_v1) / DEFAULT_LBA_SECTOR_SIZE;

    vintix::unique_ptr<minix_raw_inode_v1> buf { kmalloc(DEFAULT_LBA_SECTOR_SIZE) };

    if (buf == nullptr)
        return ENOMEM;

    if (auto err = disk_read(sup.s_dev, buf.unsafe_mutable_data(), lba, 1))
        return err;

    const u32 start = (num - 1) * sizeof(minix_raw_inode_v1) % DEFAULT_LBA_SECTOR_SIZE /
                      sizeof(minix_raw_inode_v1);

    auto *ino = static_cast<minix_inode *>(kmalloc(sizeof(minix_inode)));

    if (ino == nullptr)
        return ENOMEM;

    ino->i_count = 0;
    ino->i_num = num;
    ino->i_size = static_cast<off_t>(buf[start].size);
    ino->i_nlinks = buf[start].nlinks;
    ino->i_uid = buf[start].uid;
    ino->i_gid = buf[start].gid;
    ino->i_mtime = buf[start].time;
    ino->i_flags = INODE_IS_DIRTY;
    ino->i_mode = buf[start].mode;
    for (usize i = 0; i < ino->zone.size(); ++i)
        ino->zone[i] = buf[start].zone[i];
    ino->indirect = buf[start].indirect;
    ino->double_indirect = buf[start].double_indirect;
    ino->vtable = &minix_inode_vtable;
    ino->i_sup = &sup;

    return ino;
}
