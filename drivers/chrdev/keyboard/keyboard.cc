/**
 * Copyright (C) 2023-2024 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include "keyboard.h"
#include "vintix/kernel/core/chrdev.h"
#include "vintix/kernel/core/dev.h"
#include "vintix/kernel/filesystems/file.h"
#include "vintix/kernel/filesystems/inode.h"
#include <vintix/kernel/core/idt.h>
#include <vintix/kernel/lib/errno.h>

struct interrupt_state;

// For more information see: https://wiki.osdev.org/%228042%22_PS/2_Controller
enum keyboard_status : u8 {
    // must be set before attempting to read data port from
    // IO port 0x60
    OUTPUT_BUFFER_STATUS = 0x00, // (0 = empty, 1 = full)

    // must be clear before attempting to write data to
    // IO port 0x60 or IO port 0x64
    INPUT_BUFFER_STATUS = 0x01, // (0 = empty, 1 = full)

    // meant to be cleared on reset and set by firmware
    // (via. PS/2 controller configuration byte) if the
    // system passes self tests (POST)
    SYSTEM_FLAG = 0x02,

    // (0 = data written to input buffer is data for PS/2 device)
    // (1 = data written to input buffer is data for PS/2 controller command)
    COMMAND_DATA = 0x04,

    // chipset specific
    // may be "keyboard lock" (more likely unused on modern systems)
    UNKNOWN_1 = 0x08,

    // chipset specific
    // may be "receive time-out" or "second PS/2 port output buffer full"
    UNKNOWN_2 = 0x10,

    TIMEOUT_ERROR = 0x20, // (0 = no error, 1 = timeout error)
    PARITY_ERROR = 0x40   // (0 = no error, 1 = parity error)
};

static auto get_kbd_key() noexcept -> vintix::optional<char>
{
    // we need to read the scancode here, otherwise the buffer will be full
    const u8 scancode = read_kbd_scancode();

    // is the key not being held down?
    if (scancode & KEY_RELEASED)
        return vintix::nullopt_t;

    char ascii = read_kbd_ascii(scancode);
    return ascii;
}

static void keyboard_handler(const interrupt_state &) noexcept {}

static auto kbd_read(file *, void *b, usize count) noexcept -> ssize
{
    char *buf = static_cast<char *>(b);
    for (usize i = 0; i < count;) {
        vintix::optional<char> key = get_kbd_key();

        if (key.is_none())
            continue;

        buf[i++] = key.value();
    }

    return static_cast<ssize>(count);
}

extern "C" auto _drv_entry() noexcept -> errno
{
    static const device_vtable dev_vtable {
        .blk_read = nullptr,
        .blk_write = nullptr,
        .blk_query = nullptr,
        .close = generic_device_close,
    };

    static const file_vtable f_vtable {
        .lseek = nullptr,
        .read = kbd_read,
        .write = nullptr,
        .put_blks = nullptr,
        .close = generic_file_close,
    };

    auto *dev = static_cast<chrdev *>(kzalloc(sizeof(chrdev)));

    if (dev == nullptr)
        return ENOMEM;

    if (auto err = dev->d_name.copy("keyboard"))
        return err;

    dev->d_major = 4;
    dev->d_minor = 1;
    dev->d_type = device_type::CHRDEV;
    dev->d_perms = USRREAD | GRPREAD | OTHREAD;
    dev->dev_vtable = &dev_vtable;
    dev->f_vtable = &f_vtable;
    install_irq_handler(1, keyboard_handler);
    return register_chrdev(dev);
}
