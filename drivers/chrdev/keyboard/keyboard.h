/**
 * Copyright (C) 2023-2024 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VINTIX_KEYBOARD_H
#define VINTIX_KEYBOARD_H

#include <vintix/kernel/core/io.h>
#include <vintix/kernel/lib/types.h>

struct interrupt_state;

static constexpr u8 KEYBOARD_DATA_PORT = 0x60;
static constexpr u8 KEYBOARD_STATUS_PORT = 0x64;  // for reading
static constexpr u8 KEYBOARD_COMMAND_PORT = 0x64; // for writing

static constexpr u8 KBD_ESC = 27;
static constexpr u8 KBD_CONTROL_KEY_SCANCODE = 157;

// held down scancodes
static constexpr u8 KBD_RIGHT_SHIFT_SCANCODE = 54;
static constexpr u8 KBD_LEFT_SHIFT_SCANCODE = 42;

// scancodes when not held down
static constexpr u8 KBD_CAPS_LOCK_RELEASED_SCANCODE = 186;
static constexpr u8 KBD_RIGHT_SHIFT_RELEASED_SCANCODE = 182;
static constexpr u8 KBD_LEFT_SHIFT_RELEASED_SCANCODE = 170;

// scancode for key release
static constexpr u8 KEY_RELEASED = 0x80;

static constexpr usize US_KBD_CHARS = 128;
static constexpr char KBD_US[US_KBD_CHARS] = {
    0,   KBD_ESC, '1', '2', '3', '4', '5', '6', '7', '8', '9',  '0', '-',  '=',  '\b', '\t',
    'q', 'w',     'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[',  ']', '\n', 0, /* <-- control key */
    'a', 's',     'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', '\'', '`', 0,    '\\', 'z',  'x',
    'c', 'v',     'b', 'n', 'm', ',', '.', '/', 0,   '*', 0, /* Alt */
    ' ',                                                     /* Space bar */
    0,                                                       /* Caps lock */
    0,   0,       0,   0,   0,   0,   0,   0,   0,   0,      /* F1-F10 */
    0,                                                       /* 69 - Num lock*/
    0,                                                       /* Scroll Lock */
    0,                                                       /* Home key */
    0,                                                       /* Up Arrow */
    0,                                                       /* Page Up */
    '-', 0,                                                  /* Left Arrow */
    0,   0,                                                  /* Right Arrow */
    '+', 0,                                                  /* 79 - End key*/
    0,                                                       /* Down Arrow */
    0,                                                       /* Page Down */
    0,                                                       /* Insert Key */
    0,                                                       /* Delete Key */
    0,   0,       0,   0,                                    /* F11 Key */
    0,                                                       /* F12 Key */
    0,                                                       /* All other keys are undefined */
};

// reads a byte (the scancode) from the keyboard's
// data port
inline auto read_kbd_scancode() noexcept -> u8 { return inb(KEYBOARD_DATA_PORT); }

// returns ASCII character equivalant of scancode
// assumes US keyboard layout
inline constexpr auto read_kbd_ascii(u8 scancode) noexcept -> char { return KBD_US[scancode]; }

#endif // VINTIX_KEYBOARD_H
