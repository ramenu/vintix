/**
 * Copyright (C) 2023-2024 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/core/chrdev.h>
#include <vintix/kernel/core/vmm.h>
#include <vintix/kernel/filesystems/file.h>
#include <vintix/kernel/filesystems/fs.h>
#include <vintix/kernel/lib/string.h>

static constexpr u8 DRIVER_VERSION_MAJOR = 0x02;
static constexpr u8 DRIVER_VERSION_MINOR = 0x01;

static auto read(file *, void *buf, usize count) noexcept -> ssize
{
    memset(buf, 0, count);
    return static_cast<ssize>(count);
}

static auto write(file *fp, const void *, usize count) noexcept -> ssize
{
    void *buf = kzalloc(count);

    if (buf == nullptr)
        return -ENOMEM;

    ssize written = kwrite(fp, buf, count);
    kfree(buf);
    return written;
}

extern "C" auto _drv_entry() noexcept -> errno
{
    auto *dev = static_cast<chrdev *>(kzalloc(sizeof(chrdev)));

    if (dev == nullptr)
        return ENOMEM;

    static const device_vtable d_vtable {
        .blk_read = nullptr,
        .blk_write = nullptr,
        .blk_query = nullptr,
        .close = generic_device_close,
    };

    static const file_vtable f_vtable {
        .lseek = generic_file_lseek,
        .read = read,
        .write = write,
        .put_blks = nullptr,
        .close = generic_file_close,
    };

    UNWRAP_ERR(dev->d_name.copy("zero"));
    dev->d_type = device_type::CHRDEV;
    dev->d_major = DRIVER_VERSION_MAJOR;
    dev->d_minor = DRIVER_VERSION_MINOR;
    dev->d_perms = USRREAD | USRWRIT | GRPREAD | GRPWRIT | OTHREAD | OTHWRIT;
    dev->dev_vtable = &d_vtable;
    dev->f_vtable = &f_vtable;
    return register_chrdev(dev);
}
