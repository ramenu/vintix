/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/core/aout.h>

#include <vintix/kernel/lib/types.h>

struct aout_exec {
    u32 a_midmag;
    u32 a_text;
    u32 a_data;
    u32 a_bss;
    u32 a_syms;
    u32 a_entry;
    u32 a_trsize;
    u32 a_drsize;
};

struct aout_relocation_info {
    u32 r_address;
    u32 r_symbolnum : 24, r_pcrel : 1, r_length : 2, r_extern : 1, r_baserel : 1, r_jmptable : 1,
        r_relative : 1, r_copy : 1;
};

auto run_aout_executable() noexcept -> int { return 0; }
