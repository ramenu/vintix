/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/core/elf.h>
#include <vintix/kernel/core/pmm.h>
#include <vintix/kernel/core/process.h>
#include <vintix/kernel/core/symbols.h>
#include <vintix/kernel/core/vmm.h>
#include <vintix/kernel/lib/context.h>
#include <vintix/kernel/lib/klog.h>
#include <vintix/kernel/lib/limits.h>
#include <vintix/kernel/lib/string.h>
#include <vintix/kernel/lib/types.h>
#include <vintix/kernel/lib/utilities.h>

#define ELF32_R_SYM(i) ((i) >> 8)
#define ELF32_R_TYPE(i) (static_cast<u8>((i)))
#define ELF32_R_INFO(s, t) (((s) << 8)) + ((static_cast<u8>(t))))
#define ELF32_ST_BIND(i) ((i) >> 4)
#define ELF32_ST_TYPE(i) ((i) & 0xf)
#define ELF32_ST_INFO(b, t) (((b) << 4) + ((t) & 0xf))
#define DO_R_386_32(S, A) ((S) + (A))
#define DO_R_386_PC32(S, A, P) ((S) + (A) - (P))

enum : u32 {
    ELFMAG0 = 0x7F,
    ELFMAG1 = 'E',
    ELFMAG2 = 'L',
    ELFMAG3 = 'F',
    EV_NONE = 0,
    EV_CURRENT = 1,
    ELF_CLASS_32 = 1,
    ELFDATANONE = 0,
    ELFDATA2LSB = 1, // least significant byte occupying the lowest address
    ELFDATA2MSB = 2, // most significant byte occupying the lowest address
    EM_386 = 0x03,

    // Object file type
    ET_NONE = 0x00,
    ET_REL = 0x01,
    ET_EXEC = 0x02,
    ET_DYN = 0x03,
    ET_CORE = 0x04,
    ET_LOPROC = 0xff00,
    ET_HIPROC = 0xffff,

    // Section header types
    SHT_NULL = 0,
    SHT_PROGBITS = 1,
    SHT_SYMTAB = 2,
    SHT_STRTAB = 3,
    SHT_RELA = 4,
    SHT_HASH = 5,
    SHT_DYNAMIC = 6,
    SHT_NOTE = 7,
    SHT_NOBITS = 8,
    SHT_REL = 9,
    SHT_SHLIB = 10,
    SHT_DYNSYM = 11,

    // Segment permissions
    PF_X = 0x1,               // execute
    PF_W = 0x2,               // write
    PF_R = 0x4,               // read
    PF_MASKPROC = 0xf0000000, // unspecified

    // Identify header indexes
    EL_MAG0 = 0,       // 0x7F
    EL_MAG1 = 1,       // 'E'
    EL_MAG2 = 2,       // 'L'
    EL_MAG3 = 3,       // 'F'
    EL_CLASS = 4,      // 32-bit or 64-bit
    EL_DATA = 5,       // byte order
    EL_VERSION = 6,    // ELF version
    EL_OSABI = 7,      // OS specific
    EL_ABIVERSION = 8, // OS specific
    EL_PAD = 9,        // padding

    // Program header types
    PT_NULL = 0,
    PT_LOAD = 1,
    PT_DYNAMIC = 2,
    PT_INTERP = 3,
    PT_NOTE = 4,
    PT_SHLIB = 5,
    PT_PHDR = 6,
    PT_LOPROC = 0x70000000,
    PT_HIPROC = 0x7fffffff,

    // Special section indexes
    SHN_UNDEF = 0,
    SHN_LORESERVE = 0xff00,
    SHN_LOPROC = 0xff00,
    SHN_HIPROC = 0xff1f,
    SHN_ABS = 0xfff1,
    SHN_COMMON = 0xfff2,
    SHN_HIRESERVE = 0xffff,

    // Relocation types
    R_386_NONE = 0,
    R_386_32 = 1,
    R_386_PC32 = 2,

    // Section attribute flags
    SHF_WRITE = 0x1,
    SHF_ALLOC = 0x2,
    SHF_EXECINSTR = 0x4,
    SHF_MASKPROC = 0xf0000000,

    // Symbol binding
    STB_LOCAL = 0,
    STB_GLOBAL = 1,
    STB_WEAK = 2,
    STB_LOPROC = 13,
    STB_HIPROC = 15,

    // Symbol table indexes
    STN_UNDEF = 0,

    // Symbol types
    STT_NOTYPE = 0,
    STT_OBJECT = 1,
    STT_FUNC = 2,
    STT_SECTION = 3,
    STT_FILE = 4,
    STT_LOPROC = 13,
    STT_HIPROC = 15,
};

static auto to_symbol(void *p) { return static_cast<elf32_sym *>(p); }
static auto to_reltab(void *p) { return static_cast<elf32_rel *>(p); }

static auto elf_is_supported(const elf32_ehdr *header) noexcept -> errno
{
    // is this a ELF object file?
    if (header->e_ident[EL_MAG0] != ELFMAG0)
        return EINVAL;
    if (header->e_ident[EL_MAG1] != ELFMAG1)
        return EINVAL;
    if (header->e_ident[EL_MAG2] != ELFMAG2)
        return EINVAL;
    if (header->e_ident[EL_MAG3] != ELFMAG3)
        return EINVAL;

    // we only support machines with files and virtual
    // address spaces up to 4GiB
    if (header->e_ident[EL_CLASS] != ELF_CLASS_32)
        return EINVAL;

    // EV_CURRENT is the original file format
    if (header->e_ident[EL_VERSION] != EV_CURRENT)
        return EINVAL;

    if (header->e_ident[EL_DATA] != ELFDATA2LSB)
        return EINVAL;

    if (header->e_type != ET_EXEC && header->e_type != ET_REL)
        return EINVAL;

    // Intel architecture supported only
    if (header->e_machine != EM_386)
        return EINVAL;

    // no program header table?
    if (header->e_type == ET_EXEC && header->e_phoff == 0)
        return EINVAL;

    // no section header table?
    if (header->e_shoff == 0)
        return EINVAL;

    // no entry point?
    if (header->e_type == ET_EXEC && header->e_entry == 0)
        return EINVAL;

    // only NULL section header table is present
    if (header->e_shnum <= 1)
        return EINVAL;

    // no .strtab
    if (header->e_shstrndx == 0)
        return EINVAL;

    return ENOERR;
}

static auto elf_lookup_symbol(const char *name, kcontext cxt) noexcept -> v_addr
{
    switch (cxt) {
        default:
            return KSYM_UNDEF_SYM;
        case kcontext::KERNEL_MODULE_RELOC:
            return ksym::addressof(name);
    }
}

auto elf32_shdr::get_segment(const elf32_ehdr &ehdr) const noexcept -> const elf32_phdr *
{
    for (elf32_half i = 1; i < ehdr.e_phnum; ++i) {
        const elf32_phdr *phdr = ehdr.program_header(i);

        if (this->sh_addr < phdr->p_vaddr)
            continue;

        if (this->sh_addr + this->sh_size <= phdr->p_vaddr + phdr->p_memsz)
            return phdr;
    }

    return nullptr;
}

auto elf32_phdr::get_section(const elf32_ehdr &ehdr) const noexcept -> const elf32_shdr *
{
    for (elf32_half i = 1; i < ehdr.e_shnum; ++i) {
        const elf32_shdr *shdr = ehdr.section_header(i);

        if (shdr->sh_addr < this->p_vaddr)
            continue;

        if (shdr->sh_addr + shdr->sh_size <= this->p_vaddr + this->p_memsz)
            return shdr;
    }

    return nullptr;
}

static auto elf_alloc_nobits(const elf32_ehdr &ehdr, kcontext) noexcept -> errptr<u8>
{
    vintix::vector<elf32_shdr *> nobits_shdrs;
    usize memsz = 0;
    for (elf32_half i = 1; i < ehdr.e_shnum; ++i) {
        elf32_shdr *shdr = ehdr.section_header(i);

        if (shdr->sh_type != SHT_NOBITS || !shdr->sh_size || !(shdr->sh_flags & SHF_ALLOC))
            continue;

        if (auto err = nobits_shdrs.push_back_copy(shdr))
            return err;

        memsz += align_up<MALLOC_ALIGNMENT>(shdr->sh_size);
    }

    if (memsz == 0)
        return nullptr;

    u8 *bss = static_cast<u8 *>(kzalloc(memsz));

    if (bss == nullptr)
        return ENOMEM;

    usize off = 0;
    for (usize i = 0; i < nobits_shdrs.size(); ++i) {
        nobits_shdrs[i]->sh_offset = addressof(bss + off) - addressof(&ehdr);
        off += nobits_shdrs[i]->sh_size;
    }

    return bss;
}

static auto elf_load_exec(const char *, const elf32_ehdr &ehdr) noexcept -> result<vm_map>
{
    usize alloc_sz = 0;
    for (elf32_half i = 0; i < ehdr.e_phnum; ++i) {
        const elf32_phdr *phdr = ehdr.program_header(i);

        if (phdr->p_type != PT_LOAD)
            continue;

        if (phdr->p_align != PAGE_FRAME_SIZE)
            return EINVAL;

        alloc_sz += align_up<PAGE_FRAME_SIZE>(phdr->p_memsz);
    }

    KASSERT(alloc_sz % PAGE_FRAME_SIZE == 0);

    vm_map map { ehdr.e_entry };

    for (elf32_half i = 0; i < ehdr.e_phnum; ++i) {
        const elf32_phdr *phdr = ehdr.program_header(i);

        if (phdr->p_type != PT_LOAD)
            continue;

        vm_map_entry entry {};
        entry.start = phdr->p_vaddr;
        entry.end = phdr->p_vaddr + phdr->p_memsz;

        if (phdr->p_offset > static_cast<elf32_word>(limit_of<off_t>()) ||
            phdr->p_memsz > static_cast<elf32_word>(limit_of<off_t>()) ||
            phdr->p_filesz > static_cast<elf32_off>(limit_of<off_t>()))
            return EINVAL;

        entry.object.pager.p_offset = static_cast<off_t>(phdr->p_offset);
        entry.object.pager.p_memsz = static_cast<off_t>(phdr->p_memsz);
        entry.object.pager.p_filesz = static_cast<off_t>(phdr->p_filesz);
        if (phdr->p_flags & PF_R)
            entry.prot |= VM_PROT_READ;
        if (phdr->p_flags & PF_W)
            entry.prot |= VM_PROT_WRITE;
        if (phdr->p_flags & PF_X)
            entry.prot |= VM_PROT_EXEC;

        if (auto err = map.insert(vintix::move(entry)))
            return err;
    }

    return map;
}

auto elf32_rel::relocate(const elf32_ehdr &ehdr, const elf32_shdr &rel,
                         kcontext cxt) const noexcept -> errno
{
    KASSERT(rel.sh_type == SHT_REL || rel.sh_type == SHT_RELA);

    // see ELF spec 1-14 for interpretation of sh_info and sh_link

    // shdr index of the section to which the relocation applies
    elf32_shdr *target = ehdr.section_header(rel.sh_info);

    auto *reloc = unsafe_toptr<v_addr *>(addressof(&ehdr) + target->sh_offset + this->r_offset);
    v_addr value = 0;

    if (ELF32_R_SYM(this->r_info) == SHN_UNDEF)
        return ENOERR;

    // shdr index of the associated symbol table
    elf32_shdr *symtab = ehdr.section_header(rel.sh_link);

    if (symtab->sh_type != SHT_SYMTAB)
        return EINVAL;

    const elf32_sym *symbol = to_symbol(symtab->at(ehdr, ELF32_R_SYM(this->r_info)));

    switch (symbol->st_shndx) {
        default: {
            const elf32_shdr *shdr = ehdr.section_header(symbol->st_shndx);
            value = addressof(&ehdr) + shdr->sh_offset + symbol->st_value;
            break;
        }
        case SHN_UNDEF: {
            if (symbol->st_name == SHT_NULL)
                return EINVAL;

            value = elf_lookup_symbol(symbol->name(ehdr, *symtab), cxt);

            if (value == KSYM_UNDEF_SYM) {
                if (ELF32_ST_BIND(symbol->st_info) & STB_WEAK)
                    value = 0;
                else
                    return ENOENT;
            }

            break;
        }
        case SHN_ABS: {
            value = symbol->st_value;
            break;
        }
    }

    switch (ELF32_R_TYPE(this->r_info)) {
        default:
            return EINVAL;
        case R_386_NONE:
            break;
        case R_386_32:
            *reloc = DO_R_386_32(value, *reloc);
            break;
        case R_386_PC32:
            *reloc = DO_R_386_PC32(value, *reloc, addressof(reloc));
            break;
    }

    return ENOERR;
}

static auto elf_relocate_reltab(const elf32_ehdr &ehdr, const elf32_shdr &rel,
                                kcontext cxt) noexcept -> errno
{
    for (elf32_word i = 0; i < rel.size(); ++i) {
        elf32_rel *reltab = to_reltab(rel.at(ehdr, i));

        if (auto err = reltab->relocate(ehdr, rel, cxt))
            return err;
    }

    return ENOERR;
}

auto elf_load_kmodule(const char *filename, void *data) noexcept -> result<gen_kernel_process>
{
    DEBUG("elf_load_kmodule: Loading driver \"%s\"\n", filename);
    auto *ehdr = static_cast<const elf32_ehdr *>(data);

    if (auto err = elf_is_supported(ehdr))
        return err;

    if (ehdr->e_type != ET_REL)
        return EINVAL;

    DEBUG("elf_load_kmodule: OK... ELF header is valid for \"%s\"\n", filename);
    errptr<u8> bss = elf_alloc_nobits(*ehdr, kcontext::KERNEL_MODULE_RELOC);

    if (bss.get() != nullptr && bss.is_err())
        return bss.err();

    for (elf32_half i = 1; i < ehdr->e_shnum; ++i) {
        const elf32_shdr *shdr = ehdr->section_header(i);

        if (shdr->sh_type != SHT_REL)
            continue;

        if (auto err = elf_relocate_reltab(*ehdr, *shdr, kcontext::KERNEL_MODULE_RELOC))
            return err;
    }

    for (elf32_word i = 1; i < ehdr->e_shnum; ++i) {
        const elf32_shdr *shdr = ehdr->section_header(i);

        if (shdr->sh_type != SHT_SYMTAB)
            continue;

        for (elf32_word j = 1; j < shdr->size(); ++j) {
            const elf32_sym *symbol = to_symbol(shdr->at(*ehdr, j));

            if (ELF32_ST_TYPE(symbol->st_info) != STT_FUNC ||
                ELF32_ST_BIND(symbol->st_info) != STB_GLOBAL || symbol->st_name == STN_UNDEF ||
                symbol->st_shndx == SHN_UNDEF)
                continue;

            if (strcmp(symbol->name(*ehdr, *shdr), "_drv_entry") != 0)
                continue;

            const elf32_shdr *load_shdr = ehdr->section_header(symbol->st_shndx);
            const v_addr entrypoint = addressof(ehdr) + load_shdr->sh_offset + symbol->st_value;
            gen_kernel_process gen {};
            gen._drv_entry = reinterpret_cast<errno (*)()>(entrypoint);
            gen.data = data;
            gen.bss = bss.get();
            gen.destructor = nullptr;

            return gen;
        }
    }

    return EINVAL;
}

auto elf_load(const char *filename, const void *data) noexcept -> result<vm_map>
{
    DEBUG("elf_load: Loading \"%s\"\n", filename);
    auto *ehdr = static_cast<const elf32_ehdr *>(data);

    if (auto err = elf_is_supported(ehdr))
        return err;

    DEBUG("elf_load: OK... ELF header is valid for \"%s\"\n", filename);
    switch (ehdr->e_type) {
        default:
            return EINVAL;
        case ET_EXEC:
            return elf_load_exec(filename, *ehdr);
    }
}
