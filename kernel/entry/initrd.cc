/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <grub/multiboot.h>
#include <vintix/kernel/core/args.h>
#include <vintix/kernel/core/dev.h>
#include <vintix/kernel/core/elf.h>
#include <vintix/kernel/core/io.h>
#include <vintix/kernel/core/kassert.h>
#include <vintix/kernel/core/macros.h>
#include <vintix/kernel/core/mbt.h>
#include <vintix/kernel/core/panic.h>
#include <vintix/kernel/core/pmm.h>
#include <vintix/kernel/core/process.h>
#include <vintix/kernel/core/vmm.h>
#include <vintix/kernel/filesystems/fs.h>
#include <vintix/kernel/filesystems/initrd.h>
#include <vintix/kernel/filesystems/mount.h>
#include <vintix/kernel/lib/errno.h>
#include <vintix/kernel/lib/klog.h>
#include <vintix/kernel/lib/string.h>
#include <vintix/kernel/lib/unique_ptr.h>
#include <vintix/kernel/lib/utilities.h>

#define DRIVER_MODULES_DIR "/boot/modules"

static void *initrd = nullptr;

static auto load_initrd_modules() noexcept -> errno;

static void unload_ustar_module() noexcept { unregister_filesystem("tarfs"); }

void unload_initrd() noexcept
{
    KASSERT(initrd != nullptr);
    kfree(initrd);
    initrd = nullptr;
}

auto load_initrd() noexcept -> errno
{
    KASSERT(mbt != nullptr);

    if (mbt->mods_count < 2)
        return EINVAL;

    bool loaded_star_mod = false;
    auto *mod = unsafe_toptr<const multiboot_module_t *>(mbt->mods_addr + VADDR_OFFSET);

    for (usize i = 0; i < mbt->mods_count; ++i) {
        vintix::unique_ptr<char> path { kcmdline::path_from(mod[i].cmdline + VADDR_OFFSET) };

        if (path == nullptr)
            return ENOMEM;

        // it's a good idea to map the module somewhere else. We don't
        // know where exactly the module is loaded, so it could interfere
        // with userspace processes later on. Therefore, we should place it
        // onto the kernel heap instead.
        const usize bytes_used = mod[i].mod_end - mod[i].mod_start;
        void *module = kmalloc(bytes_used);

        if (module == nullptr)
            return ENOMEM;

        DEBUG("initrd: Allocated %ul bytes for module \"%s\"\n", bytes_used, path.get());
        memcpy(module, unsafe_toptr<const void *>(mod[i].mod_start + VADDR_OFFSET), bytes_used);

        // free the pages used by the module
        free_pages(mod[i].mod_start + VADDR_OFFSET, bytes_used / PAGE_FRAME_SIZE + 1);

        if (strcmp(path.get(), INITRD_PATH) == 0) {
            ok("found initrd at %s\n", path.get());
            DEBUG("initrd: loaded at 0x%x\n", mod[i].mod_start + VADDR_OFFSET);
            initrd = module;
        } else if (strcmp(path.get(), USTAR_MOD) == 0) {
            DEBUG("ustar: loaded at 0x%x\n", mod[i].mod_start + VADDR_OFFSET);
            gen_kernel_process gen { UNWRAP_RESULT(elf_load_kmodule(USTAR_MOD, module)) };
            gen.destructor = unload_ustar_module;
            gen.data = module;

            if (auto err = kprocess::load_module("ustar.mod", vintix::move(gen)))
                return err;

            loaded_star_mod = true;
        }
    }

    if (!loaded_star_mod || initrd == nullptr)
        return ENOENT;

    if (auto err = kmount(S_NOTMOUNTED, "/", "tarfs", MNT_RDONLY | MNT_NOATIME | MNT_KERNEL,
                          addressof(initrd)))
        return err;

    ok("mounted initrd on root\n");

    return load_initrd_modules();
}

static auto load_initrd_modules() noexcept -> errno
{
    file *fp = UNWRAP_PTR(kopen(DRIVER_MODULES_DIR, O_RDONLY | O_DIRECTORY | O_CLOEXEC | O_KERNEL));
    vintix::vector entries { UNWRAP_RESULT(ls(fp)) };

    if (entries.empty())
        return ENOERR;

    DEBUG("load_initrd_modules: Found %ul modules\n", entries.size());
    for (usize i = 0; i < entries.size(); ++i) {
        if (auto err = entries[i].prepend(DRIVER_MODULES_DIR "/"))
            return err;

        if (auto err = kprocess::load_module(entries[i].c_str()))
            return err;

        ok("loaded %s\n", entries[i].c_str());
    }

    kclose(fp);
    ok("successfully loaded modules from initrd\n");
    return ENOERR;
}
