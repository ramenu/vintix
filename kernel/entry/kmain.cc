/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <grub/multiboot.h>
#include <vintix/kernel/core/args.h>
#include <vintix/kernel/core/dev.h>
#include <vintix/kernel/core/elf.h>
#include <vintix/kernel/core/gdt.h>
#include <vintix/kernel/core/idt.h>
#include <vintix/kernel/core/interrupt.h>
#include <vintix/kernel/core/io.h>
#include <vintix/kernel/core/mbt.h>
#include <vintix/kernel/core/panic.h>
#include <vintix/kernel/core/pmm.h>
#include <vintix/kernel/core/process.h>
#include <vintix/kernel/core/serial.h>
#include <vintix/kernel/core/vmm.h>
#include <vintix/kernel/filesystems/file.h>
#include <vintix/kernel/filesystems/fs.h>
#include <vintix/kernel/filesystems/initrd.h>
#include <vintix/kernel/filesystems/mount.h>
#include <vintix/kernel/lib/errno.h>
#include <vintix/kernel/lib/klog.h>

static void print_physical_memory_layout() noexcept;

#if 0
// DO NOT REMOVE THIS!
// This is needed so that the linker can find the `atexit` stub.
extern "C" void atexit() noexcept { return; }
#endif

static auto load_disk_modules() noexcept -> errno;

// the entry point to the C++ code
extern "C" void kmain(p_addr physical_start, u32 bitmap_count) noexcept
{
    disable_interrupts();
    setup_gdt();
    DEBUG("OK... Initialized GDT\n");
    setup_idt();
    DEBUG("OK... Initialized IDT\n");
    enable_interrupts();
    physical_start = init_paging(physical_start, bitmap_count);
    init_memory(unsafe_toptr<void *>(physical_start + VADDR_OFFSET));
    if (auto err = serial_init())
        kpanic("failed to register serial driver, error code: %d\n", err);
    if (auto err = fb_init())
        kpanic("failed to initialize framebuffer, error code: %d\n", err);
    DEBUG("OK... Initialized kernel heap\n");
    DEBUG("kernel command line parameters: %s\n",
          unsafe_toptr<const char *>(VADDR_OFFSET + mbt->cmdline));
    DEBUG("kernel number of modules loaded: %u\n", mbt->mods_count);
    DEBUG("kernel module loaded at: 0x%x\n", mbt->mods_addr);

    if (auto err = kcmdline::init_cmdline(VADDR_OFFSET + mbt->cmdline))
        kpanic("failed to parse command line, error code: %d\n", err);
    DEBUG("OK... Successfully parsed command line arguments\n");

    if (auto err = load_initrd())
        kpanic("failed to load initrd, error code: %d\n", err);

    DEBUG("OK... Successfully loaded initrd\n");
    DEBUG("OK... loaded necessary drivers from initrd\n");

    if (auto err = kumount("/", MNT_KERNEL))
        kpanic("failed to unmount '/', error code: %d\n", err);

    kprocess::unload_module("ustar.mod");
    unload_initrd();

    // initrd has now been unmounted and we no longer have any use case
    // for the multiboot modules, so let's free up some space
    for (v_addr vaddr = 0x1000 + VADDR_OFFSET; vaddr < 0x80000 + VADDR_OFFSET;
         vaddr += PAGE_FRAME_SIZE)
        free_page(get_page(vaddr));

    if (auto err = kmount(kcmdline::root_dev(), "/", kcmdline::root_dev_fs(),
                          kcmdline::root_dev_flags() | MNT_KERNEL))
        kpanic("failed to mount device: error code %d\n", err);

    ok("mounted %s (%s) on root\n", kcmdline::root_dev(), kcmdline::root_dev_fs());

    if (auto err = device::init_devfs())
        kpanic("failed to initialize devfs, error code: %d\n", err);

    if (auto err = kmount(S_NOTMOUNTED, "/dev", "devtmpfs",
                          MNT_RDONLY | MNT_NOEXEC | MNT_NOATIME | MNT_KERNEL))
        kpanic("failed to mount /dev, error code: %d\n", err);

    ok("mounted /dev\n");
    if (auto err = load_disk_modules())
        kpanic("failed to load modules from disk, error code: %d\n", err);

    ok("loaded modules from disk\n");

    fb_clear();
    process::init();
    if (auto err = process::running()->load("/bin/init"))
        kpanic("failed to start \"/bin/init\", error code: %d\n", err);
}

static auto load_disk_modules() noexcept -> errno
{
    // no modules to load?
    if (kcmdline::modules() == nullptr)
        return ENOERR;

    for (usize i = 0; i < kcmdline::modules().size(); ++i) {
        vintix::string module_path { "/boot/modules/" };

        if (module_path == nullptr)
            return ENOMEM;

        if (auto err = module_path.append(kcmdline::modules()[i]))
            return err;
        if (auto err = module_path.append(".mod"))
            return err;

        if (auto err = kprocess::load_module(module_path.c_str()))
            return err;

        ok("loaded %s\n", module_path.c_str());
    }

    return ENOERR;
}
