/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/core/args.h>
#include <vintix/kernel/core/io.h>
#include <vintix/kernel/core/panic.h>
#include <vintix/kernel/core/vmm.h>
#include <vintix/kernel/filesystems/fs.h>
#include <vintix/kernel/lib/cxxstring.h>
#include <vintix/kernel/lib/errno.h>
#include <vintix/kernel/lib/result.h>
#include <vintix/kernel/lib/string.h>
#include <vintix/kernel/lib/utilities.h>

static constexpr auto get_next_arg(const char *cmdline) noexcept -> const char *
{
    for (usize i = 0; cmdline[i] != '\0'; ++i)
        if (isspace(cmdline[i]))
            return &cmdline[i + 1];

    return nullptr;
}

static constexpr auto arg_length(const char *arg) noexcept -> usize
{
    for (usize i = 0; arg[i] != '\0'; ++i)
        if (arg[i] == '=')
            return i;

    return 0;
}

static constexpr auto value_length(const char *value) noexcept -> vintix::optional<usize>
{
    for (usize i = 0; value[i] != '\0'; ++i)
        if (isspace(value[i]) || value[i + 1] == '\0')
            return i + 1;
    return vintix::nullopt_t;
}

static auto get_value(const char *value) noexcept -> char *
{
    const vintix::optional<usize> value_sz = value_length(value);

    // invalid value
    if (value_sz.is_none())
        return nullptr;

    auto s = static_cast<char *>(kzalloc(value_sz.value() + 1));

    if (s == nullptr)
        return nullptr;

    usize i = value_sz.value();
    if (value[i] != '\0')
        --i;

    memcpy(s, value, i);
    return s;
}

static auto get_arg(const char *cmdline, const char *name) noexcept -> char *
{
    const usize name_sz = strlen(name);

    while ((cmdline = get_next_arg(cmdline))) {
        const usize argsz = arg_length(cmdline);

        // invalid argument, stop parsing
        if (argsz == 0)
            return nullptr;

        if (argsz != name_sz)
            continue;

        if (strncmp(cmdline, name, argsz) == 0)
            return get_value(cmdline + argsz + 1);
    }

    return nullptr;
}

static auto get_int_arg(const char *cmdline, const char *name) -> int
{
    int n;
    char *root_flags = get_arg(cmdline, name);

    if (root_flags == nullptr)
        n = 0;
    else
        n = atoi(root_flags); // NOLINT(*-err34-c)

    kfree(root_flags);
    return n;
}

static auto get_arg_list(const char *cmdline, const char *name) -> vintix::vector<vintix::string>
{
    vintix::owned_cstr arg { get_arg(cmdline, name) };

    if (arg == nullptr)
        return nullptr;

    return arg.split(',').unwrap_or(nullptr);
}

auto kcmdline::path_from(multiboot_uint32_t cmdline_address) noexcept -> char *
{
    const char *cmdline = unsafe_toptr<const char *>(cmdline_address);

    for (usize i = 1; cmdline[i] != '\0'; ++i) {
        if (cmdline[i + 1] == '\0' || isspace(cmdline[i])) {
            char *path = static_cast<char *>(kzalloc(i + 2));

            if (path == nullptr)
                return nullptr;

            memcpy(path, cmdline, i + 1);
            return path;
        }
    }

    return nullptr;
}

auto kcmdline::init_cmdline(multiboot_uint32_t cmdline_address) noexcept -> errno
{
    const char *cmdline = unsafe_toptr<const char *>(cmdline_address);

    c_kernel_path = kcmdline::path_from(cmdline_address);
    c_root = get_arg(cmdline, "root");

    if (c_root == nullptr)
        kpanic("no device to mount root on\n");

    c_root_fs = get_arg(cmdline, "root_fs");

    if (c_root_fs == nullptr)
        kpanic("root filesystem is not specified\n");

    vintix::vector root_flags { get_arg_list(cmdline, "root_flags") };
    c_root_flags = 0;

    for (usize i = 0; i < root_flags.size(); ++i) {
        if (strcmp(root_flags[i].c_str(), "ro") == 0)
            c_root_flags |= MNT_RDONLY;
        else if (strcmp(root_flags[i].c_str(), "noexec") == 0)
            c_root_flags |= MNT_NOEXEC;
        else if (strcmp(root_flags[i].c_str(), "noatime") == 0)
            c_root_flags |= MNT_NOATIME;
        else
            kpanic("invalid mount flag specified for root: \"%s\"", root_flags[i].c_str());
    }

    c_modules = get_arg_list(cmdline, "modules");
    c_swapfile = get_arg(cmdline, "swapfile");

    return ENOERR;
}
