/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/filesystems/blkio.h>
#include <vintix/kernel/filesystems/file.h>
#include <vintix/kernel/filesystems/inode.h>
#include <vintix/kernel/lib/algorithm.h>

static constexpr int BLKIO_CHANGE_POS = 128;

static constexpr auto blk_to_sector(blkcnt_t blk, usize blksz) noexcept -> u32
{
    return blk * blksz / DEFAULT_LBA_SECTOR_SIZE;
}

static auto rw_single_block(file *fp, u8 *buf, blkcnt_t blk, usize blksz,
                            int flags) noexcept -> errno
{
    const u32 sector_start = blk_to_sector(blk, blksz);

    if (fp->is_blkdev() || blk != 0) {
        const dev_t dev = fp->is_blkdev() ? fp->inode()->i_num - 1 : fp->inode()->i_sup->s_dev;
        if (flags & BLKIO_READ) {
            if (auto err = disk_read(dev, buf, sector_start, blksz / DEFAULT_LBA_SECTOR_SIZE))
                return err;
        } else {
            if (auto err = disk_write(dev, buf, sector_start, blksz / DEFAULT_LBA_SECTOR_SIZE))
                return err;
        }
    } else {
        memsetd(buf, 0, blksz / sizeof(u32));
    }

    if (flags & BLKIO_CHANGE_POS) {
        fp->f_pos = fp->vtable->lseek(fp, fp->f_pos + static_cast<off_t>(blksz), SEEK_SET);

        // error occurred
        if (fp->f_pos < 0)
            return -fp->f_pos;
    }

    return ENOERR;
}

static auto rw_consecutive_blocks(file *fp, u8 *buf, vintix::span<const blkcnt_t> blks, usize blksz,
                                  int flags) noexcept -> errno
{
    const u32 sector_start = blk_to_sector(blks[0], blksz);

    const dev_t dev = fp->is_blkdev() ? fp->inode()->i_num - 1 : fp->inode()->i_sup->s_dev;

    if (flags & BLKIO_READ) {
        if (auto err =
                disk_read(dev, buf, sector_start, blksz * blks.size() / DEFAULT_LBA_SECTOR_SIZE))
            return err;
    } else {
        if (auto err =
                disk_write(dev, buf, sector_start, blksz * blks.size() / DEFAULT_LBA_SECTOR_SIZE))
            return err;
    }

    if (flags & BLKIO_CHANGE_POS) {
        fp->f_pos =
            fp->vtable->lseek(fp, fp->f_pos + static_cast<off_t>(blksz * blks.size()), SEEK_SET);

        // error occurred
        if (fp->f_pos < 0)
            return -fp->f_pos;
    }

    return ENOERR;
}

static auto rw_blocks(file *fp, vintix::span<const blkcnt_t> blks, u8 *buf, usize blksz,
                      int flags) noexcept -> errno
{
    usize i = 0;
    while (i < blks.size()) {
        const usize num_consecutive =
            vintix::count_consecutive_numbers<blkcnt_t>({ blks.data() + i, blks.size() - i });
        if (num_consecutive == 0) {
            if (auto err = rw_single_block(fp, buf + i * blksz, blks[i], blksz, flags))
                return err;
            ++i;
        } else {
            if (auto err = rw_consecutive_blocks(
                    fp, buf + i * blksz, { blks.data() + i, num_consecutive + 1 }, blksz, flags))
                return err;
            i += num_consecutive + 1;
        }
    }

    return ENOERR;
}

static constexpr auto offset_is_block_aligned(off_t off, usize blksz) noexcept -> bool
{
    return (static_cast<usize>(off) % blksz == 0);
}

static constexpr auto is_block_aligned(off_t off, usize blksz, usize count) noexcept -> bool
{
    return offset_is_block_aligned(off, blksz) && count % static_cast<usize>(blksz) == 0;
}

static auto blkrw(file *fp, u8 *buf, usize count, usize log_blksz,
                  vintix::span<const blkcnt_t> blks, int flags) noexcept -> ssize
{
    const usize blksz = 1 << log_blksz;
    const off_t orig = fp->f_pos;
    if (is_block_aligned(fp->f_pos, blksz, count) &&
        (fp->is_blkdev() || static_cast<usize>(orig) + count <= fp->size())) {
        if (auto err = rw_blocks(fp, blks, buf, blksz, (flags & BLKIO_READ) | BLKIO_CHANGE_POS))
            return -err;
    } else {
        usize step = 0;

        vintix::unique_ptr<u8> tmp { kmalloc(blksz) };

        if (tmp == nullptr)
            return -ENOMEM;

        // first make sure the offset is aligned
        if (!offset_is_block_aligned(fp->f_pos, blksz)) {
            if (auto err =
                    rw_single_block(fp, tmp.unsafe_mutable_data(), blks.front(), blksz, BLKIO_READ))
                return -err;

            const usize off_mod = fp->uoff() % blksz;
            const usize bytes_to_copy = (off_mod + count <= blksz) ? count : blksz - off_mod;

            if (flags & BLKIO_READ) {
                memcpy(buf, tmp.get() + off_mod, bytes_to_copy);
            } else {
                memcpy(tmp.unsafe_mutable_data() + off_mod, buf, bytes_to_copy);
                if (auto err =
                        rw_single_block(fp, tmp.unsafe_mutable_data(), blks.front(), blksz, 0))
                    return -err;
            }

            fp->f_pos =
                fp->vtable->lseek(fp, fp->f_pos + static_cast<off_t>(bytes_to_copy), SEEK_SET);

            // error occurred while seeking?
            if (fp->f_pos < 0)
                return fp->f_pos;

            if (off_mod + count <= blksz)
                return (flags & BLKIO_REACHED_EOF) ? EOF : static_cast<off_t>(bytes_to_copy);

            KASSERT(fp->uoff() % blksz == 0);
            ++step;
            count -= bytes_to_copy;
        }

        // at this point, offset is now aligned with the filesystem block size
        if (count % blksz != 0) {
            if (count > blksz) {
                const usize span_sz = (step == 0) ? blks.size() - 1 : blks.size() - step - 1;
                if (auto err =
                        rw_blocks(fp, { blks.data() + step, span_sz }, buf + (fp->f_pos - orig),
                                  blksz, (flags & BLKIO_READ) | BLKIO_CHANGE_POS))
                    return -err;
                count -= span_sz << log_blksz;
            }

            KASSERT(fp->uoff() % blksz == 0);

            // the file position is now set to the correct offset assuming
            // no errors occurred. Now let's read the rest of the block into a
            // temporary buffer and copy the contents into 'buf'.
            if (auto err =
                    rw_single_block(fp, tmp.unsafe_mutable_data(), blks.back(), blksz, BLKIO_READ))
                return -err;

            if (flags & BLKIO_READ) {
                memcpy(buf + (fp->f_pos - orig), tmp.get(), count);
            } else {
                memcpy(tmp.unsafe_mutable_data(), buf + (fp->f_pos - orig), count);
                if (auto err =
                        rw_single_block(fp, tmp.unsafe_mutable_data(), blks.back(), blksz, 0))
                    return -err;
            }

            fp->f_pos = fp->vtable->lseek(fp, static_cast<off_t>(fp->uoff() + count), SEEK_SET);

            // error occurred while seeking?
            if (fp->f_pos < 0)
                return fp->f_pos;

        } else {
            if (auto err = rw_blocks(fp, { blks.data() + step, blks.size() - step }, buf, blksz,
                                     (flags & BLKIO_READ) | BLKIO_CHANGE_POS))
                return -err;
        }
    }

    return (flags & BLKIO_REACHED_EOF) ? EOF : fp->f_pos - orig;
}

auto blkio(file *fp, void *buf, usize count, int flags) noexcept -> ssize
{
    const blksize_t log_blksz = fp->log_blksz();
    const usize blksz = 1 << log_blksz;

    // Operation not supported
    if (!fp->is_blkdev() && (fp->f_dentry->d_ino->i_sup->s_mnt_flags & MNT_NODEV))
        return -EINVAL;

    usize num_blks = (count >> log_blksz) + 1;
    if ((fp->uoff() % blksz) + (count % blksz) > blksz)
        ++num_blks;

    KASSERT(num_blks != 0);
    vintix::vector blks { static_cast<blkcnt_t *>(kmalloc(num_blks * sizeof(blkcnt_t))), num_blks };

    if (blks == nullptr)
        return -ENOMEM;

    KASSERT(fp->vtable->put_blks != nullptr);

    if (auto err = fp->vtable->put_blks(fp, vintix::span<blkcnt_t> { blks }, fp->f_pos,
                                        flags & BLKIO_READ))
        return -err;

    if constexpr (DEBUG_BUILD) {
        for (usize i = 0; i < blks.size(); ++i) {
            DEBUG("blkio: blks[%u] = %u\n", i, blks[i]);
        }
    }

    return blkrw(fp, static_cast<u8 *>(buf), count, static_cast<usize>(log_blksz),
                 vintix::span<const blkcnt_t>(blks), flags);
}
