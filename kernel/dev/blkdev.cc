/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/arch/x86-32/mbr.h>
#include <vintix/kernel/core/blkdev.h>
#include <vintix/kernel/core/kassert.h>
#include <vintix/kernel/core/vmm.h>
#include <vintix/kernel/filesystems/file.h>
#include <vintix/kernel/lib/klog.h>
#include <vintix/kernel/lib/types.h>
#include <vintix/kernel/lib/unique_ptr.h>

auto disk_add(blkdev *disk) noexcept -> errno
{
    KASSERT(disk != nullptr);
    KASSERT(disk->dev_vtable != nullptr);
    KASSERT(disk->d_name != nullptr);
    KASSERT(disk->d_type == device_type::BLKDEV);

    if (auto err = device::add_device(disk))
        return err;

    // is this a root device?
    if (disk->lba == 0x0) {
        DEBUG("%s: Checking for additional partitions...\n", disk->d_name.c_str());

        // Check for additional partitions
        vintix::unique_ptr<mbr> bootrec { kmalloc(sizeof(mbr)) };

        if (bootrec == nullptr)
            return ENOMEM;

        if (auto err = parse_master_boot_record(disk->id(), bootrec.unsafe_mutable_data());
            err != EINVAL)
            return err;

        for (id_t i = 0; i < mbr::TOTAL_PTES; ++i) {
            if (bootrec->pte[i].is_empty())
                continue;

            DEBUG("%s%u: LBA = %u\n", disk->d_name.c_str(), i + 1,
                  bootrec->pte[i].partition_start_lba);
            DEBUG("%s%u: number of sectors = %u\n", disk->d_name.c_str(), i + 1,
                  bootrec->pte[i].partition_sectors);
            DEBUG("%s%u: type = %x\n", disk->d_name.c_str(), i + 1, bootrec->pte[i].partition_type);
            DEBUG("%s%u: attributes = %x\n", disk->d_name.c_str(), i + 1,
                  bootrec->pte[i].drive_attributes);
            blkdev part;
            part.lba = bootrec->pte[i].partition_start_lba;
            part.total_sectors = bootrec->pte[i].partition_sectors;
            part.f_vtable = disk->f_vtable;
            part.dev_vtable = disk->dev_vtable;
            part.d_perms = disk->d_perms;
            part.d_major = disk->d_major;
            part.d_minor = disk->d_minor;
            part.log_secsz = disk->log_secsz;
            UNWRAP_ERR(part.d_name.copy(disk->d_name));
            UNWRAP_ERR(part.d_name.append(static_cast<char>('1' + i)));
            if (errno err; (err = device::add_device(&part)))
                return err;
        }
    }
    DEBUG("disk_add: Successfully registered block device \"%s\"\n", disk->d_name.c_str());
    return ENOERR;
}

static constexpr auto get_actual_start(const blkdev *dev, blknum_t lba_start) noexcept -> blknum_t
{
    return dev->lba + lba_start * (1 << dev->log_secsz) / (1 << dev->log_secsz);
}

static constexpr auto get_actual_count(const blkdev *dev, blkcnt_t count) noexcept -> blknum_t
{
    return count * (1 << dev->log_secsz) / (1 << dev->log_secsz);
}

auto disk_read(dev_t dev, void *dst, blknum_t lba_start, blkcnt_t count) noexcept -> errno
{
    KASSERT(dst != nullptr);
    const blkdev *blk = device::get_blkdev(dev);
    KASSERT(blk->dev_vtable->blk_read != nullptr);
    DEBUG("disk_read(%s): Reading %u sectors from LBA %u into 0x%p\n", blk->d_name.c_str(), count,
          lba_start, dst);

    if (auto err = blk->dev_vtable->blk_read(blk, dst, get_actual_start(blk, lba_start),
                                             get_actual_count(blk, count)))
        return err;

    return ENOERR;
}

auto disk_write(dev_t dev, const void *src, u32 lba_start, u32 count) noexcept -> errno
{
    KASSERT(src != nullptr);
    const blkdev *blk = device::get_blkdev(dev);
    KASSERT(blk->dev_vtable != nullptr);
    KASSERT(blk->dev_vtable->blk_write != nullptr);
    DEBUG("disk_write(%s): Writing %u sectors to LBA %u, src=0x%p\n", blk->d_name.c_str(), count,
          lba_start, src);

    if (auto err = blk->dev_vtable->blk_write(blk, src, get_actual_start(blk, lba_start),
                                              get_actual_count(blk, count)))
        return err;

    return ENOERR;
}

auto generic_disk_put_blks(const file *fp, vintix::span<blkcnt_t> blks, off_t off,
                           bool) noexcept -> errno
{
    const blkdev *dev = device::get_blkdev(fp->f_dentry->d_ino->i_num - 1);

    if (dev == nullptr)
        return ENOENT;

    KASSERT(dev->is_readable());
    KASSERT(dev->is_blkdev());

    blkcnt_t val = static_cast<usize>(off) / static_cast<usize>(1 << dev->log_secsz);
    for (usize i = 0; i < blks.size(); ++i)
        blks[i] = val++;

    return ENOERR;
}

auto chs_to_lba(const blkdev &dev, const disk_chs_address &chs, u32 &lba) noexcept -> bool
{
    if (dev.dev_vtable->blk_query == nullptr)
        return false;

    const blkdev_query_info query { dev.dev_vtable->blk_query(&dev) };
    lba = chs_to_lba(chs, query.heads, query.sectors_per_track);

    return true;
}
