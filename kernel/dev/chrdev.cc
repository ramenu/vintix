/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/core/chrdev.h>
#include <vintix/kernel/core/kassert.h>
#include <vintix/kernel/lib/klog.h>

auto register_chrdev(chrdev *dev) noexcept -> errno
{
    KASSERT(dev != nullptr);
    KASSERT(dev->f_vtable != nullptr);
    KASSERT(dev->d_type == device_type::CHRDEV);

    if (auto err = device::add_device(dev))
        return err;

    DEBUG("register_chrdev: Successfully registered character device \"%s\"\n",
          dev->d_name.c_str());
    return ENOERR;
}
