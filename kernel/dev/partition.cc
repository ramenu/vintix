/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/core/partition.h>
#include <vintix/kernel/lib/unique_ptr.h>

struct blkdev;

auto get_partitions(dev_t dev, partitions &parts) noexcept -> errno
{
    vintix::unique_ptr<mbr> master_boot_record { kmalloc(sizeof(mbr)) };

    if (master_boot_record == nullptr)
        return ENOMEM;

    if (auto err = parse_master_boot_record(dev, master_boot_record.unsafe_mutable_data()))
        return err;

    for (usize i = 0; i < mbr::TOTAL_PTES; ++i) {
        if (master_boot_record->pte[i].is_empty())
            continue;

        parts.entries[i].type = master_boot_record->pte[i].partition_type;
        parts.entries[i].drive_attributes = master_boot_record->pte[i].drive_attributes;
        parts.entries[i].start_lba = master_boot_record->pte[i].partition_start_lba;
        parts.entries[i].sectors = master_boot_record->pte[i].partition_sectors;
    }

    parts.dev = dev;
    return ENOERR;
}
