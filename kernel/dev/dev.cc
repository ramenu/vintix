/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/core/blkdev.h>
#include <vintix/kernel/core/dev.h>
#include <vintix/kernel/core/time.h>
#include <vintix/kernel/filesystems/dentry.h>
#include <vintix/kernel/filesystems/file.h>
#include <vintix/kernel/filesystems/fs.h>
#include <vintix/kernel/filesystems/inode.h>
#include <vintix/kernel/filesystems/mount.h>
#include <vintix/kernel/filesystems/super.h>
#include <vintix/kernel/lib/move.h>

auto device::assign_id() noexcept -> vintix::optional<dev_t>
{
    static constinit dev_t dev_id = 1;
    KASSERT(dev_id != NODEV);
    return dev_id++;
}

auto device::add_device(device *dev) noexcept -> errno
{
    const vintix::optional<dev_t> dev_id = assign_id();

    if (dev_id.is_none())
        return ENOMEM;

    dev->d_id = dev_id.value();
    if (auto err = d_devices.push_back(vintix::move(dev)))
        return err;

    return ENOERR;
}

auto device::remove_device(dev_t dev, errno (*close)(device *)) noexcept -> errno
{
    DEBUG("device::remove_device: Attempting to remove device ID %d\n", dev);
    vintix::optional<usize> i = get_id(dev);

    if (i.is_none())
        return ENODEV;

    if (auto err = close(d_devices[i.value()]))
        return err;

    d_devices.erase(i.value());
    return ENOERR;
}

auto device::find(const char *name) noexcept -> vintix::optional<dev_t>
{
    KASSERT(name != nullptr);

    for (usize i = 0; i < d_devices.size(); ++i)
        if (d_devices[i]->d_name == name)
            return d_devices[i]->id();

    return vintix::nullopt_t;
}

auto device::dev_lookup(const dentry *entry, const char *name) noexcept -> errptr<inode>
{
    auto *ino = static_cast<inode *>(kzalloc(sizeof(inode)));

    if (ino == nullptr)
        return ENOMEM;

    const device *dev = nullptr;
    for (usize i = 0; i < d_devices.size(); ++i)
        if (d_devices[i]->d_name == name)
            dev = d_devices[i];

    if (dev == nullptr)
        return ENOENT;

    ino->i_sup = entry->d_ino->i_sup;
    ino->i_mtime = get_unix_time_from_now();
    ino->i_nlinks = 1;
    ino->i_num = dev->id() + 1;
    ino->i_size = 0;
    ino->i_uid = ROOT_UID;
    ino->i_gid = ROOT_GID;
    ino->i_flags = INODE_MOUNTED;
    ino->i_mode = (dev->is_blkdev()) ? BLOCK_SPECIAL : CHAR_SPECIAL;
    ino->i_mode |= REGULAR_FILE | dev->d_perms;
    ino->vtable = entry->d_ino->vtable;

    return ino;
}

auto device::dev_open(inode *ino) noexcept -> errptr<file>
{
    auto *fp = static_cast<file *>(kzalloc(sizeof(file)));

    if (fp == nullptr)
        return ENOMEM;

    const device *dev = get(ino->i_num - 1);

    fp->f_pos = 0;
    fp->vtable = dev->f_vtable;

    return fp;
}

auto device::dev_root_inode(superblock *sup) noexcept -> errptr<inode>
{
    auto *ino = static_cast<inode *>(kzalloc(sizeof(inode)));

    if (ino == nullptr)
        return ENOMEM;

    static constexpr inode_vtable vtable {
        .create = nullptr,
        .rm = nullptr,
        .mkdir = nullptr,
        .rmdir = nullptr,
        .lookup = dev_lookup,
        .sync = nullptr,
        .open = dev_open,
        .ls = nullptr,
        .free = generic_inode_free,
    };

    ino->i_num = 1;
    ino->i_uid = ROOT_UID;
    ino->i_gid = ROOT_GID;
    ino->i_size = sizeof(inode);
    ino->i_mtime = get_unix_time_from_now();
    ino->i_flags = INODE_MOUNTED;
    ino->i_mode = DIRECTORY | USRRWX | GRPREAD | GRPEXEC | OTHREAD | OTHWRIT;
    ino->i_sup = sup;
    ino->i_nlinks = 1;
    ino->vtable = &vtable;

    return ino;
}

auto device::init_fs_context(const fs_context &context) noexcept -> errptr<superblock>
{
    auto *sup = static_cast<superblock *>(kzalloc(sizeof(superblock)));

    if (sup == nullptr)
        return ENOMEM;

    static constexpr superblock_vtable vtable {
        .root_inode = dev_root_inode,
        .alloc_inode = nullptr,
        .sync = nullptr,
        .free = generic_superblock_free,
    };

    sup->s_mnt_flags = context.flags;
    sup->s_dev = NODEV;
    sup->s_magic = DEVFS_MAGIC;
    sup->s_max_name_length = NAME_MAX;
    sup->s_max_file_size = INF_FILE_SIZE;
    sup->vtable = &vtable;

    return sup;
}

auto device::init_devfs() noexcept -> errno
{
    if (!root_is_mounted())
        return EINVAL;

    genfs fs {};
    fs.name = "devtmpfs";
    fs.flags = MNT_NOATIME | MNT_NOEXEC | MNT_NOSUID | MNT_RDONLY | MNT_TMP | MNT_NODIR;
    fs.init_fs_context = init_fs_context;
    fs.type = fs_type::DEVFS;

    return register_filesystem(fs);
}
