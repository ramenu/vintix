/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/core/io.h>
#include <vintix/kernel/core/pit.h>
#include <vintix/kernel/core/process.h>

struct interrupt_state;

enum pit_operating_mode : u8 {
    INTERRUPT_ON_TERMINAL_COUNT,
    HARDWARE_RETRIGGERABLE_ONE_SHOT,
    RATE_GENERATOR,
    SQUARE_WAVE_MODE,
    SOFTWARE_STROBE,
    HARDWARE_STROBE
};

enum pit_access_mode : u8 {
    LATCH_COUNT_VALUE_COMMAND,
    LOBYTE_ONLY,
    HIBYTE_ONLY,
    LOBYTE_AND_HIBYTE
};

enum pit_channel : u8 {
    CHANNEL_0,
    CHANNEL_1,
    CHANNEL_2,
    READ_BACK_COMMAND // (8254 only)
};

enum pit_pcd_binary_mode : u8 {
    SIXTEEN_BIT_BINARY,
    FOUR_DIGIT_BCD
};

static constinit u32 timer_ticks = 0;
static constexpr u32 INPUT_CLOCK_HZ = 1193180;

// I/O ports
static constexpr u8 PIT_CHAN_0_DATA_PORT = 0x40; // (read/write)
static constexpr u8 PIT_CHAN_1_DATA_PORT = 0x41; // (read/write)
static constexpr u8 PIT_CHAN_2_DATA_PORT = 0x42; // (read/write)
static constexpr u8 PIT_COMMAND_PORT = 0x43;     // (write only, a read is ignored)

// Convenient function for setting the PIT command byte
consteval static auto set_pit_command_byte(pit_pcd_binary_mode pcd_bin, pit_operating_mode op_mode,
                                           pit_access_mode access_mode,
                                           pit_channel chan) noexcept -> u8
{
    return static_cast<u8>(((chan << 6) | (access_mode << 4) | (op_mode << 1) | pcd_bin));
}

// sets the PIT's clock rate to '1193180 / hz'
void set_pit_timer_phase(u32 hz) noexcept
{
    static constexpr u8 PIT_CMD_BYTE =
        set_pit_command_byte(SIXTEEN_BIT_BINARY, SQUARE_WAVE_MODE, LOBYTE_AND_HIBYTE, CHANNEL_0);
    const u32 div = INPUT_CLOCK_HZ / hz;
    outb(PIT_COMMAND_PORT, PIT_CMD_BYTE);

    outb(PIT_CHAN_0_DATA_PORT, (div & 0x00ff));      // send low byte
    outb(PIT_CHAN_0_DATA_PORT, (div & 0xff00) >> 8); // send high byte
}

// custom IRQ handler for the the CPU PIT timer (IRQ #0)
void timer_handler(const interrupt_state &) noexcept
{
    if (timer_ticks != 0)
        --timer_ticks;
    if (process::running() == nullptr)
        return;

    //++process::running()->p_start_time;
}

// pauses regular program execution for the specified number of ticks
void ksleep(u32 ticks) noexcept
{
    // TODO: Make this work on multitasking systems as well
    timer_ticks = ticks;
    while (timer_ticks > 0)
        ;
}
