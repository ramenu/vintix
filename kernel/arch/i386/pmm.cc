/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/core/io.h>
#include <vintix/kernel/core/kassert.h>
#include <vintix/kernel/core/macros.h>
#include <vintix/kernel/core/mbt.h>
#include <vintix/kernel/core/panic.h>
#include <vintix/kernel/core/pmm.h>
#include <vintix/kernel/core/vmm.h>
#include <vintix/kernel/lib/bitmap.h>
#include <vintix/kernel/lib/bits.h>
#include <vintix/kernel/lib/klog.h>
#include <vintix/kernel/lib/string.h>
#include <vintix/kernel/lib/utilities.h>

enum pte_flags : u32 {
    PTE_NONE_FLAG = 0,

    // If the bit is set, the page is actually in physical memory at
    // the moment
    PTE_IS_PRESENT_FLAG = 1 << 0,

    // R/W or 'read/write' flag. If the bit is set, the page is
    // read/write. Otherwise it is read only. The WP bit in CR0
    // determines if this is only applied to userland, always giving
    // the kernel write access (the default) or both userland and the
    // kernel.
    PTE_RW_FLAG = 1 << 1,

    // U/S, the 'User/supervisor' bit, controls access to the page based
    // on privilege level. If the bit is set, then the page may be accessed
    // by all; if the bit is not set, however, only the supervisor can
    // access it.
    PTE_US_FLAG = 1 << 2,

    // PWT controls Write-Through abilities of the page. If the bit is set,
    // then write-through caching is enabled. If not, write-back is enabled
    // instead.
    PTE_PWT_FLAG = 1 << 3,

    // PCD, 'disable caching' bit. If the bit is set, the page will be cached.
    // Otherwise it won't be.
    PTE_PCD_FLAG = 1 << 4,

    // 'A' or 'accessed' is used to discover whether a PDE or PTE was
    // read during virtual address translation. If it has, then the bit
    // is set. Note that this bit will not be cleared by the CPU so the
    // burden falls on the OS (if it needs this bit at all).
    PTE_A_FLAG = 1 << 5,

    // 'D' or 'dirty' is used to determine whether a page has been
    // written to. If it has, then the bit is set. Note that if
    // using 4-KiB pages, this bit is available for the OS to use.
    PTE_D_FLAG = 1 << 6,

    // 'PAT' or 'Page Attribute Table'. If 'PAT' is supported, then PAT along
    // with PCD and PWT shall indicate the memory caching type. Otherwise, it
    // is reserved and must be set to 0. Use this flag for PTEs only.
    PTE_PAT_FLAG = 1 << 7,

    // 'G', or 'global' tells the processor not to invalidate the TLB entry
    // corresponding to the page upon a MOV to CR3 instruction if set to true.
    // Bit 7 (PGE) in CR4 must be set to enable global pages
    PTE_G_FLAG = 1 << 8,

    // zero out the page instead of mapping it, if set
    PTE_ZERO_FLAG = 1 << 29,

    // Not an official PTE flag, this only serves as an indicator that this
    // page needs to be mapped in when a userspace process page faults. If this
    // bit is not set and a userspace process crashes, the general protection fault
    // handler will run and terminate the program. This bit has no effect on kernel
    // pages and thus shouldn't be used for anything kernel-related.
    PTE_UNMAPPED_FLAG = 1 << 30,
};

enum pde_flags : u16 {
    PDE_NONE_FLAG = 0,

    // If the bit is set, the page is actually in physical memory at
    // the moment
    PDE_IS_PRESENT_FLAG = 1 << 0,

    // R/W or 'read/write' flag. If the bit is set, the page is
    // read/write. Otherwise it is read only. The WP bit in CR0
    // determines if this is only applied to userland, always giving
    // the kernel write access (the default) or both userland and the
    // kernel.
    PDE_RW_FLAG = 1 << 1,

    // U/S, the 'User/supervisor' bit, controls access to the page based
    // on privilege level. If the bit is set, then the page may be accessed
    // by all; if the bit is not set, however, only the supervisor can
    // access it.
    PDE_US_FLAG = 1 << 2,

    // PWT controls Write-Through abilities of the page. If the bit is set,
    // then write-through caching is enabled. If not, write-back is enabled
    // instead.
    PDE_PWT_FLAG = 1 << 3,

    // PCD, 'disable caching' bit. If the bit is set, the page will be cached.
    // Otherwise it won't be.
    PDE_PCD_FLAG = 1 << 4,

    // 'A' or 'accessed' is used to discover whether a PDE or PTE was
    // read during virtual address translation. If it has, then the bit
    // is set. Note that this bit will not be cleared by the CPU so the
    // burden falls on the OS (if it needs this bit at all).
    PDE_A_FLAG = 1 << 5,

    // Available
    PDE_AVL_FLAG = 1 << 6,

    // 'PS' or 'page size' is used to determine whether the page is 4 MiB
    // or 4 KiB. If the bit is set, then the page is 4 MiB. Otherwise, it
    // is 4 KiB.
    PDE_PS_FLAG = 1 << 7,
};

DEFINE_ENUM_BITWISE_OPS(pte_flags);
DEFINE_ENUM_BITWISE_OPS(pde_flags);

struct pf_info {
    usize i;    // index into the bitmap array
    u8 bit_off; // bit offset
};

// each of these points to 1024 page tables reserved for each
// page directory, so for example, pg_table[0] would be the
// address of where the page tables 0-1024 for pg_directory[0]
// reside
// NOLINTBEGIN(cppcoreguidelines-avoid-c-arrays)
constinit pg_table_t *pg_table[PTE_ENTRIES] {};
constinit pg_directory_t pg_directory[PDT_ENTRIES] __attribute__((aligned(PAGE_FRAME_SIZE))) {};
static constinit pg_directory_t *pgdir; // pointer to the active page directory being used
static constinit pg_table_t **pgtab;    // pointer to the active page table being used
// NOLINTEND(cppcoreguidelines-avoid-c-arrays)

static constexpr usize KERNEL_PDT = 768;
static constexpr u32 ENOPAG = 0;
static constinit usize free_pg = 0, used_pgs = 0;
NODESTROY static constinit vintix::heap_bitmap<u32> pf_bitmap {};

[[nodiscard]] static auto alloc_pg_table(u16 pde) noexcept -> bool;
static void free_pg_table(u16 i) noexcept;
static auto get_pf_info_from(page pg) noexcept -> pf_info;

static auto total_pages() noexcept -> usize { return pf_bitmap.size() * bits_in<u32>(); }

static constexpr auto page_mapped(page pg) noexcept -> bool { return (pg >> PF_ADDRESS_OFFSET); }

static void load_cr3(u32 cr3) noexcept
{
    asm volatile("mov 	eax, %0\n\t"
                 "mov 	cr3, eax\n\t"
                 :
                 : "r"(cr3)
                 : "memory", "eax");
}

static constexpr auto convert_page_flags(pg_flags flags) noexcept -> pte_flags
{
    pte_flags ret = PTE_NONE_FLAG;

    if (flags & PG_P)
        ret |= PTE_IS_PRESENT_FLAG;
    if ((flags & PG_R) || (flags & PG_W))
        ret |= PTE_RW_FLAG;
    if (flags & PG_USER)
        ret |= PTE_US_FLAG;
    if (flags & PG_PWT)
        ret |= PTE_PWT_FLAG;
    if (flags & PG_PCD)
        ret |= PTE_PCD_FLAG;
    if (flags & PG_A)
        ret |= PTE_A_FLAG;
    if (flags & PG_D)
        ret |= PTE_D_FLAG;
    if (flags & PG_PAT)
        ret |= PTE_PAT_FLAG;
    if (flags & PG_G)
        ret |= PTE_G_FLAG;

    return ret;
}

auto init_paging(p_addr physical_start, u32 bitmap_count) noexcept -> p_addr
{
    KASSERT(pg_table[KERNEL_PDT] != nullptr);
    pgdir = static_cast<pg_directory_t *>(pg_directory);
    pgtab = static_cast<pg_table_t **>(pg_table);
    pgtab[KERNEL_PDT] = unsafe_toptr<u32 *>(addressof(pgtab[KERNEL_PDT]) + VADDR_OFFSET);
    const u16 pte = vaddr_to_pte(unsafe_toptr<void *>(physical_start));
    pf_bitmap.unsafe_set_pointer(unsafe_toptr<void *>(physical_start + VADDR_OFFSET));
    pf_bitmap.unsafe_set_size(bitmap_count);
    pgtab[KERNEL_PDT][pte] = physical_start | PTE_IS_PRESENT_FLAG | PTE_RW_FLAG;
    physical_start += PAGE_FRAME_SIZE;
    memsetd(pf_bitmap.unsafe_mutable_data(), 0, pf_bitmap.size());

    // mark all the bitmaps currently in use from 0x0-physical_placement_addr
    for (usize i = 0; i < pf_bitmap.size(); ++i) {
        for (u8 j = 0; j < VIRTUAL_ADDRESS_BITS; ++j) {
            KASSERT(!(pf_bitmap[i] & (1 << j)));
            const v_addr address = (j + i * VIRTUAL_ADDRESS_BITS) * PAGE_FRAME_SIZE;
            if (address > physical_start) {
                free_pg = i;
                goto finish;
            }

            ++used_pgs;
            pf_bitmap.set(i, j);
        }
    }

finish:

    // identity map the addresses used by the multiboot modules (if any)
    // and allocate those pages as used
    if (mbt->mods_count == 0)
        return physical_start;

    auto *mod = unsafe_toptr<const multiboot_module_t *>(VADDR_OFFSET + mbt->mods_addr);
    for (usize i = 0; i < mbt->mods_count; ++i) {
        // according to multiboot spec: "the memory used goes from bytes ’mod_start’ to ’mod_end-1’
        // inclusive"
        const usize bytes_used = mod[i].mod_end - mod[i].mod_start;
        const usize pages_needed = bytes_used / PAGE_FRAME_SIZE + 1;
        for (usize j = 0; j < pages_needed; ++j) {
            const v_addr map_address = mod[i].mod_start + PAGE_FRAME_SIZE * j;
            if (!map_page(VADDR_OFFSET + map_address, map_address, PG_P | PG_R | PG_W))
                kpanic("couldn't allocate enough physical memory for multiboot modules\n");

            const pf_info pf = get_pf_info_from(map_address);
            if (pf_bitmap.is_set(pf.i, pf.bit_off))
                kpanic("modules cannot be loaded because the kernel has already overwritten all of "
                       "the data!\n");

            ++used_pgs;
            pf_bitmap.set(pf.i, pf.bit_off);
        }
    }

    return physical_start;
}

static auto get_pf_info_from(page pg) noexcept -> pf_info
{
    pg &= ~(PAGE_FRAME_SIZE - 1);
    const usize i = pg / PAGE_FRAME_SIZE / bits_in<u32>();
    const u8 bit = static_cast<u8>(pg / PAGE_FRAME_SIZE % bits_in<u32>());

    KASSERT(i < pf_bitmap.size());
    return { i, bit };
}

static void free_pg_table(u16 i) noexcept
{
    KASSERT(i < PTE_ENTRIES);
    KASSERT(pgtab[i] != nullptr);

    free_page(pgtab[i]);
    pgtab[i] = nullptr;
    pgdir[i] = 0;
}

static auto bitmap_allocate_page() noexcept -> page
{
    do {
        if (pf_bitmap.has_free_bit(free_pg))
            return free_pg * VIRTUAL_ADDRESS_BITS + pf_bitmap.allocate_bit_at(free_pg) - 1;
    } while (++free_pg != pf_bitmap.size());

    // start from the beginning to find a free page
    for (usize i = 0; i < pf_bitmap.size(); ++i) {
        if (pf_bitmap.has_free_bit(i)) {
            free_pg = i;
            u8 free_bit = pf_bitmap.allocate_bit_at(i);
            return i * VIRTUAL_ADDRESS_BITS + free_bit - 1;
        }
    }

    return ENOPAG;
}

static auto bitmap_allocate_consecutive_pages(usize num_pages) noexcept -> page
{
    if (num_pages >= total_pages() || (used_pgs + num_pages > total_pages()))
        return ENOPAG;

    page pg = pf_bitmap.allocate_consecutive_bits(num_pages);

    if (pg == ENOPAG)
        return ENOPAG;

    return pg - 1;
}

// returns a newly allocated page
auto alloc_page() noexcept -> p_addr
{
    if (used_pgs + 1 > total_pages())
        return ENOPAG;

    const usize i = bitmap_allocate_page();
    KASSERT(i != ENOPAG);
    p_addr pg = i << PF_ADDRESS_OFFSET;
    DEBUG("alloc_page: Allocated page 0x%p\n", pg);
    ++used_pgs;
    return pg;
}

auto alloc_pages(usize num_pages) noexcept -> p_addr
{
    KASSERT(num_pages != 0);
    const usize i = bitmap_allocate_consecutive_pages(num_pages);

    if (i == ENOPAG)
        return ENOPAG;

    p_addr pg = i << PF_ADDRESS_OFFSET;
    DEBUG("alloc_pages: Allocated pages: 0x%x-0x%x\n", pg, (i + num_pages) << PF_ADDRESS_OFFSET);
    return pg;
}

auto map_page(v_addr vaddr, p_addr paddr, pg_flags f) noexcept -> bool
{
    const pte_flags flags = convert_page_flags(f);
    KASSERT(flags != PTE_NONE_FLAG);

    if (paddr == ENOPAG) [[unlikely]]
        return false;

    const u16 pdt = vaddr_to_pdt(vaddr);
    if (pgtab[pdt] == nullptr) [[unlikely]] {
        if (!alloc_pg_table(pdt)) [[unlikely]]
            return false;
    }

    DEBUG("map_page: Mapping 0x%x -> 0x%x\n", vaddr, paddr);
    const u16 pte = vaddr_to_pte(vaddr);
    pgtab[pdt][pte] = paddr | flags;
    return true;
}

auto address_is_mapped(v_addr v) noexcept -> bool
{
    const u16 pdt = vaddr_to_pdt(v);

    if (pgtab[pdt] == nullptr) [[unlikely]]
        return false;

    const u16 pte = vaddr_to_pte(v);
    return page_mapped(pgtab[pdt][pte]);
}

void free_page(page *pg) noexcept
{
    DEBUG("free_page: Freeing page 0x%x\n", *pg);
    const pf_info pf = get_pf_info_from(*pg);
    KASSERT(pf_bitmap.is_set(pf.i, pf.bit_off));
    pf_bitmap.unset(pf.i, pf.bit_off);
    *pg = 0;
    --used_pgs;

// 80386 does not support invlpg
#if TARGET_MACHINE == 386
    flush_tlb();
#else
    invlpg(pg);
#endif
}

void free_pages(v_addr start, usize count) noexcept
{
    DEBUG("free_pages: Freeing %ul pages from virtual address 0x%x\n", count, start);
    for (usize i = 0; i < count; ++i)
        free_page(get_page(unsafe_toptr<page *>(start + i * PAGE_FRAME_SIZE)));
}

void alloc_pg_table(u16 pde, void *table) noexcept
{
    DEBUG("alloc_pg_table: Allocated a page table for PDE %u\n", pde);
    KASSERT(pde < PDT_ENTRIES);
    KASSERT(table != nullptr);
    KASSERT(pgtab[pde] == nullptr);

    pgtab[pde] = static_cast<u32 *>(table);
    page *pg = get_page(table);
    // set the i'th page directory to point to the page table
    pgdir[pde] = ((*pg) & ~(PAGE_FRAME_SIZE - 1)) | PDE_IS_PRESENT_FLAG | PDE_US_FLAG | PDE_RW_FLAG;
    flush_tlb();
}

static auto alloc_pg_table(u16 pde) noexcept -> bool
{
    KASSERT(pgtab[pde] == nullptr);
    void *buf = static_cast<u32 *>(kzalloc(PAGE_FRAME_SIZE, PAGE_FRAME_SIZE));

    if (buf == nullptr) [[unlikely]]
        return false;

    alloc_pg_table(pde, buf);
    return true;
}

auto get_page(const void *vaddr) noexcept -> p_addr *
{
    const u16 i = vaddr_to_pdt(vaddr);

    if (pgtab[i] == nullptr && !alloc_pg_table(i)) [[unlikely]]
        return nullptr;

    const u16 pte = vaddr_to_pte(vaddr);
    return &pgtab[i][pte];
}

consteval static auto real_mode_pages_used() noexcept -> usize
{
    usize real_mode_pages_used = 1;
    for (p_addr addr = 0x80000; addr < 0x100000; addr += 0x1000)
        ++real_mode_pages_used;

    return real_mode_pages_used;
}

auto pages_used() noexcept -> pgs_used
{
    pgs_used used;
    used.total = total_pages();
    used.count = used_pgs;
    used.real_mode = real_mode_pages_used();

    return used;
}

// NOLINTBEGIN(cppcoreguidelines-avoid-c-arrays)
void load_pg_directory(pg_directory_t cr3[PDT_ENTRIES],
                       pg_table_t *new_pg_table[PTE_ENTRIES]) noexcept
{
    cr3[KERNEL_PDT] = pgdir[KERNEL_PDT];
    new_pg_table[KERNEL_PDT] = pgtab[KERNEL_PDT];
    page pg = *get_page(cr3) & ~(PAGE_FRAME_SIZE - 1);
    load_cr3(pg);
    pgdir = cr3;
    pgtab = new_pg_table;
}
// NOLINTEND(cppcoreguidelines-avoid-c-arrays)
