/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/core/dpl.h>
#include <vintix/kernel/core/gdt.h>
#include <vintix/kernel/core/kassert.h>
#include <vintix/kernel/core/tss.h>
#include <vintix/kernel/lib/array.h>
#include <vintix/kernel/lib/utilities.h>

enum class granularity_size : u8 {
    ONE_BYTE = 0,
    FOUR_KB = 1,
};

enum class granularity_operand_size : u8 {
    SIXTEEN_BIT = 0,
    THIRTY_TWO_BIT = 1,
};

enum class descriptor_type : u8 {
    SYSTEM_SEGMENT = 0,
    CODE_OR_DATA_SEGMENT = 1,
};

enum segment_type : u8 {
    SEG_DATA_RD,        // Read-Only
    SEG_DATA_RDA,       // Read-Only, accessed
    SEG_DATA_RDWR,      // Read/Write
    SEG_DATA_RDWRA,     // Read/Write, accessed
    SEG_DATA_RDEXPD,    // Read-Only, expand-down
    SEG_DATA_RDEXPDA,   // Read-Only, expand-down, accessed
    SEG_DATA_RDWREXPD,  // Read/Write, expand-down
    SEG_DATA_RDWREXPDA, // Read/Write, expand-down, accessed
    SEG_CODE_EX,        // Execute-Only
    SEG_CODE_EXA,       // Execute-Only, accessed
    SEG_CODE_EXRD,      // Execute/Read
    SEG_CODE_EXRDA,     // Execute/Read, accessed
    SEG_CODE_EXC,       // Execute-Only, conforming
    SEG_CODE_EXCA,      // Execute-Only, conforming, accessed
    SEG_CODE_EXRDC      // Execute/Read, conforming
};

// Convenient function for setting the GDT flags
consteval static auto set_gdt_flags(granularity_size size,
                                    granularity_operand_size operand_size) noexcept -> u8
{
    return static_cast<u8>((static_cast<u8>(operand_size) << 3) | (static_cast<u8>(size) << 2));
}

// Convenient function for setting the GDT access byte
consteval static auto set_gdt_access_byte(bool segment_is_present, dpl privilege_level,
                                          descriptor_type desc_type,
                                          segment_type type) noexcept -> u8
{
    return static_cast<u8>((type | (static_cast<u8>(desc_type) << 4) | (privilege_level << 5) |
                            (segment_is_present << 7)));
}

struct gdt {
    u16 size;
    u32 base;
} __attribute__((packed));

struct gdt_descriptor {
    u16 limit_lo;
    u16 base_lo;
    u8 base_mid;
    u8 access;
    u8 limit_hi : 4;
    u8 flags : 4;
    u8 base_hi;
} __attribute__((packed));

// we must make sure that the descriptor is exactly 8 bytes
static_assert(sizeof(gdt_descriptor) == 8);

// defined in gdt.asm
// loads the global descriptor table
extern "C" void _load_gdt(const gdt &table) noexcept;

// defined in tss.asm
// load task register
extern "C" void _flush_tss() noexcept;

static constexpr usize GDT_ENTRIES = 6;
static constinit vintix::array<gdt_descriptor, GDT_ENTRIES> gdt_descriptors {};

extern u8 *kernel_stack;

static constinit tss_entry tss {};

// Creates a GDT descriptor
static constexpr auto setup_gdt_descriptor(v_addr base, v_addr limit, u8 access,
                                           u8 flags) noexcept -> gdt_descriptor
{
    gdt_descriptor desc;
    desc.limit_lo = limit & 0x0000ffff;
    desc.base_lo = base & 0x0000ffff;
    desc.base_mid = (base & 0x00ff0000) >> 16;
    desc.access = access;
    desc.limit_hi = static_cast<u8>((limit & 0x000f0000) >> 16);
    desc.flags = flags & 0xf;
    desc.base_hi = static_cast<u8>((base & 0xff000000) >> 24);

    return desc;
}

// Initializes the GDT, should be called on kernel
// startup and should be the very first thing it does!
void setup_gdt() noexcept
{
    // setup GDT pointer and size
    const gdt table { .size = sizeof(gdt_descriptor) * GDT_ENTRIES - 1,
                      .base = addressof(gdt_descriptors.data()) };

    static constexpr u8 GDT_FLAGS =
        set_gdt_flags(granularity_size::FOUR_KB, granularity_operand_size::THIRTY_TWO_BIT);

    KASSERT(gdt_descriptors.size() >= 6);

    // setup null descriptor
    gdt_descriptors[0] = setup_gdt_descriptor(0, 0, 0, 0);

    // set kernel code segment (0-4GB)
    static constexpr u8 GDT_KERNEL_CODE_SEGMENT_ACCESS_BYTE = set_gdt_access_byte(
        true, RING_0, descriptor_type::CODE_OR_DATA_SEGMENT, segment_type::SEG_CODE_EXRD);

    gdt_descriptors[1] =
        setup_gdt_descriptor(0, 0xfffff, GDT_KERNEL_CODE_SEGMENT_ACCESS_BYTE, GDT_FLAGS);

    // set kernel data segment (0-4GB)
    static constexpr u8 GDT_KERNEL_DATA_SEGMENT_ACCESS_BYTE = set_gdt_access_byte(
        true, RING_0, descriptor_type::CODE_OR_DATA_SEGMENT, segment_type::SEG_DATA_RDWR);

    gdt_descriptors[2] =
        setup_gdt_descriptor(0, 0xfffff, GDT_KERNEL_DATA_SEGMENT_ACCESS_BYTE, GDT_FLAGS);

    // set userspace code segment (0-4GB)
    static constexpr u8 GDT_USER_CODE_SEGMENT_ACCESS_BYTE = set_gdt_access_byte(
        true, RING_3, descriptor_type::CODE_OR_DATA_SEGMENT, segment_type::SEG_CODE_EXRD);

    gdt_descriptors[3] =
        setup_gdt_descriptor(0, 0xfffff, GDT_USER_CODE_SEGMENT_ACCESS_BYTE, GDT_FLAGS);

    // set userspace data segment (0-4GB)
    static constexpr u8 GDT_USER_DATA_SEGMENT_ACCESS_BYTE = set_gdt_access_byte(
        true, RING_3, descriptor_type::CODE_OR_DATA_SEGMENT, segment_type::SEG_DATA_RDWR);

    gdt_descriptors[4] =
        setup_gdt_descriptor(0, 0xfffff, GDT_USER_DATA_SEGMENT_ACCESS_BYTE, GDT_FLAGS);

    // setup TSS
    static constexpr u8 TSS_GRANULARITY =
        set_gdt_flags(granularity_size::ONE_BYTE, granularity_operand_size::SIXTEEN_BIT);

    static constexpr u8 GDT_TSS_ACCESS_BYTE = set_gdt_access_byte(
        true, RING_0, descriptor_type::SYSTEM_SEGMENT, segment_type::SEG_CODE_EXA);

    gdt_descriptors[5] = setup_gdt_descriptor(addressof(&tss), sizeof(tss_entry) - 1,
                                              GDT_TSS_ACCESS_BYTE, TSS_GRANULARITY);

    tss.ss0 = 0x10;
    tss.esp0 = addressof(&kernel_stack);
    tss.iomap_base = sizeof(tss_entry);

    // flush out old GDT and install new one
    _load_gdt(table);

    // install TSS
    _flush_tss();
}
