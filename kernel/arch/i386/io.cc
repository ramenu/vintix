/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/core/chrdev.h>
#include <vintix/kernel/core/global.h>
#include <vintix/kernel/core/io.h>
#include <vintix/kernel/core/kassert.h>
#include <vintix/kernel/core/panic.h>
#include <vintix/kernel/core/serial.h>
#include <vintix/kernel/filesystems/file.h>
#include <vintix/kernel/filesystems/inode.h>
#include <vintix/kernel/lib/errno.h>
#include <vintix/kernel/lib/escseq.h>
#include <vintix/kernel/lib/math.h>
#include <vintix/kernel/lib/string.h>
#include <vintix/kernel/lib/utilities.h>
#include <vintix/kernel/lib/vaarg.h>

static auto fb_writef(file *, const void *buf, usize count) noexcept -> ssize;
static void clear_fb_row(u16 row) noexcept;

// I/O ports
// I/O ports are calculated relative to the data port. This is
// because all serial ports (COM1, COM2, COM3, COM4) have their
// ports in the same order, but they start at different values
static constexpr auto FB_COMMAND_PORT = 0x3d4;
static constexpr auto FB_DATA_PORT = 0x3d5;

// I/O port commands
static constexpr auto FB_HIGH_BYTE_COMMAND = 14;
static constexpr auto FB_LOW_BYTE_COMMAND = 15;

static constexpr auto FRAMEBUFFER_START_ADDRESS = 0xC00B8000;

static constexpr auto TOTAL_FB_CELLS = FRAMEBUFFER_COLUMNS * FRAMEBUFFER_ROWS * FB_CELL_BYTE;

// NOLINTBEGIN(cppcoreguidelines-pro-type-reinterpret-cast)
static const auto fb = reinterpret_cast<char *>(FRAMEBUFFER_START_ADDRESS);
// NOLINTEND(cppcoreguidelines-pro-type-reinterpret-cast)
//
static constinit u16 fb_pos = 0, current_row = 0, current_column = 0;

#if 0
static void fb_enable_textmode_cursor(u8 start, u8 end) noexcept
{
	outb(0x3D4, 0x0A);
	outb(0x3D5, (inb(0x3D5) & 0xC0) | start);
 
	outb(0x3D4, 0x0B);
	outb(0x3D5, (inb(0x3D5) & 0xE0) | end);
}
#endif

// moves the cursor at 'pos'
static void fb_move_cursor(u16 pos) noexcept
{
    static constexpr auto MASK = static_cast<u16>(0xff);
    outb(FB_COMMAND_PORT, FB_HIGH_BYTE_COMMAND);
    outb(FB_DATA_PORT, (static_cast<u8>(pos >> 8) & MASK));
    outb(FB_COMMAND_PORT, FB_LOW_BYTE_COMMAND);
    outb(FB_DATA_PORT, static_cast<u8>(pos & MASK));
}

// writes a character 'c' at the framebuffer's i'th position.
// default colors are lightgrey for foreground and black for
// background.
static void fb_write_cell(usize i, char c, FBFGColor fg = DEFAULT_FBFG_COLOR,
                          FBBGColor bg = DEFAULT_FBBG_COLOR) noexcept
{
    KASSERT(i < TOTAL_FB_CELLS);

    // write the character
    fb[i] = c;

    // write the fg/bg
    fb[i + 1] = static_cast<char>(static_cast<usize>(fg) | static_cast<usize>(bg));
}

// moves the framebuffer's cursor one cell back
static void move_fb_cursor_back() noexcept
{
    // make sure that the cursor doesn't go below the
    // framebuffer's boundaries
    if (current_column != 1 || current_row != 0) {

        // did we move to the previous row?
        if (current_column-- == 0) {
            current_column = FRAMEBUFFER_COLUMNS;
            --current_row;
        }
        fb_move_cursor(--fb_pos);
    }
}

// scrolls the framebuffer
static void scroll_fb() noexcept
{
    static constexpr usize NBYTES =
        (FRAMEBUFFER_COLUMNS * FRAMEBUFFER_ROWS - FRAMEBUFFER_COLUMNS) * FB_CELL_BYTE / sizeof(u32);

    // move contents in the framebuffer one row up
    memcpyd(fb, fb + FRAMEBUFFER_COLUMNS * FB_CELL_BYTE, NBYTES);

    fb_pos -= FRAMEBUFFER_COLUMNS; // go to column 0
    --current_row;                 // go up one row
    current_column = 0;
    clear_fb_row(FRAMEBUFFER_ROWS - 1); // clear everything in the last row
}

static auto fb_lseek(const file * /*fp*/, off_t off, int /*whence*/) noexcept -> off_t
{
    auto uoff = static_cast<usize>(off);
    current_row = static_cast<u16>(uoff % FRAMEBUFFER_ROWS);
    current_column = static_cast<u16>(uoff % FRAMEBUFFER_COLUMNS);
    fb_pos = static_cast<u16>(uoff % (FRAMEBUFFER_COLUMNS * FRAMEBUFFER_ROWS));
    return fb_pos;
}

// empties the framebuffer and sets the cursor's
// position at 0
auto fb_init() noexcept -> errno
{
    fb_clear();

    static constexpr file_vtable vtable {
        .lseek = fb_lseek,
        .read = nullptr,
        .write = fb_writef,
        .put_blks = nullptr,
        .close = generic_file_close,
    };

    auto *dev = static_cast<chrdev *>(kzalloc(sizeof(chrdev)));

    if (dev == nullptr)
        return ENOMEM;

    dev->d_name = "fb";
    dev->d_type = device_type::CHRDEV;
    dev->d_major = 3;
    dev->d_minor = 1;
    dev->d_perms = USRREAD | USRWRIT | GRPREAD | GRPWRIT | OTHREAD | OTHWRIT;
    dev->f_vtable = &vtable;

    return register_chrdev(dev);
}

// clears the framebuffer row's contents
static void clear_fb_row(u16 row) noexcept
{
    KASSERT(row < FRAMEBUFFER_ROWS);
    const usize begin = FRAMEBUFFER_COLUMNS * FB_CELL_BYTE * row;
    for (usize i = begin; i < begin + FRAMEBUFFER_COLUMNS * FB_CELL_BYTE; i += 2)
        fb_write_cell(i, 0);
}

// clears the framebuffer's contents
void fb_clear() noexcept
{
    current_row = 0;
    current_column = 0;
    fb_pos = 0;
    for (usize i = 0; i < TOTAL_FB_CELLS; i += 2)
        fb_write_cell(i, 0);
    fb_move_cursor(0);
}

// should be called whenever newline is encountered,
// will automatically handle scrolling the framebuffer
// and setting the column/row numbers to their appropriate
// values
static void fb_handle_newline() noexcept
{
    fb_pos += FRAMEBUFFER_COLUMNS * current_row - fb_pos;

    // scroll if we have reached the last row
    if (++current_row == FRAMEBUFFER_ROWS)
        scroll_fb();

    fb_pos += FRAMEBUFFER_COLUMNS;
    current_column = 0;
}

static auto fb_writef(file *, const void *buf, usize count) noexcept -> ssize
{
    auto s = static_cast<const char *>(buf);
    for (usize i = 0; i < count; ++i)
        fb_writec(s[i]);

    return static_cast<ssize>(count);
}

// writes the text to the framebuffer with optional params
// including the cell foreground and background color
void fb_write(const char *buf, FBFGColor fg, FBBGColor bg) noexcept
{
    KASSERT(buf != nullptr);
    // NOLINTBEGIN(clang-analyzer-core.NullDereference)
    for (usize i = 0; buf[i] != '\0'; ++i) {

        // advance to next row, column 0 when line break is encountered
        if (buf[i] == '\n') {
            fb_handle_newline();
            continue;
        }
        // check if we've reached the maximum column #, if so we
        // need to reset the column location back to 0 and scroll the
        // framebuffer if necessary
        if (++current_column == FRAMEBUFFER_COLUMNS) {
            if (++current_row == FRAMEBUFFER_ROWS)
                scroll_fb();
            current_column = 0;
        }
        fb_write_cell(fb_pos + fb_pos, buf[i], fg, bg);
        ++fb_pos;
    }
    // NOLINTEND(clang-analyzer-core.NullDereference)

    fb_move_cursor(fb_pos);
}

// writes a character to the framebuffer
void fb_writec(char c, FBFGColor fg, FBBGColor bg) noexcept
{
    if (c == '\n') {
        fb_handle_newline();
        fb_move_cursor(fb_pos);
        return;
    }

    // on last column?
    if (++current_column == FRAMEBUFFER_COLUMNS) {
        // on last row?
        if (++current_row == FRAMEBUFFER_ROWS) {
            scroll_fb();
        }
        current_column = 0;
    }

    fb_write_cell(fb_pos + fb_pos, c, fg, bg);
    fb_move_cursor(++fb_pos);
}

void fb_writeln(const char *buf, FBFGColor fg, FBBGColor bg) noexcept
{
    fb_write(buf, fg, bg);
    fb_writec('\n');
}

// Returns the number of increments that 'format' needs to be added
// by. If zero, then there was no meaningful format to write or parse.
// This also parses ANSI escape sequences to make your life easier
static auto fb_write_format_specifier(const char *format, usize i, va_list &va, FBFGColor &fg,
                                      void(write(const char *, FBFGColor, FBBGColor)),
                                      void(writec(char, FBFGColor, FBBGColor))) noexcept -> usize
{
    switch (format[i + 1]) {
        default:
            THIS_SHOULDNT_HAPPEN("invalid format specifier");
            break;

        // print '%' literal
        case '%':
            writec('%', fg, DEFAULT_FBBG_COLOR);
            return 1;

        // print decimal (integer) number (base 10)
        case 'd': {
            const i32 d = va_arg(va, i32);
            const usize d_abs = abs(d);
            if (d < 0)
                writec('-', fg, DEFAULT_FBBG_COLOR);
            char *decimal_s = itoa(d_abs);
            write(decimal_s, fg, DEFAULT_FBBG_COLOR);
            return 1;
        }
        case '0': {
            if (format[i + 2] >= '1' && format[i + 2] <= '9' && format[i + 3] == 'd') {
                const usize d = va_arg(va, usize);
                char *s = itoa(d);
                const usize s_len = strlen(s);
                const auto zeroes_to_pad = static_cast<usize>(format[i + 2] - '0');
                if (s_len < zeroes_to_pad) {
                    usize zeroes_to_write = zeroes_to_pad - s_len;
                    while (zeroes_to_write--)
                        writec('0', fg, DEFAULT_FBBG_COLOR);
                }
                write(s, fg, DEFAULT_FBBG_COLOR);
                return 3;
            }
            THIS_SHOULDNT_HAPPEN("invalid format specifier");
            break;
        }
        // print string
        case 's': {
            const char *s = va_arg(va, const char *);
            write(s, fg, DEFAULT_FBBG_COLOR);
            return 1;
        }
        // print hexadecimal number (base 16)
        case 'x': {
            const u32 x = va_arg(va, u32);
            write(itoa(x, 16), fg, DEFAULT_FBBG_COLOR);
            return 1;
        }
        // print unsigned decimal (integer) number
        case 'u': {
            if (format[i + 2] == 'l' && format[i + 3] == 'l') {
                // fix this later
                const u64 u = va_arg(va, u64);
                char *s = itoa(static_cast<u32>(u));
                write(s, fg, DEFAULT_FBBG_COLOR);
                return 3;
            }
            const u32 u = va_arg(va, u32);
            char *s = itoa(u);
            write(s, fg, DEFAULT_FBBG_COLOR);
            if (format[i + 2] == 'l')
                return 2;

            return 1;
        }
        // print pointer address
        case 'p': {
            const auto p = addressof(va_arg(va, void *));
            write(itoa(p, 16), fg, DEFAULT_FBBG_COLOR);
            return 1;
        }
        // print character
        case 'c': {
            const char c = static_cast<char>(va_arg(va, int));
            writec(c, fg, DEFAULT_FBBG_COLOR);
            return 1;
        }
        // print octal number
        case 'o': {
            const u32 o = va_arg(va, u32);
            write(itoa(o, 8), fg, DEFAULT_FBBG_COLOR);
            return 1;
        }
        // print binary number
        case 'b': {
            const u32 b = va_arg(va, u32);
            write(itoa(b, 2), fg, DEFAULT_FBBG_COLOR);
            return 1;
        }
        // color code escape sequence?
        case '[': {
            // Color Reset
            if (format[i + 2] == '0' && format[i + 3] == 'm') {
                fg = DEFAULT_FBFG_COLOR;
                return 3; // 3 steps in total
            }
            // Non-bright color
            if (format[i + 2] == '3' && (format[i + 3] >= '0' && format[i + 3] <= '7') &&
                format[i + 4] == 'm') {
                switch (format[i + 3]) {
                    case ANSI_COLORCODE_BLACK:
                        fg = FBFGColor::Black;
                        break;
                    case ANSI_COLORCODE_RED:
                        fg = FBFGColor::Red;
                        break;
                    case ANSI_COLORCODE_GREEN:
                        fg = FBFGColor::Green;
                        break;
                    case ANSI_COLORCODE_YELLOW:
                        fg = FBFGColor::LightBrown;
                        break;
                    case ANSI_COLORCODE_BLUE:
                        fg = FBFGColor::Blue;
                        break;
                    case ANSI_COLORCODE_MAGENTA:
                        fg = FBFGColor::Magenta;
                        break;
                    case ANSI_COLORCODE_CYAN:
                        fg = FBFGColor::Cyan;
                        break;
                    case ANSI_COLORCODE_WHITE:
                        fg = FBFGColor::White;
                        break;
                }
                return 4; // 4 steps in total
            }
            // Light color
            if (format[i + 2] == '9' && (format[i + 3] >= '1' && format[i + 3] <= '7') &&
                format[i + 4] == 'm') {
                switch (format[i + 3]) {
                    case ANSI_COLORCODE_RED:
                        fg = FBFGColor::LightRed;
                        break;
                    case ANSI_COLORCODE_GREEN:
                        fg = FBFGColor::LightGreen;
                        break;
                    case ANSI_COLORCODE_YELLOW:
                        fg = FBFGColor::LightBrown;
                        break;
                    case ANSI_COLORCODE_BLUE:
                        fg = FBFGColor::LightBlue;
                        break;
                    case ANSI_COLORCODE_MAGENTA:
                        fg = FBFGColor::LightMagenta;
                        break;
                    case ANSI_COLORCODE_CYAN:
                        fg = FBFGColor::LightCyan;
                        break;
                }
                return 4;
            }
#ifndef NDEBUG
            kpanic("invalid escape sequence");
#endif
        }
    }
    return 0;
}

void kvfprintf(const char *format, va_list va) noexcept
{
    // NOLINTBEGIN(clang-analyzer-core.NullDereference)
    static constinit FBFGColor fg = DEFAULT_FBFG_COLOR;
    for (usize i = 0; format[i] != '\0'; ++i) {
        if (format[i] == '%' || is_ansi_escape_character(format[i]))
            i += fb_write_format_specifier(format, i, va, fg, fb_write, fb_writec);
        else
            fb_writec(format[i], fg);
    }
    // NOLINTEND(clang-analyzer-core.NullDereference)
}

void srl_vfprintf(const char *format, va_list va) noexcept
{
    // NOLINTBEGIN(clang-analyzer-core.NullDereference)
    static constinit FBFGColor fg = DEFAULT_FBFG_COLOR;
    for (usize i = 0; format[i] != '\0'; ++i) {
        if (format[i] == '%' || is_ansi_escape_character(format[i]))
            i += fb_write_format_specifier(format, i, va, fg, serial_puts, serial_putc);
        else
            serial_putc(format[i], fg);
    }
    // NOLINTEND(clang-analyzer-core.NullDereference)
}

void kprintf(const char *format, ...) noexcept
{
    va_list va;
    va_start(va, format);
    kvfprintf(format, va);
    va_end(va);
}

void srl_printf(const char *format, ...) noexcept
{
    va_list va;
    va_start(va, format);
    srl_vfprintf(format, va);
    va_end(va);
}
