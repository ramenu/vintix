; Copyright (C) 2023-2025 Abdur-Rahman Mansoor
;
; This file is part of Vintix.
; 
; Vintix is free software: you can redistribute it and/or modify it under the terms of the 
; GNU General Public License as published by the Free Software Foundation, either version 
; 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
; 
; Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
; See the GNU General Public License for more details.
; 
; You should have received a copy of the GNU General Public License along with Vintix. If 
; not, see <https://www.gnu.org/licenses/>.

extern interrupt_handler
extern irq_handler
global _load_idt
global _common_interrupt_handler
extern syscall_handler

%macro interrupt_handler_noerr 1
global _interrupt_handler_%1
_interrupt_handler_%1:
    push DWORD 0 ; push 0 as error code
    push DWORD %1 ; push interrupt #
    jmp _common_interrupt_handler ; jump to common handler
%endmacro

%macro interrupt_handler_witherr 1
global _interrupt_handler_%1
_interrupt_handler_%1:
    ; error code already pushed by cpu
    push DWORD %1 ; push interrupt #
    jmp _common_interrupt_handler
%endmacro

%macro irq_handler_noerr 1
global _irq_handler_%1
_irq_handler_%1:
    push DWORD 0 ; push 0 as error code
    push DWORD %1 + 32 ; push interrupt #
    jmp _common_irq_handler
%endmacro

; all of our interrupt handlers jump to this, basically just
; an entry point to call our C++ handler and then return to
; the interrupted code
_common_interrupt_handler:
    pushad
    call interrupt_handler
    popad

    ; restore stack pointer (cause we pushed error code and interrupt #)
    add esp,  8

    ; return to code that got interrupted
    iret

; all of our IRQ handlers jump to this, basically just
; an entry point to call our C++ handler and then return
; to the interrupted code
_common_irq_handler:
    pushad
    call irq_handler
    popad

    ; restore stack pointer (cause we pushed error code and interrupt #)
    add esp, 8
    iret
    

; _idt_load: loads the interrupt description table
; stack: [esp + 4] pointer to idt struct
global _load_idt
_load_idt:
    mov eax, DWORD [esp + 4]
    lidt [eax]
    ret

global _interrupt_handler_128
_interrupt_handler_128:
    push ebp
    push edi
    push esi
    push edx
    push ecx
    push ebx
    push eax

    call syscall_handler

    add esp, 4 ; keep the value of eax on the stack
    pop ebx
    pop ecx
    pop edx
    pop esi
    pop edi
    pop ebp

    iretd
    

; CPU exception interrupt handlers
interrupt_handler_noerr 0 ; divide by zero
interrupt_handler_noerr 1 ; debug
interrupt_handler_noerr 2 ; non maskable interrupt
interrupt_handler_noerr 3 ; breakpoint
interrupt_handler_noerr 4 ; overflow
interrupt_handler_noerr 5 ; bound range exceeded
interrupt_handler_noerr 6 ; invalid opcode
interrupt_handler_noerr 7 ; device not available
interrupt_handler_witherr 8 ; double fault
interrupt_handler_noerr 9 ; coprocessor segment overrun
interrupt_handler_witherr 10 ; invalid tss
interrupt_handler_witherr 11 ; segment not present
interrupt_handler_witherr 12 ; stack segment fault
interrupt_handler_witherr 13 ; general protection fault
interrupt_handler_witherr 14 ; page fault
interrupt_handler_noerr 16 ; x87 floating point exception
interrupt_handler_noerr 17 ; alignment check
interrupt_handler_noerr 18 ; machine check

; IRQ handlers (PIC1)
irq_handler_noerr 0 ; timer
irq_handler_noerr 1 ; keyboard
irq_handler_noerr 2 ; PIC 2
irq_handler_noerr 3 ; COM2
irq_handler_noerr 4 ; COM1
irq_handler_noerr 5 ; LPT2
irq_handler_noerr 6 ; floppy disk
irq_handler_noerr 7 ; LPT1

; IRQ handlers (PIC2)
irq_handler_noerr 8 ; real time clcok
irq_handler_noerr 9 ; general I/O
irq_handler_noerr 10 ; general I/O
irq_handler_noerr 11 ; general I/O
irq_handler_noerr 12 ; PS2 mouse
irq_handler_noerr 13 ; FPU
irq_handler_noerr 14 ; primary ata hard disk
irq_handler_noerr 15 ; secondary ata hard disk
