; Copyright (C) 2023-2025 Abdur-Rahman Mansoor
;
; This file is part of Vintix.
; 
; Vintix is free software: you can redistribute it and/or modify it under the terms of the 
; GNU General Public License as published by the Free Software Foundation, either version 
; 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
; 
; Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
; See the GNU General Public License for more details.
; 
; You should have received a copy of the GNU General Public License along with Vintix. If 
; not, see <https://www.gnu.org/licenses/>.

; _flush_tss - load the TSS, must be executed
;              by CPL=0. LTR loads the special
;              x86 task register with a segment
;              selector that points to the
;              specified location
global _flush_tss
_flush_tss:
    mov ax, 0x28
    ltr ax
    ret
