; Copyright (C) 2023-2025 Abdur-Rahman Mansoor
; 
; This file is part of Vintix.
; 
; Vintix is free software: you can redistribute it and/or modify it under the terms of the 
; GNU General Public License as published by the Free Software Foundation, either version 
; 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
; 
; Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
; See the GNU General Public License for more details.
; 
; You should have received a copy of the GNU General Public License along with Vintix. If 
; not, see <https://www.gnu.org/licenses/>.

; _switch_to_vm86 - switches to Virtual 8086 mode
; stack:   [esp + 8] EIP
;          [esp + 4] ESP
global _switch_to_vm86
align 4
_switch_to_vm86:
	cli
	mov 	ax, 0xffff
	push 	ax ; GS
	push 	ax ; FS
	push 	ax ; DS
	push 	ax ; ES
	push 	dword 0xffff ; SS
	push 	dword 0x0074 ; ESP
	push 	dword 0x20000 ; EFLAGS (VM = 1)
	push 	dword 0xffff ; CS
	push	dword 0x0074 ; EIP
	iret
