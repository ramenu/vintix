; Copyright (C) 2023-2025 Abdur-Rahman Mansoor
;
; This file is part of Vintix.
; 
; Vintix is free software: you can redistribute it and/or modify it under the terms of the 
; GNU General Public License as published by the Free Software Foundation, either version 
; 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
; 
; Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
; See the GNU General Public License for more details.
; 
; You should have received a copy of the GNU General Public License along with Vintix. If 
; not, see <https://www.gnu.org/licenses/>.

%ifndef KERNEL_STACK_SIZE
%error "KERNEL_STACK_SIZE not defined"
%endif

global loader                   ; the entry symbol for ELF
extern kmain
extern _load_page_directory
extern _enable_paging
extern _load_address
extern pg_directory
extern KERNEL_PHYSICAL_START
extern KERNEL_PHYSICAL_END
extern KERNEL_VIRTUAL_START
extern KERNEL_VIRTUAL_END
extern VIRTUAL_ADDRESS_OFFSET
global multiboot_info
global mbt
global kernel_stack
global u_p_begin
global u_p_end
global k_p_begin
global k_p_end
extern pg_table
extern VADDR_OFFSET
extern _pci_mechanism_supported


; declare constants
MAGIC_NUMBER equ 0x1BADB002     ; define the magic number constant
ALIGN_MODULES equ 0x00000001    ; tell GRUB to align modules
CHECKSUM     equ -(MAGIC_NUMBER + ALIGN_MODULES) ; calculate the checksum
%define PAGE_FRAME_SIZE 4096
%define CR0_FPU_FLAG (1 << 2) ; x87 FPU emulation
%define CR0_PG_FLAG (1 << 31) ; Enable paging
%define CR0_WP_FLAG (1 << 16) ; When set, the CPU cannot write to read-only pages when CPL=0
%define CR0_FLAGS CR0_PG_FLAG
%define RW_FLAG 2
%define PRESENT_FLAG 1
%define US_FLAG 4
%define PDT_ENTRIES 1024
%define PT_ENTRIES 1024
%define ONE_MIB 1048576
%define PG_FLAGS PRESENT_FLAG
%define KERNEL_PDT 768

; multiboot header
section .multiboot.data
align 4
	multiboot_header:
		dd MAGIC_NUMBER             ; write the magic number to the machine code,
		dd ALIGN_MODULES            ; the flags (in this case, we tell GRUB to align the modules we provided),
		dd CHECKSUM                 ; and the checksum
									; (magic number + checksum + flags should equal 0)

; this section will boot the kernel into the
; higher half
section .multiboot.text
align 4
loader:
	
		mov 	edx, mbt
		sub 	edx, VIRTUAL_ADDRESS_OFFSET

		; pass multiboot information given by GRUB
		; into 'mbt'
		mov 	dword [edx], ebx

		; conventional memory, we have about 29.75KiB to work with which is more
		; than enough (See https://wiki.osdev.org/Memory_Map_(x86))
		mov 	esp, 0x7bff

		; So, we need to deal with a potential problem occurring which is the
		; fact that our bootloader may load our module in a place we don't like.
		; This can cause problems later on, so it's important to address this issue
		; as soon as possible. My solution here is to simply copy the bytes of the
		; module at a high address so it won't conflict with any of the kernel stuff.
		mov 	ecx, dword [ebx + 20] ; mbt->mods_count
		test 	ecx, ecx
		jz      .no_modules
		mov 	dword [esp], KERNEL_PHYSICAL_END + 0x50000 ; offset
		mov 	edx, dword [ebx + 24] ; mbt->mods_addr

.module_loop:
		mov 	esi, dword [edx] ; mod->mod_start
		mov 	edi, dword [esp] ; offset
		push 	ecx
		mov 	ecx, dword [edx + 4] ; mod->mod_end
		sub 	ecx, esi ; number of bytes we need to copy

		; new mod->mod_end
		mov 	eax, edi
		add 	eax, ecx
		add 	eax, PAGE_FRAME_SIZE - 1
		and 	eax, ~(PAGE_FRAME_SIZE - 1)
		mov 	dword [esp + 4], eax

		; set the new `mod_start` and `mod_end` values
		mov 	dword [edx], edi
		mov 	dword [edx + 4], eax

		; align up to a DWORD for faster copying
		add 	ecx, 3
		and 	ecx, ~3

		shr 	ecx, 2 ; /= 4
		rep 	movsd ; copy the bytes

		pop 	ecx
		add 	edx, 16 ; += sizeof(multiboot_mod_list)
		dec 	ecx
		test 	ecx, ecx
		jnz 	.module_loop
		
.no_modules:
		mov 	esi, ebx
		mov 	eax, dword [esi + 8] ; mbt->mem_upper
		shl 	eax, 10 ; *= 1024
		add 	eax, ONE_MIB
		shr 	eax, 12 ; /= PAGE_FRAME_SIZE
		shr 	eax, 5 ; /= 32
		inc 	eax ; total_pf_bitmaps
		mov 	dword [esp - 4], eax
		mov 	edx, KERNEL_PHYSICAL_END + PAGE_FRAME_SIZE ; physical_placement_addr
		shl 	eax, 2 ; *= 4
		add 	eax, 4
		add 	edx, eax
		
		; align at 4kib
		add 	edx, 4095
		and 	edx, 0xfffff000
		; allocate our page tables
		mov 	edi, pg_table
		sub 	edi, VIRTUAL_ADDRESS_OFFSET
		mov 	dword [edi + KERNEL_PDT * 4], edx ; pg_table[768]
		add 	edx, PAGE_FRAME_SIZE
		mov 	dword [esp - 8], edx ; save value of physical_placement_addr
		
		; zero out the tables
		mov 	edi, dword [edi + KERNEL_PDT * 4]
		mov 	esi, edi
		mov 	ecx, PAGE_FRAME_SIZE / 4
		xor 	eax, eax
		rep 	stosd
		
		mov 	edi, esi
		xor 	ecx, ecx
		mov 	ebx, dword [esp - 8]

	.loop:

		; map the kernel
		mov 	eax, ecx
		shl 	eax, 12 ; *= PAGE_FRAME_SIZE
		or 	eax, PRESENT_FLAG | RW_FLAG
		mov 	dword [edi + ecx * 4], eax
		inc 	ecx
		add 	eax, PAGE_FRAME_SIZE
		cmp 	eax, ebx
		jl 	.loop
		
		; Before we continue, let's load the correct values
		; for the VIRTUAL_ADDRESS_OFFSET and KERNEL_PHYSICAL_START
		; symbols, since our higher half kernel will be unable to
		; reference them because of paging issues
		mov 	eax, VADDR_OFFSET
		sub 	eax, VIRTUAL_ADDRESS_OFFSET
		mov 	dword [eax], VIRTUAL_ADDRESS_OFFSET

		; load page table
		; this may seem obvious, but we have to mark
		; the page directory as R/W and allow usermode
		; pages, since the page directories control access
		; to all pages. So even if we set the page table
		; bits appropriately, it won't work if the page
		; directory doesn't have those bits set as well.
		; I found out the hard way, unfortunately...
		mov 	esi, pg_table
		sub 	esi, VIRTUAL_ADDRESS_OFFSET
		mov 	edi, pg_directory
		sub 	edi, VIRTUAL_ADDRESS_OFFSET
		mov 	eax, dword [esi + KERNEL_PDT * 4]
 		or      eax, PRESENT_FLAG | US_FLAG | RW_FLAG
		mov 	dword [edi + KERNEL_PDT * 4], eax
		; needed so we don't triple fault when doing a far jump to our higher half
		mov 	dword [edi], eax

		; load page directory
		mov 	cr3, edi

		; enable paging
		mov 	eax, cr0
		or      eax, CR0_FLAGS
		mov     cr0, eax

		jmp 	0x08:kernel_entry

section .text                   ; start of the text (code) section
align 4                         ; the code must be 4 byte aligned
	kernel_entry: 
		mov 	edx, dword [esp - 4] ; total_pf_bitmaps
		mov 	ebx, dword [esp - 8] ; physical_placement_addr
		mov 	esp, kernel_stack + KERNEL_STACK_SIZE ; set kernel stack

		mov 	dword [k_p_begin], KERNEL_PHYSICAL_START
		mov 	dword [k_p_end], KERNEL_PHYSICAL_END
		mov 	dword [u_p_begin], KERNEL_PHYSICAL_END + PAGE_FRAME_SIZE

		mov 	ecx, dword [mbt]
		mov 	ecx, dword [ecx + 8]
		shl 	ecx, 10 ; *= 1024
		add 	ecx, ONE_MIB
		mov 	dword [u_p_end], ecx
		add 	dword [mbt], VIRTUAL_ADDRESS_OFFSET
		
		; not in the lower half anymore, so we can invalidate the
		; first PDT entry
		mov 	dword [edi + VIRTUAL_ADDRESS_OFFSET], 0
		mov 	cr3, edi

		xor 	ebp, ebp ; NULL %ebp so that stack traces don't run into garbage
		push 	edx
		push 	ebx
		call 	kmain

	.loop:
		
		jmp .loop ; loop forever


; start of .bss section 
; (for statically allocated variables that are declared but do
;  not have a value yet)
section .bss
align 4
	kernel_stack: resb KERNEL_STACK_SIZE ; reserve stack for kernel
	mbt: resb 4
	k_p_begin: resb 4
	k_p_end: resb 4
	u_p_begin: resb 4
	u_p_end: resb 4

; vim: syntax=nasm
