/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/asm/regs.h>
#include <vintix/kernel/core/handler.h>
#include <vintix/kernel/core/oom.h>
#include <vintix/kernel/core/pager.h>
#include <vintix/kernel/core/panic.h>
#include <vintix/kernel/core/pmm.h>
#include <vintix/kernel/core/process.h>
#include <vintix/kernel/core/vmm.h>
#include <vintix/kernel/filesystems/file.h>
#include <vintix/kernel/lib/klog.h>
#include <vintix/kernel/lib/utilities.h>
#include <vintix/kernel/posix/signal.h>

// For more information see: https://wiki.osdev.org/Exceptions#Page_Fault
enum pg_fault_err : u8 {
    // When set, the page fault was caused by a page-protection violation.
    // When not set, it was caused by a non-present page
    PRESENT = 1 << 0,

    // When set, the page fault was caused by a write access. When
    // not set, it was caused by a read access
    WRITE = 1 << 1,

    // When set, the page fault was caused by 'CPL = 3'. This does not
    // necessarily mean that the page fault was a privilege violation.
    USER = 1 << 2,

    // When set, one or more page directory entries contain reserved
    // bits which are set to 1. This only applies when the PSE or PAE
    // flags in CR4 are set to 1
    RESERVED_WRITE = 1 << 3,

    // When set, the page fault was caused by an instruction fetch.
    // This only applies when the No-Execute bit is supported and
    // enabled
    INSTRUCTION_FETCH = 1 << 4,

    // When set, the page fault was caused by a protection-key violation.
    // The PKRU register (for user-mode accesses) or PKRS MSR (for supervisor-mode
    // accesses) specifies the protection key rights
    PROTECTION_KEY = 1 << 5,

    // When set, the page fault was caused by a shadow p_stack access
    SHADOW_STACK = 1 << 6,

    // When set, the page fault was due to an SGX violation. The fault is unrelated
    // to ordinary paging
    SOFTWARE_GUARD_EXTENSIONS = 1 << 7
};

void page_fault_handler(const interrupt_state &interrupt) noexcept
{
    const v_addr scr2 = cr2();
    DEBUG("page_fault_handler: page fault at 0x%x, EIP=0x%x, EFLAGS=0x%x\n", scr2, interrupt.eip,
          interrupt.eflags);
    if (interrupt.eip >= VADDR_OFFSET) {
        // kernel page fault, we cannot handle this (shouldn't happen under any circumstance)
        kpanic("kernel page fault at 0x%x (EIP: 0x%x) (EFLAGS: 0x%x)\n", scr2, interrupt.eip,
               interrupt.eflags);
    }

    // userspace page fault, we can handle this
    // ----------------------------------------

    if (scr2 >= VADDR_OFFSET)
        return general_protection_fault_handler();

    // page fault on the process stack?
    if (scr2 == interrupt.esp) {
        vintix::ignore = process::running()->expand_stack(scr2);
        return;
    }

    const vm_map_entry *mapping = process::running()->lookup_mapping(scr2);

    // mapping not found
    if (mapping == nullptr) {
        DEBUG("page_fault_handler: failed to find mapping for process \"%s\"\n",
              process::running()->name().c_str());
        return general_protection_fault_handler();
    }

    p_addr pg = alloc_page();

    if (!pg) {
        const pid_t offender = oom_handler();
        if (offender == process::running()->pid())
            return;
        pg = alloc_page();
        KASSERT(pg != 0);
    }

    vintix::ignore = map_page(scr2, pg, PG_P | PG_USER | PG_R | PG_W | PG_X);

    if ([[maybe_unused]] auto err =
            mapping->object.pager.get_page(unsafe_toptr<void *>(page_align(scr2)),
                                           vm_pager::determine_page_offset(scr2, mapping->start)))
        return process::terminate(process::running()->pid(), SIGBUS);
}

void general_protection_fault_handler() noexcept
{
    return process::terminate(process::running()->pid(), SIGSEGV);
}
