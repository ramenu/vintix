; Copyright (C) 2023-2025 Abdur-Rahman Mansoor
;
; This file is part of Vintix.
; 
; Vintix is free software: you can redistribute it and/or modify it under the terms of the 
; GNU General Public License as published by the Free Software Foundation, either version 
; 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
; 
; Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
; See the GNU General Public License for more details.
; 
; You should have received a copy of the GNU General Public License along with Vintix. If 
; not, see <https://www.gnu.org/licenses/>.

; [esp + 8] EIP
; _switch_to_usermode - changes the privilege level from 0 to 3
;                       and sets the segment registers to their
;                       appropriate values
;                       and executes the instruction at EIP
; stack:   [esp + 8] EIP
;          [esp + 4] ESP
global _switch_to_usermode
align 4
_switch_to_usermode:
	cli
    
        mov 	ax, 0x20 | 0x3
        mov 	ds, ax
        mov 	es, ax
        mov 	fs, ax
        mov 	gs, ax

        mov 	eax, dword [esp + 4] ; esp
        mov 	ebx, dword [esp + 8] ; eip

        push 	0x20 | 0x3 ; ss
        push 	eax
        pushfd
        push 	0x18 | 0x3 ; cs
        push 	ebx

        iret

; vim: syntax=nasm
