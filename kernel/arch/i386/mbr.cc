/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/arch/x86-32/mbr.h>
#include <vintix/kernel/core/blkdev.h>
#include <vintix/kernel/core/kassert.h>

auto parse_master_boot_record(dev_t dev, mbr *master_boot_record) noexcept -> errno
{
    KASSERT(master_boot_record != nullptr);

    if (auto err = disk_read(dev, master_boot_record, 0, 1))
        return err;

    // NOLINTBEGIN(clang-analyzer-core.NullDereference)
    // NOLINTBEGIN(clang-analyzer-core.CallAndMessage)
    if (!master_boot_record->is_valid()) {
        return EINVAL;
    }

    // NOLINTEND(clang-analyzer-core.CallAndMessage)
    // NOLINTEND(clang-analyzer-core.NullDereference)
    return ENOERR;
}
