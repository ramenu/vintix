/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/asm/inst.h>
#include <vintix/kernel/asm/regs.h>
#include <vintix/kernel/core/dpl.h>
#include <vintix/kernel/core/handler.h>
#include <vintix/kernel/core/idt.h>
#include <vintix/kernel/core/io.h>
#include <vintix/kernel/core/kassert.h>
#include <vintix/kernel/core/pit.h>
#include <vintix/kernel/core/process.h>
#include <vintix/kernel/core/vmm.h>
#include <vintix/kernel/lib/array.h>
#include <vintix/kernel/lib/klog.h>
#include <vintix/kernel/lib/utilities.h>
#include <vintix/kernel/posix/signal.h>

#define CREATE_INTERRUPT_HANDLER(n) extern "C" void _interrupt_handler_##n() noexcept;
#define CREATE_IRQ_HANDLER(n) extern "C" void _irq_handler_##n() noexcept;

CREATE_INTERRUPT_HANDLER(0)   // divide by zero
CREATE_INTERRUPT_HANDLER(1)   // debug
CREATE_INTERRUPT_HANDLER(2)   // non maskable interrupt
CREATE_INTERRUPT_HANDLER(3)   // breakpoint
CREATE_INTERRUPT_HANDLER(4)   // overflow
CREATE_INTERRUPT_HANDLER(5)   // bound range exceeded
CREATE_INTERRUPT_HANDLER(6)   // invalid opcode
CREATE_INTERRUPT_HANDLER(7)   // device not available
CREATE_INTERRUPT_HANDLER(8)   // double fault
CREATE_INTERRUPT_HANDLER(9)   // coprocessor segment overrun
CREATE_INTERRUPT_HANDLER(10)  // invalid tss
CREATE_INTERRUPT_HANDLER(11)  // segment not present
CREATE_INTERRUPT_HANDLER(12)  // stack segment fault
CREATE_INTERRUPT_HANDLER(13)  // general protection fault
CREATE_INTERRUPT_HANDLER(14)  // page fault
CREATE_INTERRUPT_HANDLER(16)  // x87 floating point exception
CREATE_INTERRUPT_HANDLER(17)  // alignment check
CREATE_INTERRUPT_HANDLER(18)  // machine check
CREATE_INTERRUPT_HANDLER(128) // software interrupt

// IRQ handlers (PIC1)
CREATE_IRQ_HANDLER(0) // timer
CREATE_IRQ_HANDLER(1) // keyboard
CREATE_IRQ_HANDLER(2) // PIC2
CREATE_IRQ_HANDLER(3) // COM2
CREATE_IRQ_HANDLER(4) // COM1
CREATE_IRQ_HANDLER(5) // LPT2
CREATE_IRQ_HANDLER(6) // floppy disk
CREATE_IRQ_HANDLER(7) // LPT1

// IRQ handlers (PIC2)
CREATE_IRQ_HANDLER(8)  // real time clock
CREATE_IRQ_HANDLER(9)  // general I/O
CREATE_IRQ_HANDLER(10) // general I/O
CREATE_IRQ_HANDLER(11) // general I/O
CREATE_IRQ_HANDLER(12) // PS2 mouse
CREATE_IRQ_HANDLER(13) // FPU
CREATE_IRQ_HANDLER(14) // primary ATA hard disk
CREATE_IRQ_HANDLER(15) // secondary ATA hard disk

#undef CREATE_IRQ_HANDLER
#undef CREATE_INTERRUPT_HANDLER

struct idt {
    u16 size;
    u32 base;
} __attribute__((packed));

enum gate_size : u8 {
    SIXTEEN_BIT,
    THIRTY_TWO_BIT
};

struct idt_descriptor {
    u16 offset_lo {};   // lowest 16 bits of 32 bit address in segment
    u16 segment_sel {}; // offset in the GDT
    u8 zero {};
    u8 flags {};      // p, dpl, and size of gate
    u16 offset_hi {}; // highest 16 bits of 32 bit address in segment
} __attribute__((packed));

static constexpr vintix::array ISR_EXCEPTION_MESSAGES { "division by zero",
                                                        "debug",
                                                        "non maskable interrupt",
                                                        "breakpoint",
                                                        "overflow",
                                                        "bound range exceeded",
                                                        "invalid opcode",
                                                        "device not available",
                                                        "double fault",
                                                        "coprocessor segment overrun",
                                                        "invalid tss",
                                                        "segment not present",
                                                        "p_stack segment fault",
                                                        "general protection fault",
                                                        "page fault",
                                                        "reserved (0x0f)",
                                                        "x87 floating point exception",
                                                        "alignment check",
                                                        "machine check" };

static constexpr u32 PIC1_PORT_A = 0x20;
static constexpr u32 PIC2_PORT_A = 0xA0;

// PIC interrupts have been remapped
static constexpr u32 PIC1_START_INTERRUPT = 0x20; // base address for master PIC
static constexpr u32 PIC2_START_INTERRUPT = 0x28; // base address for slave PIC
[[maybe_unused]] static constexpr u32 PIC1_END_INTERRUPT =
    PIC2_START_INTERRUPT;                                           // end address for master PIC
static constexpr u32 PIC2_END_INTERRUPT = PIC2_START_INTERRUPT + 7; // end address for slave PIC

static constexpr u32 PIC_ACK = 0x20; // PIC acknowledgment code

// I/O ports
static constexpr u32 PIC1_COMMAND_PORT = PIC1_START_INTERRUPT;
static constexpr u32 PIC2_COMMAND_PORT = PIC2_START_INTERRUPT;
static constexpr u32 PIC1_DATA_PORT = PIC1_START_INTERRUPT + 1;
static constexpr u32 PIC2_DATA_PORT = PIC2_START_INTERRUPT + 1;

static constexpr usize TOTAL_IRQS = PIC2_END_INTERRUPT - PIC1_START_INTERRUPT + 1;

using interrupt_handler_callback = void (*)(const interrupt_state &);
static constinit vintix::array<interrupt_handler_callback, TOTAL_IRQS> irq_handlers {};

// Installs an IRQ handler callback at 'irq'
void install_irq_handler(u8 irq, interrupt_handler_callback handler) noexcept
{
    KASSERT(irq < TOTAL_IRQS);
    KASSERT(irq_handlers[irq] == nullptr);
    irq_handlers[irq] = handler;
    DEBUG("install_irq_handler: Attached IRQ #%u to 0x%p\n", irq, handler);
}

// Uninstalls the IRQ handler callback at 'irq'
void uninstall_irq_handler(u8 irq) noexcept
{
    KASSERT(irq < TOTAL_IRQS);
    KASSERT(irq_handlers[irq] != nullptr);
    DEBUG("uninstall_irq_handler: Uninstalled IRQ #%u (attached to 0x%p)\n", irq,
          irq_handlers[irq]);
    irq_handlers[irq] = nullptr;
}

// see: https://wiki.osdev.org/Exceptions
enum int_exception : u8 {

    DIVISION_ERROR = 0x00,
    DEBUG = 0x01,
    NON_MASKABLE_INTERRUPT = 0x02,
    BREAKPOINT = 0x03,
    OVERFLOW = 0x04,
    BOUND_RANGE_EXCEEDED = 0x05,
    INVALID_OPCODE = 0x06,
    DEVICE_NOT_AVAILABLE = 0x07,
    DOUBLE_FAULT = 0x08,
    COPROCESSOR_SEGMENT_OVERRUN = 0x09,
    INVALID_TSS = 0x0a,
    SEGMENT_NOT_PRESENT = 0x0b,
    STACK_SEGMENT_FAULT = 0x0c,
    GENERAL_PROTECTION_FAULT = 0x0d,
    PAGE_FAULT = 0x0e,
    // 0x0f reserved
    X87_FLOATING_POINT_EXCEPTION = 0x10,
    ALIGNMENT_CHECK = 0x11,
    MACHINE_CHECK = 0x12,
    SIMD_FLOATING_POINT_EXCEPTION = 0x13,
    VIRTUALIZATION_EXCEPTION = 0x14,
    CONTROL_PROTECTION_EXCEPTION = 0x15,
    // 0x16-0x1b reserved
    HYPERVISOR_INJECTION_EXCEPTION = 0x1c,
    VMM_COMMUNICATION_EXCEPTION = 0x1d,
    SECURITY_EXCEPTION = 0x1e,
    // 0x1f reserved
    SOFTWARE_EXCEPTION = 0x80
};

enum hardware_int : u8 {
    // PIC1 hardware interrupts
    TIMER = PIC1_START_INTERRUPT,
    KEYBOARD = PIC1_START_INTERRUPT + 1,
    PIC2 = PIC1_START_INTERRUPT + 2,
    COM2 = PIC1_START_INTERRUPT + 3,
    COM1 = PIC1_START_INTERRUPT + 4,
    LPT2 = PIC1_START_INTERRUPT + 5,
    FLOPPY_DISK = PIC1_START_INTERRUPT + 6,
    LPT1 = PIC1_START_INTERRUPT + 7,

    // PIC2 hardware interrupts
    REAL_TIME_CLOCK = PIC2_START_INTERRUPT,
    GENERAL_IO1 = PIC2_START_INTERRUPT + 1,
    GENERAL_IO2 = PIC2_START_INTERRUPT + 2,
    GENERAL_IO3 = PIC2_START_INTERRUPT + 3,
    PS2_MOUSE = PIC2_START_INTERRUPT + 4,
    FPU = PIC2_START_INTERRUPT + 5,
    PRIMARY_ATA_HARD_DISK = PIC2_START_INTERRUPT + 6,
    SECONDARY_ATA_HARD_DISK = PIC2_START_INTERRUPT + 7
};

extern "C" void interrupt_handler(interrupt_state interrupt) noexcept;

// defined in 'interrupt.asm'
// loads the interrupt descriptor table
extern "C" void _load_idt(const idt &table) noexcept;

// ICW = initialization command word

// specify whether the fourth initialization word is required
static constexpr u8 ICW1_ICW4 = 0x01;

// specify whether the PIC is operating in single mode or cascade mode.
// Note that in single mode the PIC manages the interrupts by itself. In
// cascade mode, multiple PICS are used to manage multiple interrupt levels.
[[maybe_unused]] static constexpr u8 ICW1_SINGLE = 0x02; // single (cascade) mode

[[maybe_unused]] static constexpr u8 ICW1_INTERVAL4 = 0x04; // call address interval 4 (8)

// specifies the trigger mode. In level-triggered mode, the interrupt
// remains active until explicitly acknowledged. In edge triggered mode
// the interrupt is triggered by a transition from low to high on the
// interrupt request line
[[maybe_unused]] static constexpr u8 ICW1_LEVEL = 0x08; // level triggered (edge) mode

static constexpr u8 ICW1_INIT = 0x10; // initialization - required!

// specifies mode of operation, in 8086/86 mode the PIC behaves like
// a 8086/86 processor
static constexpr u8 ICW4_8086 = 0x01; // 8086/88 (MCS-80/85) mode

static constexpr usize NUM_INTERRUPT_DESCRIPTORS = 256;
static constinit vintix::array<idt_descriptor, NUM_INTERRUPT_DESCRIPTORS> descriptors {};

// Sets the IDT flags
// Used to make the code more readable
consteval static auto set_idt_flags(bool handler_is_present, dpl dpl, gate_size size) noexcept -> u8
{
    // Bit:     | 7  | 6 5  | 4  |  3 |  2  1 0 |
    // Content: | P  | DPL  | 0  |  D |  1  1 0 |
    // where:
    // D = size of gate (1 = 32 bits, 0 = 16 bits)
    // DPL = descriptor privilege level (from 0-3)
    // p = handler is present?

    return static_cast<u8>(((handler_is_present << 7) | (dpl << 5) | (size << 3) | 0b110));
}

// Sends an acknowledgment for the interrupt # to the PIC
static void pic_acknowledge(u32 int_n) noexcept
{
    if (int_n >= PIC2_START_INTERRUPT)
        outb(PIC2_PORT_A, PIC_ACK); // send acknowledgment to IRQ2

    outb(PIC1_PORT_A, PIC_ACK); // send acknowledgment to IRQ1
}

// Remaps the PIC to 'PIC1_START_INTERRUPT' to 'PIC2_END_INTERRUPT'
// because it overlaps with the CPU exceptions, this is a mandatory
// requirement and should be called when initializing the kernel
static void pic_remap() noexcept
{
    // initialize both PICs (this will make the PIC wait for 3 extra
    // "initialization words" on the data port)
    outb(PIC1_COMMAND_PORT, ICW1_INIT | ICW1_ICW4);
    io_wait();
    outb(PIC2_COMMAND_PORT, ICW1_INIT | ICW1_ICW4);
    io_wait();

    // specify vector offsets (ICW2)
    outb(PIC1_DATA_PORT, PIC1_START_INTERRUPT);
    io_wait();
    outb(PIC2_DATA_PORT, PIC2_START_INTERRUPT);
    io_wait();

    // tell how it is wired to master/slaves (ICW3)
    outb(PIC1_DATA_PORT, 4); // tell master PIC that there is a slave PIC at IRQ2
    io_wait();
    outb(PIC2_DATA_PORT, 2); // tell slave PIC its cascade identity
    io_wait();

    // configure additional information about environment
    // specify mode of operation, tell PIC behave like 8086 processor
    outb(PIC1_DATA_PORT, ICW4_8086);
    io_wait();
    outb(PIC2_DATA_PORT, ICW4_8086);
    io_wait();

    // Enable all IRQ lines
    outb(PIC1_DATA_PORT, 0x00);
    io_wait();
    outb(PIC2_DATA_PORT, 0x00);
    io_wait();
}

// Sets up an idt_descriptor automatically with the given parameters where:
// base: 32 bit address which represents the entry point of the ISR
// selector: a 'segment selector' with multiple fields which must point to a valid segment in the
// GDT gate type: a 4-bit value which defines the type of gate this interrupt descriptor represents
static auto setup_idt_descriptor(void (*base)(), u16 sel, u8 flags) noexcept -> idt_descriptor
{
    const u32 b = addressof(base);
    idt_descriptor desc;
    // set 32-bit address
    desc.offset_lo = b & 0xffff;
    desc.offset_hi = static_cast<u16>((b >> 16) & 0xffff);

    desc.flags = flags;
    desc.zero = 0; // must always be 0
    desc.segment_sel = sel;

    return desc;
}

// Installs the mandatory ISRS and IQRs. Should be called upon initialization of kernel
static void install_isrs_and_iqrs() noexcept
{
    static constexpr u16 GDT_OFFSET = 0x08;
    static constexpr bool HANDLER_IS_PRESENT = true;
    static constexpr u8 IDT_FLAGS = set_idt_flags(HANDLER_IS_PRESENT, RING_0, THIRTY_TWO_BIT);
    static constexpr u8 IDT_FLAGS_USER = set_idt_flags(HANDLER_IS_PRESENT, RING_3, THIRTY_TWO_BIT);

    // Set descriptors for CPU interrupt exceptions
    descriptors[DIVISION_ERROR] = setup_idt_descriptor(_interrupt_handler_0, GDT_OFFSET, IDT_FLAGS);
    descriptors[DEBUG] = setup_idt_descriptor(_interrupt_handler_1, GDT_OFFSET, IDT_FLAGS);
    descriptors[NON_MASKABLE_INTERRUPT] =
        setup_idt_descriptor(_interrupt_handler_2, GDT_OFFSET, IDT_FLAGS);
    descriptors[BREAKPOINT] = setup_idt_descriptor(_interrupt_handler_3, GDT_OFFSET, IDT_FLAGS);
    descriptors[OVERFLOW] = setup_idt_descriptor(_interrupt_handler_4, GDT_OFFSET, IDT_FLAGS);
    descriptors[BOUND_RANGE_EXCEEDED] =
        setup_idt_descriptor(_interrupt_handler_5, GDT_OFFSET, IDT_FLAGS);
    descriptors[INVALID_OPCODE] = setup_idt_descriptor(_interrupt_handler_6, GDT_OFFSET, IDT_FLAGS);
    descriptors[DEVICE_NOT_AVAILABLE] =
        setup_idt_descriptor(_interrupt_handler_7, GDT_OFFSET, IDT_FLAGS);
    descriptors[DOUBLE_FAULT] = setup_idt_descriptor(_interrupt_handler_8, GDT_OFFSET, IDT_FLAGS);
    descriptors[COPROCESSOR_SEGMENT_OVERRUN] =
        setup_idt_descriptor(_interrupt_handler_9, GDT_OFFSET, IDT_FLAGS);
    descriptors[INVALID_TSS] = setup_idt_descriptor(_interrupt_handler_10, GDT_OFFSET, IDT_FLAGS);
    descriptors[SEGMENT_NOT_PRESENT] =
        setup_idt_descriptor(_interrupt_handler_11, GDT_OFFSET, IDT_FLAGS);
    descriptors[STACK_SEGMENT_FAULT] =
        setup_idt_descriptor(_interrupt_handler_12, GDT_OFFSET, IDT_FLAGS);
    descriptors[GENERAL_PROTECTION_FAULT] =
        setup_idt_descriptor(_interrupt_handler_13, GDT_OFFSET, IDT_FLAGS);
    descriptors[PAGE_FAULT] = setup_idt_descriptor(_interrupt_handler_14, GDT_OFFSET, IDT_FLAGS);
    descriptors[X87_FLOATING_POINT_EXCEPTION] =
        setup_idt_descriptor(_interrupt_handler_16, GDT_OFFSET, IDT_FLAGS);
    descriptors[ALIGNMENT_CHECK] =
        setup_idt_descriptor(_interrupt_handler_17, GDT_OFFSET, IDT_FLAGS);
    descriptors[MACHINE_CHECK] = setup_idt_descriptor(_interrupt_handler_18, GDT_OFFSET, IDT_FLAGS);
    descriptors[SOFTWARE_EXCEPTION] =
        setup_idt_descriptor(_interrupt_handler_128, GDT_OFFSET, IDT_FLAGS_USER);

    // Set descriptors for hardware interrupts
    descriptors[TIMER] = setup_idt_descriptor(_irq_handler_0, GDT_OFFSET, IDT_FLAGS);
    descriptors[KEYBOARD] = setup_idt_descriptor(_irq_handler_1, GDT_OFFSET, IDT_FLAGS);
    descriptors[PIC2] = setup_idt_descriptor(_irq_handler_2, GDT_OFFSET, IDT_FLAGS);
    descriptors[COM2] = setup_idt_descriptor(_irq_handler_3, GDT_OFFSET, IDT_FLAGS);
    descriptors[COM1] = setup_idt_descriptor(_irq_handler_4, GDT_OFFSET, IDT_FLAGS);
    descriptors[LPT2] = setup_idt_descriptor(_irq_handler_5, GDT_OFFSET, IDT_FLAGS);
    descriptors[FLOPPY_DISK] = setup_idt_descriptor(_irq_handler_6, GDT_OFFSET, IDT_FLAGS);
    descriptors[LPT1] = setup_idt_descriptor(_irq_handler_7, GDT_OFFSET, IDT_FLAGS);
    descriptors[REAL_TIME_CLOCK] = setup_idt_descriptor(_irq_handler_8, GDT_OFFSET, IDT_FLAGS);
    descriptors[GENERAL_IO1] = setup_idt_descriptor(_irq_handler_9, GDT_OFFSET, IDT_FLAGS);
    descriptors[GENERAL_IO2] = setup_idt_descriptor(_irq_handler_10, GDT_OFFSET, IDT_FLAGS);
    descriptors[GENERAL_IO3] = setup_idt_descriptor(_irq_handler_11, GDT_OFFSET, IDT_FLAGS);
    descriptors[PS2_MOUSE] = setup_idt_descriptor(_irq_handler_12, GDT_OFFSET, IDT_FLAGS);
    descriptors[FPU] = setup_idt_descriptor(_irq_handler_13, GDT_OFFSET, IDT_FLAGS);
    descriptors[PRIMARY_ATA_HARD_DISK] =
        setup_idt_descriptor(_irq_handler_14, GDT_OFFSET, IDT_FLAGS);
    descriptors[SECONDARY_ATA_HARD_DISK] =
        setup_idt_descriptor(_irq_handler_15, GDT_OFFSET, IDT_FLAGS);

    // install custom IRQ handlers
    set_pit_timer_phase(100);
    install_irq_handler(0, timer_handler);
}

// Initializes the IDT, ISRS, IQRs and remaps the PIC
void setup_idt() noexcept
{
    const idt table { .size = sizeof(idt_descriptor) * NUM_INTERRUPT_DESCRIPTORS - 1,
                      .base = addressof(descriptors.data()) };

    // We need to remap the IRQ entries since they overlap with the CPU interrupt exceptions
    pic_remap();

    install_isrs_and_iqrs();
    _load_idt(table);
}

// Handles all code for IRQs
extern "C" void irq_handler(interrupt_state interrupt) noexcept
{
    // get the appropriate callback handler
    auto *handler = irq_handlers[interrupt.no - 32];

    // if a custom handler is defined, call it
    if (handler != nullptr)
        handler(interrupt);

    // acknowledge that the interrupt was handled
    pic_acknowledge(interrupt.no);
}

// Handles all code for CPU interrupt exceptions
extern "C" void interrupt_handler(const interrupt_state interrupt) noexcept
{
    switch (interrupt.no) {
        case PAGE_FAULT:
            return page_fault_handler(interrupt);
        case BOUND_RANGE_EXCEEDED:
        case GENERAL_PROTECTION_FAULT:
            return general_protection_fault_handler();
        case INVALID_OPCODE:
            return process::terminate(process::running()->pid(), SIGILL);
        case DIVISION_ERROR:
            KASSERT(interrupt.eip < VADDR_OFFSET);
            return process::terminate(process::running()->pid(), SIGFPE);
    }

    kprintf("\n------------------------------\n");
    kprintf("EIP: 0x%x\n", interrupt.eip);
    kprintf("EFLAGS: 0b%b\n", interrupt.eflags);
    kprintf("CR2: 0x%x\n", cr2());
    kprintf("ERR: 0x%x\n", interrupt.error_code);
    kprintf("%s\n Exception. System halted!\n", ISR_EXCEPTION_MESSAGES[interrupt.no]);

    halt();
}
