; Copyright (C) 2023-2025 Abdur-Rahman Mansoor
; 
; This file is part of Vintix.
; 
; Vintix is free software: you can redistribute it and/or modify it under the terms of the 
; GNU General Public License as published by the Free Software Foundation, either version 
; 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
; 
; Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
; See the GNU General Public License for more details.
; 
; You should have received a copy of the GNU General Public License along with Vintix. If 
; not, see <https://www.gnu.org/licenses/>.


global memset
align 4
memset:
	push 	edi
	mov 	edi, dword [esp + 8]
	mov 	eax, dword [esp + 12]
	mov 	ecx, dword [esp + 16]
	mov 	edx, ecx
	and 	edx, 3
	jz 	.dword_memset
	mov 	edx, ecx
	and 	edx, 1
	jz 	.word_memset
	rep 	stosb
	jmp 	.ret

.word_memset:
	shr 	ecx, 1
	rep 	stosw
	jmp 	.ret

.dword_memset:
	shr 	ecx, 2
	rep 	stosd

.ret:
	pop 	edi
	ret

global memcpy
align 4
memcpy:
	push 	esi
	push 	edi
	mov 	edi, dword [esp + 12]
	mov 	esi, dword [esp + 16]
	mov 	ecx, dword [esp + 20]
	mov 	edx, ecx
	and 	edx, 3
	jz 	.dword_memcpy
	mov 	edx, ecx
	and 	edx, 1
	jz 	.word_memcpy
	rep 	movsb
	jmp 	.ret

.word_memcpy:
	shr 	ecx, 1 ; /= 2
	rep 	movsw
	jmp 	.ret

.dword_memcpy:
	shr 	ecx, 2 ; /= 4
	rep 	movsd

.ret:
	pop 	edi
	pop 	esi
	ret

; vim: syntax=nasm
