/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/core/cmos.h>
#include <vintix/kernel/core/rtc.h>

auto rtc_get_time() noexcept -> rtc_time
{
    rtc_time time, last;
    while (cmos_update_in_progress())
        ;

    time.second = rtc_get_seconds();
    time.minute = rtc_get_minutes();
    time.hour = rtc_get_hours();
    time.day = rtc_get_day_of_month();
    time.month = rtc_get_month();
    time.year = rtc_get_year();

    // we read from the registers twice until we (hopefully)
    // get the correct value
    do {
        last.second = time.second;
        last.minute = time.minute;
        last.hour = time.hour;
        last.day = time.day;
        last.month = time.month;
        last.year = time.year;

        while (cmos_update_in_progress())
            ;

        time.second = rtc_get_seconds();
        time.minute = rtc_get_minutes();
        time.hour = rtc_get_hours();
        time.day = rtc_get_day_of_month();
        time.month = rtc_get_month();
        time.year = rtc_get_year();

    } while (time != last);

    const u8 b = rtc_get_statusB();

    // convert BCD mode to binary, if necessary
    if (!(b & 0x04)) {
        time.second = static_cast<u8>((time.second & 0x0F) + ((time.second / 16) * 10));
        time.minute = static_cast<u8>((time.minute & 0x0F) + ((time.minute / 16) * 10));
        time.hour = static_cast<u8>((time.hour & 0x0F) + (((time.hour & 0x70) / 16) * 10)) |
                    (time.hour & 0x80);
        time.day = static_cast<u8>((time.day & 0x0F) + ((time.day / 16) * 10));
        time.month = static_cast<u8>((time.month & 0x0F) + ((time.month / 16) * 10));
        time.year = static_cast<u8>((time.year & 0x0F) + ((time.year / 16) * 10));
    }

    return time;
}
