/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/core/rtc.h>
#include <vintix/kernel/core/time.h>

static auto rtc_to_unix_time(const rtc_time &rtc) noexcept -> time_t
{
    const u32 year = rtc_year_to_4digityear(rtc.year);

    // note that this doesnt calculate the leap year correctly, leap year
    // doesnt occur ever 100 years (with a 400 year exemption), but I won't be
    // alive during that time, so I don't care
    const u32 nleaps = (year - 1970) / 4;

    time_t total_days = ((rtc_year_to_4digityear(rtc.year) - 1970 - nleaps) * 365) + nleaps * 366;
    total_days += ((rtc.month - 1) * 30) + rtc.day + 1;
    time_t total_secs =
        (total_days * 24 * 60 * 60) + (rtc.hour * 60 * 60) + (rtc.minute * 60) + rtc.second;

    return total_secs;
}

auto get_unix_time_from_now() -> time_t
{
    const rtc_time rtc { rtc_get_time() };
    return rtc_to_unix_time(rtc);
}
