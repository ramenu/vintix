/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/core/chrdev.h>
#include <vintix/kernel/core/dev.h>
#include <vintix/kernel/core/serial.h>
#include <vintix/kernel/filesystems/file.h>
#include <vintix/kernel/filesystems/fs.h>
#include <vintix/kernel/filesystems/inode.h>

static auto serial_write(file *, const void *buf, usize count) noexcept -> ssize
{
    auto s = static_cast<const char *>(buf);
    for (usize i = 0; i < count; ++i)
        serial_putc(s[i]);

    return static_cast<ssize>(count);
}

auto serial_init() noexcept -> errno
{
    outb(SERIAL_COM1_BASE + 1, 0x00); // Disable all interrupts
    outb(SERIAL_COM1_BASE + 3, 0x80); // Enable DLAB (set baud rate divisor)
    outb(SERIAL_COM1_BASE + 0, 0x03); // Set divisor to 3 (lo byte) 38400 baud
    outb(SERIAL_COM1_BASE + 1, 0x00); //                  (hi byte)
    outb(SERIAL_COM1_BASE + 3, 0x03); // 8 bits, no parity, one stop bit
    outb(SERIAL_COM1_BASE + 2, 0xC7); // Enable FIFO, clear them, with 14-byte threshold
    outb(SERIAL_COM1_BASE + 4, 0x0B); // IRQs enabled, RTS/DSR set
    outb(SERIAL_COM1_BASE + 4, 0x1E); // Set in loopback mode, test the serial chip
    outb(SERIAL_COM1_BASE + 0,
         0xAE); // Test serial chip (send byte 0xAE and check if serial returns same byte)

    // Check if serial is faulty (i.e: not same byte as sent)
    if (inb(SERIAL_COM1_BASE + 0) != 0xAE)
        return EINVAL;

    // If serial is not faulty set it in normal operation mode
    // (not-loopback with IRQs enabled and OUT#1 and OUT#2 bits enabled)
    outb(SERIAL_COM1_BASE + 4, 0x0F);

    static constexpr device_vtable dev_vtable {
        .blk_read = nullptr,
        .blk_write = nullptr,
        .blk_query = nullptr,
        .close = generic_device_close,
    };

    static constexpr file_vtable f_vtable {
        .lseek = nullptr,
        .read = nullptr,
        .write = serial_write,
        .put_blks = nullptr,
        .close = generic_file_close,
    };

    auto *dev = static_cast<chrdev *>(kzalloc(sizeof(chrdev)));

    if (dev == nullptr)
        return ENOMEM;

    dev->d_name = vintix::string { "serial" };

    if (dev->d_name == nullptr)
        return ENOMEM;

    dev->d_major = 5;
    dev->d_minor = 1;
    dev->d_type = device_type::CHRDEV;
    dev->d_perms = USRWRIT;
    dev->dev_vtable = &dev_vtable;
    dev->f_vtable = &f_vtable;
    return register_chrdev(dev);
}
