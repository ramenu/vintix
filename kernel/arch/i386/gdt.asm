; Copyright (C) 2023-2025 Abdur-Rahman Mansoor
;
; This file is part of Vintix.
; 
; Vintix is free software: you can redistribute it and/or modify it under the terms of the 
; GNU General Public License as published by the Free Software Foundation, either version 
; 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
; 
; Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
; See the GNU General Public License for more details.
; 
; You should have received a copy of the GNU General Public License along with Vintix. If 
; not, see <https://www.gnu.org/licenses/>.

; _load_gdt: loads the global description table
; stack: [esp + 4] pointer to gdt struct
global _load_gdt
_load_gdt:
    mov 	eax, dword [esp + 4]
    lgdt 	[eax]

    ; have to do a far jump to load 'cs'
    jmp 	0x08:.flush_cs

.flush_cs:
    mov 	ax, 0x10
    mov 	ds, ax
    mov 	es, ax
    mov 	fs, ax
    mov 	gs, ax
    mov 	ss, ax
    ret
