/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/core/io.h>
#include <vintix/kernel/core/kassert.h>
#include <vintix/kernel/core/panic.h>
#include <vintix/kernel/core/pci.h>
#include <vintix/kernel/core/vmm.h>
#include <vintix/kernel/lib/klog.h>
#include <vintix/kernel/lib/types.h>

// See https://wiki.osdev.org/PCI#Configuration_Space_Access_Mechanism_.231
enum {
    BUS_FUNCTION_NUMBER_BIT_OFFSET = 8,
    BUS_DEVICE_NUMBER_BIT_OFFSET = 11,
    BUS_NUMBER_BIT_OFFSET = 16,
    REGISTER_OFFSET_MASK = 0xFC,

    PCI_TOTAL_FUNCTIONS = 7,
    ENOPCI = 0,
    PCI_SUCCESS = 1,
    PCI_SLOTS_PER_BUS = 32,
    PCI_TOTAL_BUSES = 256,
    PCI_TOTAL_CLASSES = 21,

    // enable flag for determining when accesses to CONFIG_DATA should be
    // translated to configuration cycles.
    ENABLE_BIT = 1u << 31,

    // Two 32-bit I/O locations are used, the first location (0xCF8) is
    // nnamed `CONFIG_ADDRESS`, and the second (0xCFC) is called `CONFIG_DATA`.
    // `CONFIG_ADDRESS` specifies the configuration address that is required
    // to be accessed, while accesses to `CONFIG_DATA` will actually generate
    // the configuration access and will transfer the data to or from the
    // `CONFIG_DATA` register.
    CONFIG_ADDRESS = 0xCF8,
    CONFIG_DATA = 0xCFC,
};

static auto pci_device_class_string(pci_device_class clss,
                                    pci_device_subclass subclss) noexcept -> const char *
{
    using enum pci_device_subclass;

    // NOLINTBEGIN(cppcoreguidelines-avoid-goto)
    switch (clss) {
        case pci_device_class::UNCLASSIFIED_DEVICE: {
            switch (subclss) {
                default:
                    goto error;
                case NON_VGA_UNCLASSIFIED_DEVICE:
                    return "Non-VGA Unclassified Device";
                case VGA_COMPATIBLE_UNCLASSIFIED_DEVICE:
                    return "VGA Compatible Unclassified Device";
                case IMAGE_COPROCESSOR:
                    return "Image Co-processor";
            }
        }
        case pci_device_class::MASS_STORAGE_CONTROLLER: {
            switch (subclss) {
                default:
                    return "Generic Mass Storage Controller";
                case SCSI_STORAGE_CONTROLLER:
                    return "SCSI Storage Controller";
                case IDE_INTERFACE:
                    return "IDE Interface Controller";
                case FLOPPY_DISK_CONTROLLER:
                    return "Floppy Disk Controller";
                case IPI_BUS_CONTROLLER:
                    return "IPI Bus Controller";
                case RAID_BUS_CONTROLLER:
                    return "RAID Bus Controller";
                case ATA_CONTROLLER:
                    return "ATA Controller";
                case SATA_CONTROLLER:
                    return "SATA Controller";
                case SERIAL_ATTACHED_SCSI_CONTROLLER:
                    return "Serial Attached SCSI Controller";
                case NON_VOLATILE_MEMORY_CONTROLLER:
                    return "Non-Volatile Memory Controller";
                case UNIVERSAL_FLASH_STORAGE_CONTROLLER:
                    return "Universal Flash Storage Controller";
            }
        }
        case pci_device_class::NETWORK_CONTROLLER: {
            switch (subclss) {
                default:
                    return "Generic Network Controller";
                case ETHERNET_CONTROLLER:
                    return "Ethernet Controller";
                case TOKEN_RING_NETWORK_CONTROLLER:
                    return "Token Ring Network Controller";
                case FDDI_NETWORK_CONTROLLER:
                    return "FDDI Network Controller";
                case ATM_NETWORK_CONTROLLER:
                    return "ATM Network Controller";
                case ISDN_CONTROLLER:
                    return "ISDN Controller";
                case WORLDFIP_CONTROLLER:
                    return "WorldFip Controller";
                case PICMG_CONTROLLER:
                    return "PICMG Controller";
                case INFINIBAND_CONTROLLER:
                    return "Infiniband Controller";
                case FABRIC_CONTROLLER:
                    return "Fabric Controller";
            }
        }
        case pci_device_class::DISPLAY_CONTROLLER: {
            switch (subclss) {
                default:
                    return "Generic Display Controller";
                case VGA_COMPATIBLE_CONTROLLER:
                    return "VGA-Compatible Controller";
                case XGA_COMPATIBLE_CONTROLLER:
                    return "XGA-Compatible Controller";
                case _3D_CONTROLLER:
                    return "3D Controller";
            }
        }
        case pci_device_class::MULTIMEDIA_CONTROLLER: {
            switch (subclss) {
                default:
                    return "Generic Multimedia Controller";
                case MULTIMEDIA_VIDEO_CONTROLLER:
                    return "Multimedia Video Controller";
                case MULTIMEDIA_AUDIO_CONTROLLER:
                    return "Multimedia Audio Controller";
                case COMPUTER_TELEPHONY_DEVICE:
                    return "Computer Telephony Device";
                case AUDIO_DEVICE:
                    return "Audio Device";
            }
        }
        case pci_device_class::MEMORY_CONTROLLER: {
            switch (subclss) {
                default:
                    return "Generic Memory Controller";
                case RAM_MEMORY:
                    return "RAM Controller";
                case FLASH_MEMORY:
                    return "Flash Memory Controller";
                case CXL:
                    return "Compute Express Link Controller";
            }
        }
        case pci_device_class::INPUT_DEVICE_CONTROLLER: {
            switch (subclss) {
                default:
                    return "Generic Input Device Controller";
                case KEYBOARD_CONTROLLER:
                    return "Keyboard Controller";
                case DIGITIZER_PEN:
                    return "Digitizer Pen Controller";
                case MOUSE_CONTROLLER:
                    return "Mouse Controller";
                case SCANNER_CONTROLLER:
                    return "Scanner Controller";
                case GAMEPORT_CONTROLLER:
                    return "Gameport Controller";
            }
        }
        case pci_device_class::BRIDGE: {
            switch (subclss) {
                default:
                    return "Generic Bridge Controller";
                case ISA_BRIDGE:
                    return "ISA Bridge Controller";
                case EISA_BRIDGE:
                    return "EISA Bridge Controller";
                case MICROCHANNEL_BRIDGE:
                    return "Microchannel Bridge Controller";
                case PCI_BRIDGE:
                    return "PCI Bridge Controller";
                case PCMCIA_BRIDGE:
                    return "PCMCIA Bridge Controller";
                case NUBUS_BRIDGE:
                    return "NuBus Bridge Controller";
                case CARDBUS_BRIDGE:
                    return "Cardbus Bridge Controller";
                case RACEWAY_BRIDGE:
                    return "Raceway Bridge Controller";
                case SEMI_TRANSPARENT_PCI_TO_PCI_BRIDGE:
                    return "Semi Transparent PCI-to-PCI Bridge Controller";
                case INFINIBAND_TO_PCI_HOST_BRIDGE:
                    return "Infiniband PCI-to-PCI Bridge Controller";
            }
        }
        case pci_device_class::COMMUNICATION_CONTROLLER: {
            switch (subclss) {
                default:
                    return "Generic Communication Controller";
                case SERIAL_CONTROLLER:
                    return "Serial Controller";
                case PARALLEL_CONTROLLER:
                    return "Parallel Controller";
                case MULTIPORT_SERIAL_CONTROLLER:
                    return "Multiport Serial Controller";
                case MODEM:
                    return "Modem Controller";
                case GPIB_CONTROLLER:
                    return "General Purpose Interface Bus Controller";
                case SMART_CARD_CONTROLLER:
                    return "Smart Card Controller";
            }
        }
        case pci_device_class::GENERIC_SYSTEM_PERIPHERAL: {
            switch (subclss) {
                default:
                    return "Generic System Peripheral";
                case PIC:
                    return "Programmable Interrupt Controller";
                case DMA_CONTROLLER:
                    return "Direct Memory Access Controller";
                case TIMER:
                    return "Timer";
                case RTC:
                    return "Real Time Clock";
                case PCI_HOT_PLUG_CONTROLLER:
                    return "PCI Hot Plug Controller";
                case SD_HOST_CONTROLLER:
                    return "SD Host Controller";
                case IOMMU:
                    return "I/O Memory Management Unit";
                case TIMING_CARD:
                    return "Timing Card";
            }
        }
        case pci_device_class::DOCKING_STATION: {
            return "Generic Docking Station";
        }
        case pci_device_class::PROCESSOR: {
            switch (subclss) {
                default:
                    goto error;
                case I386:
                    return "Intel i386";
                case I486:
                    return "Intel i486";
                case PENTIUM:
                    return "Intel Pentium";
                case ALPHA:
                    return "Alpha";
                case POWERPC:
                    return "Power PC";
                case MIPS:
                    return "MIPS";
                case CO_PROCESSOR:
                    return "Co-processor";
            }
        }
        case pci_device_class::SERIAL_BUS_CONTROLLER: {
            switch (subclss) {
                default:
                    return "Generic Serial Bus Controller";
                case FIREWIRE_IEEE_1394:
                    return "Firewire IEEE 1394 Controller";
                case ACCESS_BUS:
                    return "Access Bus Controller";
                case SSA:
                    return "Serial Storage Architecture";
                case USB_CONTROLLER:
                    return "USB Controller";
                case FIBRE_CHANNEL:
                    return "Fibre Channel";
                case SMBUS:
                    return "System Management Bus";
                case INFINIBAND:
                    return "Infiniband Serial Bus Controller";
                case IPMI_INTERFACE:
                    return "IPMI Interface";
                case SERCOS_INTERFACE:
                    return "SERCOS Interface";
                case CANBUS:
                    return "CANBUS";
            }
        }
        case pci_device_class::WIRELESS_CONTROLLER: {
            switch (subclss) {
                default:
                    return "Generic Wireless Controller";
                case IRDA_CONTROLLER:
                    return "IRDA Controller";
                case CONSUMER_IR_CONTROLLER:
                    return "Consumer IR Controller";
                case RF_CONTROLLER:
                    return "RF Controller";
                case BLUETOOTH:
                    return "Bluetooth Controller";
                case BROADBAND:
                    return "Broadband Controller";
                case _802_1A_CONTROLLER:
                    return "802.1a Controller";
                case _802_1B_CONTROLLER:
                    return "802.1b Controller";
            }
        }
        case pci_device_class::INTELLIGENT_CONTROLLER: {
            return "Intelligent Controller";
        }
        case pci_device_class::SATELITE_COMMUNICATIONS_CONTROLLER: {
            switch (subclss) {
                default:
                    goto error;
                case SATELLITE_TV_CONTROLLER:
                    return "Satellite TV Controller";
                case SATELLITE_AUDIO_COMMUNICATION_CONTROLLER:
                    return "Satellite Audio Communication Controller";
                case SATELLITE_VOICE_COMMUNICATION_CONTROLLER:
                    return "Satellite Voice Communication Controller";
                case SATELLITE_DATA_COMMUNICATION_CONTROLLER:
                    return "Satellite Data Communication Controller";
            }
        }
        case pci_device_class::ENCRYPTION_CONTROLLER: {
            switch (subclss) {
                default:
                    return "Generic Encryption Controller";
                case NETWORK_AND_COMPUTING_ENCRYPTION_DEVICE:
                    return "Network and Computing Encryption Device";
                case ENTERTAINMENT_ENCRYPTION_DEVICE:
                    return "Entertainment Encryption Device";
            }
        }
        case pci_device_class::SIGNAL_PROCESSING_CONTROLLER: {
            switch (subclss) {
                default:
                    return "Generic Signal Processing Controller";
                case DPIO_MODULE:
                    return "DPIO Module Controller";
                case PERFORMANCE_COUNTERS:
                    return "Performance Counters Controller";
                case COMMUNICATION_SYNCHRONIZER:
                    return "Communication Synchronizer Controller";
                case SIGNAL_PROCESSING_MANAGEMENT:
                    return "Signal Processing Management Controller";
            }
        }
        case pci_device_class::PROCESSING_ACCELERATORS: {
            switch (subclss) {
                default:
                    goto error;
                case PROCESSING_ACCELERATORS:
                    return "Processing Accelerator Controller";
                case SNIA_SMART_DATA_ACCELERATOR_INTERFACE_CONTROLLER:
                    return "SNIA Smart Data Accelerator Interface Controller";
            }
        }
        case pci_device_class::NON_ESSENTIAL_INSTRUMENTATION: {
            return "Non Essential Instrumentation Device";
        }
        case pci_device_class::COPROCESSOR: {
            return "Co-processor";
        }
        case pci_device_class::UNASSIGNED_CLASS: {
            return "Unassigned Device Class";
        }
    }
    // NOLINTEND(cppcoreguidelines-avoid-goto)
error:
    kpanic("(PCI) unrecognized device\n    class code: %x\n    subclass code: %x\n", clss, subclss);
}

static auto pci_chk_function(u8 bus, u8 slot, u8 function) noexcept -> int;
static void pci_chk_bus(u8 bus) noexcept;
static void pci_chk_device(u8 bus, u8 slot) noexcept;
static auto pci_total_devs() noexcept -> usize;
static void pci_handle_general_device(u8 bus, u8 slot, u8 function, pci_device_class clss,
                                      pci_device_subclass subclss) noexcept;
static auto pci_get_secondary_bus(u8 bus, u8 slot, u8 function) noexcept -> u8;
static auto pci_total_devs() noexcept -> usize;

// NOTE: `offset` must be in bytes!
static constexpr auto cfg_address(u32 lbus, u32 lslot, u32 lfunc, u8 offset) noexcept -> u32
{
    return ((lbus << BUS_NUMBER_BIT_OFFSET) | (lslot << BUS_DEVICE_NUMBER_BIT_OFFSET) |
            (lfunc << BUS_FUNCTION_NUMBER_BIT_OFFSET) | (offset & REGISTER_OFFSET_MASK) |
            (ENABLE_BIT));
}

static constexpr auto reg_32b_frst_word(u8 offset) noexcept -> u32
{
    // ((offset & 2) * 8) = 0 will choose the first word of the 32-bit register
    return (((offset & 2) * 8) & 0xFFFF);
}

static void pci_chk_bus(u8 bus) noexcept
{
    for (u8 slot = 0; slot < PCI_SLOTS_PER_BUS; ++slot)
        pci_chk_device(bus, slot);
}

auto pci_cfg_read_word(u8 bus, u8 slot, u8 func, u8 offset) noexcept -> u16
{
    const u32 lbus = bus;
    const u32 lslot = slot;
    const u32 lfunc = func;

    const u32 address = cfg_address(lbus, lslot, lfunc, offset);

    outl(CONFIG_ADDRESS, address);
    u16 in = static_cast<u16>(inl(CONFIG_DATA) >> reg_32b_frst_word(offset));

    return in;
}

auto pci_cfg_read_dword(u8 bus, u8 slot, u8 func, u8 offset) noexcept -> u32
{
    const u32 lbus = bus;
    const u32 lslot = slot;
    const u32 lfunc = func;

    const u32 address = cfg_address(lbus, lslot, lfunc, offset);
    outl(CONFIG_ADDRESS, address);

    return inl(CONFIG_DATA);
}

void pci_cfg_write_word(u8 bus, u8 slot, u8 func, u8 offset, u16 data) noexcept
{
    const u32 lbus = bus;
    const u32 lslot = slot;
    const u32 lfunc = func;

    const u32 address = cfg_address(lbus, lslot, lfunc, offset);

    outl(CONFIG_ADDRESS, address);
    u32 in = inl(CONFIG_DATA);
    in &= ~(0xFFFFu << reg_32b_frst_word(offset));
    in |= static_cast<u32>(data << reg_32b_frst_word(offset));
    outl(CONFIG_DATA, in);
}

static void pci_chk_device(u8 bus, u8 slot) noexcept
{
    u8 function = 0;
    const int status = pci_chk_function(bus, slot, function);

    if (status != ENOPCI && pci_is_multifunction_device(bus, slot)) {
        for (function = 1; function < PCI_TOTAL_FUNCTIONS; ++function) {
            if (pci_chk_function(bus, slot, function) == ENOPCI)
                break;
        }
    }
}

static auto pci_chk_function(u8 bus, u8 slot, u8 function) noexcept -> int
{
    const pci_vendor_id vendor = pci_chk_vendor(bus, slot, function);

    if (vendor == pci_vendor_id::PCI_NO_DEVICE)
        return ENOPCI;

    const pci_device_class clss = pci_chk_device_class(bus, slot, function);
    const pci_device_subclass subclss = pci_chk_device_subclass(bus, slot, function);

    if (clss == pci_device_class::BRIDGE && subclss == pci_device_subclass::PCI_BRIDGE) {
        const u8 secondary_bus = pci_get_secondary_bus(bus, slot, function);
        pci_chk_bus(secondary_bus);
    }

    return PCI_SUCCESS;
}

static void pci_handle_general_device(u8 bus, u8 slot, u8 function) noexcept
{
    KASSERT(pci_chk_header(bus, slot, function) == pci_header_type::GENERAL_DEVICE);
}

auto get_pci_devs() noexcept -> pcidev_list
{
    pcidev_list pcidevs {};
    const usize total_devs = pci_total_devs();

    if (total_devs == 0)
        return pcidevs;

    pcidevs.dev = static_cast<pcidev *>(kcalloc(total_devs, sizeof(pcidev)));

    if (pcidevs.dev == nullptr)
        return pcidevs;

    pcidevs.sz = total_devs;

    if (!pci_is_multifunction_device(0, 0)) {
        usize i = 0;
        for (u8 device = 0; device < PCI_SLOTS_PER_BUS; ++device) {
            for (u8 fn = 0; fn < PCI_TOTAL_FUNCTIONS; ++fn) {
                if (pci_chk_vendor(0, device, fn) == pci_vendor_id::PCI_NO_DEVICE)
                    break;

                pcidevs.dev[i].bus = 0;
                pcidevs.dev[i].device = device;
                pcidevs.dev[i].fn = fn;
                pcidevs.dev[i].clss = pci_chk_device_class(0, device, fn);
                pcidevs.dev[i].subclss = pci_chk_device_subclass(0, device, fn);
                pcidevs.dev[i].prog_if = pci_chk_prog_if(0, device, fn);
                pcidevs.dev[i].int_line = pci_chk_int_line(0, device, fn);

                DEBUG("get_pcidevs: PCI(%02d.%02d.%02d): \"%s\"\n", 0, device, fn,
                      pci_device_class_string(pcidevs.dev[i].clss, pcidevs.dev[i].subclss));
                ++i;
                KASSERT(i <= total_devs);
            }
        }
    } else {
        // TODO: Fill out
    }

    return pcidevs;
}

static auto pci_total_devs() noexcept -> usize
{
    usize total_devs = 0;

    if (!pci_is_multifunction_device(0, 0)) {
        for (u8 device = 0; device < PCI_SLOTS_PER_BUS; ++device) {
            for (u8 fn = 0; fn < PCI_TOTAL_FUNCTIONS; ++fn) {
                if (pci_chk_vendor(0, device, fn) == pci_vendor_id::PCI_NO_DEVICE)
                    break;

                ++total_devs;
            }
        }
    } else {
        // TODO: Fill out
    }

    return total_devs;
}

static auto pci_get_secondary_bus(u8, u8, u8) noexcept -> u8
{
    // TODO: Implement
    return 0;
}
