/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/bios/bios.h>
#include <vintix/kernel/core/handler.h>
#include <vintix/kernel/core/pmm.h>
#include <vintix/kernel/core/process.h>
#include <vintix/kernel/core/vmm.h>
#include <vintix/kernel/filesystems/dentry.h>
#include <vintix/kernel/filesystems/file.h>
#include <vintix/kernel/filesystems/fs.h>
#include <vintix/kernel/filesystems/mount.h>
#include <vintix/kernel/filesystems/path.h>
#include <vintix/kernel/lib/overflow.h>
#include <vintix/kernel/lib/types.h>
#include <vintix/kernel/posix/fcntl.h>
#include <vintix/kernel/posix/signal.h>
#include <vintix/kernel/sys/syscall.h>

static auto in_real_mode_region(u32 p) noexcept -> bool
{
    return (p < REAL_MODE_ADDRESS_SPACE_END);
}

static auto userspace_ptr_is_ok(const void *p) noexcept -> bool
{
    return (addressof(p) > REAL_MODE_ADDRESS_SPACE_END || addressof(p) < VADDR_OFFSET);
}

static void ensure_userspace_ptr_is_ok(const void *p) noexcept
{
    if (!userspace_ptr_is_ok(p))
        return general_protection_fault_handler();
}
static void ensure_userspace_ptr_is_ok(const void *p, usize sz) noexcept
{
    usize res;
    if (chk_add_overflow(addressof(p), sz, &res) || !userspace_ptr_is_ok(p) ||
        !userspace_ptr_is_ok(unsafe_toptr<const void *>(res)))
        return general_protection_fault_handler();
}

static void ensure_userspace_str_is_ok(const char *s) noexcept
{
    return ensure_userspace_ptr_is_ok(s, strlen(s) + 1);
}

auto sys_open(const char *pathname, int flags, mode_t mode) noexcept -> int
{
    ensure_userspace_str_is_ok(pathname);

    if (flags > O_ACCMODE)
        return -EPERM;

    file *fp = UNWRAP_PTR(kopen(pathname, flags, mode));
    // return process::add_file(fp);
    return ENOERR;
}

auto sys_creat(const char *pathname, mode_t mode) noexcept -> int
{
    return sys_open(pathname, O_CREAT | O_WRONLY | O_TRUNC, mode);
}

auto sys_mkdir(const char *pathname, mode_t mode) noexcept -> int
{
    return sys_open(pathname, O_CREAT | O_WRONLY | O_EXCL | O_DIRECTORY, mode);
}

auto sys_read(int fd, void *buf, size_t count) noexcept -> ssize_t
{
    ensure_userspace_ptr_is_ok(buf, count);
    errptr<file> fp = process::get_file(fd);

    if (fp.is_err())
        return -fp.err();

    ssize_t res = kread(fp.get(), buf, count);
    return res;
}

auto sys_write(int fd, const void *buf, size_t count) noexcept -> ssize_t
{
    ensure_userspace_ptr_is_ok(buf, count);
    errptr<file> fp = process::get_file(fd);

    if (fp.is_err())
        return -fp.err();

    ssize_t res = kwrite(fp.get(), buf, count);
    return res;
}

void sys_exit(int status) noexcept
{
    process::running()->set_exit_status(status);
    process::kill(process::running()->pid(), SIGTERM);
    process::schedule_new_process(nullptr);
}

auto sys_mount(const char *source, const char *target, const char *fs_type,
               int mount_flags) noexcept -> int
{
    ensure_userspace_str_is_ok(source);
    ensure_userspace_str_is_ok(target);
    ensure_userspace_str_is_ok(fs_type);
    errno res = kmount(source, target, fs_type, mount_flags);
    return res;
}

auto sys_umount(const char *target) noexcept -> int
{
    ensure_userspace_str_is_ok(target);
    errno res = kumount(target);
    return res;
}

auto sys_close(int fd) noexcept -> int
{
    errptr<file> fp = process::get_file(fd);

    if (fp.is_err())
        return -fp.err();

    errno res = kclose(fp.get());
    return res;
}

auto sys_mmap(void *addr, usize len, int prot, int flags, fd_t fd, off_t off) noexcept -> void *
{
    ensure_userspace_ptr_is_ok(addr, len);

    /*
    int pg_flags = PTE_US_FLAG; // PROT_NONE

    if (prot & PROT_READ)
        pg_flags |= PTE_IS_PRESENT_FLAG;
    if (prot & PROT_WRITE)
        pg_flags |= PTE_RW_FLAG;

    if (process::add_segment(addr, len, static_cast<pte_flags>(pg_flags)) != ENOERR)
        return MAP_FAILED;
    */

    return addr;
}

auto sys_getuid() noexcept -> uid_t { return process::running()->uid(); }
auto sys_getgid() noexcept -> gid_t { return process::running()->gid(); }
auto sys_getpid() noexcept -> pid_t { return process::running()->pid(); }

auto sys_lseek(fd_t fd, off_t off, int whence) noexcept -> off_t
{
    errptr<file> fp = process::get_file(fd);

    if (fp.is_err())
        return -fp.err();

    off_t new_off = klseek(fp.get(), off, whence);
    return new_off;
}

auto sys_chdir(const char *p) noexcept -> int
{
    ensure_userspace_str_is_ok(p);

    vintix::string path { p };

    if (path == nullptr)
        return ENOMEM;

    path_normalize(path);
    if (auto err = path_chk_valid(vintix::string_view { path }))
        return err;

    errptr<dentry> dent = get_dentry(vintix::string_view { path }, DENTRY_CHECK_PERMS);

    if (dent.is_err())
        return dent.err();

    if (!dent->d_ino->is_dir())
        return ENOTDIR;

    process::running()->chdir(dent.get());
    return ENOERR;
}

auto sys_chroot(const char *p) noexcept -> int
{
    ensure_userspace_str_is_ok(p);

    if (process::running()->uid() != ROOT_UID)
        return EPERM;

    vintix::string path { p };

    if (path == nullptr)
        return ENOMEM;

    path_normalize(path);
    if (auto err = path_chk_valid(vintix::string_view { path }))
        return err;

    errptr<dentry> dent = get_dentry(vintix::string_view { path }, DENTRY_CHECK_PERMS);

    if (dent.is_err())
        return dent.err();

    if (!dent->d_ino->is_dir())
        return ENOTDIR;

    process::running()->chroot(dent.get());
    return ENOERR;
}

auto sys_sched_yield(const syscall_regs &state) noexcept -> int
{
    process::schedule_new_process(&state);
    return ENOERR;
}

auto sys_umask(mode_t mask) noexcept -> mode_t
{
    process::running()->umask(mask);
    return mask;
}

auto sys_munmap(void *addr, usize len) noexcept -> int
{
    ensure_userspace_ptr_is_ok(addr, len);
    const v_addr vaddr = addressof(addr);
    for (usize i = 0; i < len / PAGE_FRAME_SIZE + 1; ++i)
        if (address_is_mapped(vaddr + i * PAGE_FRAME_SIZE))
            free_page(get_page(vaddr + i * PAGE_FRAME_SIZE));

    // process::remove_segment(addr, len);
    return ENOERR;
}

auto sys_kill(pid_t pid, int sig) noexcept -> int
{
    if (sig < 0 || sig > _MAX_SIGNAL_NO)
        return EINVAL;

    if (!process::exists(pid))
        return ESRCH;

    // fix later
    if (process::running()->uid() != ROOT_UID)
        return EPERM;

    process::kill(pid, sig);
    return ENOERR;
}

auto sys_wait(int *status) noexcept -> pid_t { return sys_waitpid(-1, status, 0); }

auto sys_waitpid(pid_t pid, int *status, int options) noexcept -> pid_t
{
    ensure_userspace_ptr_is_ok(status);
    // TODO: Work on this later
    return pid;
}

auto sys_stat(const char *__restrict__ pathname, stat *__restrict__ buf) noexcept -> int
{
    ensure_userspace_str_is_ok(pathname);
    ensure_userspace_ptr_is_ok(buf, sizeof(stat));
    vintix::noleak::file fp { UNWRAP_PTR(kopen(pathname, O_RDONLY)) };

    if (!fp->is_readable())
        return EACCES;

    kstat(fp.unsafe_mutable_data(), buf);
    return ENOERR;
}

auto sys_fstat(int fd, stat *buf) noexcept -> int
{
    ensure_userspace_ptr_is_ok(buf, sizeof(stat));
    file *fp = UNWRAP_PTR(process::get_file(fd));
    kstat(fp, buf);
    return ENOERR;
}

auto sys_lstat(const char *pathname, stat *__restrict__ buf) noexcept -> int
{
    ensure_userspace_str_is_ok(pathname);
    ensure_userspace_ptr_is_ok(buf, sizeof(stat));
    // TODO: Implement
    return ENOERR;
}

auto sys_chmod(const char *path, mode_t mode) noexcept -> int
{
    ensure_userspace_str_is_ok(path);
    vintix::noleak::file fp { UNWRAP_PTR(kopen(path, O_RDWR)) };

    if (!fp->is_writable())
        return EPERM;

    fp->f_dentry->d_ino->i_mode = mode;

    if (auto err = ksync(fp.unsafe_mutable_data()))
        return err;

    // TODO: Implement
    return ENOERR;
}

auto sys_fchmod(int fd, mode_t mode) noexcept -> int
{
    errptr<file> fp = process::get_file(fd);

    if (fp.is_err())
        return fp.err();

    if (!fp->is_writable())
        return EPERM;

    fp->f_dentry->d_ino->i_mode = mode;

    if (auto err = ksync(fp.get()))
        return err;

    return ENOERR;
}

auto sys_chown(const char *path, uid_t uid, gid_t gid) noexcept -> int
{
    ensure_userspace_str_is_ok(path);

    vintix::noleak::file fp { UNWRAP_PTR(kopen(path, O_RDWR)) };

    if (!fp->is_writable())
        return EPERM;

    if (process::running()->uid() != ROOT_UID)
        return EPERM;

    fp->f_dentry->d_ino->i_uid = uid;
    fp->f_dentry->d_ino->i_gid = gid;

    if (auto err = ksync(fp.unsafe_mutable_data()))
        return err;

    return ENOERR;
}

auto sys_lchown(const char *path, uid_t uid, gid_t gid) noexcept -> int
{
    ensure_userspace_str_is_ok(path);
    // TODO: Implement

    return ENOERR;
}

auto sys_fchown(int fd, uid_t uid, gid_t gid) noexcept -> int
{
    file *fp = UNWRAP_PTR(process::get_file(fd));

    if (!fp->is_writable())
        return EPERM;

    if (process::running()->uid() != ROOT_UID)
        return EPERM;

    fp->f_dentry->d_ino->i_uid = uid;
    fp->f_dentry->d_ino->i_gid = gid;

    if (auto err = ksync(fp))
        return err;

    return ENOERR;
}

auto sys_mprotect(void *addr, usize len, int prot) noexcept -> int
{
    ensure_userspace_ptr_is_ok(addr, len);
    const v_addr vaddr = addressof(addr);

    if (vaddr % PAGE_FRAME_SIZE != 0)
        return EINVAL;

    /*
    u32 pg_flags = PTE_NONE_FLAG;
    if (prot & PROT_READ)
        pg_flags |= PTE_IS_PRESENT_FLAG;
    if (prot & PROT_WRITE)
        pg_flags |= PTE_RW_FLAG;

    for (usize i = 0; i < process::segments().size(); ++i) {
        segment_map_info *seg = &process::running()->p_segments[i];
        if (vaddr >= seg->vaddr && vaddr + len <= seg->vaddr + seg->memsz)
            seg->pg_flags = pg_flags;
    }*/

    return ENOERR;
}

extern "C" void syscall_handler(const syscall_regs regs) noexcept
{
    const u32 syscall_num = regs.eax().get<u32>();
    DEBUG("syscall_handler: process %d performing system call #%ul\n", process::running()->pid(),
          syscall_num);

    switch (syscall_num) {
        default: {
            process::terminate(process::running()->pid(), SIGSYS);
            return;
        }
        case 1:
            sys_open(regs.ebx().as_ptr<const char *>(), regs.ecx().get<int>(),
                     regs.edx().get<mode_t>());
            break;
        case 2:
            sys_creat(regs.ebx().as_ptr<const char *>(), regs.ecx().get<mode_t>());
            break;
        case 3:
            sys_mkdir(regs.ebx().as_ptr<const char *>(), regs.ecx().get<mode_t>());
            break;
        case 4:
            sys_read(regs.ebx().get<int>(), regs.ecx().as_ptr<void *>(), regs.edx().get());
            break;
        case 5:
            sys_exit(regs.ebx().get<int>());
            break;
        case 6:
            sys_write(regs.ebx().get<int>(), regs.ecx().as_ptr<const void *>(), regs.edx().get());
            break;
        case 7:
            sys_mount(regs.ebx().as_ptr<const char *>(), regs.ecx().as_ptr<const char *>(),
                      regs.edx().as_ptr<const char *>(), regs.esi().get<int>());
            break;
        case 8:
            sys_umount(regs.ebx().as_ptr<const char *>());
            break;
        case 9:
            sys_close(regs.ebx().get<int>());
            break;
        case 10:
            sys_mmap(regs.ebx().as_ptr<void *>(), regs.ecx().get<usize>(), regs.edx().get<int>(),
                     regs.esi().get<int>(), regs.edi().get<int>(), regs.ebp().get<int>());
            break;
        case 11:
            sys_getuid();
            break;
        case 12:
            sys_getgid();
            break;
        case 13:
            sys_getpid();
            break;
        case 14:
            sys_lseek(regs.ebx().get<int>(), regs.ecx().get<off_t>(), regs.edx().get<int>());
            break;
        case 15:
            sys_chdir(regs.ebx().as_ptr<const char *>());
            break;
        case 16:
            sys_chroot(regs.ebx().as_ptr<const char *>());
            break;
        case 17:
            sys_sched_yield(regs);
            break;
        case 18:
            sys_umask(regs.ebx().get<mode_t>());
            break;
        case 19:
            sys_munmap(regs.ebx().as_ptr<void *>(), regs.ecx().get<usize>());
            break;
        case 20:
            sys_kill(regs.ebx().get<pid_t>(), regs.ecx().get<int>());
            break;
        case 21:
            sys_wait(regs.ebx().as_ptr<int *>());
            break;
        case 22:
            sys_waitpid(regs.ebx().get<pid_t>(), regs.ecx().as_ptr<int *>(), regs.edx().get<int>());
            break;
        case 23:
            sys_stat(regs.ebx().as_ptr<const char *>(), regs.ecx().as_ptr<stat *>());
            break;
        case 24:
            sys_lstat(regs.ebx().as_ptr<const char *>(), regs.ecx().as_ptr<stat *>());
            break;
        case 25:
            sys_fstat(regs.ebx().get<int>(), regs.ecx().as_ptr<stat *>());
            break;
        case 26:
            sys_chmod(regs.ebx().as_ptr<const char *>(), regs.ecx().get<mode_t>());
            break;
        case 27:
            sys_fchmod(regs.ebx().get<int>(), regs.ecx().get<mode_t>());
            break;
        case 28:
            sys_chown(regs.ebx().as_ptr<const char *>(), regs.ecx().get<uid_t>(),
                      regs.edx().get<gid_t>());
            break;
        case 29:
            sys_lchown(regs.ebx().as_ptr<const char *>(), regs.ecx().get<uid_t>(),
                       regs.edx().get<gid_t>());
            break;
        case 30:
            sys_fchown(regs.ebx().get<int>(), regs.ecx().get<uid_t>(), regs.edx().get<gid_t>());
            break;
        case 31:
            sys_mprotect(regs.ebx().as_ptr<void *>(), regs.ecx().get<usize>(),
                         regs.edx().get<int>());
            break;
    }
}
