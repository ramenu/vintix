/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/asm/inst.h>
#include <vintix/kernel/core/elf.h>
#include <vintix/kernel/core/kassert.h>
#include <vintix/kernel/core/oom.h>
#include <vintix/kernel/core/panic.h>
#include <vintix/kernel/core/pmm.h>
#include <vintix/kernel/core/process.h>
#include <vintix/kernel/core/vmm.h>
#include <vintix/kernel/filesystems/dentry.h>
#include <vintix/kernel/filesystems/fd.h>
#include <vintix/kernel/filesystems/file.h>
#include <vintix/kernel/filesystems/path.h>
#include <vintix/kernel/lib/errno.h>
#include <vintix/kernel/lib/klog.h>
#include <vintix/kernel/lib/limits.h>
#include <vintix/kernel/lib/string_view.h>
#include <vintix/kernel/lib/types.h>
#include <vintix/kernel/lib/vector.h>
#include <vintix/kernel/posix/signal.h>
#include <vintix/kernel/sys/syscall.h>

#define USER_STACK_BEGIN (VADDR_OFFSET)
static constexpr pg_flags STACK_PG_FLAGS = PG_P | PG_R | PG_W | PG_USER;

auto process::get_pid_index(pid_t pid) noexcept -> i32
{
    KASSERT(procs.size() <= static_cast<usize>(limit_of<i32>()));

    for (i32 i = 0; i < static_cast<i32>(procs.size()); ++i)
        if (procs[static_cast<usize>(i)]->pid() == pid)
            return i;

    return -1;
}

void process::save_state(const syscall_regs &regs) noexcept
{
#ifdef __i386__
    p_eax = regs.eax().get();
    p_ebx = regs.ebx().get();
    p_ecx = regs.ecx().get();
    p_edx = regs.edx().get();
    p_ebp = regs.ebp().get();
    p_esp = regs.esp().get();
    p_esi = regs.esi().get();
    p_edi = regs.edi().get();
    p_eip = regs.eip().get();
#else
#error unsupported architecture
#endif
}

auto process::kill(pid_t pid, int sig) noexcept -> errno
{
    DEBUG("process::kill: Killing process %d (caught signal \"%s\")\n", pid, signumtostr(sig));

    if (pid < 0 || static_cast<usize>(pid) >= procs.size())
        return EINVAL;

    process *proc = procs.at(static_cast<usize>(pid));

    proc->p_files.~vector();
    proc->p_name.~string();
    proc->p_argv.~vector();
    proc->p_envp.~vector();
    proc->p_map.~vm_map();

    // PID 0 uses our statically allocated page directory, so we cannot de-allocate that
    if (proc->p_pid != 0) {
        if (proc != p_active_process)
            kfree(proc->p_pgdir);
    }

    const i32 index = process::get_pid_index(pid);
    KASSERT(index != -1);
    procs.erase(static_cast<usize>(index));

    return ENOERR;
}

void process::schedule_new_process(const syscall_regs *regs) noexcept
{
    if (regs != nullptr)
        p_active_process->save_state(*regs);

    // no processes to run
    if (process::number_of_processes() == 0) {
        DEBUG("process::schedule_new_process: Nothing to run, halting indefinitely...\n");
        halt();
    }
}

auto kprocess::load_module(const char *path) noexcept -> errno
{
    vintix::noleak::file fp { UNWRAP_PTR(kopen(path, O_RDWR | O_EXEC | O_KERNEL)) };
    void *data = kzalloc(fp->size());

    if (data == nullptr)
        return ENOMEM;

    if (auto count = kread(fp.unsafe_mutable_data(), data, fp->size()) < 0) {
        kfree(data);
        return -count;
    }

    result<gen_kernel_process> gen { elf_load_kmodule(fp->name().c_str(), data) };

    if (gen.is_err()) {
        kfree(data);
        return gen.err();
    }

    errno err = kprocess::load_module(fp->name().c_str(), vintix::move(gen.get()));
    return err;
}

auto kprocess::load_module(const char *name, gen_kernel_process &&gen) noexcept -> errno
{
    DEBUG("load_module: Loading driver \"%s\"\n", name);
    kprocess process {};
    if (auto err = process.k_name.copy(name)) [[unlikely]]
        return err;

    process._drv_entry = gen._drv_entry;
    process.k_destructor = gen.destructor;
    process.k_data = gen.data;
    process.k_bss = gen.bss;

    KASSERT(process._drv_entry != nullptr);
    if (auto err = modules.push_back(vintix::move(process)))
        return err;

    return modules.back()._drv_entry();
}

void kprocess::unload_module(const char *name) noexcept
{
    DEBUG("unload_module: Unloading module \"%s\"\n", name);
    for (usize i = 0; i < modules.size(); ++i) {
        if (modules[i].k_name != name)
            continue;

        if (modules[i].k_destructor != nullptr)
            modules[i].k_destructor();

        kfree(modules[i].k_data);
        kfree(modules[i].k_bss);
        modules.erase(i);
        break;
    }
}

auto process::memory_usage() const noexcept -> usize
{
    usize mem_usage = 0;

    mem_usage += p_stack.memory_usage();
    mem_usage += p_map.memory_usage();

    return mem_usage;
}

auto process::load(const char *path) noexcept -> errno
{
    DEBUG("process::load: Attempting to load process \"%s\"\n", path);

    vintix::unique_ptr<u8> binary { kzalloc(PAGE_FRAME_SIZE) };

    if (binary == nullptr)
        return ENOMEM;

    vintix::noleak::file fp { UNWRAP_PTR(kopen(path, O_RDONLY | O_EXEC)) };
    if (ssize count =
            kread(fp.unsafe_mutable_data(), binary.unsafe_mutable_data(), PAGE_FRAME_SIZE);
        count < 0)
        return -count;

    result<vm_map> mapping { elf_load(path, binary.unsafe_mutable_data()) };

    if (mapping.is_err())
        return mapping.err();

    if (auto err = p_name.copy(path_last_entry(vintix::string_view { path }))) [[unlikely]]
        return err;

    p_pid = static_cast<pid_t>(procs.size());
    p_uid = fp->uid();
    p_gid = fp->gid();
    p_root = get_dentry_root();
    p_workdir = p_root;
    p_parent = nullptr;
    p_map = mapping.unwrap();
    p_eip = p_map.entrypoint();

#ifndef NDEBUG
    DEBUG("process::load: Displaying mappings of \"%s\"\n", p_name.c_str());
    for (usize i = 0; i < p_map.size(); ++i) {
        DEBUG("     start=0x%x\n", p_map.at(i).start);
        DEBUG("     end=0x%x\n", p_map.at(i).end);
        DEBUG("     prot=0x%x\n", p_map.at(i).prot);
        DEBUG("--------------------\n");
    }
#endif
    vintix::shared_ptr pstdin { UNWRAP_PTR(kopen("/dev/keyboard", O_RDONLY)) };

    if (!pstdin.is_ok())
        return ENOMEM;

    if (auto err = p_files.push_back(vintix::move(pstdin))) [[unlikely]]
        return err;

    vintix::shared_ptr pstdout { UNWRAP_PTR(kopen("/dev/fb", O_RDWR)) };

    if (!pstdout.is_ok())
        return ENOMEM;

    if (auto err = p_files.push_back(vintix::move(pstdout))) [[unlikely]]
        return err;

    // stderr
    if (auto err = p_files.push_back_copy(p_files[STDOUT_FILENO])) [[unlikely]]
        return err;

    if (auto err = procs.push_back(this))
        return err;

    p_active_process = this;
    p_active_process->p_state = process_state::RUNNING;

    if (auto err = p_active_process->init_process_stack())
        return err;

    DEBUG("process::load: Executing \"%s\"\n", p_active_process->p_name.c_str());
    p_active_process->run();
    return ENOERR;
}

auto process::init_process_stack() noexcept -> errno
{
    if (!map_page(USER_STACK_BEGIN - 1, alloc_page(), STACK_PG_FLAGS))
        return ENOMEM;

    p_stack.start = USER_STACK_BEGIN - 1;
    p_stack.end = USER_STACK_BEGIN - PAGE_FRAME_SIZE;
    p_stack.prot = VM_PROT_READ | VM_PROT_WRITE;
    p_stack.object.pager.p_fp = nullptr;
    p_stack.object.pager.p_offset = 0;
    p_stack.object.pager.p_memsz = 0;

    vintix::string pname_tmp;
    if (auto err = pname_tmp.copy(p_name)) [[unlikely]]
        return err;

    if (auto err = p_argv.push_back(vintix::move(pname_tmp))) [[unlikely]]
        return err;

    usize dec_stack_count = 0;
    dec_stack_count += sizeof(uintptr_t); // for return value
    dec_stack_count += sizeof(int);       // for argc
    dec_stack_count += sizeof(char **);   // for argv
    dec_stack_count += sizeof(char **);   // for envp
    dec_stack_count += sizeof(uintptr_t) * (p_argv.size() + p_envp.size());
    for (usize i = 0; i < p_argv.size(); ++i)
        dec_stack_count += align_up<sizeof(uintptr_t)>(p_argv[i].length() + 1);
    for (usize i = 0; i < p_envp.size(); ++i)
        dec_stack_count += align_up<sizeof(uintptr_t)>(p_envp[i].length() + 1);
    dec_stack_count += sizeof(uintptr_t) * 2; // for argv and envp NULL terminators
    p_esp = USER_STACK_BEGIN - dec_stack_count;

    // zero the stack for security
    memsetd(unsafe_toptr<void *>(page_align(p_esp)), 0, PAGE_FRAME_SIZE / sizeof(DWORD));

    auto *esp = unsafe_toptr<uintptr_t *>(p_esp);
    // esp[0] = RETURN ADDRESS
    esp[1] = p_argv.size();                                     // argc
    esp[2] = addressof(esp + 4);                                // argv
    usize offset = (4 + p_argv.size() + 1) * sizeof(uintptr_t); // +1 for null terminator
    for (usize i = 0; i < p_argv.size(); ++i) {
        esp[4 + i] = addressof(esp) + offset;
        memcpy(unsafe_toptr<void *>(addressof(esp) + offset), p_argv[i].c_str(),
               p_argv[i].length() + 1);
        offset += p_argv[i].length() + 1;
    }
    esp[4 + p_argv.size()] = 0; // null terminator

    esp[3] = addressof(esp) + offset; // envp
    const usize s = offset / sizeof(uintptr_t);
    offset += (p_envp.size() + 1) * sizeof(uintptr_t); // + 1 for null terminator
    for (usize i = 0; i < p_envp.size(); ++i) {
        esp[s + i] = addressof(esp) + offset;
        memcpy(unsafe_toptr<void *>(addressof(esp) + offset), p_envp[i].c_str(),
               p_envp[i].length() + 1);
        offset += p_envp[i].length() + 1;
    }
    esp[s + p_envp.size()] = 0; // null terminator

    KASSERT(addressof(esp) + offset < VADDR_OFFSET);
    return ENOERR;
}

void process::init() noexcept
{
    p_active_process = static_cast<process *>(kzalloc(sizeof(process)));

    if (p_active_process == nullptr)
        kpanic("unable to start any processes, not enough memory available\n");
}

auto process::expand_stack(v_addr page_fault_address) noexcept -> errno
{
    p_addr pg;
    do {
        pg = alloc_page();
        if (!pg) {
            const pid_t offender = oom_handler();
            if (offender == this->pid())
                return ENOMEM;
        }
    } while (!pg);

    vintix::ignore = map_page(page_fault_address, pg, STACK_PG_FLAGS);

    if (page_fault_address < p_stack.start)
        p_stack.start = page_align(page_fault_address);

    return ENOERR;
}

void process::terminate(pid_t pid, int sig) noexcept
{
    process::kill(pid, sig);
    process::schedule_new_process(nullptr);
}
