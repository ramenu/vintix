/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/lib/cxxstring.h>
#include <vintix/kernel/lib/string_functions.h>
#include <vintix/kernel/lib/string_view.h>

void _cstring_from_cstr(_cstring *self, const char *s, bool has_size) noexcept
{
    if (has_size)
        self->s_len = 0;

    if (s == nullptr) {
        self->s_str = nullptr;
        return;
    }

    const usize s_len = strlen(s);
    self->s_str = static_cast<char *>(kzalloc(s_len + 1));
    if (self->s_str == nullptr)
        return;

    if (has_size)
        self->s_len = s_len;

    memcpy(self->s_str, s, s_len + 1);
}

void _cstring_dtor(_cstring *self, bool has_size) noexcept
{
    KASSERT(self != nullptr);
    if (self->s_str != nullptr) {
        kfree(self->s_str);
        self->s_str = nullptr;
        if (has_size)
            self->s_len = 0;
    }
}

auto _cstring_move(_cstring *self, _cstring *other, bool has_size) noexcept -> _cstring *
{
    KASSERT(self != nullptr && other != nullptr);
    if (self == other)
        return self;

    if (self != nullptr)
        kfree(self->s_str);

    self->s_str = other->s_str;
    if (has_size) {
        self->s_len = other->s_len;
        other->s_len = 0;
    }
    other->s_str = nullptr;

    return self;
}

auto _cstring_copy(_cstring *self, const _cstring *other, u8 has_size) noexcept -> errno
{
    KASSERT(self != nullptr);
    if (self->s_str != nullptr)
        _cstring_dtor(self, has_size & __CSTRING_SELF_HAS_SIZE);

    errno err;
    const usize o_sz = has_size & __CSTRING_OTHER_HAS_SIZE ? other->s_len : strlen(other->s_str);
    self->s_str = static_cast<char *>(krealloc(self->s_str, o_sz + 1, err));

    if (err) [[unlikely]]
        return err;

    memcpy(self->s_str, other->s_str, o_sz + 1);

    if (has_size & __CSTRING_SELF_HAS_SIZE)
        self->s_len = o_sz;

    return ENOERR;
}

auto _cstring_prepend(_cstring *self, const _cstring *other, u8 has_size) noexcept -> errno
{
    const usize o_sz = has_size & __CSTRING_OTHER_HAS_SIZE ? other->s_len : strlen(other->s_str);

    if (self->s_str == nullptr) {
        self->s_str = static_cast<char *>(kmalloc(o_sz + 1));

        if (self->s_str == nullptr) [[unlikely]]
            return ENOMEM;

        if (has_size)
            self->s_len = o_sz;
        memcpy(self->s_str, other->s_str, o_sz + 1);
    } else {
        errno err;
        const usize s_sz = has_size & __CSTRING_SELF_HAS_SIZE ? self->s_len : strlen(self->s_str);
        self->s_str = static_cast<char *>(krealloc(self->s_str, s_sz + o_sz + 1, err));

        if (err) [[unlikely]]
            return err;

        vintix::string tmp { *reinterpret_cast<vintix::string *>(self) };

        if (tmp == nullptr) [[unlikely]]
            return ENOMEM;

        memcpy(self->s_str + o_sz, tmp.c_str(), s_sz + 1);
        memcpy(self->s_str, other->s_str, o_sz);
        if (has_size)
            self->s_len += o_sz;
    }
    return ENOERR;
}

auto _cstring_append(_cstring *self, const _cstring *other, u8 has_size) noexcept -> errno
{
    errno err;
    const usize o_sz = has_size & __CSTRING_OTHER_HAS_SIZE ? other->s_len : strlen(other->s_str);
    self->s_str = static_cast<char *>(krealloc(self->s_str, self->s_len + o_sz + 1, err));

    if (err) [[unlikely]]
        return err;

    const usize s_len = has_size & __CSTRING_SELF_HAS_SIZE ? self->s_len : strlen(self->s_str);
    memcpy(self->s_str + s_len, other->s_str, o_sz + 1);

    if (has_size & __CSTRING_SELF_HAS_SIZE)
        self->s_len += o_sz;
    return ENOERR;
}

auto _cstring_prepend_char(_cstring *self, char c, bool has_size) noexcept -> errno
{
    errno err;
    const usize s_len = has_size ? self->s_len : strlen(self->s_str);
    self->s_str = static_cast<char *>(krealloc(self->s_str, s_len + 2, err));

    if (err) [[unlikely]]
        return err;

    if (s_len == 0) {
        self->s_str[0] = c;
        self->s_str[1] = '\0';
        if (has_size)
            ++self->s_len;
        return ENOERR;
    }

    vintix::string tmp { *reinterpret_cast<vintix::string *>(self) };

    if (tmp == nullptr) [[unlikely]]
        return ENOMEM;

    for (usize i = 0; i <= s_len - 1; ++i)
        self->s_str[i + 1] = tmp[i];

    self->s_str[0] = c;
    self->s_str[s_len + 1] = '\0';
    if (has_size)
        ++self->s_len;

    return ENOERR;
}

auto _cstring_append_char(_cstring *self, char c, bool has_size) noexcept -> errno
{
    const usize s_len = has_size ? self->s_len : strlen(self->s_str);
    errno err;
    self->s_str = static_cast<char *>(krealloc(self->s_str, s_len + 2, err));

    if (err) [[unlikely]]
        return err;

    self->s_str[s_len] = c;
    self->s_str[s_len + 1] = '\0';

    if (has_size)
        ++self->s_len;
    return ENOERR;
}

auto _cstring_starts_with(const _cstring *self, const _cstring *other, u8 has_size) noexcept -> bool
{
    if ((has_size & __CSTRING_SELF_HAS_SIZE) && self->s_len < other->s_len)
        return false;
    const usize o_sz = has_size & __CSTRING_OTHER_HAS_SIZE ? other->s_len : strlen(other->s_str);
    return strncmp(self->s_str, other->s_str, o_sz) == 0;
}

auto _cstring_ends_with(const _cstring *self, const _cstring *other, u8 has_size) noexcept -> bool
{
    const usize s_len = has_size & __CSTRING_SELF_HAS_SIZE ? self->s_len : strlen(self->s_str);
    const usize o_len = has_size & __CSTRING_OTHER_HAS_SIZE ? other->s_len : strlen(other->s_str);
    if (s_len < o_len)
        return false;

    return strncmp(self->s_str - other->s_len, other->s_str, o_len) == 0;
}

auto _cstring_contains(const _cstring *self, const _cstring *other, u8 has_size) noexcept -> bool
{
    const usize s_len = has_size & __CSTRING_SELF_HAS_SIZE ? self->s_len : strlen(self->s_str);
    const usize o_len = has_size & __CSTRING_OTHER_HAS_SIZE ? other->s_len : strlen(other->s_str);
    if (s_len < o_len)
        return false;

    for (usize i = 0; i < s_len; i += o_len)
        if (strncmp(self->s_str + i, other->s_str, o_len) == 0)
            return true;
    return false;
}

auto _cstring_equals(const _cstring *self, const _cstring *other, u8) noexcept -> bool
{
    return (strcmp(self->s_str, other->s_str) == 0);
}

auto _cstring_contains_char(const _cstring *self, char c) noexcept -> bool
{
    for (usize i = 0; self->s_str[i] != '\0'; ++i)
        if (self->s_str[i] == c)
            return true;
    return false;
}

auto _cstring_count_char(const _cstring *self, char c) noexcept -> usize
{
    usize count = 0;
    for (usize i = 0; self->s_str[i] != '\0'; ++i)
        if (self->s_str[i] == c)
            ++count;
    return count;
}

auto _cstring_split(const _cstring *self, char c,
                    vintix::vector<vintix::string> &buf) noexcept -> errno
{
    usize start = 0;
    for (usize i = start; self->s_str[i] != '\0'; ++i) {
        if ((self->s_str[i + 1] != '\0' && self->s_str[i] != c) || i == start)
            continue;

        vintix::string value { vintix::string_view { self->s_str + start, (i - start) + 1 } };

        if (value == nullptr)
            return ENOMEM;

        if (self->s_str[i + 1] != '\0')
            value.erase_last();

        if (auto err = buf.push_back(move(value)))
            return err;

        start = i + 1;
    }

    return ENOERR;
}
