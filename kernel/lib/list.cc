/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/lib/list.h>
#include <vintix/kernel/lib/string.h>

[[nodiscard]] auto list_move(clist *self, clist *other, void (*dtor)(void *)) noexcept -> clist *
{
    if (self == other)
        return self;

    if (self->l_head != nullptr)
        list_dtor(self, dtor);

    self->l_head = other->l_head;
    other->l_head = nullptr;

    return self;
}

auto list_insert(list_node **node, void *data, usize sz) noexcept -> void *
{
    KASSERT(node != nullptr);
    auto new_node = static_cast<list_node *>(kzalloc(sizeof(list_node) + sz));

    if (new_node == nullptr) {
        memset(data, 0, sz);
        return nullptr;
    }

    new_node->n_next = nullptr;
    void *new_data = reinterpret_cast<u8 *>(new_node) + sizeof(void *);
    memcpy(new_data, data, sz);

    if (*node == nullptr) {
        *node = new_node;
        memset(data, 0, sz);
        return new_data;
    }

    list_node *cur = *node;
    while (cur->n_next != nullptr)
        cur = cur->n_next;

    cur->n_next = new_node;
    memset(data, 0, sz);
    return new_data;
}

auto list_size(list_node *head) noexcept -> usize
{
    if (head == nullptr)
        return 0;

    usize count = 1;
    const list_node *node = head;

    while ((node = node->n_next) != nullptr)
        ++count;

    return count;
}

void list_dtor(clist *self, void (*dtor)(void *)) noexcept
{
    list_node *cur = self->l_head;

    while (cur != nullptr) {
        list_node *tmp = cur->n_next;
        void *n_data = reinterpret_cast<u8 *>(cur) + sizeof(void *);
        dtor(n_data);
        cur = tmp;
    }

    self->l_head = nullptr;
}

void list_remove(clist *self, void *elem, void (*dtor)(void *)) noexcept
{
    KASSERT(self->l_head != nullptr);

    bool found = false;
    list_node *cur = self->l_head, *prev = nullptr;

    while (cur != nullptr) {
        void *n_data = reinterpret_cast<u8 *>(cur) + sizeof(void *);
        if (elem != n_data) {
            prev = cur;
            cur = cur->n_next;
            continue;
        }

        found = true;
        dtor(n_data);
        if (prev != nullptr) {
            prev->n_next = cur->n_next;
        } else if (self->l_head->n_next != nullptr) {
            self->l_head = self->l_head->n_next;
        } else
            list_dtor(self, dtor);

        prev = cur;
        cur = cur->n_next;
    }

    KASSERT(found);
}
