/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/core/panic.h>
#include <vintix/kernel/filesystems/fs.h>
#include <vintix/kernel/filesystems/path.h>
#include <vintix/kernel/lib/cxxstring.h>

auto path_parent_of(vintix::string_view path) noexcept -> vintix::string_view
{
    // root directory has no parent
    if (path.size() == 1)
        return vintix::string_view { nullptr };

    if (path.count('/') == 1)
        return vintix::string_view { "/" };

    for (usize i = path.size() - 1; i != 0; --i)
        if (path[i] == '/' || i == 1)
            return { path.data(), i - 1 };

    kpanic("this shouldn't happen");
}

auto path_last_entry(vintix::string_view path) noexcept -> const char *
{
    KASSERT(!path.empty());
    if (path.size() == 1)
        return path.c_str();

    KASSERT(!path.ends_with('/'));
    for (usize i = path.size(); i != 0; --i)
        if (path[i - 1] == '/')
            return &path.c_str()[i];

    return nullptr;
}

// note this requires the full path, not just the name
auto path_consume(const char *path, vintix::span<char> buf, usize &off) noexcept -> errno
{
    if (off == 0) {
        buf[0] = '/';
        buf[1] = '\0';
        ++off;
        return ENOERR;
    }

    cstr s_path { path + off };
    const vintix::optional i = s_path.find('/');

    if (i.is_none()) {
        strcpy(buf.unsafe_mutable_data(), s_path.c_str());
        return EPARSE;
    }

    if (i.value() > buf.size())
        return ENAMETOOLONG;

    memcpy(buf.unsafe_mutable_data(), s_path.c_str(), i.value());
    buf[i.value() + 1] = '\0';
    off += i.value() + 1;
    return ENOERR;
}

auto path_chk_valid(vintix::string_view path) noexcept -> errno
{
    // The length of a pathname exceeds {PATH_MAX}, or pathname resolution of a symbolic
    // link produced an intermediate result with a length that exceeds {PATH_MAX}.
    if (path.length() > PATH_MAX)
        return ENAMETOOLONG;

    return ENOERR;
}

auto path_consume_until(const char *path, vintix::span<char> buf,
                        vintix::optional<usize> steps) noexcept -> errno
{
    usize offset = 0;

    if (steps.is_some()) {
        for (usize i = 0; i < steps.value(); ++i) {
            errno err = path_consume(
                path, { buf.unsafe_mutable_data() + offset, buf.size() - offset }, offset);
            buf[offset - 1] = '/';
            if (err && i != steps.value() - 1 && err != EPARSE)
                return err;
        }
        return ENOERR;
    }

    errno err = ENOERR;
    while (!err) {
        err =
            path_consume(path, { buf.unsafe_mutable_data() + offset, buf.size() - offset }, offset);
        buf[offset - 1] = '/';
        if (err) {
            if (err == EPARSE) {
                if (offset != 1)
                    buf[offset - 1] = '\0';
                return ENOERR;
            }
            return err;
        }
    }

    __builtin_unreachable();
}
// Converts a path like '////usr///bin/////ls//' to '/usr/bin/ls'
void path_normalize(vintix::string &path) noexcept
{
    usize slash_count = 0;
    usize j = 0;
    for (usize i = 0; path[i] != '\0'; ++i) {
        if (path[i] == '/') {
            if (slash_count == 0 && j != 0) {
                path[j] = path[i];
                ++j;
            }
            if (j == 0 && slash_count == 1)
                j = i;
            ++slash_count;
        } else {
            slash_count = 0;
            if (j != 0) {
                path[j] = path[i];
                ++j;
            }
        }
    }
    if (j != 0) {
        if (path[j - 1] == '/')
            path[j - 1] = '\0';
        else
            path[j] = '\0';
    }

    path.unsafe_set_size(path.size() - j);
}
