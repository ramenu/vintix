/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/core/io.h>
#include <vintix/kernel/core/kassert.h>
#include <vintix/kernel/core/panic.h>
#include <vintix/kernel/core/serial.h>
#include <vintix/kernel/lib/klog.h>
#include <vintix/kernel/lib/types.h>
#include <vintix/kernel/lib/vaarg.h>
#include <vintix/kernel/misc/color.h>

static void write_debug_msg() noexcept { serial_puts("[ DEBUG ] ", FBFGColor::Brown); }

void debug_log(const char *format, ...) noexcept
{
    KASSERT(format != nullptr);
    write_debug_msg();
    va_list va;
    va_start(va, format);
    srl_vfprintf(format, va);
    va_end(va);
}

void pci_log(u32 pci, const char *format, ...) noexcept
{
    KASSERT(format != nullptr);
    const u8 bus = pci & 0x0000ff;
    const u8 dev = (pci & 0x00ff00) >> 8;
    const u8 fn = (pci & 0xff0000) >> 16;
    write_debug_msg();
    srl_printf("PCI(%02d.%02d.%02d): ", bus, dev, fn);
    va_list va;
    va_start(va, format);
    srl_vfprintf(format, va);
    va_end(va);
}

void ok(const char *format, ...) noexcept
{
    KASSERT(format != nullptr);
    fb_write("[ OK ] ", FBFGColor::Green);
    va_list va;
    va_start(va, format);
    kprintf(COLOR_WHITE);
    kvfprintf(format, va);
    kprintf(COLOR_RESET);
    va_end(va);
}

void _kassert(bool expr, const char *msg, const char *file, const char *function, u32 line) noexcept
{
    if (!expr)
        kpanic("\n%s:%u:%s: assertion failed: %s\n", file, line, function, msg);
}
