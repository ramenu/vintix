/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/core/kassert.h>
#include <vintix/kernel/lib/array.h>
#include <vintix/kernel/lib/string.h>
#include <vintix/kernel/lib/types.h>

// copy 'N' bytes from 'src' to 'dest'
auto memmove(void *dest, const void *src, usize len) noexcept -> void *
{
    auto *d = static_cast<char *>(dest);
    auto *s = static_cast<const char *>(src);

    if (dest < src) {
        while (len--)
            *d++ = *s++;
    } else {
        const char *lasts = s + (len - 1);
        char *lastd = d + (len - 1);

        while (len--)
            *lastd-- = *lasts--;
    }

    return dest;
}

#ifndef __i386__
auto memcpy(void *dest, const void *src, usize len) noexcept -> void *
{
    char *d = static_cast<char *>(dest);
    const char *s = static_cast<const char *>(src);

    while (len--)
        *d++ = *s++;
    return dest;
}

// set 'N' bytes at 'dest' to 'val'
auto memset(void *dest, int val, usize len) noexcept -> void *
{
    auto *ptr = static_cast<char *>(dest);
    while (len-- != 0)
        *ptr++ = static_cast<char>(val);
    return dest;
}
#endif

// converts a integer to a string with the given
// base. (10 by default)
// NOTE: Doesn't support negative integers at the moment
auto itoa(usize val, unsigned base) noexcept -> char *
{
    KASSERT(base > 0 && base <= 16);
    static vintix::array<char, 32> buf;
    unsigned i = 30;

    if (val == 0) {
        buf[30] = '0';
        return &buf[30];
    }

    for (; val && i; --i, val /= base)
        buf.at(i) = "0123456789ABCDEF"[val % base];

    return &buf[i + 1];
}
