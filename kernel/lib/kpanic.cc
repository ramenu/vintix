/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/asm/inst.h>
#include <vintix/kernel/core/global.h>
#include <vintix/kernel/core/interrupt.h>
#include <vintix/kernel/core/io.h>
#include <vintix/kernel/core/kassert.h>
#include <vintix/kernel/core/panic.h>
#include <vintix/kernel/core/trace.h>

static constexpr usize STACK_TRACE_FRAMES = 10;

void kpanic(const char *format, ...) noexcept
{
    KASSERT(format != nullptr);
    fb_write("kernel panic: ");

    va_list va;
    va_start(va, format);
    kvfprintf(format, va);
    va_end(va);

    print_stack_trace(STACK_TRACE_FRAMES);
    disable_interrupts();
    halt();
}
