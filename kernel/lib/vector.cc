/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/lib/string.h>
#include <vintix/kernel/lib/vector.h>

auto cvector_push_back(cvector *self, void *data, usize obj_size) noexcept -> errno
{
    KASSERT(obj_size % sizeof(DWORD) == 0);
    errno err;
    self->v_data = krealloc(self->v_data, (self->v_sz + 1) * obj_size, err);

    if (err)
        return err;

    memcpy(static_cast<u8 *>(self->v_data) + self->v_sz * obj_size, data, obj_size);
    memset(data, 0, obj_size);
    ++self->v_sz;

    return ENOERR;
}

void cvector_erase(cvector *self, usize i, void (*dtor)(void *), void *(*move)(void *, void *),
                   usize obj_size) noexcept
{
    KASSERT(i < self->v_sz);
    auto v_data = static_cast<u8 *>(self->v_data);
    dtor(v_data + i * obj_size);
    for (usize j = i; j + 1 != self->v_sz; ++j) {
        const void *obj = move(v_data + j * obj_size, v_data + (j + 1) * obj_size);
        memcpy(v_data + j * obj_size, obj, obj_size);
    }

    --self->v_sz;
}

void cvector_erase_primitive(cvector *self, usize i, usize obj_size) noexcept
{
    KASSERT(i < self->v_sz);
    auto v_data = static_cast<u8 *>(self->v_data);
    for (usize j = i; j + 1 != self->v_sz; ++j)
        memcpy(v_data + j * obj_size, v_data + (j + 1) * obj_size, obj_size);
    --self->v_sz;
}

// auto cvector_move_assignment(cvector *self, cvector *other, )
void cvector_dtor(cvector *self, void (*dtor)(void *), usize obj_size) noexcept
{
    if (self->v_data != nullptr) {
        for (usize i = 0; i < self->v_sz; ++i)
            dtor(static_cast<u8 *>(self->v_data) + i * obj_size);
        kfree(self->v_data);
        self->v_data = nullptr;
        self->v_sz = 0;
    }
}

auto cvector_move(cvector *self, cvector *other, void (*dtor)(void *)) noexcept -> cvector *
{
    if (self == other)
        return self;

    if (self->v_data != nullptr)
        dtor(self);

    self->v_data = other->v_data;
    self->v_sz = other->v_sz;
    other->v_data = nullptr;
    other->v_sz = 0;

    return self;
}

void cvector_dtor_primitive(cvector *self) noexcept
{
    if (self->v_data != nullptr) {
        kfree(self->v_data);
        self->v_data = nullptr;
        self->v_sz = 0;
    }
}
