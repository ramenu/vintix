/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/core/uvm.h>
#include <vintix/kernel/lib/math.h>

auto vm_map::insert(vm_map_entry &&entry) noexcept -> errno
{
    return v_entries.push_back(vintix::move(entry));
}

auto vm_map::remove(vm_off_t start, vm_off_t end) noexcept -> void
{
    for (usize i = 0; i < v_entries.size(); ++i) {
        if (v_entries[i].end > end)
            break;

        if (v_entries[i].start >= start)
            v_entries.erase(i);
    }
}

auto vm_map::lookup(vm_off_t pos) const noexcept -> const vm_map_entry *
{
    // TODO: Make this work with submaps
    for (usize i = 0; i < v_entries.size(); ++i)
        if (pos >= v_entries[i].start && pos <= v_entries[i].end)
            return &v_entries[i];

    // not found
    return nullptr;
}

vm_map::~vm_map() noexcept
{
    // TODO: Handle submaps
    for (usize i = 0; i < v_entries.size(); ++i) {
        if (!address_is_mapped(v_entries[i].start))
            continue;
        free_pages(v_entries[i].start, max(1, v_entries[i].memory_usage() / PAGE_FRAME_SIZE));
    }
    v_entries.~vector();
}
