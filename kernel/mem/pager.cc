/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/core/blkdev.h>
#include <vintix/kernel/core/pager.h>
#include <vintix/kernel/filesystems/file.h>
#include <vintix/kernel/lib/string.h>

auto vm_pager::get_page(void *buf, off_t page_off) const noexcept -> errno
{
    static constexpr off_t PAGE_FRAME_SIZE_OFFT = PAGE_FRAME_SIZE;
    if (page_off < 0 || page_off > this->total_number_of_pages())
        return EINVAL;

    if (p_fp == nullptr) {
        memsetd(buf, 0, PAGE_FRAME_SIZE / sizeof(DWORD));
        return ENOERR;
    }

    if (page_off > number_of_pages_filesz() && page_off <= total_number_of_pages()) {
        const usize bytes_to_copy = static_cast<usize>(page_off % PAGE_FRAME_SIZE_OFFT == 0
                                                           ? PAGE_FRAME_SIZE_OFFT
                                                           : p_filesz % PAGE_FRAME_SIZE_OFFT);
        memset(buf, 0, bytes_to_copy);
        return ENOERR;
    }

    p_fp->f_pos = klseek(p_fp, p_offset + page_off * PAGE_FRAME_SIZE_OFFT, SEEK_SET);

    KASSERT(p_fp->f_pos < p_fp->ssize() - PAGE_FRAME_SIZE_OFFT);
    const ssize bytes_read = kread(p_fp, buf, PAGE_FRAME_SIZE);

    if (bytes_read < 0)
        return -bytes_read;

    const usize bytes_remaining = static_cast<usize>(p_fp->ssize() % PAGE_FRAME_SIZE_OFFT);
    if (bytes_read == EOF && bytes_remaining > 0)
        memset(static_cast<u8 *>(buf) + PAGE_FRAME_SIZE - bytes_remaining, 0, bytes_remaining);

    return ENOERR;
}