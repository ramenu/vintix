/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/core/global.h>
#include <vintix/kernel/core/io.h>
#include <vintix/kernel/core/kassert.h>
#include <vintix/kernel/core/panic.h>
#include <vintix/kernel/core/pmm.h>
#include <vintix/kernel/core/segment.h>
#include <vintix/kernel/core/vmm.h>
#include <vintix/kernel/lib/align.h>
#include <vintix/kernel/lib/klog.h>
#include <vintix/kernel/lib/math.h>
#include <vintix/kernel/lib/overflow.h>
#include <vintix/kernel/lib/string.h>
#include <vintix/kernel/lib/utilities.h>

struct block {
    // the size of the block
    usize sz;

    // previous block node
    block *prev { nullptr };

    // next block node
    block *next { nullptr };
};

static v_addr *kernel_heap_begin;
static constexpr v_addr KERNEL_HEAP_END = 0xffffffff;

static block *head = nullptr;

static void free_block(block *blk) noexcept;
static auto ptr_to_block(void *p) noexcept
{
    return unsafe_toptr<block *>(addressof(p) - sizeof(block));
}

// #ifndef NDEBUG

// used so that we can track the number of blocks, this is only here
// for debugging purposes, so that if a large number of blocks suddenly
// dissapear from the linked list, then we know the heap is in an inconsistent
// state and some error occurred.
static constinit usize nblks = 1;

static auto ensure_block_is_ok(const block *blk) noexcept -> const char *
{
    KASSERT(blk != nullptr);

    if (blk->prev != nullptr) {
        const auto cur = addressof(blk);
        const auto prev = addressof(blk->prev);
        if ((prev + blk->prev->sz + sizeof(block)) > cur)
            return "Yikes. The previous block overlaps with the current one. Fix this "
                   "immediately!\n";
    }

    return nullptr;
}

static auto ensure_block_exists(const block *blk) noexcept -> bool
{
    block *current = head;

    while (current != nullptr) {
        if (current == blk)
            return true;
        current = current->next;
    }

    return false;
}

static auto ensure_heap_is_ok() noexcept -> bool
{
    block *blk = head;
    usize count = 0;
    bool err = false;
    do {
        if (blk == head)
            KASSERT(blk->prev == nullptr);

        if (ensure_block_is_ok(blk) != nullptr)
            err = true;
        ++count;
    } while ((blk = blk->next) != nullptr);

    err |= count != nblks;

    if (err) {
        const bool YIKES = debug_block_state();
        if (count != nblks) {
            if (!YIKES)
                kpanic("Yikes. A number of blocks were completely lost.\n");
            kpanic("DOUBLE YIKES! A number of blocks were completely lost\n");
        }
    }

    return (err == false);
}

// returns true if an error occurred
auto debug_block_state() noexcept -> bool
{
    const char *err_msg = nullptr;
    block *blk = head;
    do {
        const auto cur = addressof(blk);
        const auto prev = (blk->prev == nullptr) ? cur : addressof(blk->prev);
#ifndef NDEBUG
        DEBUG("debug_block_state: blk=0x%p, sz=%ul, df=%ul, prv=0x%p, nxt=0x%p\n",
              addressof(blk) + sizeof(block), blk->sz, cur - prev, blk->prev, blk->next);
        DEBUG("debug_block_state: nblks=%ul\n", nblks);
#else
        kprintf("debug_block_state: blk=0x%p, sz=%ul, df=%ul, prv=0x%p, nxt=0x%p\n",
                addressof(blk) + sizeof(block), blk->sz, cur - prev, blk->prev, blk->next);
#endif
        if ((err_msg = ensure_block_is_ok(blk)) != nullptr) {
            kpanic(err_msg);
            return true;
        }
    } while ((blk = blk->next) != nullptr);

#ifndef NDEBUG
    DEBUG("debug_block_state: nblks=%ul\n", nblks);
#else
    kprintf("debug_block_state: nblks=%ul\n", nblks);
#endif
    return false;
}
// #endif

// Returns the number of pages to free starting from
// the given block
static auto pgs_to_free(const block *RESTRICT blk) noexcept -> usize
{
    KASSERT(blk != nullptr);
    KASSERT(blk->prev != nullptr);
    // NOLINTBEGIN(clang-analyzer-core.NullDereference)

    if (blk->next == nullptr) {
        if (!on_the_same_page(addressof(blk), addressof(blk->prev) + blk->prev->sz))
            return blk->sz / PAGE_FRAME_SIZE;
        return 0;
    }

    const v_addr nxt = align_down<PAGE_FRAME_SIZE>(addressof(blk->next));
    const v_addr cur = align_down<PAGE_FRAME_SIZE>(addressof(blk));

    if (blk->sz < PAGE_FRAME_SIZE)
        return 0;

    const usize num_pages = (nxt - cur - PAGE_FRAME_SIZE) / PAGE_FRAME_SIZE;
    return num_pages;
    // NOLINTEND(clang-analyzer-core.NullDereference)
}

[[nodiscard]] static auto alloc_n_pages(void *const vaddr, usize n) noexcept -> bool
{
    usize alloced = 0;
    auto *v = static_cast<v_addr *>(vaddr);

    do {
        if (address_is_mapped(addressof(v)))
            continue;

        const p_addr pg = alloc_page();

        if (!map_page(addressof(v), pg, PG_P | PG_R | PG_W | PG_X)) {
            // unset the previous pages we allocated
            for (v_addr *addr = static_cast<v_addr *>(vaddr), i = 0; i < alloced;
                 addr += PAGE_FRAME_SIZE / sizeof(v_addr), ++i)
                free_page(get_page(addr));
            DEBUG("failed to allocate %u pages at 0x%p\n", n, vaddr);
            return false;
        }

        ++alloced;
    } while (v += PAGE_FRAME_SIZE / sizeof(v_addr), --n);

    return true;
}

auto kmalloc(usize n, usize align) noexcept -> void *
{
    KASSERT(n > 0);
    KASSERT(head != nullptr);
#ifndef NDEBUG
    KASSERT(ensure_heap_is_ok());
#endif

    KASSERT(is_power_of_two(align));
    n = align_up<MALLOC_ALIGNMENT>(n);

    block *blk = head;
    do {

        if (blk == head && blk->next != nullptr)
            continue;

        const auto blk_vaddr = addressof(blk);

        const v_addr end = (blk->next == nullptr) ? KERNEL_HEAP_END : addressof(blk->next);
        v_addr new_blk_start = blk_vaddr + sizeof(block) + blk->sz;

        if (align != sizeof(void *)) {
            new_blk_start += sizeof(block);
            new_blk_start = align_up(new_blk_start, align);
            KASSERT(new_blk_start % align == 0);
            new_blk_start -= sizeof(block);
        }

        if (end > (new_blk_start + n + sizeof(block))) {

            const u32 pages = (new_blk_start + n + sizeof(block) - blk_vaddr) / PAGE_FRAME_SIZE + 2;
            auto *new_blk = unsafe_toptr<block *>(new_blk_start);

            if (!alloc_n_pages(new_blk, pages))
                return nullptr;

            new_blk->prev = blk;
            new_blk->next = blk->next;
            if (new_blk->next != nullptr)
                new_blk->next->prev = new_blk;
            blk->next = new_blk;
            new_blk->sz = n;

            ++nblks;

            auto ret = unsafe_toptr<void *>(new_blk_start + sizeof(block));
            DEBUG("kmalloc: Allocated %ul bytes at 0x%p (align=%ul)\n", n, ret, align);
            return ret;
        }
    } while ((blk = blk->next) != nullptr);

    DEBUG("kmalloc: Failed to allocate %ul bytes!\n", n);
    return nullptr;
}

auto kcalloc(usize length, usize sz, usize align) noexcept -> void *
{
    usize n;
    if (chk_mul_overflow(length, sz, &n))
        return nullptr;
    return kzalloc(n, align);
}

auto kzalloc(usize n, usize align) noexcept -> void *
{
    void *p = kmalloc(n, align);

    if (p == nullptr)
        return nullptr;

    void *tmp = p;
    n = align_up<MALLOC_ALIGNMENT>(n) / sizeof(u32);

    memsetd(tmp, 0, n);
    return p;
}

auto krealloc(void *p, usize n, errno &err, usize align) noexcept -> void *
{
#ifndef NDEBUG
    KASSERT(ensure_heap_is_ok());
#endif
    KASSERT(p >= kernel_heap_begin);
    err = ENOERR;

    void *new_p;
    if (p != nullptr) {
        block *blk = ptr_to_block(p);
        KASSERT(ensure_block_exists(blk));

        // no need to reallocate since enough memory has
        // already been allocated
        if (blk->sz >= n)
            return p;

        new_p = kmalloc(n, align);

        if (new_p == nullptr) {
            err = ENOMEM;
            return p;
        }

        memcpyd(new_p, p, blk->sz / sizeof(DWORD));
        memsetd(static_cast<u8 *>(new_p) + blk->sz, 0,
                (align_up<MALLOC_ALIGNMENT>(n) - blk->sz) / sizeof(DWORD));
        free_block(blk);

        return new_p;
    }
    new_p = kzalloc(n, align);

    if (new_p == nullptr) {
        err = ENOMEM;
        return p;
    }

    return new_p;
}

void kfree(void *p) noexcept
{
    if (p == nullptr)
        return;

    KASSERT(addressof(p) >= VADDR_OFFSET);
#ifndef NDEBUG
    KASSERT(ensure_heap_is_ok());
#endif

    return free_block(ptr_to_block(p));
}

static void free_block(block *RESTRICT blk) noexcept
{
#ifndef NDEBUG
    DEBUG("free_block: Attempting to free block 0x%x\n", blk);
    KASSERT(ensure_block_exists(blk));
#endif
    const usize n = pgs_to_free(blk);

    DEBUG("free_block: Freeing %ul bytes from 0x%x\n", blk->sz, addressof(blk) + sizeof(block));
    for (usize i = 1; i <= n; ++i)
        free_page(get_page(addressof(blk) + i * PAGE_FRAME_SIZE));

    if (blk->next != nullptr && blk->prev != nullptr) {
        blk->prev->next = blk->next;
        blk->next->prev = blk->prev;
    } else if (blk->prev != nullptr && blk->next == nullptr) {
        blk->prev->next = nullptr;
    }
    // TODO: put this into one else branch
    else if (blk->prev == nullptr && blk->next != nullptr) {
        blk->sz = 0;
    } else {
        KASSERT(blk->prev == nullptr && blk->next == nullptr);
        blk->sz = 0;
    }

    if (n > 0 && !on_the_same_page(blk, addressof(blk->prev) + blk->prev->sz))
        free_page(get_page(blk));
    --nblks;
}

void init_memory(void *heap_start) noexcept
{
    KASSERT(heap_start != nullptr);
    if (heap_start == nullptr ||
        !map_page(addressof(heap_start), alloc_page(), PG_P | PG_R | PG_W | PG_X)) [[unlikely]]
        kpanic("failed to initialize kernel heap\n");

    head = static_cast<block *>(heap_start);
    head->sz = 0;
    head->prev = nullptr;
    head->next = nullptr;

    // we need to make sure there is enough space
    // currently, alloc_pg_table() calls kzalloc underneath to allocate the
    // page table. However, assuming the kernel heap starts at 0xC0000000 and
    // the kernel heap reaches to somewhere like 0xC0400000, we will need to allocate
    // another page table. But once again, if alloc_pg_table() calls kzalloc underneath
    // then it won't be able to allocate any memory because the kernel heap will cause a
    // page fault when allocating the memory. Somehow, this circular dependency needs to be
    // broken.
    //
    // my solution is to just allocate all of the needed page tables when initializing
    // the heap. This has very little memory overhead (approx 4KiB per 4MiB of memory)
    // and it is straightforward to implement.
    //
    // Also, because $heap_start will most probably not be aligned, we need to allocate
    // an additional page table
    const usize num_pg_tables = u_p_end / (PAGE_FRAME_SIZE * PTE_ENTRIES) + 1;

    if (num_pg_tables > 0) {
        u8 *buf = static_cast<u8 *>(kzalloc(PAGE_FRAME_SIZE * num_pg_tables, PAGE_FRAME_SIZE));
        if (buf == nullptr) [[unlikely]]
            kpanic("failed to allocate page tables for the kernel heap\n");

        for (usize i = 1; i <= num_pg_tables; ++i)
            alloc_pg_table(vaddr_to_pdt(buf + i * (PAGE_FRAME_SIZE * PTE_ENTRIES)), buf);
    }
}

auto kernel_memory_usage() noexcept -> usize { return k_p_end - k_p_begin; }

auto kernel_heap_memory_usage() noexcept -> usize
{
    usize heap_usage = 0;
    block *blk = head;
    while (blk != nullptr) {
        heap_usage += blk->sz + sizeof(block);
        blk = blk->next;
    }

    return heap_usage;
}
