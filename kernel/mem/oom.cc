/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/core/oom.h>
#include <vintix/kernel/core/process.h>
#include <vintix/kernel/posix/signal.h>

auto oom_handler() noexcept -> pid_t
{
    usize hi = 0;
    pid_t offender_pid = 0;
    for (usize i = 0; i < process::processes().size(); ++i) {
        const usize mem_usage = process::processes()[i]->memory_usage();
        if (mem_usage > hi) {
            offender_pid = process::processes()[i]->pid();
            hi = mem_usage;
        }
    }

    // die villain!!!
    process::terminate(offender_pid, SIGKILL);
    return offender_pid;
}
