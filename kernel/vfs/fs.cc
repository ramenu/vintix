/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/core/blkdev.h>
#include <vintix/kernel/core/global.h>
#include <vintix/kernel/core/panic.h>
#include <vintix/kernel/core/time.h>
#include <vintix/kernel/filesystems/blkio.h>
#include <vintix/kernel/filesystems/dentry.h>
#include <vintix/kernel/filesystems/file.h>
#include <vintix/kernel/filesystems/fs.h>
#include <vintix/kernel/filesystems/inode.h>
#include <vintix/kernel/filesystems/path.h>
#include <vintix/kernel/filesystems/super.h>
#include <vintix/kernel/lib/algorithm.h>
#include <vintix/kernel/lib/array.h>
#include <vintix/kernel/lib/klog.h>
#include <vintix/kernel/lib/limits.h>
#include <vintix/kernel/lib/overflow.h>
#include <vintix/kernel/lib/string_view.h>
#include <vintix/kernel/lib/types.h>
#include <vintix/kernel/lib/unique_ptr.h>
#include <vintix/kernel/lib/vector.h>
#include <vintix/kernel/posix/fcntl.h>

static constexpr usize MAXIMUM_MOUNTPOINTS = 4;
NODESTROY static constinit vintix::array<genfs, MAXIMUM_MOUNTPOINTS> filesystems {};

static auto get_file(dentry *entry, int oflags) noexcept -> errptr<file>
{
    DEBUG("get_file: name=\"%s\" uid=%u gid=%u size=%ulb mode=0o%o\n", entry->d_name.c_str(),
          entry->d_ino->i_uid, entry->d_ino->i_gid, entry->d_ino->i_size, entry->d_ino->i_mode);
    errptr<file> fp = entry->d_ino->vtable->open(entry->d_ino);

    if (fp.is_err())
        return fp.err();

    fp->f_dentry = entry;
    fp->f_oflags = oflags;

    increment_dentry_parent_count(entry);
    debug_mount_graph(get_dentry_root());
    return fp;
}

// Should be called by open() if O_CREAT is set
static auto kopen_creat(vintix::string_view path, int flags, mode_t) noexcept -> errptr<file>
{
    // If O_CREAT and O_DIRECTORY are set and the requested access mode is
    // neither O_WRONLY nor O_RDWR, the result is unspecified.
    if (flags & O_DIRECTORY)
        if (!((flags & O_WRONLY) | (flags & O_RDWR)))
            return EINVAL;

    vintix::string_view parent_path { path_parent_of(path) };

    // no root directory, invalid fs
    if (parent_path == nullptr)
        return EPERM;

    int dflags = (flags & O_KERNEL) ? 0 : DENTRY_CHECK_PERMS;
    errptr<dentry> entry = get_dentry(path, dflags);
    errptr<dentry> d_parent = get_dentry(parent_path, dflags);

    if (d_parent.is_err())
        return d_parent.err();

    KASSERT(d_parent.get() != nullptr);
    KASSERT(d_parent.get()->d_ino != nullptr);

    const char *name = path_last_entry(path);
    inode *parent_ino = d_parent->d_ino;
    errptr<dentry> d_child = get_dentry(d_parent.get(), vintix::string_view { name });

    if (!d_child.is_err()) {
        // If O_CREAT and O_EXCL are set, open() shall fail if the file exists
        if (flags & O_EXCL)
            return EEXIST;

        return get_file(d_child.get(), flags);
    }

    errptr<inode> ino;

    if ((flags & O_DIRECTORY)) {
        if (parent_ino->vtable->mkdir == nullptr)
            return EPERM;

        // make a directory
        ino = parent_ino->vtable->mkdir(d_parent.get(), name);
    } else {
        if (parent_ino->vtable->create == nullptr)
            return EPERM;

        // make a regular file
        ino = parent_ino->vtable->create(d_parent.get(), name);
    }

    if (ino.is_err())
        return ino.err();

    const time_t timestamp = get_unix_time_from_now();

    ino->i_count = 1;
    ino->i_mtime = timestamp;
    ino->i_flags = INODE_MOUNTED | INODE_IS_DIRTY;
    ino->i_gid = ROOT_GID;
    ino->i_uid = ROOT_UID;
    d_child = add_dentry(d_parent.get(), vintix::string_view { name }, ino.get());

    if (d_child.is_err()) {
        ino->vtable->free(ino.get());
        return d_child.err();
    }

    parent_ino->i_mtime = timestamp;
    parent_ino->i_flags |= INODE_IS_DIRTY;

    if (ino->is_dir())
        ++parent_ino->i_nlinks;

    return get_file(d_child.get(), flags);
}

auto kopen(const char *p, int flags, mode_t mode) noexcept -> errptr<file>
{
    // Values for oflag are constructed by a bitwise-inclusive OR of flags from the following list,
    // defined in <fcntl.h>. Applications shall specify exactly one of the first four
    // values (file access modes) below in the value of oflag.
    const bool valid_file_access_method =
        (flags & O_RDONLY) | (flags & O_WRONLY) | (flags & O_EXEC) | (flags & O_RDWR);
    if (!valid_file_access_method || mode > 0777)
        return EINVAL;

    // Open for execute only (non-directory files). The result is unspecified if
    // this flag is applied to a directory.
    if (flags & O_DIRECTORY && flags & O_EXEC)
        return EINVAL;

    vintix::string path { p };

    if (path == nullptr)
        return ENOMEM;

    path_normalize(path);
    if (auto err = path_chk_valid(vintix::string_view { path }))
        return err;

    if (flags & O_CREAT)
        return kopen_creat(vintix::string_view { path }, flags, mode);

    int dflags = flags & O_KERNEL ? 0 : DENTRY_CHECK_PERMS;
    errptr<dentry> entry = get_dentry(vintix::string_view { path }, dflags);

    if (entry.is_err())
        return entry.err();

    KASSERT(entry.get() != nullptr);
    KASSERT(entry.get()->d_ino != nullptr);

    if (entry->d_ino->is_dir()) {
        if (!(flags & O_DIRECTORY))
            return EISDIR;

        // The named file is a directory and oflag includes O_WRONLY or O_RDWR,
        // or includes O_CREAT without O_DIRECTORY.
        if (flags & O_WRONLY || flags & O_RDWR)
            return EISDIR;
        // Open for execute only (non-directory files). The result is unspecified
        // if this flag is applied to a directory.
        if (flags & O_EXEC)
            return EISDIR;
    } else {
        if (flags & O_DIRECTORY)
            return ENOTDIR;
    }

    return get_file(entry.get(), flags);
}

static auto eof_is_reached(const file *fp, usize &count) noexcept -> bool
{
    bool reached_eof = false;

    if (!fp->is_device()) {
        reached_eof = static_cast<usize>(fp->f_pos) + count >= fp->size();

        if (reached_eof)
            count = fp->size();
    } else if (fp->is_blkdev()) {
        const blkdev *dev = device::get_blkdev(fp->f_dentry->d_ino->i_num - 1);
        KASSERT(dev != nullptr);

        const blksize_t log_blksz = fp->log_blksz();
        const auto start_sector = static_cast<blknum_t>(fp->f_pos >> log_blksz);
        const blknum_t sector_count = count >> log_blksz;
        blknum_t sector_end;

        if (chk_add_overflow(start_sector, sector_count, &sector_end) ||
            sector_end > dev->total_sectors) {
            count = (dev->total_sectors - start_sector) * (1 << log_blksz);
            reached_eof = true;
        }
    }

    return reached_eof;
}

auto kread(file *fp, void *buf, usize count) noexcept -> ssize
{
    KASSERT(fp->vtable != nullptr);

    if (fp->is_dir())
        return -EISDIR;

    if (fp->f_oflags & O_WRONLY)
        return -EPERM;

    KASSERT(fp->f_pos >= 0);

    // According to POSIX.1, if count is greater than SSIZE_MAX, the result
    // is implementation-defined.
    if (count > SSIZE_MAX)
        return -EINVAL;

    if (count == 0)
        return 0;

    // The file is a regular file, nbyte is greater than 0, the starting
    // position is before the end-of-file and the starting position is greater
    // than or equal to the offset maximum established in the open file description
    // associated with fildes.
    //
    // (not sure how to interpret this.. in all honesty)
    if (fp->is_regular_file() && (fp->uoff() > fp->size() && fp->uoff() >= fp->maximum_file_size()))
        return -EOVERFLOW;

    const bool reached_eof = eof_is_reached(fp, count);
    DEBUG("kread: Reading %ul bytes from offset %d from \"%s\" into 0x%p\n", count, fp->f_pos,
          fp->absolute_path().c_str(), buf);

    // Some files may have a special read implementation (e.g., character devices,
    // or unique filesystems).
    if (fp->vtable->read != nullptr)
        return fp->vtable->read(fp, buf, count);

    const int blkio_flags = reached_eof | BLKIO_READ;
    return blkio(fp, buf, count, blkio_flags);
}

auto kwrite(file *fp, const void *buf, usize count) noexcept -> ssize
{
    KASSERT(fp->vtable != nullptr);

    if (fp->is_readonly() || !fp->inode()->is_writable())
        return -EPERM;

    if (fp->super()->is_readonly())
        return -EROFS;

    if (fp->is_dir())
        return -EISDIR;
    KASSERT(fp->f_pos >= 0);

    // According to POSIX.1, if count is greater than SSIZE_MAX, the result
    // is implementation-defined.
    if (count > SSIZE_MAX)
        return -EINVAL;

    // If nbyte is 0, write() will return 0 and have no other results if
    // the file is a regular file; otherwise, the results are unspecified.
    if (count == 0)
        return 0;

    usize tmp;
    const bool reached_eof =
        chk_add_overflow(fp->uoff(), count, &tmp) || fp->uoff() + count >= fp->size();

    // An attempt was made to write a file that exceeds the implementation-dependent
    // maximum file size or the process' file size limit.
    if (!fp->is_chrdev() && fp->uoff() + count > fp->maximum_file_size())
        return -EFBIG;

    // const bool reached_eof = eof_is_reached(fp, count);
    KASSERT(count != 0);
    DEBUG("kwrite: Writing %ul bytes from offset %d from 0x%p into \"%s\"\n", count, fp->f_pos, buf,
          fp->absolute_path().c_str());

    ssize result;
    // Some files may have a special write implementation (e.g., character devices,
    // or unique filesystems).
    if (fp->vtable->write != nullptr) {
        result = fp->vtable->write(fp, buf, count);
    } else {
        // NOLINTBEGIN(cppcoreguidelines-pro-type-const-cast)
        result = blkio(fp, const_cast<void *>(buf), count, 0);
        // NOLINTEND(cppcoreguidelines-pro-type-const-cast)
    }

    if (result < 0)
        return result;

    fp->f_dentry->d_ino->i_size += static_cast<off_t>(count);
    if (auto err = ksync(fp))
        return err;

    return (reached_eof) ? EOF : result;
}

auto ksync(file *fp) noexcept -> errno
{
    KASSERT(fp != nullptr);

    if (fp->is_readonly())
        return EPERM;

    if (fp->super()->is_readonly())
        return EROFS;

    const time_t time = get_unix_time_from_now();
    if (fp->f_dentry->d_parent != nullptr) {
        inode *parent = fp->f_dentry->d_parent->d_ino;
        parent->i_mtime = time;

        if (parent->vtable->sync != nullptr) {
            if (auto err = parent->vtable->sync(fp->f_dentry->d_parent))
                return -err;

            parent->i_flags &= ~INODE_IS_DIRTY;
            // TODO: Update parent superblock
        }
    }

    inode *child = fp->f_dentry->d_ino;

    child->i_mtime = time;
    if (child->vtable->sync != nullptr) {
        if (auto err = child->vtable->sync(fp->f_dentry))
            return err;

        child->i_flags &= ~INODE_IS_DIRTY;

        // putting it here temporaily for now, but later on remove
        if (auto err = child->i_sup->vtable->sync(child->i_sup))
            return err;
    }

    return ENOERR;
}

auto klseek(file *fp, off_t offset, int whence) noexcept -> off_t
{
    KASSERT(fp != nullptr);
    return fp->vtable->lseek(fp, offset, whence);
}

auto generic_file_lseek(const file *fp, off_t off, int whence) noexcept -> off_t
{
    switch (whence) {
        default:
            return -EINVAL;
        case SEEK_SET:
            break;
        case SEEK_CUR: {
            if (off == 0)
                return fp->f_pos;
            break;
        }
        case SEEK_END: {
            off += fp->ssize();
        }
    }

    const u64 max_filesz = fp->inode()->i_sup->s_max_file_size;
    if (off < 0 || (max_filesz != INF_FILE_SIZE && static_cast<usize>(off) > max_filesz))
        return -EINVAL;

    return off;
}

auto kstat(file *fp, stat *st) noexcept -> errno
{
    KASSERT(st != nullptr);
    st->st_dev = fp->inode()->i_sup->s_dev;
    st->st_gid = fp->gid();
    st->st_uid = fp->uid();
    st->st_ino = fp->inode()->i_num;
    st->st_mode = fp->mode();
    st->st_size = fp->ssize();
    st->st_mtime = fp->modified_time();
    st->st_nlink = fp->inode()->i_nlinks;
    st->st_blksize = 1 << fp->inode()->i_sup->s_log_blksz;
    st->st_blocks = fp->size() / DEFAULT_LBA_SECTOR_SIZE;

    return ENOERR;
}

auto register_filesystem(const genfs &fs) noexcept -> errno
{
    KASSERT(fs.name != nullptr);
    for (usize i = 0; i < filesystems.size(); ++i) {
        if (filesystems[i].name == nullptr) {
            filesystems[i].name = fs.name;
            filesystems[i].flags = fs.flags;
            filesystems[i].init_fs_context = fs.init_fs_context;
            filesystems[i].type = fs.type;
            DEBUG("VFS: successfully registered \"%s\" filesystem\n", filesystems[i].name);
            return ENOERR;
        }
    }

    return ENOMEM;
}

auto get_fs(const char *type) noexcept -> const genfs *
{
    for (usize i = 0; i < filesystems.size(); ++i) {
        if (strcmp(type, filesystems[i].name) == 0)
            return &filesystems[i];
    }

    return nullptr;
}

auto kclose(file *fp) noexcept -> errno
{
    decrement_dentry_count(fp->f_dentry);

    if (fp->vtable->close == nullptr)
        kfree(fp);
    else {
        if (auto err = fp->vtable->close(fp)) {
            kpanic("received error %d when attempting to close file with custom callback, "
                   "filesystem state may be inconsistent!\n",
                   err);
        }
    }

    return remove_dentry(fp->f_dentry);
}

auto ls(const file *fp) noexcept -> result<vintix::vector<vintix::string>>
{
    if (fp->f_dentry->d_ino->vtable->ls != nullptr)
        return fp->f_dentry->d_ino->vtable->ls(fp->f_dentry);
    return EINVAL;
}

auto unregister_filesystem(const char *fs_name) noexcept -> errno
{
    DEBUG("VFS: Unregistering \"%s\" filesystem\n", fs_name);
    for (usize i = 0; i < filesystems.size(); ++i) {
        if (strcmp(filesystems[i].name, fs_name) == 0) {
            filesystems[i].name = nullptr;
            filesystems.erase(i);
            return ENOERR;
        }
    }
    return ENOENT;
}
