/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/core/time.h>
#include <vintix/kernel/filesystems/dentry.h>
#include <vintix/kernel/filesystems/path.h>
#include <vintix/kernel/filesystems/super.h>
#include <vintix/kernel/lib/concepts.h>
#include <vintix/kernel/lib/cxxstring.h>

NODESTROY static dentry m_graph {};

static auto fetch_new_dentry(dentry *parent, const char *name) noexcept -> errptr<dentry>
{
    KASSERT(parent != nullptr);
    KASSERT(parent->d_ino != nullptr);
    KASSERT(parent->d_name != nullptr);
    KASSERT(parent->d_ino->vtable != nullptr);
    DEBUG("VFS(fetch_new_dentry): Looking up \"%s\"\n", name);
    inode *ino = UNWRAP_PTR(parent->d_ino->vtable->lookup(parent, name));
    KASSERT(ino->i_count == 0);
    DEBUG("VFS(fetch_new_dentry): Found \"%s\"\n", name);
    return add_dentry(parent, vintix::string_view { name }, ino);
}

static auto fetch_new_dentry_nosave(dentry *parent, const char *name) noexcept -> errptr<dentry>
{
    auto *entry = static_cast<dentry *>(kzalloc(sizeof(dentry)));
    entry->d_parent = parent;
    entry->d_ino = UNWRAP_PTR(parent->d_ino->vtable->lookup(parent, name));
    UNWRAP_ERR(entry->d_name.copy(name));

    return entry;
}

auto get_dentry_root() noexcept -> dentry * { return &m_graph; }

auto change_dentry_root(inode *ino, const char *name) noexcept -> errno
{
    m_graph.d_ino = ino;

    if (name != nullptr) {
        if (auto err = m_graph.d_name.copy(name))
            return err;
    } else {
        m_graph.d_name = nullptr;
    }
    return ENOERR;
}

auto get_dentry(dentry *parent, vintix::string_view name) noexcept -> errptr<dentry>
{
    dentry *child = parent->search(name);

    if (child == nullptr)
        return fetch_new_dentry(parent, name.c_str());

    return child;
}

auto get_dentry(vintix::string_view path, int flags) noexcept -> errptr<dentry>
{
    vintix::array<char, NAME_MAX> d_name {};
    dentry *dir_tree = &m_graph;

    // root directory?
    if (path.length() == 1)
        return dir_tree;

    errno path_err;
    usize off = 0;
    usize free_entry = 0;
    DEBUG("VFS(get_dentry): Searching for \"%s\"\n", path.c_str());

    do {
        KASSERT(dir_tree != nullptr);

        if (flags & DENTRY_CHECK_PERMS)
            if (!dir_tree->d_ino->is_readable())
                return EACCES;

        if ((path_err = path_consume(path.c_str(), vintix::span<char> { d_name }, off));
            path_err != ENOERR && path_err != EPARSE)
            return path_err;

        const usize name_length = strlen(d_name.data());
        KASSERT(name_length != 0);
        dentry *child = dir_tree->search(cstr { d_name.data() });
        if (child == nullptr) {
            if (flags & DENTRY_SEARCH_CACHE) {
                if (free_entry >= 2) {
                    KASSERT(flags & DENTRY_DONT_CACHE);
                    kfree(dir_tree);
                }
                return ENOENT;
            }

            dentry *entry;
            if (flags & DENTRY_DONT_CACHE) {
                entry = UNWRAP_PTR(fetch_new_dentry_nosave(dir_tree, d_name.data()));
                ++free_entry;
            } else {
                entry = UNWRAP_PTR(fetch_new_dentry(dir_tree, d_name.data()));
            }

            DEBUG("VFS(get_dentry): Found path component \"%s\"\n", d_name.data());

            if (free_entry >= 2)
                kfree(dir_tree);

            dir_tree = entry;
        } else {
            DEBUG("VFS(get_dentry): Found path component \"%s\" in the cache\n", d_name.data());
            KASSERT(free_entry == 0);
            dir_tree = child;
        }

        memsetd(d_name.unsafe_mutable_data(), 0, d_name.size() / sizeof(u32));
    } while (path_err != EPARSE);

    KASSERT(dir_tree->d_ino != nullptr);
    KASSERT(dir_tree->d_parent != nullptr);
    return dir_tree;
}

auto remove_dentry(vintix::string_view path) noexcept -> errno
{
    dentry *entry = UNWRAP_PTR(get_dentry(path, DENTRY_SEARCH_CACHE));
    return remove_dentry(entry);
}

void increment_dentry_parent_count(dentry *parent) noexcept
{
    KASSERT(parent != nullptr);

    while (parent != nullptr) {
        KASSERT(parent->d_ino != nullptr);
        ++parent->d_ino->i_count;
        parent = parent->d_parent;
    }
}

void decrement_dentry_count(dentry *entry) noexcept
{
    KASSERT(entry != nullptr);

    while (entry != nullptr) {
        KASSERT(entry->d_ino != nullptr);
        KASSERT(entry->d_ino->i_count != 0);
        --entry->d_ino->i_count;
        entry = entry->d_parent;
    }
}

auto remove_dentry(dentry *dir) noexcept -> errno
{
    KASSERT(dir != nullptr);
    KASSERT(dir->d_ino != nullptr);

    if (dir->d_ino->i_count != 0)
        return EBUSY;

    dentry *cur = dir;

    // no more references to this dentry, remove it
    while (cur != nullptr && cur->d_ino->i_count == 0) {
        DEBUG("VFS(remove_dentry): removing dentry: \"%s\"\n", cur->d_name.c_str());
        dentry *parent = cur->d_parent;

        // removing the dentry in the parent's list first before deleting it
        if (parent != nullptr)
            parent->remove(cur);

        cur->~dentry();
        cur = parent;
    }

    return ENOERR;
}

void debug_mount_graph(const dentry *root) noexcept
{
    const char *parent = root->parent() == nullptr ? "NULL" : root->parent()->d_name.c_str();
    DEBUG("debug_mount_graph: path=\"%s\", d_ino->i_count=%u, parent=\"%s\"\n",
          root->d_name.c_str(), root->d_ino->i_count, parent);

    if (root->d_entries != nullptr) {
        const list_node_g<dentry> *entry = root->d_entries.front();

        while (entry != nullptr) {
            debug_mount_graph(&entry->n_data);
            entry = entry->n_next;
        }
    }
}

auto add_dentry(dentry *dir, vintix::string_view name, inode *ino) noexcept -> errptr<dentry>
{
    DEBUG("VFS(add_dentry): Caching \"%s\"\n", name.c_str());
    dentry new_entry {};

    new_entry.d_ino = ino;
    new_entry.d_parent = dir;
    if (auto err = new_entry.d_name.copy(name)) {
        new_entry.d_ino->vtable->free(new_entry.d_ino);
        return err;
    }
    KASSERT(new_entry.d_name.length() != 0);
    DEBUG("VFS(add_dentry): Adding \"%s\" -> inode=0x%p  parent=0x%p\n", new_entry.d_name.c_str(),
          new_entry.d_ino, new_entry.d_parent);

    auto err = dir->insert(vintix::move(new_entry));
    debug_mount_graph(&m_graph);
    return err;
}
