/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/core/blkdev.h>
#include <vintix/kernel/core/kassert.h>
#include <vintix/kernel/core/process.h>
#include <vintix/kernel/filesystems/dentry.h>
#include <vintix/kernel/filesystems/fs.h>
#include <vintix/kernel/filesystems/mount.h>
#include <vintix/kernel/filesystems/path.h>
#include <vintix/kernel/filesystems/super.h>
#include <vintix/kernel/lib/cxxstring.h>
#include <vintix/kernel/lib/klog.h>
#include <vintix/kernel/lib/math.h>
#include <vintix/kernel/lib/move.h>

NODESTROY static vintix::vector<vintix::string> mountpoints {};

static auto get_mount(vintix::string_view path) noexcept -> vintix::string_view
{
    if (!root_is_mounted())
        return vintix::string_view { nullptr };

    for (usize i = 0; i < mountpoints.size(); ++i) {
        if (path == mountpoints[i])
            return vintix::string_view { mountpoints[i] };

        vintix::string_view tmp { path };
        while ((tmp = path_parent_of(tmp)) != nullptr)
            if (tmp == mountpoints[i])
                return vintix::string_view { mountpoints[i] };
    }

    return vintix::string_view { nullptr };
}

auto root_is_mounted() noexcept -> bool { return mountpoints.size() > 0; }

auto kmount(const char *src, const char *t, const char *type, int mount_flags,
            v_addr start) noexcept -> errno
{
    DEBUG("kmount: Attempting to mount \"%s\" on \"%s\"\n", src, t);
    if (src == nullptr)
        return EINVAL;

    if (!(mount_flags & MNT_KERNEL)) {
        if (process::running()->uid() != ROOT_UID)
            return EPERM;
    }

    vintix::string target { t };

    if (target == nullptr)
        return ENOMEM;

    if (mountpoints.contains(target))
        return EBUSY;

    path_normalize(target);

    if (auto err = path_chk_valid(vintix::string_view { target }))
        return err;

    // if the kernel hasn't mounted root yet then there's no point of checking
    // if target is mounted, since it doesn't exist.
    if (root_is_mounted() && !path_is_root(vintix::string_view { target }) &&
        !(mount_flags & MNT_KERNEL)) {
        const dentry *mnt =
            UNWRAP_PTR(get_dentry(get_mount(path_parent_of(vintix::string_view { target }))));

        if (mnt == nullptr)
            return ENOTDIR;

        // block device 'name' is located on a filesystem with MNT_NODEV set
        if (mnt->d_ino->i_sup->s_mnt_flags & MNT_NODEV)
            return EACCES;
    }

    vintix::optional<dev_t> dev = src[0] == '\0' ? NODEV : device::find(src);

    if (dev.is_none())
        return ENOENT;

    const genfs *fs = get_fs(type);

    if (fs == nullptr)
        return ENODEV;

    if (!(mount_flags & MNT_RDONLY) && (fs->flags & MNT_RDONLY))
        return EROFS;

    if (!(mount_flags & MNT_NOEXEC) && (fs->flags & MNT_NOEXEC))
        return EPERM;

    const fs_context context {
        .flags = static_cast<u32>(fs->flags | mount_flags),
        .fs_dev = dev.value(),
        .vaddr_offset = start,
    };

    KASSERT(fs->init_fs_context != nullptr);
    superblock *sup = UNWRAP_PTR(fs->init_fs_context(context));
    inode *ino = UNWRAP_PTR(sup->vtable->root_inode(sup));

    KASSERT(ino->i_count == 0);
    if (get_dentry_root()->d_ino == nullptr) {
        if (auto err = change_dentry_root(ino, path_last_entry(vintix::string_view { target })))
            return err;
    } else {
        dentry *parent = UNWRAP_PTR(get_dentry(path_parent_of(vintix::string_view { target })));
        if (errptr<dentry> entry = add_dentry(
                parent, vintix::string_view { path_last_entry(vintix::string_view { target }) },
                ino);
            entry.is_err())
            return entry.err();
    }

    if (auto err = mountpoints.push_back(move(target)))
        return err;

    ++ino->i_count;
    return ENOERR;
}

auto kumount(const char *t, int umount_flags) noexcept -> errno
{
    DEBUG("kumount: Attempting to unmount \"%s\"\n", t);

    if (!(umount_flags & MNT_KERNEL)) {
        if (process::running()->uid() != ROOT_UID)
            return EPERM;
    }

    vintix::string_view target { t };
    if (!mountpoints.contains(target))
        return EINVAL;

    dentry *entry = UNWRAP_PTR(get_dentry(target));

    if (entry->d_ino->i_count != 1)
        return EBUSY;

    inode *root = entry->d_ino;
    decrement_dentry_count(entry);
    debug_mount_graph(entry);
    vintix::ignore = remove_dentry(entry);
    mountpoints.erase(mountpoints.find(target).value());

    root->i_sup->vtable->free(root->i_sup);

    if (path_is_root(target))
        change_dentry_root(nullptr, nullptr);

    return ENOERR;
}
