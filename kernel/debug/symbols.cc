/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/core/blkdev.h>
#include <vintix/kernel/core/chrdev.h>
#include <vintix/kernel/core/idt.h>
#include <vintix/kernel/core/io.h>
#include <vintix/kernel/core/panic.h>
#include <vintix/kernel/core/pci.h>
#include <vintix/kernel/core/pmm.h>
#include <vintix/kernel/core/symbols.h>
#include <vintix/kernel/core/time.h>
#include <vintix/kernel/core/vmm.h>
#include <vintix/kernel/filesystems/file.h>
#include <vintix/kernel/filesystems/fs.h>
#include <vintix/kernel/filesystems/inode.h>
#include <vintix/kernel/filesystems/path.h>
#include <vintix/kernel/lib/klog.h>
#include <vintix/kernel/lib/string.h>
#include <vintix/kernel/lib/types.h>
#include <vintix/kernel/lib/utilities.h>

struct symbol_mapping {
    const char *symbol_name;
    v_addr symbol_address;
};

// NOLINTBEGIN(cppcoreguidelines-avoid-c-arrays)
static const symbol_mapping ksymbols[] {
    { "memset", ADDRESSOF(memset) },
    { "memcpy", ADDRESSOF(memcpy) },
    { "memmove", ADDRESSOF(memmove) },
    { "free_page", ADDRESSOF(free_page) },
    { "free_pages", ADDRESSOF(free_pages) },
    { "kmalloc", ADDRESSOF(kmalloc) },
    { "kzalloc", ADDRESSOF(kzalloc) },
    { "kcalloc", ADDRESSOF(kcalloc) },
    { "krealloc", ADDRESSOF(krealloc) },
    { "kfree", ADDRESSOF(kfree) },
    { "address_is_mapped", ADDRESSOF(address_is_mapped) },
    { "alloc_page", ADDRESSOF(alloc_page) },
    { "alloc_pages", ADDRESSOF(alloc_pages) },
    { "map_page", ADDRESSOF(map_page) },
    { "kpanic", ADDRESSOF(kpanic) },
    { "_kassert", ADDRESSOF(_kassert) },
    { "kprintf", ADDRESSOF(kprintf) },
    { "fb_writeln", ADDRESSOF(fb_writeln) },
    { "fb_write", ADDRESSOF(fb_write) },
    { "fb_writec", ADDRESSOF(fb_writec) },
    { "itoa", ADDRESSOF(itoa) },
    { "disk_add", ADDRESSOF(disk_add) },
    { "disk_read", ADDRESSOF(disk_read) },
    { "disk_write", ADDRESSOF(disk_write) },
    { "kvfprintf", ADDRESSOF(kvfprintf) },
    { "get_unix_time_from_now", ADDRESSOF(get_unix_time_from_now) },
    { "srl_printf", ADDRESSOF(srl_printf) },
    { "srl_vfprintf", ADDRESSOF(srl_vfprintf) },
    { "get_pci_devs", ADDRESSOF(get_pci_devs) },
    { "pci_cfg_read_word", ADDRESSOF(pci_cfg_read_word) },
    { "pci_cfg_write_word", ADDRESSOF(pci_cfg_write_word) },
    { "pci_cfg_read_dword", ADDRESSOF(pci_cfg_read_dword) },
    { "register_filesystem", ADDRESSOF(register_filesystem) },
    { "unregister_filesystem", ADDRESSOF(unregister_filesystem) },
    { "register_chrdev", ADDRESSOF(register_chrdev) },
    { "generic_file_lseek", ADDRESSOF(generic_file_lseek) },
    { "debug_log", ADDRESSOF(debug_log) },
    { "pci_log", ADDRESSOF(pci_log) },
    { "ok", ADDRESSOF(ok) },
    { "install_irq_handler", ADDRESSOF(install_irq_handler) },
    { "uninstall_irq_handler", ADDRESSOF(uninstall_irq_handler) },
    { "path_consume", ADDRESSOF(path_consume) },
    { "path_consume_until", ADDRESSOF(path_consume_until) },
    { "path_normalize", ADDRESSOF(path_normalize) },
    { "path_parent_of", ADDRESSOF(path_parent_of) },
    { "path_last_entry", ADDRESSOF(path_last_entry) },
    { "list_size", ADDRESSOF(list_size) },
    { "generic_file_close", ADDRESSOF(generic_file_close) },
    { "generic_inode_free", ADDRESSOF(generic_inode_free) },
    { "generic_disk_put_blks", ADDRESSOF(generic_disk_put_blks) },
    { "kopen", ADDRESSOF(kopen) },
    { "kcreat", ADDRESSOF(kcreat) },
    { "kmkdir", ADDRESSOF(kmkdir) },
    { "kread", ADDRESSOF(kread) },
    { "kwrite", ADDRESSOF(kwrite) },
    { "cvector_push_back", ADDRESSOF(cvector_push_back) },
    { "cvector_dtor", ADDRESSOF(cvector_dtor) },
    { "cvector_dtor_primitive", ADDRESSOF(cvector_dtor_primitive) },
    { "_cstring_from_cstr", ADDRESSOF(_cstring_from_cstr) },
    { "_cstring_dtor", ADDRESSOF(_cstring_dtor) },
    { "_cstring_move", ADDRESSOF(_cstring_move) },
    { "_cstring_copy", ADDRESSOF(_cstring_copy) },
    { "_cstring_prepend", ADDRESSOF(_cstring_prepend) },
    { "_cstring_append", ADDRESSOF(_cstring_append) },
    { "_cstring_prepend_char", ADDRESSOF(_cstring_prepend_char) },
    { "_cstring_append_char", ADDRESSOF(_cstring_append_char) },
    { "_cstring_equals", ADDRESSOF(_cstring_equals) },
    { "_cstring_contains", ADDRESSOF(_cstring_contains) },
    { "_cstring_starts_with", ADDRESSOF(_cstring_starts_with) },
    { "_cstring_ends_with", ADDRESSOF(_cstring_ends_with) },
    { "_cstring_count_char", ADDRESSOF(_cstring_count_char) },
    { "_cstring_contains_char", ADDRESSOF(_cstring_contains_char) },
};
// NOLINTEND(cppcoreguidelines-avoid-c-arrays)

namespace ksym {
    auto addressof(const char *symbol_name) noexcept -> v_addr
    {
        for (auto ksymbol : ksymbols)
            if (strcmp(symbol_name, ksymbol.symbol_name) == 0)
                return ksymbol.symbol_address;

        return KSYM_UNDEF_SYM;
    }
} // namespace ksym
