/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/core/panic.h>
#include <vintix/kernel/posix/signal.h>

auto signumtostr(int signum) noexcept -> const char *
{
    switch (signum) {
        default:
            kpanic("received invalid signal number: %d\n", signum);
        case 1:
            return "SIGABRT";
        case 2:
            return "SIGALRM";
        case 3:
            return "SIGFPE";
        case 4:
            return "SIGHUP";
        case 5:
            return "SIGILL";
        case 6:
            return "SIGINT";
        case 7:
            return "SIGKILL";
        case 8:
            return "SIGPIPE";
        case 9:
            return "SIGQUIT";
        case 10:
            return "SIGSEGV";
        case 11:
            return "SIGTERM";
        case 12:
            return "SIGUSR1";
        case 13:
            return "SIGUSR2";
        case 14:
            return "SIGCHLD";
        case 15:
            return "SIGCONT";
        case 16:
            return "SIGSTOP";
        case 17:
            return "SIGSTP";
        case 18:
            return "SIGTTIN";
        case 19:
            return "SIGTTOU";
        case 20:
            return "SIGBUS";
        case 21:
            return "SIGPOLL";
        case 22:
            return "SIGPROF";
        case 23:
            return "SIGSYS";
        case 24:
            return "SIGTRAP";
        case 25:
            return "SIGURG";
        case 26:
            return "SIGVTALRM";
        case 27:
            return "SIGXCPU";
        case 28:
            return "SIGFSZ";
    }
}
