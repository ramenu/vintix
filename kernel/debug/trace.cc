/*
 * Copyright (C) 2023-2025 Abdur-Rahman Mansoor
 *
 * This file is part of Vintix.
 *
 * Vintix is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version that is approved by Abdur-Rahman Mansoor.
 *
 * Vintix is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Vintix. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <vintix/kernel/asm/regs.h>
#include <vintix/kernel/core/global.h>
#include <vintix/kernel/core/io.h>
#include <vintix/kernel/core/trace.h>
#include <vintix/kernel/lib/utilities.h>

struct stack_frame {
    stack_frame *ebp { nullptr };
    u32 eip;
};

void print_stack_trace(usize max_frames) noexcept
{
    // frame pointer is emitted on release builds which
    // can cause insane undefined behavior
    if constexpr (DEBUG_BUILD) {
        auto *stk = unsafe_toptr<stack_frame *>(ebp());
        kprintf("\nstack trace:\n");
        for (usize frame = 0; frame < max_frames && stk->ebp != nullptr; ++frame) {
            kprintf("   [<0x%x>]\n", stk->eip);
            stk = stk->ebp;
        }
    }
}
